_, skill = ...
local private = {}
private.font_file = [=[Interface\Addons\skill\gui\verdana.ttf]=]
private.skill_font = CreateFont("skill")
private.gui = CreateFrame("Frame","gui", UIParent)

local COLOR_BACKGROUND = {43 / 255, 43 / 255, 43 / 255, 1.0}
local COLOR_LABEL_TEXT = {0.5, 0.5, 0.5, 1.0}
local COLOR_BUTTON_TEXT = {0.7, 0.7, 0.7, 1.0}
local COLOR_BUTTON = {43 / 255 * 1.5, 43 / 255 * 1.5, 43 / 255 * 1.5, 1.0}
local COLOR_BUTTON_HIGHLIGHT = {43 / 255 * 2.0, 43 / 255 * 2.0, 43 / 255 * 2.0, 1.0}
skill.colors = {}
skill.colors.background = COLOR_BACKGROUND
skill.colors.label_text = COLOR_LABEL_TEXT
skill.colors.button_text = COLOR_BUTTON_TEXT
skill.colors.button = COLOR_BUTTON
skill.colors.button_highlight = COLOR_BUTTON_HIGHLIGHT
local ELEMENTS = {
  ["label"] = {
    ["column_width"] = 0,
    ["frames"] = {},
  },
  ["text"] = {
    ["column_width"] = 0,
    ["frames"] = {},
  },
  ["frame"] = {
    ["column_width"] = 0,
    ["frames"] = {},
  },
}

function private.stop_moving_or_resizing(self, ...)
  for i = 1, self:GetNumPoints() do
    local _, _, relativePoint, xOfs, yOfs = self:GetPoint(i)
    if relativePoint == "TOPLEFT" then
      skill_char_settings.gui = skill_char_settings.gui or {}
      skill_char_settings.gui.left = xOfs
      skill_char_settings.gui.top = yOfs
      break
    end
  end
  self:StopMovingOrSizing(...)
end

function private.update_queue(self)
  self.string:SetText(GetQueuedSpell())
end
function private.update_combat_cast_spell(self)
  self.string:SetText(CombatCastCurrentSpell())
end
function private.update_combat_cast_unit(self)
  local unit = CombatCastCurrentUnit()
  if unit then
    self.string:SetText(unit.. ": "..(UnitName(unit) or "nil"))
  else
    self.string:SetText("")
  end
end
function private.update_num_enemies_within(self)
  self.string:SetText(skill.NumEnemiesWithin(8))
end
function private.update_deficit(self)
  self.string:SetText(GetHighestDeficitUnits()[1])
end
function private.update_deficit_percent(self)
  self.string:SetText(HighestDeficitPercentGroupUnits()[1])
end
function private.toggle_should_interrupt(self)
  skill_settings = skill_settings or {}
  skill_settings.should_interrupt = not (skill_settings.should_interrupt or false)
  skill.should_interrupt = skill_settings.should_interrupt
  self.string:SetText(tostring(skill_settings.should_interrupt))
end
function private.init_should_interrupt(self)
  skill_settings = skill_settings or {}
  skill_settings.should_interrupt = skill_settings.should_interrupt or false
  skill.should_interrupt = skill_settings.should_interrupt
  self.string:SetText(tostring(skill_settings.should_interrupt))
end
local DEFAULT_CONFIG = {
  --{ ["label"] = "Queue:", ["frame"] = nil, ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_queue, ["click"] = nil, ["init"] = nil },
  --{ ["label"] = "Spell:", ["frame"] = nil, ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_combat_cast_spell, ["click"] = nil, ["init"] = nil },
  --{ ["label"] = "Unit:", ["frame"] = nil, ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_combat_cast_unit, ["click"] = nil, ["init"] = nil },
  --{ ["label"] = "Enemies(8):", ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_num_enemies_within, ["click"] = nil, ["init"] = nil },
  --{ ["label"] = "deficit:", ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_deficit, ["click"] = nil, ["init"] = nil },
  --{ ["label"] = "deficit_percent:", ["text"] = "XXXXXXXXXXXXXXXXXXXX", ["update"] = private.update_deficit_percent, ["click"] = nil, ["init"] = nil },
  { ["label"] = "interrupt:", ["text"] = "false", ["update"] = nil, ["click"] = private.toggle_should_interrupt, ["init"] = private.init_should_interrupt },
}

function private.init(gui, config)
  local rows = 0
  local row_height = 14
  local row_space = 1
  for type, value in pairs(ELEMENTS) do
    value["column_width"] = pixelScale(150)
    if type == "text" then
      for _, f in ipairs(value["frames"]) do
        if f ~= nil then
          f:SetScript("OnMouseDown", nil)
          f:SetScript("OnUpdate", nil)
        end
      end
    end
  end
  for i, row in ipairs(config) do
    rows = rows + 1
    for type, value in pairs(ELEMENTS) do
      local root = gui
      local string
      if type == "text" then
        value["frames"][i] = value["frames"][i] or CreateFrame("Button", nil, root)
        root = value["frames"][i]
      end
      string = root.string or root:CreateFontString(nil, "OVERLAY", "skill")
      value["frames"][i] = value["frames"][i] or string
      value["frames"][i].string = string
      value["frames"][i].string:SetText(row[type])
      value["frames"][i].string:SetFont("skill", pixelScale(12))
      value["column_width"] = max(value["column_width"], value["frames"][i].string:GetStringWidth() or 0)
    end
  end

  local padding_left = pixelScale(3)
  local padding_top = pixelScale(3)
  local padding_mid = pixelScale(3)
  local anchor_left = padding_left
  local anchor_top = -padding_top
  for i, row in ipairs(config) do
    if row.frame_func then
      ELEMENTS["frame"]["frames"][i] = CreateFrame("Frame", nil, gui)
      ELEMENTS["frame"]["frames"][i]:SetSize(ELEMENTS["frame"]["column_width"], row_height)
      ELEMENTS["frame"]["frames"][i]:SetPoint("TOPLEFT", gui, "TOPLEFT", anchor_left, anchor_top)
      row.frame_func(ELEMENTS["frame"]["frames"][i])
      --ELEMENTS["text"]["frames"][i].frame = row.frame_func(frame)

      --anchor_left = padding_left + ELEMENTS["label"]["column_width"] + padding_mid
      --ELEMENTS["text"]["frames"][i]:SetSize(ELEMENTS["text"]["column_width"], row_height)
      --ELEMENTS["text"]["frames"][i]:SetPoint("TOPLEFT", gui, "TOPLEFT", anchor_left, anchor_top)
      --ELEMENTS["text"]["frames"][i].string:SetTextColor(unpack(COLOR_BUTTON_TEXT))
    else
      ELEMENTS["label"]["frames"][i]:SetSize(ELEMENTS["label"]["column_width"], row_height)
      ELEMENTS["label"]["frames"][i]:SetPoint("TOPLEFT", gui, "TOPLEFT", anchor_left, anchor_top)
      ELEMENTS["label"]["frames"][i].string:SetJustifyH("RIGHT")
      ELEMENTS["label"]["frames"][i].string:SetTextColor(unpack(COLOR_LABEL_TEXT))
      anchor_left = padding_left + ELEMENTS["label"]["column_width"] + padding_mid
      ELEMENTS["text"]["frames"][i]:SetSize(ELEMENTS["text"]["column_width"], row_height)
      ELEMENTS["text"]["frames"][i]:SetPoint("TOPLEFT", gui, "TOPLEFT", anchor_left, anchor_top)
      ELEMENTS["text"]["frames"][i].string:SetTextColor(unpack(COLOR_BUTTON_TEXT))
      if row["click"] or row["update"] then
        ELEMENTS["text"]["frames"][i].string:SetAllPoints(ELEMENTS["text"]["frames"][i])
      end
      ELEMENTS["text"]["frames"][i].string:SetJustifyH("LEFT")
      if row["click"] then
        ELEMENTS["text"]["frames"][i].normal_texture = ELEMENTS["text"]["frames"][i]:CreateTexture()
        ELEMENTS["text"]["frames"][i].normal_texture:SetColorTexture(unpack(COLOR_BUTTON))
        ELEMENTS["text"]["frames"][i].normal_texture:SetAllPoints(ELEMENTS["text"]["frames"][i])
        ELEMENTS["text"]["frames"][i]:SetNormalTexture(ELEMENTS["text"]["frames"][i].normal_texture)
        ELEMENTS["text"]["frames"][i].highlight_texture = ELEMENTS["text"]["frames"][i]:CreateTexture()
        ELEMENTS["text"]["frames"][i].highlight_texture:SetColorTexture(unpack(COLOR_BUTTON_HIGHLIGHT))
        ELEMENTS["text"]["frames"][i].highlight_texture:SetAllPoints(ELEMENTS["text"]["frames"][i])
        ELEMENTS["text"]["frames"][i]:SetHighlightTexture(ELEMENTS["text"]["frames"][i].highlight_texture)
        ELEMENTS["text"]["frames"][i]:RegisterForClicks("AnyUp", "AnyDown")
        ELEMENTS["text"]["frames"][i]:SetScript("OnMouseDown", function(self, button) row["click"](ELEMENTS["text"]["frames"][i], button) end)
      end
      if row["update"] then
        ELEMENTS["text"]["frames"][i]:SetScript("OnUpdate", function() row["update"](ELEMENTS["text"]["frames"][i]) end)
      end
      if row.init then
        row.init(ELEMENTS["text"]["frames"][i])
      end
    end
    anchor_left = padding_left
    anchor_top = -padding_top - (row_height + row_space) * i
  end

  gui:ClearAllPoints()
  gui:SetWidth(padding_left + ELEMENTS["label"]["column_width"] + padding_mid + ELEMENTS["text"]["column_width"] + padding_left)
  gui:SetHeight(padding_top + (row_height + row_space) * rows + padding_top)
  skill_char_settings = skill_char_settings or {}
  local pos = skill_char_settings.gui or {}
  local left = pos.left or pixelScale(GetScreenWidth() / 2)
  local top = pos.top or pixelScale(-GetScreenHeight() / 2)
  gui:SetPoint("TOPLEFT", UIParent, "TOPLEFT", left, top)
  gui:SetMovable(true)
  gui:EnableMouse(true)
  gui:RegisterForDrag("LeftButton")
  gui:SetScript("OnMouseDown", gui.StartMoving)
  gui:SetScript("OnMouseUp", private.stop_moving_or_resizing)
  gui.texture = gui:CreateTexture()
  gui.texture:SetAllPoints(gui)
  gui.texture:SetColorTexture(unpack(COLOR_BACKGROUND))
  gui:Show()
end

local config = {}
local additional_config = {}
function private.gui.reload()
  wipe(config)
  for i = 1, #DEFAULT_CONFIG do
    table.insert(config, DEFAULT_CONFIG[i])
  end
  for _, sub_config in ipairs(additional_config) do
    table.insert(config, sub_config)
  end
  private.init(private.gui, config)
  wipe(additional_config)
end

private.skill_font:SetFont(private.font_file, pixelScale(24))

function reload_gui()
  private.gui.reload()
end

function AddGuiElement(entry)
  table.insert(additional_config, entry)
end