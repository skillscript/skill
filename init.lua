_, skill = ...

skill.modules = {}
skill.modules.deathknight = {}
skill.modules.demonhunter = {}
skill.modules.druid = {}
skill.modules.hunter = {}
skill.modules.mage = {}
skill.modules.monk = {}
skill.modules.paladin = skill.modules.paladin or {}
skill.modules.priest = {}
skill.modules.rogue = {}
skill.modules.shaman = {}
skill.modules.warlock = {}
skill.modules.warrior = {}

C_CVar.SetCVar("scriptErrors", 1)
if tonumber(C_CVar.GetCVar("scriptProfile")) == 1 then
  print("|cFFFF0000WARNING: Script profiling is enabled.|r")
end
function ALERT(...)
  if not skill_settings or skill_settings.alert then
    ChatFrame4:AddMessage(string.format("|cFF00DDDD%.3f|r %s", GetTime(), ...))
  end
end

local ALERT_TIMEOUT = 300000
local alerted = {}
function ALERT_ONCE(...)
  if not alerted[...] or (alerted[...] + ALERT_TIMEOUT < GetTime()) then
    local stack = ""
    local i = 3
    local print_stack = true
    while print_stack do
      local line = debugstack(i, 1, 0)
      local file, line_number = string.match(line, "Interface\\AddOns\\skill\\([^\"]*)\"\]:(%d+).*")
      i = i + 1
      if not string.find(line, "\(tail call\)") then
        if not file then
          break
        end
        stack = string.format("%s\n              %s:%d", stack, file, line_number)
      end
    end
    local file, line_number = string.match(debugstack(2, 1, 0), "Interface\\AddOns\\skill\\([^\"]*)\"\]:(%d+).*")
    ALERT(string.format("|cFFAAAAAA%s:%d|r %s|cFFAAAAAA%s|r", file, line_number, (...), stack))
    alerted[...] = GetTime()
  end
end

-- local y = string.match(GetCVar("gxWindowedResolution"), "%d+x(%d+)");
-- local uiScale = floor(UIParent:GetScale() * 100 + 0.5) * 0.01
local res = select(max(1, GetCurrentResolution()), GetScreenResolutions())
local height = string.sub(res, string.find(res, "x") + 1)
--function pixelScale(i) return i*(GetScreenHeight()/height) end
function pixelScale(i) return i*(GetScreenHeight()/1080) / (height/1080)end


function hash_map(t)
  local tmp = {}
  for _, v in ipairs(t) do
    tmp[v] = true
  end
  return tmp
end

-- 2160p
-- ui scale 0.8
-- GetScreenHeight() 960
-- UIParent:GetScale() 0.8

--height / 1080

-- 1080p
-- ui scale 0.8
-- GetScreenHeight() 960
-- UIParent:GetScale() 0.8