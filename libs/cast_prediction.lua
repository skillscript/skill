local GROUP_UNITS = {
    "player",
    "party1",
    "party2",
    "party3",
    "party4",
    --"pet",
    --"partypet1",
    --"partypet2",
    --"partypet3",
    --"partypet4",
}
local private = {
    frame = nil,
    next_successful_spell_start = 0,
    next_successful_spell = nil,
    last_successful_spell_end = 0,

    units = GROUP_UNITS,
    heal_cache_time = 0.1,
    incoming_heal_timestamp = 0,
    incoming_heal = {},
    confirmed_health_timestamp = 0,
    confirmed_health = {},
    highest_deficit_percent_group_units_timestamp = 0,
    highest_deficit_percent_group_units = {},
}
function private.on_event(self, event, ...)
    private.on_combat_log_event(self, event, ...)
    private.on_group_roster_update(self, event, ...)
end
function private.on_group_roster_update(self, event, ...)
    if event == "GROUP_ROSTER_UPDATE" then
        private.reset_health_tracking()
    end
end
function private.on_combat_log_event(self, event, ...)
    if event == "COMBAT_LOG_EVENT_UNFILTERED" then
        --local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()
        local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, _, spell = CombatLogGetCurrentEventInfo()
        --local _, subevent, _,  sourceGUID,  _, _, _, destGUID, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()

        if sourceGUID == UnitGUID("player") then
            if string.find(subevent, "SPELL_") == 1 then
                if subevent == "SPELL_CAST_START" then
                    --print(CombatLogGetCurrentEventInfo())
                    private.update_incoming_heal()
                    private.next_successful_spell_start = GetTime()
                    private.next_successful_spell = spell
                    --print(private.next_successful_spell)
                elseif (subevent == "SPELL_CAST_FAILED" or subevent == "SPELL_CAST_SUCCESS") and spell == private.next_successful_spell then
                    --print(CombatLogGetCurrentEventInfo())
                    --print("removing spell")
                    private.next_successful_spell_start = 0
                    private.next_successful_spell = nil
                    if subevent == "SPELL_CAST_SUCCESS" then
                        private.last_successful_spell_end = GetTime()
                    end
                elseif (subevent == "SPELL_HEAL" or subevent == "SPELL_DAMAGE") then
                    --print(CombatLogGetCurrentEventInfo())

                end
            end
        end
        for _, unit in ipairs(private.units) do
            if UnitGUID(unit) == destGUID  then
                --private.last_group_health_confirmed[unit] = private.last_group_health_confirmed[unit]
            end
        end
    end
end

function private.next_spell()
    return private.next_successful_spell
end

function private.next_spell_target()
    return private.next_successful_spell_target
end

function private.reset_health_tracking()
    wipe(private.incoming_heal)
    wipe(private.confirmed_health)
    private.confirmed_health["player"] = 0
    private.incoming_heal["player"] = 0
    for _, unit in ipairs(private.units) do
        if UnitGUID(unit) ~= nil then
            private.confirmed_health[unit] = 0
            private.incoming_heal[unit] = 0
        end
    end
    private.update_current_health()
    private.update_incoming_heal()
end

function private.GroupUnits()
    return private.units
end
function private.update_current_health()
    for unit, health in pairs(private.confirmed_health) do
        private.confirmed_health[unit] = UnitHealth(unit, "player") or 0
    end
    private.confirmed_health_timestamp = GetTime()
end
function private.update_incoming_heal()
    for unit, heal in pairs(private.incoming_heal) do
        private.incoming_heal[unit] = UnitGetIncomingHeals(unit, "player") or 0
    end
    private.incoming_heal_timestamp = GetTime()
end

function private.UnitHealth(unit)
    -- TODO: make sure it works even if we dont check health for a bit and start freeze briefly before expected cast end
    local time = GetTime()
    if time > private.last_successful_spell_end + private.heal_cache_time then
        if private.confirmed_health_timestamp < time then
            private.update_current_health()
        end
        if private.incoming_heal_timestamp < time then
            private.update_incoming_heal()
        end
    end
    if not UnitGUID(unit) then
        --ALERT(unit)
    end
    --ALERT(unit)
    return private.incoming_heal[unit] + private.confirmed_health[unit]
end

function private.UnitHealthDeficit(unit)
    return UnitHealthMax(unit) - private.UnitHealth(unit)
end

function private.UnitHealthDeficitPercent(unit)
    return (UnitHealthMax(unit) - private.UnitHealth(unit)) / UnitHealthMax(unit)
end

function private.HighestDeficitPercentGroupUnits()
    local time = GetTime()
    if private.highest_deficit_percent_group_units_timestamp < time then
        wipe(private.highest_deficit_percent_group_units)
        for _, unit in ipairs(CastPrediction.units) do
            if UnitGUID(unit) and skill.IsUnitAlive(unit) then
                table.insert(private.highest_deficit_percent_group_units, unit)
            end
        end
        table.sort(private.highest_deficit_percent_group_units, private.nextStepDeficitComparator)
        private.highest_deficit_percent_group_units_timestamp = time
    end
    return private.highest_deficit_percent_group_units
end
function private.nextStepDeficitComparator(a, b)
    local a_current = CastPrediction.UnitHealth(a)
    local b_current = CastPrediction.UnitHealth(b)
    return  (UnitHealthMax(a) - a_current) / UnitHealthMax(a) > (UnitHealthMax(b) - b_current) / UnitHealthMax(b)
end
function private.nextStepHighestDeficit()

end

function private.load()
    private.next_successful_spell_start = 0
    private.next_successful_spell = nil
    private.reset_health_tracking()

    private.frame = CreateFrame("Frame", nil, UIParent)
    private.frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    private.frame:RegisterEvent("GROUP_ROSTER_UPDATE")
    private.frame:SetScript("OnEvent", private.on_event)
end
function private.unload()
    private.frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    private.frame:UnregisterEvent("GROUP_ROSTER_UPDATE")
    private.frame:SetScript("OnEvent", nil)
    private.frame = nil
end
CastPrediction = private