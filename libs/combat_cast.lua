--[[
Arbitrary in-combat targeted cast facility.

Usage:
Just call TryCastSpellOnUnit(spell, unit)

Example:
local function TryMyInCombatCast()
  return TryCastSpellOnUnit("Blessing of Protection", "partypet4")
end

Notes:
Needs to have the "CLICK CombatCastButton:LeftButton" action manually bound.
Unbinds all bindings on the keypad (and announces that it has been unbound).
--]]
_, skill = ...

-- Maps rotation return value to keycode (for pixel).
skill.combat_cast = {}
--  ["cast_combat_cast"] = true,
--  ["start_attack"] = true,
--  ["stop_casting"] = true,
--  ["target_nearest_enemy"] = true,
--  ["nothing"] = true,
--}

setglobal("BINDING_NAME_CLICK CombatCastButton:LeftButton", "Execute the CombatCast action")
setglobal("BINDING_NAME_CLICK StartAttackButton:LeftButton", "Execute the /startattack macro")
setglobal("BINDING_NAME_CLICK StopCastingButton:LeftButton", "Execute the /stopcasting macro")
setglobal("BINDING_NAME_CLICK TargetNearestEnemyButton:LeftButton", "Target the nearest enemy")
setglobal("BINDING_NAME_CLICK Toggle Skill", "Toggle Skill")

local combatCastButton
local startAttackButton
local stopCastingButton
local targetNearestEnemyButton
local TARGETS = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
  "pet",
  "partypet1",
  "partypet2",
  "partypet3",
  "partypet4",
  "mouseover",
  "target",
  "ground",
  "focus",
  "playertarget",
  "pettarget",
  "mouseovertarget",
  "targettarget",
  "focustarget",
  "party1target",
  "party2target",
  "party3target",
  "party4target",
  "partypet1target",
  "partypet2target",
  "partypet3target",
  "partypet4target",
  "arena1",
  "arena2",
  "arena3",
  "arena4",
  "raid1",
  "raid2",
  "raid3",
  "raid4",
  "raid5",
  "raid6",
  "raid7",
  "raid8",
  "raid9",
  "raid10",
  "raid11",
  "raid12",
  "raid13",
  "raid14",
  "raid15",
  "raid16",
  "raid17",
  "raid18",
  "raid19",
  "raid20",
  "raid21",
  "raid22",
  "raid23",
  "raid24",
  "raid25",
  "arena1",
  "arena2",
  "arena3",
  "arena4",
  "arena5",
}
local ENEMIES = {
  "mouseover",
  "target",
  "ground",
  "focus",
  "playertarget",
  "pettarget",
  "mouseovertarget",
  "targettarget",
  "focustarget",
  "party1target",
  "party2target",
  "party3target",
  "party4target",
  "partypet1target",
  "partypet2target",
  "partypet3target",
  "partypet4target",
  "arena1",
  "arena2",
  "arena3",
  "arena4",
  "arena5",
}
local MACROS = {
  "/startattack",
  "/stopcasting",
  "/equipset Shield",
  "/equipset 2H",
  -- Discipline Priest Mage Tower
  "/target Tugar", -- Tugar Bloodtotem
  "/target Fel", -- Fel Surge Totem
  "/target Jormog", -- Jormog the Behemoth
  "/focus target",
  "/focus mouseover",
  "/target mouseover",
  -- Holy Priest Mage Tower
  --"/target Corrupted Risen Mage",
  --"/target Corrupted Risen Soldier",
  --"/target Corrupted Risen Arbalest",
  --"/target Damaged Mage",
  --"/target Damaged Soldier",
  --"/target Damaged Arbalest",
}
local function all_true(unit, ...)
  local result = true
  for i = 1, select("#", ...) do
    result = result and select(i, ...)(unit)
    if not result then
      return false
    end
  end
  return true
end
local FILTERED_TARGETS = {}
local FILTERED_RESULT = {}
function UniqueFilteredCombatCastUnits(...)
  wipe(FILTERED_TARGETS)
  wipe(FILTERED_RESULT)
  if ... then
    for _, unit in ipairs(TARGETS) do
      if not FILTERED_TARGETS[UnitGUID(unit)] and all_true(unit, ...) then
        FILTERED_TARGETS[UnitGUID(unit)] = true
        table.insert(FILTERED_RESULT, unit)
      end
    end
  else
    ALERT_ONCE("No filter functions passed to UniqueFilteredCombatCastUnits()")
  end
  return FILTERED_RESULT
end
local unique_existing_combat_cast_units = {}
function UniqueExistingCombatCastUnits()
  local result = UniqueFilteredCombatCastUnits(UnitExists)
  wipe(unique_existing_combat_cast_units)
  for _, unit in ipairs(result) do
    table.insert(unique_existing_combat_cast_units, unit)
  end
  return unique_existing_combat_cast_units
end

local function UnitInRaidOrPartyOrFocus(unit)
  return unit == "player" or UnitPlayerOrPetInParty(unit) or UnitPlayerOrPetInRaid(unit) or (UnitGUID(unit) == UnitGUID("focus"))
end
local group_combat_cast_units_result = {}
local group_combat_cast_units_timestamp = 0
function GroupCombatCastUnits()
  local time = GetTime()
  if group_combat_cast_units_timestamp < time then
    wipe(group_combat_cast_units_result)
    local units = UniqueFilteredCombatCastUnits(UnitExists, UnitInRaidOrPartyOrFocus, skill.IsUnitAlive, skill.IsUnitPlayerFriend)
    for _, unit in ipairs(units) do
      table.insert(group_combat_cast_units_result, unit)
    end
    group_combat_cast_units_timestamp = time
  end
  return group_combat_cast_units_result
end

local function highest_deficit_comparator(a, b)
  return UnitHealthAndHealAbsorbDeficit(a) > UnitHealthAndHealAbsorbDeficit(b)
end
local highest_deficit_group_units_result = {}
local highest_deficit_group_units_timestamp = 0
function HighestDeficitGroupUnits()
  local time = GetTime()
  if highest_deficit_group_units_timestamp < time then
    wipe(highest_deficit_group_units_result)
    for _, unit in ipairs(GroupCombatCastUnits()) do
      table.insert(highest_deficit_group_units_result, unit)
    end
    table.sort(highest_deficit_group_units_result, highest_deficit_comparator)
    highest_deficit_group_units_timestamp = time
  end
  return highest_deficit_group_units_result
end

local function highest_deficit_percent_comparator(a, b)
  return UnitHealthAndHealAbsorbDeficit(a) / UnitHealthMax(a) > UnitHealthAndHealAbsorbDeficit(b) / UnitHealthMax(b)
end
local highest_deficit_percent_group_units_result = {}
local highest_deficit_percent_group_units_timestamp = 0
function HighestDeficitPercentGroupUnits()
  local time = GetTime()
  if highest_deficit_percent_group_units_timestamp < time then
    wipe(highest_deficit_percent_group_units_result)
    for _, unit in ipairs(GroupCombatCastUnits()) do
      table.insert(highest_deficit_percent_group_units_result, unit)
    end
    table.sort(highest_deficit_percent_group_units_result, highest_deficit_percent_comparator)
    highest_deficit_percent_group_units_timestamp = time
  end
  return highest_deficit_percent_group_units_result
end

function ForUniqueFilteredCombatCastUnits(func, ...)
  for _, unit in ipairs(UniqueFilteredCombatCastUnits(...)) do
    local result = func(unit) or nil
    if result then
      return result
    end
  end
  return nil
end

local TARGET_TO_INDEX = {}
local INDEX_TO_TARGET = {}
local SPELL_TO_INDEX = {}
local INDEX_TO_SPELL = {}
local COMBAT_CAST_BIT_BUTTONS = {}
local numSpellBits = 7
local numTargetBits = 8

local spell_book_entry_id = {}
local spell_book_entry_book_id = {}
local function spell_book_entry(spell)
  if not spell_book_entry_id[spell] then
    local start = 1000
    local stop = 0
    for i = 1, GetNumSpellTabs() do
      local _, _, offset, numEntries, _, offspecID = GetSpellTabInfo(i)
      if offspecID == 0 then
        start = math.min(start, offset)
        stop = math.max(stop, offset + numEntries)
      end
    end
    for i = start, stop do
      local type, spell_id = GetSpellBookItemInfo(i, "spell")
      if type == "SPELL" or type == "FUTURESPELL" then
        local name = GetSpellBookItemName(i, "spell")
        if name == spell then
          spell_book_entry_book_id[spell] = i
          spell_book_entry_id[spell] = spell_id
          break
        end
      end
    end
  end
  return spell_book_entry_book_id[spell], spell_book_entry_id[spell]
end
local function spell_book_entry_on_event(event)
  wipe(spell_book_entry_id)
  wipe(spell_book_entry_book_id)
end
local spell_book_entry_frame = CreateFrame("Frame", nil, UIParent)
spell_book_entry_frame:RegisterEvent("SPELLS_CHANGED")
spell_book_entry_frame:SetScript("OnEvent", spell_book_entry_on_event)
function SpellBookId(spell)
  return select(1, spell_book_entry(spell))
end
function SpellId(spell)
  return select(2, spell_book_entry(spell))
end

function PetBookId(spell)
  ALERT_ONCE("PetBookId NYI")
end

local EXTRA_SPELLS = {
  "Swift Purple Gryphon",
  "Concentrated Flame",
  "Soothe",
  "Hold on Tight",
  "Praise",
}
local EXTRA_ITEMS = {
  "Repurposed Fel Focuser",
  "Healthstone",
  "Weather-Beaten Fishing Hat",
  "Phial of Serenity",
  "13",
  "14",
}
local BANNED_SPELLS = {
  "Wartime Ability",
  "Revive Battle Pets",
  "Vindicaar Matrix Crystal",
  "Armor Skills",
  "Combat Ally",
  "Command Demon",
}
COMBAT_CAST_KEYS = {
  [0] = "NUMPAD0",
  [1] = "NUMPAD1",
  [2] = "NUMPAD2",
  [3] = "NUMPAD3",
  [4] = "NUMPAD4",
  [5] = "NUMPAD5",
  [6] = "NUMPAD6",
  [7] = "NUMPAD7",
  [8] = "NUMPAD8",
  [9] = "NUMPAD9",
  [10] = "NUMPADDECIMAL",
  [11] = "NUMPADPLUS",
  [12] = "NUMPADMINUS",
  [13] = "NUMPADMULTIPLY",
  [14] = "NUMPADDIVIDE",
}

local function BitCycleFunctionString()
  return [[
    local bit = self:GetParent():GetAttribute(self:GetName()) or 0
    --local newbit = bit > -3 and bit - 1 or 3
    local newbit = bit > 0 and bit - 1 or 1
    self:GetParent():SetAttribute(self:GetName(), newbit)

    local spellIndex = 0
    local numSpellBits = self:GetParent():GetAttribute("numSpellBits")
    local targetIndex = math.pow(2, numSpellBits)
    local numCombatCastButtons = self:GetParent():GetAttribute("numCombatCastButtons")
    for i = 0, numCombatCastButtons do
      local attribute = self:GetParent():GetAttribute("CombatCastBitButton"..i) or 0
      local currentBit = math.min(attribute > 0 and 1 or 0, 1)
      if i < numSpellBits then
        spellIndex = spellIndex + math.pow(2, i) * currentBit
      else
        targetIndex = targetIndex + math.pow(2, i - numSpellBits) * currentBit
      end
    end
    local type = self:GetParent():GetAttribute("type_"..spellIndex)
    local spell = self:GetParent():GetAttribute("spell_"..spellIndex)
    local target = self:GetParent():GetAttribute("target_"..targetIndex)
    --print("=====================")
    --print("time: "..tostring(self:GetParent():GetAttribute("time")))
    --print("name: "..tostring(self:GetName()))
    --print("newbit: "..tostring(newbit))
    --print("spellIndex: "..tostring(spellIndex))
    --print("targetIndex: "..tostring(targetIndex))
    --print("type: "..tostring(type))
    --print("spell: "..tostring(spell))
    --print("unit: "..tostring(target))
    --print(tostring(spell) .. " => " .. tostring(target))

    self:GetParent():SetAttribute("spell", nil)
    self:GetParent():SetAttribute("item", nil)
    self:GetParent():SetAttribute("unit", nil)
    self:GetParent():SetAttribute("type", type)
    if type == "macro" then
      self:GetParent():SetAttribute("macrotext", spell)
    else
      self:GetParent():SetAttribute(type or "spell", spell)
      self:GetParent():SetAttribute("unit", target ~= "ground" and target or nil)
    end
  ]]
end

local COMMAND_TO_CAST_NAME = {
  ["CLICK CombatCastButton:LeftButton"] = "cast_combat_cast",
  ["CLICK StartAttackButton:LeftButton"] = "start_attack",
  ["CLICK StopCastingButton:LeftButton"] = "stop_casting",
  ["CLICK TargetNearestEnemyButton:LeftButton"] = "target_nearest_enemy",
  ["ACTIONBUTTON1"] = "ACTIONBUTTON1",
  ["ACTIONBUTTON2"] = "ACTIONBUTTON2",
  ["ACTIONBUTTON3"] = "ACTIONBUTTON3",
  ["ACTIONBUTTON4"] = "ACTIONBUTTON4",
  ["ACTIONBUTTON5"] = "ACTIONBUTTON5",
  ["INTERACTMOUSEOVER"] = "INTERACTMOUSEOVER",
}
local function UpdateCombatCastBindings()
  -- Maps user configured keybinds to keycodes.
  skill.combat_cast.actions = skill.combat_cast.actions or {}
  for i = 1, GetNumBindings() do
    local commandName, category, binding1, binding2 = GetBinding(i)
    --if category == "Skill" then
      for command, cast_name in pairs(COMMAND_TO_CAST_NAME) do
        if commandName == command then
          skill.combat_cast.actions[cast_name] = skill.KEY_CODE[binding1 or binding2]
        end
      end
    --end
  end
  skill.combat_cast.actions["nothing"] = 0
end

local function UpdateSpellAttributes()
  for index, indexedSpell in pairs(INDEX_TO_SPELL) do
    local type = "spell"
    if indexedSpell == "target" then
      type = "target"
    else
      for _, item in ipairs(EXTRA_ITEMS) do
        if tostring(item) == tostring(indexedSpell) then
          type = "item"
          break
        end
      end
      for _, item in ipairs(MACROS) do
        if tostring(item) == tostring(indexedSpell) then
          type = "macro"
          break
        end
      end
    end
    combatCastButton:SetAttribute("spell_" .. index, tostring(indexedSpell))
    combatCastButton:SetAttribute("type_" .. index, type)
  end
  combatCastButton:SetAttribute("numSpellBits", numSpellBits)
  combatCastButton:SetAttribute("numCombatCastButtons", #COMBAT_CAST_KEYS)
end
local function UpdateTargetAttributes()
  for index, target in pairs(INDEX_TO_TARGET) do
    combatCastButton:SetAttribute("target_"..index, target)
  end
end

local function SetUpCombatCastBitButtons()
  for i = 0, #COMBAT_CAST_KEYS do
    COMBAT_CAST_BIT_BUTTONS[i] = CreateFrame("Button", "CombatCastBitButton"..i, combatCastButton, "SecureHandlerClickTemplate")
    COMBAT_CAST_BIT_BUTTONS[i]:SetAttribute("_onclick", BitCycleFunctionString())
  end
end
local function UpdateBindings()
  combatCastButton:UnregisterEvent("UPDATE_BINDINGS")
  for bitIndex = 0, #COMBAT_CAST_KEYS do
    local wowName = COMBAT_CAST_KEYS[bitIndex]
    SetOverrideBindingClick(combatCastButton, true, wowName, "CombatCastBitButton"..bitIndex)
  end
  combatCastButton:RegisterEvent("UPDATE_BINDINGS")
  for i = 1, GetNumBindings() do
    local commandName, category, binding1, binding2 = GetBinding(i)
    if category == "Skill" then
      for command, _ in pairs(COMMAND_TO_CAST_NAME) do
        if commandName == command then
          if not binding1 and not binding2 then
            ALERT("CombatCast command not bound: "..tostring(getglobal("BINDING_NAME_"..command)))
          end
        end
      end
    end
  end
end

local function UpdateTargetMap()
  local numTargets = 0
  for i, unit in ipairs(TARGETS) do
    local index = math.pow(2, numSpellBits) - 1 + i
    --print(tostring(unit)..": "..tostring(index))
    TARGET_TO_INDEX[unit] = index
    INDEX_TO_TARGET[index] = unit
    numTargets = numTargets + 1
  end
  if numTargets > math.pow(2, numTargetBits) then
    ALERT("TOO MANY TARGETS!")
  end
end

local function is_spell_banned(spell)
  for _, v in ipairs(BANNED_SPELLS) do
    if v == spell then
      --ALERT("BAN: "..v)
      return true
    end
  end
  return false
end
--local function UpdateSpells()
--  for i = start, stop do
--    local type, spell_id = GetSpellBookItemInfo(i, "spell")
--    --ALERT("spell_id: "..tostring(spell_id))
--    --ALERT("IsPassiveSpell(spell_id): "..tostring(IsPassiveSpell(spell_id)))
--    --ALERT("GetSpellInfo(spell_id): "..tostring(GetSpellInfo(spell_id)))
--    if not spell_id or IsPassiveSpell(spell_id) then
--      break
--    end
--    local spell = GetSpellBookItemName(i, "spell")
--    if not is_spell_banned(spell) then
--      if type == "SPELL" then
--        if spell and not SPELL_TO_INDEX[spell] then
--          SPELL_TO_INDEX[spell] = numSpells
--          INDEX_TO_SPELL[numSpells] = spell
--          numSpells = numSpells + 1
--        end
--      elseif type == "FLYOUT" then
--        local _, _, numSlots = GetFlyoutInfo(spell_id)
--        for j = 1, numSlots do
--          local spellID = GetFlyoutSlotInfo(spell_id, j)
--          local spellName = GetSpellInfo(spellID)
--          if spellName and not SPELL_TO_INDEX[spellName] then
--            SPELL_TO_INDEX[spellName] = numSpells
--            INDEX_TO_SPELL[numSpells] = spellName
--            numSpells = numSpells + 1
--          end
--        end
--      end
--    end
--  end
--end
local function UpdateSpellMap()
  local numSpells = 0
  SPELL_TO_INDEX["target"] = numSpells
  INDEX_TO_SPELL[numSpells] = "target"
  numSpells = numSpells + 1
  for i = 1, GetNumSpellTabs() do
    local _, _, offset, numEntries, _, offspecID = GetSpellTabInfo(i)
    if offspecID == 0 then
      local start = 1 + offset
      local stop = start + numEntries
      for j = start, stop do
        local type, spell_id = GetSpellBookItemInfo(j, "spell")
        --ALERT("j: "..tostring(j))
        --ALERT("spell_id: "..tostring(spell_id))
        --ALERT("IsPassiveSpell(spell_id): "..tostring(IsPassiveSpell(spell_id)))
        --ALERT("GetSpellInfo(spell_id): "..tostring(GetSpellInfo(spell_id)))
        if not spell_id or IsPassiveSpell(spell_id) then
          break
        end
        local spell = GetSpellBookItemName(j, "spell")
        if not is_spell_banned(spell) then
          --ALERT("GetSpellInfo(spell_id): "..tostring(GetSpellInfo(spell_id)))
          if type == "SPELL" then
            if spell and not SPELL_TO_INDEX[spell] then
              --if spell == "Consecration" then
              --  ALERT("j: "..tostring(j))
              --  ALERT("numSpells: "..tostring(numSpells))
              --end
              SPELL_TO_INDEX[spell] = numSpells
              INDEX_TO_SPELL[numSpells] = spell
              numSpells = numSpells + 1
            end
          elseif type == "FLYOUT" then
            local _, _, numSlots = GetFlyoutInfo(spell_id)
            for j = 1, numSlots do
              local spellID = GetFlyoutSlotInfo(spell_id, j)
              local spellName = GetSpellInfo(spellID)
              if spellName and not SPELL_TO_INDEX[spellName] then
                SPELL_TO_INDEX[spellName] = numSpells
                INDEX_TO_SPELL[numSpells] = spellName
                numSpells = numSpells + 1
              end
            end
          end
        end
      end
    end
  end

  local numPetSpells = HasPetSpells()
  if numPetSpells then
    for i = 1, numPetSpells do
      local skillType = GetSpellBookItemInfo(i, BOOKTYPE_PET)
      local name, _, _, _, _, _, spellID = GetSpellInfo(i, BOOKTYPE_PET)
      if name and not SPELL_TO_INDEX[name] and (skillType == "SPELL" or skillType == "PETACTION") and not IsPassiveSpell(spellID) then
        SPELL_TO_INDEX[name] = numSpells
        INDEX_TO_SPELL[numSpells] = name
        --ALERT(name..": "..numSpells)
        numSpells = numSpells + 1
      end
    end
  end
  for _, spell in ipairs(EXTRA_SPELLS) do
    local name = GetSpellInfo(spell)
    if name and not SPELL_TO_INDEX[name] then
      SPELL_TO_INDEX[name] = numSpells
      INDEX_TO_SPELL[numSpells] = name
      numSpells = numSpells + 1
    end
  end
  local module = GetModule()
  if module and module.extra_spells then
    for _, spell in ipairs(module.extra_spells) do
      if not SPELL_TO_INDEX[spell] then
        SPELL_TO_INDEX[spell] = numSpells
        INDEX_TO_SPELL[numSpells] = spell
        numSpells = numSpells + 1
      else
        ALERT_ONCE(string.format("Trying to add already existing spell '|cFFFF0000%s|r'.", spell))
      end
    end
  end
  for _, item in ipairs(EXTRA_ITEMS) do
    if item and not SPELL_TO_INDEX[item] then
      SPELL_TO_INDEX[item] = numSpells
      INDEX_TO_SPELL[numSpells] = item
      numSpells = numSpells + 1
    end
  end
  for _, item in ipairs(MACROS) do
    if item and not SPELL_TO_INDEX[item] then
      SPELL_TO_INDEX[item] = numSpells
      INDEX_TO_SPELL[numSpells] = item
      numSpells = numSpells + 1
    end
  end

  for _, info in ipairs(C_ZoneAbility.GetActiveAbilities()) do
    local override = FindSpellOverrideByID(info.spellID)
    if override then
      local name = GetSpellInfo(override)
      if not SPELL_TO_INDEX[name] then
        SPELL_TO_INDEX[name] = numSpells
        INDEX_TO_SPELL[numSpells] = name
        numSpells = numSpells + 1
      else
        ALERT_ONCE(string.format("Trying to add already existing tone ability spell '|cFFFF0000%s|r'.", spell))
      end
    end
  end
  if numSpells > math.pow(2, numSpellBits) then
    ALERT_ONCE("TOO MANY SPELLS!")
    ALERT_ONCE("numSpells: "..tostring(numSpells))
    ALERT_ONCE("max: "..tostring(math.pow(2, numSpellBits)))
  end
end

local function UpdateCombatCastBitButtons()
  wipe(SPELL_TO_INDEX)
  wipe(INDEX_TO_SPELL)
  UpdateSpellMap()
  UpdateSpellAttributes()
end
local function ExtendCombatCastBitButtons()
--  for spell, index in pairs(SPELL_TO_INDEX) do
--
--  end
end
local function CreateCombatCastBitButtons()
  SetUpCombatCastBitButtons()
  UpdateBindings()

  -- Since targets are static, we can create the map once and skip it when updating.
  UpdateTargetMap()
  UpdateTargetAttributes()
end


local function GetTargetingSpell()
  for spell, _ in pairs(SPELL_TO_INDEX) do
    if IsCurrentSpell(spell) then
      return spell
    end
  end
  return nil
end
local function TryCombatCast(unit, spell)
  if SpellIsTargeting() then
    if unit == "ground" then
      return "nothing"
    end
    local targeting_spell = GetTargetingSpell()
    if targeting_spell and skill.IsGroundTargeted(targeting_spell) then
      return "nothing"
    end
  end
  if not unit or not spell then
    return nil
  end
  if rate_limiter.is_suspended(spell, unit) then
    return nil
  end
  local tti = skill.time_till_interrupt()
  if tti and tti > 0 and tti < skill.cast_time(spell) + 0.2 then
    return nil
  end
  if not SPELL_TO_INDEX[tostring(spell)] then
    ALERT_ONCE(string.format("Trying to cast unknown spell '|cFFFF0000%s|r' on unit '|cFFFF0000%s|r'.", tostring(spell), tostring(unit)))
    return nil
  end
  --ALERT("-----------------")
  --ALERT("spell: "..tostring(spell))
  --ALERT("unit: "..tostring(unit))
  --if spell ~= "target" then
  --  ALERT("IsUnitInRange(spell, unit): "..tostring(IsUnitInRange(spell, unit)))
  --  ALERT("GetCD(spell): "..tostring(GetCD(spell)))
  --end
  if spell == "start_attack" or spell == "stop_casting" or spell == "target_nearest_enemy" then
    return spell
  end
  if spell == "target" then
    for i, target in ipairs(TARGETS) do
      if UnitGUID("target") == UnitGUID(unit) then
        return nil
      end
      if target == unit and UnitExists(unit) then
        --ALERT("targeting: "..tostring(spell.."_"..target))
        return "cast_combat_cast"
      end
    end
  end
  for _, item in ipairs(EXTRA_ITEMS) do
    if tostring(item) == tostring(spell) then
      local n = tonumber(item)
      if n then
        -- TODO: Check CD and other stuff here.
        return "cast_combat_cast" or nil
      end
    end
  end
  for _, item in ipairs(MACROS) do
    if spell == item then
      return "cast_combat_cast" or nil
    end
  end
  return
    skill.IsUnitAlive(unit) and
    skill.IsUsable(spell) and
    (unit == "ground" or IsUnitInRange(spell, unit)) and
    GetCD(spell) <= GlobalCD() and
    "cast_combat_cast" or nil
end

local current_unit = "player"
local current_spell = "target"
local new_wanted_state_this_frame = false
local last_error
local function set_wanted_state(unit, spell)
  if new_wanted_state_this_frame then
    local error = string.format("\n|cFFFF0000[%s @ %s]|r => |cFFFF0000[%s @ %s]|r\n", current_spell, current_unit, spell, unit)
    if error ~= last_error then
      last_error = error
      ALERT("|cFFFF0000WARNING:\n|rCombatCast updated the wanted state multiple times per frame."..error.."This is probably not what you want.")
    end
  end
  new_wanted_state_this_frame = true
  current_unit = tostring(unit)
  current_spell = tostring(spell)
  --ALERT("unit: "..tostring(unit))
  --ALERT("setting wanted state spell: "..tostring(spell))
end
local ready = false
function skill.combat_cast.is_ready()
  return ready
end
local function update_current_state()
  if not current_spell or not current_unit then
    for i = 0, #COMBAT_CAST_KEYS do
      skill.set_pixel_value("CombatCastPixelButton"..i, 0)
    end
    return
  end
  ready = true
  local target_offset = math.pow(2, numSpellBits)
  --local debugCurrent = ""
  --local debugRequired = ""
  for i = 0, #COMBAT_CAST_KEYS do
    local frame = COMBAT_CAST_BIT_BUTTONS[i]
    local current = math.min(1, frame:GetParent():GetAttribute(frame:GetName()) or 0) > 0
    if not SPELL_TO_INDEX[current_spell] then
      ALERT_ONCE("band() error current_spell: "..tostring(current_spell))
    end
    if not TARGET_TO_INDEX[current_unit] then
      ALERT_ONCE("band() current_unit: "..tostring(current_unit))
    end
    local spellRequired = math.min(1, bit.band(SPELL_TO_INDEX[current_spell] or 0, math.pow(2, i))) > 0
    local targetRequired = math.min(1, bit.band((target_offset + TARGET_TO_INDEX[current_unit]) or 0, math.pow(2, i - numSpellBits))) > 0
    --ALERT("spellRequired: "..tostring(spellRequired))
    --ALERT("targetRequired: "..tostring(targetRequired))
    --ALERT("current_spell: "..tostring(current_spell))
    --ALERT("current_unit: "..tostring(current_unit))
    local value = current == (spellRequired or targetRequired) and 0 or skill.KEY_CODE[COMBAT_CAST_KEYS[i]]
    value = value > 0 and value + (255 - value) * 2^8 or 0
    if ready == true then
      ready = value == 0
    end
    skill.set_pixel_value("CombatCastPixelButton"..i, value)
    --debugCurrent = debugCurrent .. (current and "1" or "0")
    --debugRequired = debugRequired .. ((spellRequired or targetRequired) and "1" or "0")
  end
  --ALERT(tostring(debugCurrent)..": debugCurrent")
  --ALERT(tostring(debugRequired)..": debugRequired")
end

function CanCastSpellOnUnit(spell, unit)
  local result = TryCombatCast(unit, spell)
  return
    result == "cast_combat_cast" or
    result == "start_attack" or
    result == "stop_casting" or
    result == "target_nearest_enemy"
end
function do_nothing(spell, unit)
  return "nothing"
end
function skill.combat_cast.try_cast_spell_on_unit_internal(spell, unit)
  --local enabled = combatCastButton:GetScript("OnUpdate")
  --if not enabled then
  --  return "nothing"
  --end
  if not CanCastSpellOnUnit(spell, unit) then
    return nil
  end
  local state_up_to_date =
    (current_unit == "ground" or current_unit == combatCastButton:GetAttribute("unit")) and
    (current_spell == combatCastButton:GetAttribute("spell") or
     current_spell == combatCastButton:GetAttribute("item") or
     current_spell == combatCastButton:GetAttribute("target"))
  if combatCastButton:GetAttribute("type") == "macro" then
    state_up_to_date = combatCastButton:GetAttribute("macrotext") == current_spell
  end
  if state_up_to_date and unit == current_unit and tostring(spell) == tostring(current_spell) then
    return TryCombatCast(unit, spell)
  end
  set_wanted_state(unit, spell)
  update_current_state()
  return "nothing"
end
local function InternalTryCastSpellOnUnit(spell, unit)
  return skill.combat_cast.try_cast_spell_on_unit_internal(spell, unit)
end
function TryCastSpellOnTarget(spell)
  return TryCastSpellOnUnit(spell, "target")
end
function TryCastSpellOnPlayer(spell)
  return TryCastSpellOnUnit(spell, "player")
end
function TryMacro(macrotext)
  for _, macro in ipairs(MACROS) do
    if macro == macrotext then
      return TryCastSpellOnPlayer(macro)
    end
  end
  ALERT_ONCE("Trying to execute macro '"..macrotext.."'. Consider adding it to MACROS.")
  return nil
end
function CombatCastCurrentUnit()
  return current_unit
end
function CombatCastCurrentSpell()
  return current_spell
end
TryCastSpellOnUnit = do_nothing

function skill.combat_cast.on_update()
  new_wanted_state_this_frame = false
  update_current_state()
end
local function OnEvent(_, event, ...)
  if event == "ADDON_LOADED" then
    local name = ...
    if name == "skill" then
      combatCastButton:RegisterEvent("PLAYER_LOGIN")
      combatCastButton:RegisterEvent("PLAYER_ENTERING_WORLD")
      combatCastButton:RegisterEvent("PLAYER_LEAVING_WORLD")
      combatCastButton:RegisterEvent("UPDATE_BINDINGS")
      combatCastButton:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")
      combatCastButton:RegisterEvent("PLAYER_TALENT_UPDATE")
      combatCastButton:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
      combatCastButton:UnregisterEvent("ADDON_LOADED")
    end
  elseif event == "PLAYER_ENTERING_WORLD" then
    combatCastButton:RegisterEvent("SPELLS_CHANGED")
    CreateCombatCastBitButtons()
    UpdateCombatCastBindings()
    UpdateCombatCastBitButtons()
    for _, name in pairs(INDEX_TO_SPELL) do
      local range = RangeOfSpell(name)
      --ALERT(name..": "..range)
    end
  elseif event == "PLAYER_LEAVING_WORLD" then
    combatCastButton:UnregisterEvent("SPELLS_CHANGED")
    TryCastSpellOnUnit = do_nothing
  elseif event == "SPELLS_CHANGED" then
    TryCastSpellOnUnit = InternalTryCastSpellOnUnit
    --set_wanted_state("player", "target")
    if not UnitAffectingCombat("player") then
      UpdateCombatCastBitButtons()
    else
      ExtendCombatCastBitButtons()
    end
  elseif event == "PLAYER_SPECIALIZATION_CHANGED" or event == "PLAYER_TALENT_UPDATE" then
    UpdateCombatCastBindings()
    UpdateCombatCastBitButtons()
  elseif event == "UPDATE_BINDINGS" then
    UpdateBindings()
  elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _, spell_name = CombatLogGetCurrentEventInfo()
    if source_guid == UnitGUID("player") and sub_event == "SPELL_CAST_SUCCESS" and spell_name == current_spell and IsCurrentSpell(select(7, GetSpellInfo(current_spell))) then
      --ALERT("Cancelling queued double cast: "..tostring(current_spell))
      SpellCancelQueuedSpell()
    end
  end
end


combatCastButton = CreateFrame("Button", "CombatCastButton", UIParent, "SecureActionButtonTemplate")
combatCastButton:SetAttribute("type", "spell")
combatCastButton:RegisterEvent("ADDON_LOADED")
combatCastButton:SetScript("OnEvent", OnEvent)
startAttackButton = CreateFrame("Button", "StartAttackButton", UIParent, "SecureActionButtonTemplate")
startAttackButton:SetAttribute("type", "macro")
startAttackButton:SetAttribute("macrotext", "/startattack")
stopCastingButton = CreateFrame("Button", "StopCastingButton", UIParent, "SecureActionButtonTemplate")
stopCastingButton:SetAttribute("type", "macro")
stopCastingButton:SetAttribute("macrotext", "/stopcasting")
targetNearestEnemyButton = CreateFrame("Button", "TargetNearestEnemyButton", UIParent, "SecureActionButtonTemplate")
targetNearestEnemyButton:SetAttribute("type", "macro")
targetNearestEnemyButton:SetAttribute("macrotext", "/targetenemy")

--local test_button = CreateFrame("Button", "CombatCastTestButton", UIParent, "SecureActionButtonTemplate")
--test_button:SetWidth(200)
--test_button:SetHeight(200)
--test_button:SetPoint("CENTER", UIParent, "CENTER", 0, 0)
--test_button:RegisterForClicks("AnyDown")
--test_button.texture = test_button:CreateTexture()
--test_button.texture:SetAllPoints(test_button)
--test_button.texture:SetColorTexture(43 / 255, 43 / 255, 43 / 255, 1.0)
--test_button:Show()
--test_button:SetAttribute("type", "item")
--test_button:SetAttribute("item", "13")

function skill.combat_cast.get_current_combat_cast_spell()
  return combatCastButton:GetAttribute("spell")
end

function skill.combat_cast.get_current_combat_cast_action()
  return skill.combat_cast.actions[skill.combat_cast.get_current_combat_cast_spell()]
end
