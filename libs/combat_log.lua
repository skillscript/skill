local CombatLog = {}

local numBaseValues = 11
local prefixToOffset = {
  RANGE = 3,
  SPELL = 3,
  DAMAGE = 3,
  ENCHANT = 3,
  UNIT = 2,
  ENVIRONMENTAL = 1,
}
local function getOffset(prefix)
  return prefixToOffset[prefix] or 0
end

local function setPrefixValues(outTable, extraValues, prefix)
  if prefix == "RANGE" or prefix == "SPELL" or prefix == "DAMAGE" then
    outTable.spellId = extraValues[numBaseValues + 1]
    outTable.spellName = extraValues[numBaseValues + 2]
    outTable.spellSchool = extraValues[numBaseValues + 3]
  elseif prefix == "ENCHANT" then
    outTable.spellName = extraValues[numBaseValues + 1]
    outTable.itemID = extraValues[numBaseValues + 2]
    outTable.itemName = extraValues[numBaseValues + 3]
  elseif prefix == "UNIT" then
    outTable.recapID = extraValues[numBaseValues + 1]
    outTable.unconsciousOnDeath = extraValues[numBaseValues + 2]
  elseif prefix == "ENVIRONMENTAL" then
    outTable.environmentalType = extraValues[numBaseValues + 1]
  end
end

local function setSuffixValues(outTable, extraValues, prefix, suffix)
  local base_index = numBaseValues + getOffset(prefix)
  if suffix == "DAMAGE" or suffix == "SHIELD" or suffix == "SPLIT" then
    outTable.amount = extraValues[base_index + 1]
    outTable.overkill = extraValues[base_index + 2]
    outTable.school = extraValues[base_index + 3]
    outTable.resisted = extraValues[base_index + 4]
    outTable.blocked = extraValues[base_index + 5]
    outTable.absorbed = extraValues[base_index + 6]
    outTable.critical = extraValues[base_index + 7]
    outTable.glancing = extraValues[base_index + 8]
    outTable.crushing = extraValues[base_index + 9]
    outTable.isOffHand = extraValues[base_index + 10]
  elseif suffix == "MISSED" or suffix == "SHIELD_MISSED" then
    outTable.missType = extraValues[base_index + 1]
    outTable.isOffHand = extraValues[base_index + 2]
    outTable.amountMissed = extraValues[base_index + 3]
  elseif suffix == "HEAL" or suffix == "PERIODIC_HEAL" then
    outTable.amount = extraValues[base_index + 1]
    outTable.overhealing = extraValues[base_index + 2]
    outTable.absorbed = extraValues[base_index + 3]
    outTable.critical = extraValues[base_index + 4]
  elseif suffix == "ENERGIZE" then
    outTable.amount = extraValues[base_index + 1]
    outTable.overEnergize = extraValues[base_index + 2]
    outTable.powerType = extraValues[base_index + 3]
    outTable.alternatePowerType = extraValues[base_index + 4]
  elseif suffix == "DRAIN" then
    outTable.amount = extraValues[base_index + 1]
    outTable.powerType = extraValues[base_index + 2]
    outTable.extraAmount = extraValues[base_index + 3]
  elseif suffix == "LEECH" then
    outTable.amount = extraValues[base_index + 1]
    outTable.powerType = extraValues[base_index + 2]
    outTable.extraAmount = extraValues[base_index + 3]
  elseif suffix == "INTERRUPT" then
    outTable.extraSpellId = extraValues[base_index + 1]
    outTable.extraSpellName = extraValues[base_index + 2]
    outTable.extraSchool = extraValues[base_index + 3]
  elseif suffix == "DISPEL" then
    outTable.extraSpellId = extraValues[base_index + 1]
    outTable.extraSpellName = extraValues[base_index + 2]
    outTable.extraSchool = extraValues[base_index + 3]
    outTable.auraType = extraValues[base_index + 4]
  elseif suffix == "DISPEL_FAILED" then
    outTable.extraSpellId = extraValues[base_index + 1]
    outTable.extraSpellName = extraValues[base_index + 2]
    outTable.extraSchool = extraValues[base_index + 3]
  elseif suffix == "STOLEN" then
    outTable.extraSpellId = extraValues[base_index + 1]
    outTable.extraSpellName = extraValues[base_index + 2]
    outTable.extraSchool = extraValues[base_index + 3]
    outTable.auraType = extraValues[base_index + 4]
  elseif suffix == "EXTRA_ATTACKS" then
    outTable.amount = extraValues[base_index + 1]
  elseif suffix == "AURA_APPLIED" then
    outTable.auraType = extraValues[base_index + 1]
    outTable.amount = extraValues[base_index + 2]
  elseif suffix == "AURA_REMOVED" then
    outTable.auraType = extraValues[base_index + 1]
    outTable.amount = extraValues[base_index + 2]
  elseif suffix == "AURA_APPLIED_DOSE" then
    outTable.auraType = extraValues[base_index + 1]
    outTable.amount = extraValues[base_index + 2]
  elseif suffix == "AURA_REMOVED_DOSE" then
    outTable.auraType = extraValues[base_index + 1]
    outTable.amount = extraValues[base_index + 2]
  elseif suffix == "AURA_REFRESH" then
    outTable.auraType = extraValues[base_index + 1]
    outTable.amount = extraValues[base_index + 2]
  elseif suffix == "AURA_BROKEN" then
    outTable.auraType = extraValues[base_index + 1]
  elseif suffix == "AURA_BROKEN_SPELL" then
    outTable.extraSpellId = extraValues[base_index + 1]
    outTable.extraSpellName = extraValues[base_index + 2]
    outTable.extraSchool = extraValues[base_index + 3]
    outTable.auraType = extraValues[base_index + 4]
  elseif suffix == "CAST_FAILED" then
    outTable.failedType = extraValues[base_index + 1]
  end
end


local extraValues = {}
local outTable = {}
function CombatLog.GetCurrentEventInfo()
  wipe(outTable)
  local timestamp, subevent, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags,
  i_12, i_13, i_14, i_15, i_16, i_17, i_18, i_19, i_20, i_21, i_22, i_23, i_24, i_25, i_26, i_27, i_28, i_29 = CombatLogGetCurrentEventInfo()

  outTable.timestamp = timestamp
  outTable.subevent = subevent
  outTable.hideCaster = hideCaster
  outTable.sourceGUID = sourceGUID
  outTable.sourceName = sourceName
  outTable.sourceFlags = sourceFlags
  outTable.sourceRaidFlags = sourceRaidFlags
  outTable.destGUID = destGUID
  outTable.destName = destName
  outTable.destFlags = destFlags
  outTable.destRaidFlags = destRaidFlags

  extraValues[12] = i_12
  extraValues[13] = i_13
  extraValues[14] = i_14
  extraValues[15] = i_15
  extraValues[16] = i_16
  extraValues[17] = i_17
  extraValues[18] = i_18
  extraValues[19] = i_19
  extraValues[20] = i_20
  extraValues[21] = i_21
  extraValues[22] = i_22
  extraValues[23] = i_23
  extraValues[24] = i_24
  extraValues[25] = i_25
  extraValues[26] = i_26
  extraValues[27] = i_27
  extraValues[28] = i_28
  extraValues[29] = i_29

  local underscoreIndex = strfind(subevent, "_") or strlen(subevent)
  local prefix = strsub(subevent, 1, underscoreIndex - 1)
  local suffix = strsub(subevent, underscoreIndex + 1)
  outTable.prefix = prefix
  outTable.suffix = suffix
  setPrefixValues(outTable, extraValues, prefix)
  setSuffixValues(outTable, extraValues, prefix, suffix)

  return outTable
end

function CombatLog.unit_spell_cast_success(unit, spell)
  local info = combat_log.GetCurrentEventInfo()
  return
    info.prefix == "SPELL" and
    info.suffix == "CAST_SUCCESS" and
    info.sourceGUID == UnitGUID(unit) and
    info.spellName == spell
end
combat_log = CombatLog