_, skill = ...

local function WARNING(...)
  if true then
    ALERT(...)
  end
end

SKILL_PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}

function PlayerClass()
  return string.lower(select(2, UnitClass("player"))) or nil
end

function PlayerSpec()
  local spec = GetSpecialization() or nil
  local name = spec and select(2, GetSpecializationInfo(spec))
  if name == "" then
    name = "low_level"
  end
  return name and string.lower(string.gsub(name, " ", "_")) or nil
end

function GetModule()
  local class = PlayerClass()
  local spec = PlayerSpec()
  return skill and skill.modules and class and spec and skill.modules[class][spec] or nil
end

function ValidName(name_or_id)
  local name = GetSpellInfo(name_or_id)
  if not name then
    name = select(10, GetItemInfo(name_or_id))
  end
  if not name then
    if IsTalent(name_or_id) and not IsTalentSelected(name_or_id) then
      return nil
    end
  end
  if not name then
    ALERT_ONCE("Unknown name_or_id: "..tostring(name_or_id))
  end
  return type(name_or_id) == type(0) and name or name_or_id
end

function IsTalent(name)
  for tier = 1, 7 do
    for column = 1, 3 do
      local _, talent = GetTalentInfoBySpecialization(GetSpecialization(), tier, column)
      if talent == name then
        return true
      end
    end
  end
  return false
end

function IsTalentSelected(name)
  for tier = 1, 7 do
    for column = 1, 3 do
      local _, talent, _, _, _, _, _, _, _, selected, granted_by_aura = GetTalentInfoBySpecialization(GetSpecialization(), tier, column)
      if talent == name then
        return selected or granted_by_aura
      end
    end
  end
  return false
end

local range_check = LibStub("LibRangeCheck-2.0")
function skill.UnitRangeMax(unit)
  local _, range_max = range_check:GetRange(unit)
  return range_max
end
function IsUnitInRange(spell, unit)
  if unit == "player" then
    return true
  elseif not UnitExists(unit) then
    return false
  end
  local isSpellInRangeResult = IsSpellInRange(spell, unit)
  if isSpellInRangeResult == nil then
    local spell_range = RangeOfSpell(spell)
    if spell_range <= 0 then
      ALERT_ONCE(string.format("Trying to cast '|cFFFF0000%s|r' (range 0) on '|cFF00FFFF%s|r'.", spell, unit))
    end
    local _, range_max = range_check:GetRange(unit)
    if range_max == nil then
      return false
    end
    return range_max <= spell_range
  end
  return isSpellInRangeResult == 1
end

function IsTargetInRange(name_or_id)
  return IsUnitInRange(name_or_id, "target")
end

function IsUnitInMeleeRange(unit)
  return IsUnitInRange("Attack", unit)
end

function IsTargetInMeleeRange()
  return IsUnitInMeleeRange("target")
end

function HastedSeconds(seconds)
  return max(1, seconds / (1 + GetHaste() / 100))
end

function GlobalCDMax()
  return HastedSeconds(1.5)
end

local CD_OFFSET = 0.3
function GlobalCD(offset)
  offset = offset or CD_OFFSET
  local start, duration = GetSpellCooldown(61304)
  return start and max(0, start + duration - GetTime() - offset) or 0
end

function GetCD(name_or_id)
  local spell = GetSpellInfo(ValidName(name_or_id))
  if spell then
    local start, duration = GetSpellCooldown(name_or_id)
    return start and max(0, start + duration - GetTime() - CD_OFFSET) or 0
  end
  return GetItemCD(name_or_id)
  --if not start then
  --  start, duration = GetItemCooldown(ValidName(name_or_id))
  --end
end

function GetCDMax(spell)
  local cd_max_millis, gcd_millis = GetSpellBaseCooldown(ValidName(spell))
  return HastedSeconds(max(cd_max_millis, gcd_millis) / 1000)
end

function skill.IfOffGlobal(spell)
  local _, gcd_millis = GetSpellBaseCooldown(spell)
  return gcd_millis <= 0
end

--function GetItemSpellId(name)
--  local item, itemLink = GetItemInfo(name)
--  if item then
--    local _, _, _, _, id = string.find(itemLink, "|?c?f?f?(%x*)|?H?([^:]*):?(%d+):?(%d*):?(%d*):?(%d*):?(%d*):?(%d*):?(%-?%d*):?(%-?%d*):?(%d*):?(%d*)|?h?%[?([^%[%]]*)%]?|?h?|?r?")
--    return id
--  end
--end
function GetItemCD(name)
  local item, itemLink = GetItemInfo(name)
  if item then
    local _, _, _, _, id = string.find(itemLink, "|?c?f?f?(%x*)|?H?([^:]*):?(%d+):?(%d*):?(%d*):?(%d*):?(%d*):?(%d*):?(%-?%d*):?(%-?%d*):?(%d*):?(%d*)|?h?%[?([^%[%]]*)%]?|?h?|?r?")
    local start, duration, enable = GetItemCooldown(id)
    if enable == 0 or not start then
      return 1337
    end
    return start and max(0, start + duration - GetTime()) or 0
  end
  return 1337
end

function GetCharges(name_or_id)
  return select(1, GetSpellCharges(ValidName(name_or_id))) or 1
end

function GetChargesMax(name_or_id)
  return select(2, GetSpellCharges(ValidName(name_or_id))) or 1
end

function GetChargeCD(name_or_id)
  local charges, charges_max, start, duration = GetSpellCharges(ValidName(name_or_id))
  if not charges then
    return GetCD(name_or_id)
  end
  return
    charges < charges_max and
    start ~= nil and
    max(0, start + duration - GetTime()) or 0
end

function GetChargeCDMax(name_or_id)
  return select(4, GetSpellCharges(ValidName(name_or_id))) or GetCDMax(name_or_id)
end

function GetCastTime(name_or_id)
  local _, _, _, cast_time = GetSpellInfo(ValidName(name_or_id))
  return cast_time / 1000 or 0
end

--- @param unit string
--- @param spell string
--- @param filter string
local function UnitAuraByNameOrId(unit, spell, filter)
  local i = 1
  while true do
    local name, _, count, type, duration, expirationTime, _, _, _, spell_id, _, _, _, _, _, absorb = UnitAura(unit, i, filter)
    if name == spell or spell_id == spell then
      return name, count, type, duration, expirationTime, i, absorb
    elseif not name then
      return nil
    end
    i = i + 1
  end
end
function skill.UnitAuraDuration(unit, spell, filter)
  filter = filter and "|" .. filter or ""
  local name, _, _, duration, expiration_time = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name == nil then
    name, _, _, duration, expiration_time = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
  end
  if duration then
    return expiration_time > 0 and max(0, expiration_time - GetTime()) or 1337
  else
    return 0
  end
end
function UnitAuraDuration(unit, spell, filter)
  return skill.UnitAuraDuration(unit, spell, filter)
end
function UnitAuraDurationMax(unit, spell, filter)
  filter = filter and "|" .. filter or ""
  local name, _, _, duration = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name == nil then
    name, _, _, duration = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
  end
  return duration or 0
end
function UnitAuraAbsorb(unit, spell, filter)
  filter = filter and "|" .. filter or ""
  local name, _, _, _, _, index, absorb = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name == nil then
    name, _, _, _, _, index, absorb = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
  end
  return absorb or 0
end

local dispelable_auras_type_count_result = {
  --[guid] = {
  --  ["timestamp"] = 0,
  --  [type] = 0,
  --},
}
function DispelableAuraTypeCount(unit, type)
  local time = GetTime()
  local guid = UnitGUID(unit)
  if not dispelable_auras_type_count_result[guid] or dispelable_auras_type_count_result[guid]["timestamp"] < time then
    dispelable_auras_type_count_result[guid] = {}
    dispelable_auras_type_count_result[guid]["timestamp"] = time
    local i = 1
    while true do
      local name, _, count, t = UnitAura(unit, i, "HARMFUL")
      if name then
        if t then
          local current = dispelable_auras_type_count_result[guid][t] or 0
          dispelable_auras_type_count_result[guid][t] = current + max(count, 1)
        end
      else
        break
      end
      i = i + 1
    end
  end
  dispelable_auras_type_count_result[guid][type] = dispelable_auras_type_count_result[guid][type] or 0
  return dispelable_auras_type_count_result[guid][type]
end

local function match_any_tooltip_line(tt, pattern)
  for i = 1, select("#", tt:GetRegions()) do
    local region = select(i, tt:GetRegions())
    if region and region:GetObjectType() == "FontString" and region:GetText() then
      local match = string.match(region:GetText(), pattern)
      if match then
        return match
      end
    end
  end
end
function UnitAuraMatch(unit, spell, pattern, filter)
  filter = filter and "|" .. filter or ""
  local name, _, _, _, _, index = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name then
    tooltip:ClearLines()
    tooltip:SetUnitAura(unit, index, "HELPFUL"..filter)
    return match_any_tooltip_line(tooltip, pattern)
  else
    name, _, _, _, _, index = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
    if name then
      tooltip:ClearLines()
      tooltip:SetUnitAura(unit, index, "HARMFUL"..filter)
      return match_any_tooltip_line(tooltip, pattern)
    end
  end
  return nil
end
function UnitAuraAbsorb(unit, spell, filter)
  local absorb = UnitAuraMatch(unit, spell, "Absorbs (%d+) damage.", filter)
  if not absorb then
    absorb = UnitAuraMatch(unit, spell, "preventing (%d+) total damage.", filter)
  end
  return tonumber(absorb) or 0
end
function UnitAuraDispelType(unit, spell, filter)
  filter = filter and "|" .. filter or ""
  local name, _, type = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name == nil then
    name, _, type = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
  end
  if name then
    return type
  end
end
local unit_auras_result = {}
function UnitAuras(unit, filter)
  wipe(unit_auras_result)
  filter = filter and "|" .. filter or ""
  local name, icon, count, type, duration, expirationTime = "start"
  local i = 1
  while name do
    name, icon, count, type, duration, expirationTime = UnitAura(unit, i, filter)
    table.insert(unit_auras_result, name)
    i = i + 1
  end
  return unit_auras_result
end
local unit_typed_auras_result = {}
function UnitTypedAuras(unit, auraType, filter)
  wipe(unit_typed_auras_result)
  filter = filter and "|" .. filter or ""
  local name, icon, count, type, duration, expirationTime = "start"
  local i = 1
  while name do
    name, icon, count, type, duration, expirationTime = UnitAura(unit, i, filter)
    if type == auraType then
      table.insert(unit_typed_auras_result, name)
    end
    i = i + 1
  end
  return unit_typed_auras_result
end
function UnitMaybeGetMagicBuff(unit)
  return UnitTypedAuras(unit, "Magic", "HELPFUL")[1]
end

function PlayerAuraDuration(spell)
  return UnitAuraDuration("player", spell)
end

function TargetAuraDuration(spell)
  return UnitAuraDuration("target", spell)
end

function PlayerMyAuraDuration(spell)
  return UnitAuraDuration("player", spell, "PLAYER")
end

function TargetMyAuraDuration(spell)
  return UnitAuraDuration("target", spell, "PLAYER")
end

function UnitMyAuraDuration(unit, spell)
  return UnitAuraDuration(unit, spell, "PLAYER")
end

function UnitAuraStacks(unit, spell, filter)
  filter = filter and "|" .. filter or ""
  local name, count, _, duration = UnitAuraByNameOrId(unit, spell, "HELPFUL"..filter)
  if name == nil then
    name, count, _, duration = UnitAuraByNameOrId(unit, spell, "HARMFUL"..filter)
  end
  if duration then
    return max(count, 1)
  else
    return 0
  end
end

function PlayerAuraStacks(spell)
  return UnitAuraStacks("player", spell)
end

function TargetAuraStacks(spell)
  return UnitAuraStacks("target", spell)
end

function UnitMyAuraStacks(unit, spell)
  return UnitAuraStacks("player", spell, "PLAYER")
end

function PlayerMyAuraStacks(spell)
  return UnitAuraStacks("player", spell, "PLAYER")
end

function TargetMyAuraStacks(spell)
  return UnitAuraStacks("target", spell, "PLAYER")
end

function UnitHealthDeficit(unit)
  if UnitAuraDuration(unit, "Gluttonous Miasma") > 0 then
    return 0
  end
  return UnitHealthMax(unit) - UnitHealth(unit)
end

function UnitHealthAndHealAbsorbDeficit(unit)
  if UnitAuraDuration(unit, "Gluttonous Miasma") > 0 then
    return 0
  end
  return UnitHealthDeficit(unit) + UnitGetTotalHealAbsorbs(unit)
end
function UnitHealthAndHealAbsorbDeficitPercent(unit)
  return UnitHealthAndHealAbsorbDeficit(unit) / UnitHealthMax(unit)
end

local CR_VERSATILITY = 29
function VersatilityMultiplier()
  return 1 + GetCombatRatingBonus(CR_VERSATILITY) / 100
end

function skill.UnitCast(unit)
  local ttf = nil
  local interruptible = nil
  local is_channel = false
  local name, a, b, startTime, endTime, d, e, notInterruptible, f = UnitCastingInfo(unit)
  if not name then
    name, _, _, startTime, endTime, _, notInterruptible = UnitChannelInfo(unit)
    is_channel = true
  end
  if name then
    local startTime = startTime / 1000
    local endTime = endTime / 1000
    interruptible = not notInterruptible
    ttf = is_channel and 0 or endTime - GetTime()
  end
  return name, ttf, interruptible, is_channel
end
function TargetCast()
  return skill.UnitCast("target")
end


function skill.UnitIsBoss(unit)
  if not UnitExists(unit) then
    return false
  end
  return
    ((UnitLevel(unit) == -1 or select(4, GetInstanceInfo()) == "Torghast") and UnitClassification(unit) == "elite") or
    UnitClassification(unit) == "rareelite" or
    UnitGUID("boss1") == UnitGUID(unit) or
    UnitGUID("boss2") == UnitGUID(unit) or
    UnitGUID("boss3") == UnitGUID(unit) or
    UnitGUID("boss4") == UnitGUID(unit) or
    UnitLevel(unit) >= UnitLevel("player") + 3 or
    UnitClassification(unit) == "worldboss"
end
function IsUnitBoss(unit)
  return skill.UnitIsBoss(unit)
end

function skill.UnitIsStunImmune(unit)
  return skill.UnitIsBoss(unit)
end

function ShouldSaveInterrupt(saved_interrupts, interrupt, casted_spell)
  local interrupt_saved_for_spell
  for spell, interrupt_to_save in pairs(saved_interrupts) do
    if interrupt == interrupt_to_save then
      interrupt_saved_for_spell = true
      if spell == casted_spell then
        return false
      end
    end
  end
  return interrupt_saved_for_spell
end

function ShouldSaveInterruptForTarget(interrupt, casted_spell)
  local save_interrupt_for = GetModule()["save_interrupt_for"]
  if save_interrupt_for then
    for unit, saved_interrupts in pairs(save_interrupt_for) do
      if unit == UnitName("target") then
        return ShouldSaveInterrupt(saved_interrupts, interrupt, casted_spell)
      end
    end
  end
  return false
end

local function ReturnFalse(...)
  return false
end

local function IsInterruptEffective(spell, interruptible)
  local m = GetModule()
  local IsStun = m["is_stun"] or ReturnFalse
  local IsDisorient = m["is_disorient"] or ReturnFalse
  local IsIncapacitate = m["is_incapacitate"] or ReturnFalse
  local IsInterrupt = m["is_interrupt"] or ReturnFalse
  local IsSilence = m["is_silence"] or ReturnFalse
  if IsStun(spell) or IsDisorient(spell) or IsIncapacitate(spell) then
    return not skill.UnitIsStunImmune("target")
  elseif IsInterrupt(spell) or IsSilence(spell) then
    return interruptible
  end
  return false
end

local function ShouldInterruptChannelingOnly(spell)
  return
    spell == "Knife Dance" or
    spell == "Brutal Attack"
end

local function CanInterruptWith(spell, threshold)
  local casted_spell, ttf, interruptible, is_channel = TargetCast()
  local time_to_interrupt = max(0, ttf - threshold)
  --ALERT("spell: "..tostring(spell))
  --ALERT("IsInterruptEffective(spell, interruptible): "..tostring(IsInterruptEffective(spell, interruptible)))
  return
    IsTargetInRange(spell) and
    GetCD(spell) <= time_to_interrupt and
    IsInterruptEffective(spell, interruptible) and
    not (ShouldInterruptChannelingOnly(casted_spell) and not is_channel) and
    not ShouldSaveInterruptForTarget(spell, casted_spell)
end

local next_print = 0
function BestInterrupt(threshold)
  if TargetCast() then
    local interrupts = GetModule()["interrupts"]
    if interrupts then
      for i, spell in ipairs(interrupts) do
        local name = ValidName(spell)
        if name and CanInterruptWith(spell, threshold) then
          return spell
        end
      end
    else
      ALERT_ONCE("Using BestInterrupt() without defining 'interrupts' in the module table.")
    end
  end
end

local function CheckInterruptInternal(frame_time)
  local _, _, lag_home, lag_world = GetNetStats()
  local threshold = max(0.4, 2 * frame_time + 4 * lag_world / 1000)
  local _, ttf = TargetCast()
  local spell = BestInterrupt(threshold)
  if ttf and next_print < GetTime() then
    next_print = GetTime() + 1
  end
  if spell then
    if ttf - threshold <= 0 then
      return spell
    end
    local IsOffGlobal = GetModule()["is_off_global"] or ReturnFalse
    if not IsOffGlobal(spell) then
      if ttf - threshold > GlobalCD() + GlobalCDMax() then
        return nil
      else
        return "nothing"
      end
    end
  end
end

local last_interrupt
local clear_last_interrupt = false
local next_check_interrupt_internal = 0
local function CheckInterrupt(frame_time)
  local interrupt = CheckInterruptInternal(frame_time)
  if next_check_interrupt_internal < GetTime() then
    if clear_last_interrupt then
      last_interrupt = nil
      clear_last_interrupt = false
    end
    if last_interrupt and interrupt and last_interrupt ~= interrupt then
      local IsOffGlobal = GetModule()["is_off_global"] or ReturnFalse
      if IsOffGlobal(interrupt) ~= IsOffGlobal(last_interrupt) then
        next_check_interrupt_internal = GetTime() + 0.4
        return last_interrupt
      end
    end
    last_interrupt = interrupt
  else
    clear_last_interrupt = true
  end
  return last_interrupt
end

local last_try_interrupt = {}
local try_interrupt_result
local try_interrupt_last_start_time = 0
function TryInterruptUnit(unit)
  if not skill.should_interrupt then
    return nil
  end
  local time = GetTime()
  if not last_try_interrupt[unit] or last_try_interrupt[unit] < time then
    last_try_interrupt[unit] = time
    local last_frametime = GetTime() - try_interrupt_last_start_time
    try_interrupt_last_start_time = GetTime()
    try_interrupt_result = CheckInterrupt(last_frametime)
  end
  return
    (try_interrupt_result == "nothing" and "nothing") or
    (try_interrupt_result and TryCastSpellOnUnit(try_interrupt_result, unit)) or
    nil
end
function skill.TryInterruptTarget()
  return TryInterruptUnit("target")
end

function skill.DoNothingIfTargetOutOfCombat()
  if skill.ShouldAlwaysAttackTarget() then
    return nil
  end
  return UnitExists("target") and not skill.IsUnitInCombatWithPlayer("target") and "nothing" or nil
end

function skill.IsTargetInCombat()
  return UnitExists("target") and (UnitAffectingCombat("target") or skill.ShouldAlwaysAttackTarget())
end

function skill.DoNothingIfDrinking()
  return (PlayerAuraDuration("Food & Drink") > 0 or PlayerAuraDuration("Refreshment") > 0) and "nothing" or nil
end

function skill.PlayerCastingTimeLeft()
  -- name, text, texture, startTimeMillis, endTimeMillis, isTradeSkill, notInterruptible, spellId = UnitCastingInfo("player")
  local name, _, _, _, end_time_millis = UnitCastingInfo("player")
  if not name then
    name, _, _, _, end_time_millis = UnitChannelInfo("player")
  end
  if name then
    end_time_millis = end_time_millis / 1000
    return max(0, end_time_millis - GetTime())
  end
  return 0
end

function skill.DoNothingOutOfCombat()
  return
    not UnitAffectingCombat("player")
    and "nothing" or
    nil
end

function skill.IsMounted()
  return IsMounted() or PlayerAuraDuration("Stolen Shadehound") > 0 or PlayerAuraDuration("Mount Sorrow Courser") > 0
end
function skill.DoNothingIfMounted()
  return
    skill.IsMounted() and
    "nothing" or
    nil
end

function skill.DoNothingIfTargetSoftCCd()
  if TargetAuraDuration("Repentance") > 0 or
      TargetAuraDuration("Shackle") > 0 or
      TargetAuraDuration("Blind") > 0 or
      TargetAuraDuration("Sap") > 0 or
      TargetAuraDuration("Hex") > 0 or
      TargetAuraDuration("Imprison") > 0 or
      TargetAuraDuration("Freezing Trap") > 0 or
      TargetAuraDuration("Polymorph") > 0 then
    return "nothing"
  end
end

function KeepOnCd(name_or_id)
  local current, max, start, duration = GetSpellCharges(ValidName(name_or_id))
  local remaining = (start + duration - GetTime()) * (max - current)
  return
    current >= max - 1 and
    remaining <= 0.2 and
    TryCastSpellOnTarget(name_or_id) or
    nil
end

function skill.IsCasting(cast_offset)
  return skill.PlayerSpellCastName(cast_offset) ~= nil
end
local CAST_OFFSET = 0.2
local UNKNOWN_LATENCY = 0.1
function skill.PlayerSpellCastName(cast_offset)
  cast_offset = cast_offset or CAST_OFFSET
  local latency = select(4, GetNetStats()) / 1000
  if latency == 0 then
    latency = UNKNOWN_LATENCY
  end
  local spell, _, _, _, end_millis = UnitCastingInfo("player")
  if spell then
    return (GetTime() < end_millis / 1000 - latency - cast_offset) and spell or nil
  end
  return nil
end
function skill.IsChanneling()
  return skill.PlayerSpellChannelName() ~= nil
end
function skill.PlayerSpellChannelName()
  local latency = select(4, GetNetStats()) / 1000
  if latency == 0 then
    latency = UNKNOWN_LATENCY
  end
  local spell, _, _, _, end_millis = UnitChannelInfo("player")
  if spell then
    return (GetTime() < end_millis / 1000 - latency / 2) and spell or nil
  end
  return nil
end
function skill.DoNothingIfCasting(cast_offset)
  return skill.IsCasting(cast_offset) and "nothing" or nil
end
function skill.DoNothingIfChanneling()
  return skill.IsChanneling() and "nothing" or nil
end
function skill.DoNothingIfCastingOrGlobalCd()
  return (skill.IsCasting() or GlobalCD() > 0) and "nothing" or nil
end
function skill.UnitSpellCastRemaining(unit, spell)
  local name, _, _, _, end_millis = UnitCastingInfo(unit)
  if name and name == spell and end_millis then
    return (end_millis / 1000) - GetTime()
  end
  return 0
end
function skill.AvoidInterrupt()
  local tti = skill.time_till_interrupt()
  if not tti then
    return nil
  end
  if tti <= 0 then
    return nil
  end
  if skill.IsChanneling() and tti < 0.5 then
    return "stop_casting"
  end
  local spell = skill.PlayerSpellCastName()
  if spell and tti < skill.cast_time(spell) + 0.1 then
    return "stop_casting"
  end
  return nil
end

function skill.time_till_interrupt()
  local quake = PlayerAuraDuration("Quake")
  if quake > 0 then
    return quake
  end
  local name, ttf = TargetCast()
  if name == "Deafening Crash" or name == "Sonic Scream" then
    return ttf
  end
  -- Discipline Priest Mage Tower
  name, ttf = skill.UnitCast("focus")
  if name == "Sonic Scream" then
    return ttf
  end
  ---- Discipline Priest Mage Tower
  --if UnitExists("focus") and skill.IsUnitAlive("focus") and UnitName("focus") == "Jormog the Behemoth" then
  --  local ttl = skill.UnitSpellCastRemaining("focus", "Sonic Scream")
  --  if ttl > 0 then
  --    ALERT("skill.UnitSpellCastRemaining(\"focus\", \"Sonic Scream\"): "..tostring(skill.UnitSpellCastRemaining("focus", "Sonic Scream")))
  --    return ttl
  --  end
  --  return 0
  --end
  --if UnitExists("target") and skill.IsUnitAlive("target") and UnitName("target") == "Jormog the Behemoth" then
  --  return skill.UnitSpellCastRemaining("target", "Sonic Scream") or 0
  --end
  return 0
end
function skill.cast_time(spell)
  local millis = select(4, GetSpellInfo(spell))
  return millis and millis / 1000 or 0
end

function skill.AcquireTarget()
  if UnitExists("target") then
    return nil
  end
  --if not UnitAffectingCombat("player") then
  --  return
  --end
  --local target = UnitGUID("player")
  if UnitExists("target") and (UnitAffectingCombat("target") or skill.ShouldAlwaysAttackTarget()) then
    return nil
  end
  return "target_nearest_enemy"
end

function skill.IsUnitAlive(unit)
  return not UnitIsDeadOrGhost(unit)
end
function skill.IsUnitPlayerFriend(unit)
  return UnitIsFriend(unit, "player")
end

function DoNothingIfCursorPickup()
  local type = GetCursorInfo()
  return type and "nothing" or nil
end

function UnitHealthPercent(unit)
  return UnitHealth(unit) / UnitHealthMax(unit)
end

function PlayerHealthPercent()
  return UnitHealth("player") / UnitHealthMax("player")
end

function TargetHealthPercent()
  return UnitHealth("target") / UnitHealthMax("target")
end

function TargetHealth()
  return UnitHealth("target")
end

function PlayerHealth()
  return UnitHealth("player")
end

function PlayerHealthMax()
  return UnitHealthMax("player")
end

function PlayerMana()
  return UnitPower("player", SPELL_POWER_MANA)
end

function PlayerManaMax()
  return UnitPowerMax("player", SPELL_POWER_MANA)
end

function PlayerManaPercent()
  return PlayerMana() / PlayerManaMax()
end

function AlwaysTrue(unused)
  return true
end

local function IsFlaskActive()
  return
    --PlayerAuraDuration("Flask of the Undertow") > 0 or
    --PlayerAuraDuration("Flask of the Currents") > 0 or
    --PlayerAuraDuration("Flask of the Vast Horizon") > 0 or
    --PlayerAuraDuration("Flask of Endless Fathoms") > 0 or
    PlayerAuraDuration("Spectral Flask of Power") > 0 or
    PlayerAuraDuration("Eternal Flask") > 0 or
    PlayerAuraDuration("Flask of Ten Thousand Scars") > 0 or
    PlayerAuraDuration("Flask of the Whispered Pact") > 0
end

function skill.KeepRepurposedFelFocuserUp()
  return
    not IsFlaskActive() and
    GetItemCount("Repurposed Fel Focuser") >= 1 and
    PlayerAuraDuration("Fel Focus") <= 15 * 60 and
    GetCD("Repurposed Fel Focuser") <= 0 and
    TryCastSpellOnPlayer("Repurposed Fel Focuser") or
    nil
end

function skill.TryPreventFallDamage()
  local spell = GetModule()["anti_fall_damage_spell"]
  return
    spell and
    PlayerAuraDuration("Fel Focus") <= 15 * 60 and
    GetCD("Repurposed Fel Focuser") <= 0 and
    not IsFlaskActive() and
    TryCastSpellOnPlayer("Repurposed Fel Focuser") or
    nil
end

function UnitAbsorb(unit)
  return UnitGetTotalAbsorbs(unit)
end

function UnitHealAbsorb(unit)
  return UnitGetTotalHealAbsorbs(unit)
end

local PHYSICAL = 1
local HOLY = 2
local FIRE = 3
local NATURE = 4
local FROST = 5
local SHADOW = 6
local ARCANE = 7
function GetSpellBonusDamagePhysical()
  return GetSpellBonusDamage(PHYSICAL)
end
function GetSpellBonusDamageHoly()
  return GetSpellBonusDamage(HOLY)
end
function GetSpellBonusDamageFire()
  return GetSpellBonusDamage(FIRE)
end
function GetSpellBonusDamageNature()
  return GetSpellBonusDamage(NATURE)
end
function GetSpellBonusDamageFrost()
  return GetSpellBonusDamage(FROST)
end
function GetSpellBonusDamageShadow()
  return GetSpellBonusDamage(SHADOW)
end
function GetSpellBonusDamageArcane()
  return GetSpellBonusDamage(ARCANE)
end

function skill.IsUnitRole(unit, role)
  return
    UnitGroupRolesAssigned(unit) == role or
    --(role == "HEALER" and UnitName(unit) == "Sooli the Survivalist") or
    (role == "TANK" and UnitName(unit) == "Oto the Protector")
end
function skill.IsUnitTank(unit)
  return skill.IsUnitRole(unit, "TANK")
end
function skill.IsUnitHealer(unit)
  return skill.IsUnitRole(unit, "HEALER")
end
function skill.IsUnitDamager(unit)
  return skill.IsUnitRole(unit, "DAMAGER")
end
function skill.TryGetFirstRoleUnit(role)
  return skill.TryGetRoleUnit(role, 1)
end
function skill.TryGetRoleUnit(role, n)
  n = n or 1
  local num = GetNumGroupMembers()
  local type = num > 5 and "raid" or "party"
  local x = 1
  for i = 1, num do
    local unit = type..i
    if skill.IsUnitRole(unit, role) then
      if x >= n then
        return unit
      else
        x = x + 1
      end
    end
  end
  return nil
end

function skill.TryGetFirstHealerUnit()
  return skill.TryGetFirstRoleUnit("HEALER")
end

function skill.TryGetFirstTankUnit()
  return skill.TryGetFirstRoleUnit("TANK")
end

function skill.GetNumAliveGroupDamageDealers()
  local num = GetNumGroupMembers()
  local type = num > 5 and "raid" or "party"
  local sum = skill.IsUnitDamager("player") and 1 or 0
  for i = 1, num do
    local unit = type..i
    if skill.IsUnitDamager(unit) and skill.IsUnitAlive(unit) then
      sum = sum + 1
    end
  end
  return sum
end


local NEVER_DISPEL_SPELLS = {
  ["Whispers of Power"] = true,  -- Last boss of Shrine of the Storms
  ["Might of the Sun"] = true,
  ["Frozen Binds"] = true,  -- Last boss of Necrotic Wake
  ["Lingering Doubt"] = true,
  ["Burst"] = true,
  --["Cosmic Artifice"] = true,
  ["Purification Protocol"] = true,
  ["Phantasmal Parasite"] = true,
}
local NEVER_PURGE_SPELLS = {
  ["Inner Flames"] = true,
  ["Stygian Rune Tap"] = true,
}
function TryRemoveAuraFromUnit(unit, spell, if_stacks_at_least, removal_spells, never_removed_spells)
  if never_removed_spells[spell] then
    return nil
  end
  if_stacks_at_least = if_stacks_at_least or 1
  if removal_spells then
    local type_to_remove = UnitAuraDispelType(unit, spell)
    for removal_spell, removal_types in pairs(removal_spells) do
      for _, removal_type in ipairs(removal_types) do
        local r =
          removal_type == type_to_remove and
          UnitExists(unit) and
          UnitAuraDuration(unit, spell) > GlobalCD() and
          (UnitAuraStacks(unit, spell) or 1) >= if_stacks_at_least and
          TryCastSpellOnUnit(removal_spell, unit) or
          nil
        if r then
          return r
        end
      end
    end
  end
  return nil
end

local highest_stacked_group_aura_timestamp = 0
local highest_stacked_group_aura_count = 0
local highest_stacked_group_aura_spell
local highest_stacked_group_aura_unit
function TryDispelHighestStackedAuraFromGroup()
  local time = GetTime()
  if highest_stacked_group_aura_timestamp < time then
    highest_stacked_group_aura_timestamp = time
    highest_stacked_group_aura_spell = nil

    local module = GetModule()
    local removal_spells = module and module.spell and module.spell.dispel
    if not removal_spells then
      return nil
    end
    for removal_spell, removal_types in pairs(removal_spells) do
      for _, unit in ipairs(GroupCombatCastUnits()) do
        local i = 1
        while true do
          local name, _, count, type, _, expiration_time = UnitAura(unit, i, "HARMFUL")
          count = count or 1
          if count > 1 and name and expiration_time > time then
            for _, removal_type in ipairs(removal_types) do
              if type == removal_type then
                if count > highest_stacked_group_aura_count then
                  highest_stacked_group_aura_count = count
                  highest_stacked_group_aura_spell = removal_spell
                  highest_stacked_group_aura_unit = unit
                end
              end
            end
          else
            break
          end
          i = i + 1
        end
      end
    end
  end
  return
    TryCastSpellOnUnit(highest_stacked_group_aura_spell, highest_stacked_group_aura_unit) or
    nil
end

function TryDispelUnit(unit, spell, if_stacks_at_least)
  local module = GetModule()
  return
    module and
    module.spell and
    module.spell.dispel and
    TryRemoveAuraFromUnit(unit, spell, if_stacks_at_least, module.spell.dispel, NEVER_DISPEL_SPELLS)
end
function TryPurgeUnit(unit, spell, if_stacks_at_least)
  local module = GetModule()
  return
    module and
    module.spell and
    module.spell.purge and
    TryRemoveAuraFromUnit(unit, spell, if_stacks_at_least, module.spell.purge, NEVER_PURGE_SPELLS)
end

function TryRemoveAnythingFromUnit(unit, removal_func, filter)
  for _, aura in ipairs(UnitAuras(unit, filter)) do
    local r = removal_func(unit, aura)
    if r then
      return r
    end
  end
  return nil
end
function TryDispelAnythingFromUnit(unit)
  return TryRemoveAnythingFromUnit(unit, TryDispelUnit, "HARMFUL")
end
function TryPurgeAnythingFromUnit(unit)
  return TryRemoveAnythingFromUnit(unit, TryPurgeUnit, "HELPFUL")
end

function skill.TryPurgeAnythingFromTarget()
  return TryPurgeAnythingFromUnit("target")
end

function TryDispelAnythingFromParty()
  return ForUniqueFilteredCombatCastUnits(TryDispelAnythingFromUnit, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend)
end

function TryDispelFromParty(spell, if_stacks_at_least)
  return
    TryDispelUnit("player", spell, if_stacks_at_least) or
    TryDispelUnit("party1", spell, if_stacks_at_least) or
    TryDispelUnit("party2", spell, if_stacks_at_least) or
    TryDispelUnit("party3", spell, if_stacks_at_least) or
    TryDispelUnit("party4", spell, if_stacks_at_least) or
    nil
end


function skill.DoTillersStuff()
  return skill.DoYoloTillersStuff()
end

function skill.TargetIsDummy()
  return skill.ShouldAlwaysAttackTarget()
end
local ALWAYS_ATTACK_ENEMIES = {
  ["Glacial Spike"] = true,
  ["Spiked Ball"] = true,
  ["Domination Arrow"] = true,
  ["Decrepit Orb"] = true,
  ["Mawsworn Vanguard"] = true,
  ["Mawsworn Hopebreaker"] = true,
  ["Mawsworn Souljudge"] = true,
  ["Mawsworn Summonere"] = true,
  ["Mawsworn Goliath"] = true,
  ["Chain of Domination"] = true,
  ["Tadpole Collector"] = true,
  ["Frozen Core"] = true,
  ["AzerMEK Mk. II"] = true,
  ["War Machine"] = true,
  ["Spirit of Gold"] = true,
  ["Dungeoneer's Training Dummy"] = true,
  ["Raider's Training Dummy"] = true,
  ["Training Dummy"] = true,
  ["Imprisoned Forgefiend"] = true,
  ["Swarm Caller"] = true,
  ["Training Bag"] = true,
  ["Goliath Egg"] = true,
  ["Barrier"] = true,
  ["Stone Prison"] = true,
  ["Helya's Grasp"] = true,
  ["Reinforced Guardian"] = true,
  ["Essence Orb"] = true,
  ["Shattered Visage"] = true,
  ["Valiant's Humility"] = true,
  ["Humility's Obedience"] = true,
  ["Valiant's Resolve"] = true,
  ["Pride's Resolve"] = true,
  ["Necrolord's Resolve"] = true,
  ["Nature's Resolve"] = true,
  ["Cleave Training Dummy"] = true,
  ["Swarm Training Dummy"] = true,
  ["Twilight Volunteer"] = true,
}
function skill.ShouldAlwaysAttackUnit(unit)
  local unit_name = UnitName(unit)
  local result =
    (UnitExists(unit) and skill.IsUnitAlive(unit) and ALWAYS_ATTACK_ENEMIES[unit_name]) or
    string.find(unit, "Player") == nil
  if result then
    return true
  end
  for _, u in ipairs(GroupCombatCastUnits()) do
    if unit_name == UnitName(u) then
      return true
    end
  end
  return false
end
function skill.ShouldAlwaysAttackTarget()
  return skill.ShouldAlwaysAttackUnit("target")
end
function skill.IsUnitInCombatWithPlayer(unit)
  return UnitThreatSituation("player", unit) ~= nil or skill.ShouldAlwaysAttackUnit(unit)
end

function skill.StartAttack()
  if UnitExists("target") and skill.IsUnitInCombatWithPlayer("target") and not IsCurrentSpell(6603) then
    return "start_attack"
  end
  return nil
end

function skill.TryUseConcentratedFlame()
  local name = GetSpellInfo("Concentrated Flame")
  return
    name and
    IsUsableSpell(name) and
    not skill.IsUnitPlayerFriend("target") and
    TargetMyAuraDuration("Concentrated Flame") <= GlobalCDMax() + GlobalCD() and
    TryCastSpellOnTarget("Concentrated Flame") or
    nil
end

function skill.TrySelfHealWithConcentratedFlame()
  local name = GetSpellInfo("Concentrated Flame")
  return
    name and
    IsUsableSpell(name) and
    TryCastSpellOnPlayer("Concentrated Flame") or
    nil
end

function skill.TryUseHealthstone(ifBelowHealthPercent)
  ifBelowHealthPercent = ifBelowHealthPercent or 0.5
  return
    PlayerHealthPercent() < ifBelowHealthPercent and
    GetItemCount("Healthstone", nil, true) > 0 and
    GetItemCD(5512) <= 0 and
    TryCastSpellOnPlayer("Healthstone") or
    nil
end

--function skill.TryUseItemOnUnit(item, unit)
--  for bag = 0, 4 do
--    for slot = 1, GetContainerNumSlots(bag) do
--      local item_link = GetContainerItemLink(bag, slot)
--      --local _, _, _, _, _, _, item_link, _, _, id = GetContainerItemInfo(bag, slot)
--      if item_link then
--        local name = GetItemInfo(item_link)
--        if name == item then
--          return TryUseItemOnUnit(bag, slot, unit)
--        end
--      end
--    end
--  end
--  return nil
--end

local function TryUseEquippedItem(name, unit, first, last)
  if not name or not first or not last or abs(first - last) > 20 then
    return nil
  end
  local inc = first < last and 1 or -1
  for i = first, last, inc do
    local id = GetInventoryItemID("player", i)
    if id then
      local item = GetItemInfo(id)
      if name == item then
        return
          GetItemCD(name) <= 0 and
          TryCastSpellOnUnit(i, unit)
      end
    end
  end
  return nil
end
function skill.TryUseEquippedTrinketOnUnit(name, unit)
  return TryUseEquippedItem(name, unit, 13, 14)
end
function skill.TryUseEquippedItemOnUnit(name, unit)
  ALERT_ONCE("NYI TryUseEquippedItemOnUnit")
  return TryUseEquippedItem(name, unit, 1, 19)
end
function skill.TryUseAnyEquippedTrinket()
  return TryUseEquippedItem(name, unit, 13, 14)
end

function TryGetTank(n)
  local units = UniqueExistingCombatCastUnits()
  for i = (n or 1), #units do
    if UnitExists(units[i]) and UnitIsFriend("player", units[i]) and (UnitGroupRolesAssigned(units[i]) == "TANK" or UnitName(units[i]) == "Oto the Protector") then
      return units[i]
    end
  end
end

function skill.TryLoot()
  return
    not UnitAffectingCombat("player") and
    not skill.IsCasting(0) and
    UnitExists("mouseover") and
    CanLootUnit(UnitGUID("mouseover")) and
    (range_check:GetRange("mouseover") <= 10 or false) and
    "INTERACTMOUSEOVER" or
    nil
end
torghast = {}
local GATHER_TEXTS = {
  "Marrowroot",
  "Death Blossom",
  "Nightshade",
  "Nagrand Arrowbloom",
  "Gorgrond Flytrap",
  "Talador Orchid",
  "Starflower",
  "Frostweed",
  "Fireweed",
}
local function is_gathering_node_text(text)
  for _, t in ipairs(GATHER_TEXTS) do
    if text == t then
      return true
    end
  end
  return false
end
local gathering_frame = CreateFrame("Frame", "gathering_frame", nil)
gathering_frame.ready_to_gather = function(self, event, unit, spell_guid, spell_id)
  --ALERT("event: "..tostring(event))
  --ALERT("GetSpellInfo(spell_id): "..tostring(GetSpellInfo(spell_id)))
  local spell = GetSpellInfo(spell_id)
  if spell == "Herb Gathering" or spell == "Herbalism" then
    --ALERT("ready_to_gather")
    gathering_frame.gathering_started = false
    gathering_frame.gathering_stopped_at = GetTime()
    gathering_frame:RegisterEvent("UNIT_SPELLCAST_START")
    gathering_frame:UnregisterEvent("UNIT_SPELLCAST_STOP")
    gathering_frame:UnregisterEvent("UNIT_SPELLCAST_SUCCEEDED")
    gathering_frame:SetScript("OnEvent", gathering_frame.gathering)
  end
end
gathering_frame.gathering = function(self, event, unit, spell_guid, spell_id)
  --ALERT("event: "..tostring(event))
  --ALERT("GetSpellInfo(spell_id): "..tostring(GetSpellInfo(spell_id)))
  local spell = GetSpellInfo(spell_id)
  if spell == "Herb Gathering" or spell == "Herbalism" then
    --ALERT("gathering")
    gathering_frame.gathering_started = true
    gathering_frame:UnregisterEvent("UNIT_SPELLCAST_START")
    gathering_frame:RegisterEvent("UNIT_SPELLCAST_STOP")
    gathering_frame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
    gathering_frame:SetScript("OnEvent", gathering_frame.ready_to_gather)
  end
end
gathering_frame.ready_to_gather(nil, nil, nil, nil, 195114)
gathering_frame.gathering_started = false
gathering_frame.gathering_stopped_at = 0
function skill.TryGather()
  return
    not UnitAffectingCombat("player") and
    not skill.IsCasting() and
    is_gathering_node_text(_G["GameTooltipTextLeft1"]:GetText()) and
    not gathering_frame.gathering_started and
    GetTime() > gathering_frame.gathering_stopped_at + 0.5 and
    "INTERACTMOUSEOVER" or
    nil
end

function skill.common_priority_start()
  return
    (not UnitAffectingCombat("player") and not IsPlayerMoving() and skill.TryLoot() or nil) or
    (GetZoneText() == "Lunarfall" and not IsPlayerMoving() and skill.TryGather() or nil) or
    --skill.AvoidInterrupt() or
    TryCastQueuedSpell() or
    skill.DoNothingIfMounted() or
    skill.DoNothingIfCasting() or
    skill.DoNothingIfChanneling() or
    skill.DoNothingIfDrinking() or
    skill.KeepRepurposedFelFocuserUp() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfTargetOutOfCombat() or
    skill.TryInterruptTarget() or
    skill.TryUseHealthstone(0.33) or
    nil
end

function skill.IsUsable(spell)
  local usable, no_mana = IsUsableSpell(spell)
  return (usable and not no_mana) or GetItemCount(spell) > 0
end
local is_usable_timestamp = {}
local is_usable_result = {}
function skill.IsUsable(spell)
  if not spell then
    return nil
  end
  local time = GetTime()
  if is_usable_result[spell] == nil or is_usable_timestamp[spell] < time then
    local usable, no_mana = IsUsableSpell(spell)
    is_usable_result[spell] = (usable and not no_mana) or GetItemCount(spell) > 0
    is_usable_timestamp[spell] = time
  end
  return is_usable_result[spell]
end

function skill.UnitHealReceivedMultiplier(unit)
  return 1
end
function skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent)
  local current_cast = skill.PlayerSpellCastName(0)
  if current_cast then
    return "nothing"
  end
  -- TODO: Reconsider validity. Added this to avoid double casts of the same spell.
  if spell == current_cast then
    return nil
  end
  -- TODO: Come up with a solution for timing issues.
  if unit == "target" or unit == "mouseover" then
    return nil
  end
  -- Necrolord Heirmir ability
  if UnitAuraDuration(unit, "Soul Exhaustion") > 60 * 2 - 10 then
    return nil
  end
  -- Necrotic affix
  if UnitAuraDuration(unit, "Necrotic Wound") > GlobalCD() and UnitAuraStacks(unit, "Necrotic Wound") >= 50 then
    return nil
  end
  --ALERT("spell: "..tostring(spell))
  --ALERT("ifBelowHealthPercent: "..tostring(ifBelowHealthPercent))
  --ALERT("maxOverhealPercent: "..tostring(maxOverhealPercent))
  local deficit
  if maxOverhealPercent then
    deficit = UnitHealthAndHealAbsorbDeficit(unit)
    local heal_received = HealOfSpell(spell) * skill.UnitHealReceivedMultiplier(unit)
    local overheal = max(0, heal_received - deficit)
    --ALERT("overheal: "..tostring(overheal))
    local overhealPercent = overheal / heal_received
    if overhealPercent > maxOverhealPercent then
      return nil
    end
  end
  if ifBelowHealthPercent then
    deficit = deficit or UnitHealthAndHealAbsorbDeficit(unit)
    local unitHealthPercent = (UnitHealthMax(unit) - deficit) / UnitHealthMax(unit)
    if unitHealthPercent >= ifBelowHealthPercent then
      return nil
    end
  end
  local cast_time = select(4, GetSpellInfo(spell))
  return
    -- TODO: Migrate movement/instant detection to combat_cast itself.
    (cast_time <= 0 or not IsPlayerMoving()) and
    skill.IsUnitAlive(unit) and
    TryCastSpellOnUnit(spell, unit) or
    nil
end

local daily_frame = CreateFrame("Frame", "daily_frame", nil)
daily_frame:RegisterEvent("CHAT_MSG_RAID_BOSS_WHISPER")
daily_frame:RegisterEvent("CHAT_MSG_MONSTER_SAY")
daily_frame:RegisterEvent("ACTIONBAR_UPDATE_COOLDOWN")
function skill.HandleJustWingingIt()
  if not GetZoneText() == "Maldraxxus" then
    return nil
  end
  if not UnitExists("pet") or not UnitName("pet") == "Taloned Flayedwing" then
    return nil
  end
  return
    daily_frame.just_winging_it_spell or
    nil
end
function skill.Aspirant()
  return
    (GetZoneText() == "Bastion" or GetZoneText() == "Korthia") and
    (GetMinimapZoneText() == "Vestibule of Eternity" or GetMinimapZoneText() == "Korthia") and
    HasOverrideActionBar() and
    daily_frame.aspirant_button and
    GetTime() > daily_frame.aspirant_wait_until and
    GetTime() < daily_frame.aspirant_wait_until + 0.5 and
    daily_frame.aspirant_button or
    nil
end
daily_frame.just_winging_it_spell = nil
function daily_frame.on_event(self, event, ...)
  if event == "CHAT_MSG_RAID_BOSS_WHISPER" then
    local message = ...
    if message == "The flayedwing is scared, soothe it with gentle pats!" then
      daily_frame.just_winging_it_spell = "ACTIONBUTTON1"
    elseif message == "The flayedwing is trying to shake you off, hold on tight!" then
      daily_frame.just_winging_it_spell = "ACTIONBUTTON2"
    elseif message == "The flayedwing is flying smoothly, praise them!" then
      daily_frame.just_winging_it_spell = "ACTIONBUTTON3"
    else
      daily_frame.just_winging_it_spell = nil
    end
  elseif event == "CHAT_MSG_MONSTER_SAY" then
    local message, unit_name = ...
    if unit_name == "Trainer Ikaros" then
      if message == "Jab." or message == "Slash." or message == "Strike." then
        daily_frame.aspirant_button = "ACTIONBUTTON1"
      elseif message == "Kick." or message == "Bash." or message == "Sweep." then
        daily_frame.aspirant_button = "ACTIONBUTTON2"
      elseif message == "Dodge." or message == "Block." or message == "Parry." then
        daily_frame.aspirant_button = "ACTIONBUTTON3"
      end
      daily_frame.aspirant_wait_until = GetTime() + 0.5
    elseif unit_name == "Nadjia the Mistblade" then
      if message == "Lunge!" then
        daily_frame.aspirant_button = "ACTIONBUTTON1"
      elseif message == "Parry!" then
        daily_frame.aspirant_button = "ACTIONBUTTON2"
      elseif message == "Riposte!" then
        daily_frame.aspirant_button = "ACTIONBUTTON3"
      end
      daily_frame.aspirant_wait_until = GetTime() + 0.0
    end
  elseif event == "ACTIONBAR_UPDATE_COOLDOWN" then
    daily_frame.aspirant_button = nil
  end
end
daily_frame:SetScript("OnEvent", daily_frame.on_event)
function skill.dailies()
  return
    skill.HandleJustWingingIt() or
    skill.Aspirant() or

    nil
end


kyrian = {}
function kyrian.TryRefreshPhials()
  return
  GetItemCount("Phial of Serenity") <= 0 and
  GetCD("Summon Steward") <= 0 and
  TryCastSpellOnPlayer("Summon Steward") or
  nil
end
local STEWARDS = {
  ["Amylynn"] = "Asellia",
  ["Yve"] = "Chaermi",
}
function kyrian.TryDismissSteward()
  local steward = GameTooltip:NumLines() == 3 and _G["GameTooltipTextLeft1"]:GetText() == STEWARDS[UnitName("player")]
  local text = "Hoo! Can help? Can do many tasks, or just keep company!"
  return
    GetItemCount("Phial of Serenity") > 0 and
    (C_GossipInfo.GetText() == text and C_GossipInfo.GetNumOptions() == 5 and C_GossipInfo.GetOptions()[5].name == "Thank you. See you later!" and C_GossipInfo.SelectOption(5) and "nothing" or nil) or
    steward and
    "INTERACTMOUSEOVER" or
    nil
end
function kyrian.TryUsePhialOfSerenity(belowHealthPercent)
  belowHealthPercent = belowHealthPercent or 0.5
  return
    PlayerHealthPercent() < belowHealthPercent and
    GetItemCount("Phial of Serenity") >= 1 and
    GetCD("Phial of Serenity") <= 0 and
    TryCastSpellOnPlayer("Phial of Serenity") or
    nil
end
function kyrian.HandleBoonOfTheAscended()
  if PlayerAuraDuration("Boon of the Ascended") <= GlobalCD() then
    return nil
  end
  return
    skill.AcquireTarget() or
    (skill.NumEnemiesWithin(8) >= 5 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
    TryCastSpellOnTarget("Ascended Blast") or
    (skill.NumEnemiesWithin(8) > 0 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
    nil
end

torghast = {}
local SOUL_TEXTS = {
  "Stake",
  "Soul Cage",
  "Bound Soul Remnant",
  "Heavily Bound Soul Remnant",
  "Boiling Soul Remnant",
  "Tormented Soul Remnant",
  "Partially-Infused Soul Remnant",
  "Spectral Key",
}
local function is_soul_text(text)
  for _, t in ipairs(SOUL_TEXTS) do
    if text == t then
      return true
    end
  end
  return false
end
function torghast.TryFreeSouls()
  if GameTooltip:NumLines() < 1 then
    return nil
  end
  return
    not IsPlayerMoving() and
    not UnitAffectingCombat("player") and
    (C_GossipInfo.GetNumOptions() == 2 and C_GossipInfo.GetOptions()[1].name == "Free the Soul Remnant." and C_GossipInfo.SelectOption(1) and "nothing" or nil) or
    is_soul_text(_G["GameTooltipTextLeft1"]:GetText()) and
    "INTERACTMOUSEOVER" or
    nil
end
function torghast.TryOpenAshenPhylactery()
  local phylactery = GameTooltip:NumLines() >= 1 and _G["GameTooltipTextLeft1"]:GetText() == "Ashen Phylactery"
  return
    phylactery and
    IsUnitInRange("Attack", "mouseover") and
    "INTERACTMOUSEOVER" or
    nil
end
function torghast.ChorusOfDeadSouls()
  if PlayerAuraDuration("Chorus of Dead Souls") <= 0 then
    return nil
  end
  return
    skill.NumEnemiesWithin(5) > 0 and
    "ACTIONBUTTON2" or
    nil
end
function skill.TryLoot()
  return
    not UnitAffectingCombat("player") and
    UnitExists("mouseover") and
    CanLootUnit(UnitGUID("mouseover")) and
    (range_check:GetRange("mouseover") <= 10 or false) and
    "INTERACTMOUSEOVER" or
    nil
end
function torghast.DontKillMawratsIfFracturingForces()
  return
    PlayerAuraDuration("Torment: Fracturing Forces") > 0 and
    (UnitName("target") == "Mawrat" or UnitName("target") == "Oddly Large Mawrat") and
    "nothing" or
    nil
end
function torghast.torghast_stuff()
  return
    torghast.TryFreeSouls() or
    torghast.TryOpenAshenPhylactery() or
    skill.TryLoot() or
    torghast.ChorusOfDeadSouls() or
    --torghast.DontKillMawratsIfFracturingForces() or
    nil
end
function torghast.IsTargetMawrat()
  return UnitName("target") == "Mawrat" or UnitName("target") == "Oddly Large Mawrat"
end

function skill.ShouldFocusSingleTarget()
  local name = UnitName("target")
  return
    name == "Spiked Ball" or
    name == "Painsmith Raznal" or
    name == "Explosives" or
    name == "Zolramus Necromancer"
end

function skill.ShouldDoAoeDamage()
  return false
end

function skill.IsAoeDamageAllowed()
  return true
end

local function CanNotCast(func, spell)
  local spell_book_id = SpellBookId(spell)
  if spell_book_id then
    return not func(spell_book_id, BOOKTYPE_SPELL)
  end
  ALERT_ONCE(string.format("No spell book id found for '%s'.", spell))
  return false
end

function skill.CanCastOnEnemies(spell)
  return CanNotCast(IsHelpfulSpell, spell)
end

function skill.CanCastOnFriends(spell)
  return CanNotCast(IsHarmfulSpell, spell)
end

--function kyrian.ascension()
--  return
--    TryCastSpellOnTarget("ACTIONBUTTON1") or
--    nil
--end

function skill.UnitTimeToLive(unit, window)
  if not UnitExists(unit) then
    return 60 * 60
  end
  local dps = GetTime() - UnitOldestHealthChangedTime(unit, window) > 5 and -UnitHealthChangedPerSecond(unit, window) or 0
  return dps > 0 and UnitHealth(unit) / dps or 0
end

function skill.TargetTimeToLive(window)
  return skill.UnitTimeToLive("target", window)
end

function skill.IsPlayerInCombat()
  return UnitAffectingCombat("player")
end

function skill.UnitDamageTakenMultiplier(unit)
  -- TODO
  return 1
end

local COVENANTS = {
  [1] = "Kyrian",
  [2] = "Venthyr",
  [3] = "NightFae",
  [4] = "Necrolord",
}
function skill.GetCovenant()
  return COVENANTS[C_Covenants.GetActiveCovenantID()]
end
function skill.HasChosenCovenant()
  return C_Covenants.GetActiveCovenantID() == 0
end
function skill.IsKyrian()
  return C_Covenants.GetActiveCovenantID() == 1 or true
end
function skill.IsVenthyr()
  return C_Covenants.GetActiveCovenantID() == 2
end
function skill.IsNightFae()
  return C_Covenants.GetActiveCovenantID() == 3
end
function skill.IsNecrolord()
  return
    C_Covenants.GetActiveCovenantID() == 4 or
    (skill.HasChosenCovenant() and GetZoneText() == "Maldraxxus")
  -- TODO: instances
end


-- UnitAura("player", i, "MAW")

function skill.IsGroundTargeted(spell)
  return
    spell == "Holy Word: Sanctify" or
    spell == "Power Word: Barrier" or
    spell == "Mass Dispel" or
    spell == "Light's Hammer" or
    spell == "Healing Rain" or
    spell == "Efflorescence"
end

function skill.mount()
  local map = C_Map.GetBestMapForUnit("player")
  local map_name = C_Map.GetMapInfo(map).name
  if map and (map_name == "The Maw" or map_name == "Korthia") then
    if not C_QuestLog.IsQuestFlaggedCompleted(63994) then
      return "Corridor Creeper"
    end
  end
  if IsResting() or select(2, GetInstanceInfo()) ~= "none" then
    if IsFlyableArea() then
      return "Swift Purple Gryphon"
    end
    if UnitName("Natalii") then
      return "Swift Purple Gryphon"
    end
    return "Swift Alliance Steed"
  end
  if UnitName("player") == "Yve" then
    return "Sky Golem"
  end
  return IsFlyableArea() and "Swift Purple Gryphon" or "Swift Alliance Steed"
end

function skill.HeroismDuration()
  return
  PlayerAuraDuration("Heroism") +
  PlayerAuraDuration("Bloodlust") +
  PlayerAuraDuration("Time Warp") +
  PlayerAuraDuration("Ancient Hysteria")
end

soulbinds = {}
function soulbinds.is_active(spell)
  for _, node in ipairs(C_Soulbinds.GetSoulbindData(C_Soulbinds.GetActiveSoulbindID()).tree.nodes) do
    if node.state == Enum.SoulbindNodeState.Selected then
      local name = node.spellID > 0 and GetSpellInfo(node.spellID)
      if not name then
        name = node.conduitID > 0 and GetSpellInfo(C_Soulbinds.GetConduitSpellID(node.conduitID, node.conduitRank))
      end
      if name == spell then
        return true
      end
    end
  end
  return false
end
function soulbinds.active_rank(spell)

end

skill.should_interrupt = true

function skill.DispelPurificationProtocol()
  local spell = "Purification Protocol"
  return
  TryDispelUnit("party1", spell) or
  TryDispelUnit("party2", spell) or
  TryDispelUnit("party3", spell) or
  TryDispelUnit("party4", spell) or
  nil
end
function skill.DispelPhantasmalParasite()
  local spell = "Phantasmal Parasite"
  local num = GetNumGroupMembers()
  local type = num > 5 and "raid" or "party"
  for i = 1, num do
    local unit = type..i
    local result =
    skill.IsUnitRole(unit, "DAMAGER") and
    UnitAuraDuration(unit, spell) > GlobalCD() and
    TryCastSpellOnUnit("Cleanse", unit) or
    nil
    if result then
      return result
    end
  end
  local healer = skill.TryGetFirstRoleUnit("HEALER")
  if healer then
    return
    TryDispelUnit(healer, spell) or
    nil
  end
  return nil
end

function skill.will_cap_stacks(spell)
  local clip_duration = skill.IfOffGlobal(spell) and 0 or GlobalCDMax() + GlobalCD()
  return
    GetCharges(spell) >= GetChargesMax(spell) or
    (GetCharges(spell) == GetChargesMax(spell) - 1 and GetChargeCD(spell) <= clip_duration)
end
