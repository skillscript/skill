_, skill = ...

--local lastNameplate = {}
local last_enemies_update = {}
local enemies = {}
function skill.GetEnemies(ExtraCondition)
  ExtraCondition = ExtraCondition or AlwaysTrue
  if not last_enemies_update[ExtraCondition] then
    last_enemies_update[ExtraCondition] = 0
    enemies[ExtraCondition] = {}
  end
  if last_enemies_update[ExtraCondition] >= GetTime() then
    return enemies[ExtraCondition]
  end
  last_enemies_update[ExtraCondition] = GetTime()
  wipe(enemies[ExtraCondition])
  for i = 1, 50 do
    local unit = "nameplate"..i
    local guid = UnitGUID(unit)
    if UnitExists(unit) and not UnitIsDeadOrGhost(unit) and UnitCanAttack("player", unit) and ExtraCondition(unit) then
      enemies[ExtraCondition][guid] = unit
    end
  end
  --local i = 1
  --local unit = "nameplate"..i
  --local guid = UnitGUID(unit)
  --local newLastNameplate = 0
  --while guid or i < ((lastNameplate[ExtraCondition] or 0) + 1) do
  --  newLastNameplate = guid and i or newLastNameplate
  --  ALERT("unit: "..tostring(unit))
  --  ALERT("name: "..tostring(UnitName(unit)))
  --  ALERT("ExtraCondition: "..tostring(ExtraCondition(unit)))
  --  if UnitExists(unit) and not UnitIsDeadOrGhost(unit) and UnitCanAttack("player", unit) and ExtraCondition(unit) then
  --    enemies[ExtraCondition][guid] = unit
  --  end
  --  i = i + 1
  --  unit = "nameplate"..i
  --  guid = UnitGUID(unit)
  --end
  --lastNameplate[ExtraCondition] = newLastNameplate
  --ALERT("lastNameplate[ExtraCondition]: "..tostring(lastNameplate[ExtraCondition]))
  return enemies[ExtraCondition]
end

local cached = true
if cached then
  local range_check = LibStub("LibRangeCheck-2.0")
  local enemies_within_timestamp = {}
  local enemies_within_result = {}
  function skill.EnemiesWithin(max_distance)
    local time = GetTime()
    enemies_within_result[max_distance] = enemies_within_result[max_distance] or {}
    enemies_within_timestamp[max_distance] = enemies_within_timestamp[max_distance] or 0
    if enemies_within_timestamp[max_distance] < time then
      wipe(enemies_within_result[max_distance])
      enemies_within_timestamp[max_distance] = time
      local current_enemies = skill.GetEnemies()
      for guid, unit in pairs(current_enemies) do
        local _, distance = range_check:GetRange(unit)
        distance = distance or 1337
        enemies_within_result[max_distance][guid] = distance <= max_distance and unit or nil
      end
    end
    return enemies_within_result[max_distance]
  end

  local num_enemies_within_timestamp = {}
  local num_enemies_within_result = {}
  function skill.NumEnemiesWithin(max_distance)
    local time = GetTime()
    num_enemies_within_result[max_distance] = num_enemies_within_result[max_distance] or {}
    num_enemies_within_timestamp[max_distance] = num_enemies_within_timestamp[max_distance] or 0
    if num_enemies_within_timestamp[max_distance] < time then
      num_enemies_within_timestamp[max_distance] = time
      num_enemies_within_result[max_distance] = 0
      local localEnemiesWithin = skill.EnemiesWithin(max_distance)
      for _, _ in pairs(localEnemiesWithin) do
        num_enemies_within_result[max_distance] = num_enemies_within_result[max_distance] + 1
      end
    end
    return num_enemies_within_result[max_distance]
  end
else
  local range_check = LibStub("LibRangeCheck-2.0")
  local enemiesWithin = {}
  function skill.EnemiesWithin(maxDistance)
    wipe(enemiesWithin)
    local currentEnemies = skill.GetEnemies()
    for guid, unit in pairs(currentEnemies) do
      local _, distance = range_check:GetRange(unit)
      distance = distance or 1337
      enemiesWithin[guid] = distance <= maxDistance and unit or nil
    end
    return enemiesWithin
  end

  function skill.NumEnemiesWithin(maxDistance)
    local result = 0
    local localEnemiesWithin = skill.EnemiesWithin(maxDistance)
    for _, _ in pairs(localEnemiesWithin) do
      result = result + 1
    end
    return result
  end
end

function skill.CountEnemies(localEnemiesWithin)
  local result = 0
  for k, v in pairs(localEnemiesWithin) do
    result = result + 1
  end
  return result
end






local enemy_tracker_frame = CreateFrame("Frame", nil, UIParent)
local function on_event(self, event, ...)
  if event == "PLAYER_TARGET_CHANGED" then
  end
end
enemy_tracker_frame:RegisterEvent("PLAYER_TARGET_CHANGED")
enemy_tracker_frame:SetScript("OnEvent", on_event)

local UNIT_TO_GUID = {}
local GUID_TO_UNIT = {}
local build_maps_timestamp = 0
local function build_maps()
  local time = GetTime()
  if time > build_maps_timestamp then
    wipe(UNIT_TO_GUID)
    wipe(GUID_TO_UNIT)
    local combat_cast_units = UniqueExistingCombatCastUnits()
    for i = 1, #combat_cast_units do
      local guid = UnitGUID(combat_cast_units[i])
      UNIT_TO_GUID[combat_cast_units[i]] = guid
      if guid then
        GUID_TO_UNIT[guid] = combat_cast_units[i]
      end
    end
    build_maps_timestamp = time
  end
end
function UnitToGuid(unit)
  build_maps()
  return UNIT_TO_GUID[unit]
end
function GuidToUnit(guid)
  build_maps()
  return GUID_TO_UNIT[guid]
end

local NOT_FOUND_GUID = {}
local scanning = false
local before_scan_target_guid
local scan_start_guid
local scan_step = false
local SCAN_PAUSE_TIME = 5.0
local STEP_PAUSE_TIME = 1.0
function TryScanForGuid(guid)
  if not guid or ((NOT_FOUND_GUID[guid] or 0) + SCAN_PAUSE_TIME > GetTime()) then
    scan_step = true
    return
  end
  -- TODO: clear expired guids in on_update
  NOT_FOUND_GUID[guid] = nil
  if scan_step then
    scan_step = false
    return "nothing"
  end
  local target_guid = UnitGUID("target")
  if target_guid == guid then
    ALERT("found")
    scanning = false
    scan_start_guid = nil
    return
  end
  if not scanning then
    ALERT("starting")
    before_scan_target_guid = target_guid
    scanning = true
    scan_step = true
    return "target_nearest_enemy"
  end
  if not target_guid then
    ALERT("no targets")
    before_scan_target_guid = nil
    scanning = false
    return
  end
  if not scan_start_guid then
    scan_start_guid = target_guid
  elseif scan_start_guid == target_guid then
    -- cannot find guid
    ALERT("cannot find guid")
    NOT_FOUND_GUID[guid] = GetTime()
    scan_start_guid = nil
    scanning = false
    return
  end
  scan_step = true
  return "target_nearest_enemy"
end
