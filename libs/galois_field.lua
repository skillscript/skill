_, skill = ...
local g = {}
g.current = 1
g.min_poly = 29

function g.add(a, b)
  return bit.bxor(a, b)
end

function g.mul(a, b)
  a = bit.band(a, 0xFF)
  b = bit.band(b, 0xFF)
  local r = 0
  while b > 0 do
    if bit.band(b, 1) ~= 0 then
      r = bit.bxor(r, a)
    end
    if bit.band(a, 0x80) ~= 0 then
      a = bit.bxor(bit.band(bit.lshift(a, 1), 0xFF), g.min_poly)
    else
      a = bit.band(bit.lshift(a, 1), 0xFF)
    end
    b = bit.rshift(b, 1)
  end
  return r
end

galois = {}
function galois.increment()
  g.current = g.mul(g.current, 2)
  --ALERT("g.current: "..tostring(g.current))
  return g.current
end