_, skill = ...
local OLDSTREAMS = {
  ["player_guid"] = {
    ["time"] = {},
    ["health"] = {},
    ["absorb"] = {},
    --["health_max"] = {},
  },
}

local function this(...)
  return ...
end
local function combat_log_event_unfiltered_guid(...)
  return select(8, CombatLogGetCurrentEventInfo()) or UnitGUID(...)
end
local STREAMS = {
  ["health"] = {
    ["amount"] = UnitHealth,
    ["unit"] = this,
    ["events"] = {
      "UNIT_HEALTH",
      --"COMBAT_LOG_EVENT_UNFILTERED",
    }
  },
  ["absorb"] = {
    ["amount"] = UnitAbsorb,
    ["unit"] = this,
    ["events"] = {
      "UNIT_ABSORB_AMOUNT_CHANGED",
    }
  },
  ["heal_absorb"] = {
    ["amount"] = UnitHealAbsorb,
    ["unit"] = this,
    ["events"] = {
      "UNIT_HEAL_ABSORB_AMOUNT_CHANGED",
    }
  },
}
local STREAM_PARTS = {
  "amount",
  "gained",
  "lost",
}
local GUIDS = {
  --["guid"] = {
  --  ["health"] = {
  --    ["amount"] = {
  --      ["time"] = {},
  --      ["value"] = {},
  --    },
  --    ["gained"] = {
  --      ["time"] = {},
  --      ["value"] = {},
  --    },
  --    ["lost"] = {
  --      ["time"] = {},
  --      ["value"] = {},
  --    },
  --  },
  --},
}
local health_frame = CreateFrame("Frame", nil, UIParent)
local function all_of_type(t, ...)
  local result = true
  for i = 1, select("#", ...) do
    result = result and type(select(i, ...)) == t
    if not result then
      return false
    end
  end
  return true
end
local function trim(stream, seconds)
  local cutoff = GetTime() - seconds
  for _, part in ipairs(STREAM_PARTS) do
    for i = 1, #stream[part].time - 1 do
      if stream[part].time[i] and stream[part].time[i] < cutoff then
        table.remove(stream[part].time, i)
        table.remove(stream[part].amount, i)
      end
    end
  end
end
local function OnEvent(self, event, ...)
  if event == "PLAYER_ENTERING_WORLD" then
    for stream, info in pairs(STREAMS) do
      if type(info.amount) ~= "function" then
        ALERT_ONCE(string.format("Field '|cFFFF0000STREAMS.%s.amount|r' not of type '|cFF00FFFFfunction|r'. Not registering any events.", stream))
        break
      end
      if type(info.unit) ~= "function" then
        ALERT_ONCE(string.format("Field '|cFFFF0000STREAMS.%s.unit|r' not of type '|cFF00FFFFfunction|r'. Not registering any events.", stream))
        break
      end
      if type(info.events) ~= "table" or select("#", info.events) < 1 then
        ALERT_ONCE(string.format("Field '|cFFFF0000STREAMS.%s.events|r' not of type '|cFF00FFFFtable|r'. Not registering any events.", stream))
        break
      end
      if select("#", info.events) < 1 then
        ALERT_ONCE(string.format("Field '|cFFFF0000STREAMS.%s.events|r' is empty. Not registering any events.", stream))
        break
      end
      if not all_of_type("string", unpack(info.events)) then
        ALERT_ONCE(string.format("Not all elements in field '|cFFFF0000STREAMS.%s.events|r' are of type '|cFF00FFFFstring|r'. Not registering any events.", stream))
        break
      end
      for _, e in ipairs(info.events) do
        health_frame:RegisterEvent(e)
        OnEvent(self, e, "player")
      end
    end
  end
  for s, info in pairs(STREAMS) do
    for _, e in ipairs(info.events) do
      if event == e then
        local unit = info.unit(...)
        local guid = UnitGUID(unit)
        if not guid then
          break
        end
        if not GUIDS[guid] then
          GUIDS[guid] = {}
          for stream, _ in pairs(STREAMS) do
            GUIDS[guid][stream] = {}
            for _, part in ipairs(STREAM_PARTS) do
              GUIDS[guid][stream][part] = {}
              GUIDS[guid][stream][part].time = {}
              GUIDS[guid][stream][part].value = {}
            end
          end
        end
        local stream = GUIDS[guid][s]
        local time = GetTime()
        --local unit = GuidToUnit(guid)
        local value = info.amount(unit)
        table.insert(stream.amount.time, time)
        table.insert(stream.amount.value, value)
        local last_value = #stream.amount.value > 1 and stream.amount.value[#stream.amount.value - 1] or nil
        if last_value then
          if value > last_value then
            table.insert(stream.gained.time, time)
            table.insert(stream.gained.value, value - last_value)
            --ALERT("gained: "..tostring(value - last_value))
          elseif value < last_value then
            table.insert(stream.lost.time, time)
            table.insert(stream.lost.value, last_value - value)
            --ALERT("lost: "..tostring(last_value - value))
          end
        end
        --trim(stream, 60)
      end
    end
  end
end
health_frame:RegisterEvent("PLAYER_ENTERING_WORLD")
health_frame:SetScript("OnEvent", OnEvent)

local function sum(stream, window, left_bound_func, right_bound_func)
  if not stream then
    ALERT_ONCE(string.format("Not a stream '%s'.", stream))
    return nil
  end
  local result = 0
  local time = GetTime()
  local cutoff = time - window
  for i = #stream.time, 1, -1 do
    if stream.time[i] > cutoff then
      result = result + stream.value[i]
    else
      break
    end
  end
  return result
end
local function youngest_value(stream, t)
  return stream.value[#stream.time]
end
local function oldest_value(stream, t)
  return stream.value[1]
end
local function youngest_time(stream, window)
  return max(stream.time[#stream.time] or 0, 0)
end
local function oldest_time(stream, window)
  return min(stream.time[1] or 0, GetTime() - window)
end
local function fractional_value_at(stream, t)
  ALERT("t: "..tostring(t))
  for i = #stream.time, 1, -1 do
    ALERT("i: "..tostring(i))
    if t > stream.time[#stream.time] then
      local dt = t - stream.time[#stream.time]
      ALERT(string.format("dt1: %8.2f",dt))
      return stream.value[#stream.time] * dt
    elseif t == stream.time[i] then
      return stream.value[i]
    elseif t > stream.time[i] or not stream.time[i] then
      local dt = stream.time[i + 1] - t
      ALERT(string.format("dt2: %8.2f",dt))
      return stream.value[i + 1] * dt
    --else
    --  ALERT(""..nil)
    end
  end
  ALERT_ONCE("fractional_value_at() error")
  return 0
end
local function avg(stream, window)
  if not stream then
    ALERT_ONCE(string.format("Not a stream '%s'.", stream))
    return nil
  end
  local result = 0
  local time = GetTime()
  local cutoff = time - window
  local n = 0
  for i = #stream.time, 1, -1 do
    if stream.time[i] > cutoff then
      result = result + stream.value[i]
      n = n + 1
    else
      if n > 0 then
        result = result + stream.value[i + 1]
        result = result + stream.value[#stream.time]
        n = n + 2
      end
      break
    end
  end
  return n > 0 and result / n or 0
end


local function guid_func(func, guid, stream, part, window)
  if not guid then
    ALERT_ONCE(string.format("No GUID given."))
    return 0
  end
  if not GUIDS[guid] then
    --ALERT_ONCE(string.format("No stream for GUID '%s'.", guid))
    return 0
  end
  if window <= 0 then
    ALERT_ONCE(string.format("Window == %s <= 0.", window))
    return nil
  end
  return func(GUIDS[guid][stream][part], window)
end

function GuidOldestHealthGainedTime(guid, window)
  return guid_func(oldest_time, guid, "health", "gained", window)
end
function GuidOldestHealthLostTime(guid, window)
  return guid_func(oldest_time, guid, "health", "lost", window)
end
function GuidOldestHealthChangedTime(guid, window)
  return max(GuidOldestHealthGainedTime(guid, window), GuidOldestHealthLostTime(guid, window))
end
function GuidHealthGained(guid, window)
  return guid_func(sum, guid, "health", "gained", window)
end
function GuidHealthGainedPerSecond(guid, window)
  return guid_func(avg, guid, "health", "gained", window) / window
end
function GuidHealthLost(guid, window)
  return guid_func(sum, guid, "health", "lost", window)
end
function GuidHealthLostPerSecond(guid, window)
  return guid_func(avg, guid, "health", "lost", window) / window
end
function GuidHealthChanged(guid, window)
  return GuidHealthGained(guid, window) - GuidHealthLost(guid, window)
end
function GuidHealthChangedPerSecond(guid, window)
  return GuidHealthGainedPerSecond(guid, window) - GuidHealthLostPerSecond(guid, window)
end

function UnitOldestHealthGainedTime(guid, window)
  return GuidOldestHealthGainedTime(UnitGUID(guid), window)
end
function UnitOldestHealthLostTime(guid, window)
  return GuidOldestHealthLostTime(UnitGUID(guid), window)
end
function UnitOldestHealthChangedTime(guid, window)
  return GuidOldestHealthChangedTime(UnitGUID(guid), window)
end
function UnitHealthGained(unit, window)
  return GuidHealthGained(UnitGUID(unit), window)
end
function UnitHealthGainedPerSecond(unit, window)
  return GuidHealthGainedPerSecond(UnitGUID(unit), window)
end
function UnitHealthLost(unit, window)
  return GuidHealthLost(UnitGUID(unit), window)
end
function UnitHealthLostPerSecond(unit, window)
  return GuidHealthLostPerSecond(UnitGUID(unit), window)
end
function UnitHealthChanged(unit, window)
  return GuidHealthChanged(UnitGUID(unit), window)
end
function UnitHealthChangedPerSecond(unit, window)
  return GuidHealthChangedPerSecond(UnitGUID(unit), window)
end

local ONE_HOUR = 60 * 60
function UnitTimeToLiveByHealthChanged(unit, window)
  local health_change_per_second = UnitHealthChangedPerSecond(unit, window)
  return health_change_per_second < 0 and UnitHealth(unit) / -health_change_per_second or ONE_HOUR
end
