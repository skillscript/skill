local HEALTH_HISTORY_STREAMS = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}

local healthHistoryTimestamp = {}
local healthHistoryAmount = {}
local healthHistorySchool = {}

local function TrimEvents(seconds)
  local cutoff = GetTime() - seconds
  for i, unit in ipairs(HEALTH_HISTORY_STREAMS) do
    local guid = UnitGUID(unit)
    local unitTimestamps = healthHistoryTimestamp[guid]
    if unitTimestamps then
      for i = 1, #healthHistoryTimestamp[guid] - 1 do
        if healthHistoryTimestamp[guid][i] and healthHistoryTimestamp[guid][i] <= cutoff then
          table.remove(healthHistoryTimestamp[guid], i)
          table.remove(healthHistoryAmount[guid], i)
          table.remove(healthHistorySchool[guid], i)
        else
          break
        end
      end
    end
  end
end

local healthHistoryFrame = CreateFrame("Frame", nil, UIParent)
healthHistoryFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
healthHistoryFrame:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")

local function IsTrackedUnit(guid)
  for i, unit in ipairs(HEALTH_HISTORY_STREAMS) do
    if guid == UnitGUID(unit) then
      return true
    end
  end
  return false
end

local function HasInitializedTables(guid)
  for timestampGuid, _ in ipairs(healthHistoryTimestamp) do
    if guid == timestampGuid then
      return true
    end
  end
  return false
end

local function ResetHealthHistory()
  for guid, _ in pairs(healthHistoryTimestamp) do
    if not IsTrackedUnit(guid) then
      healthHistoryTimestamp[guid] = nil
      healthHistoryAmount[guid] = nil
      healthHistorySchool[guid] = nil
    end
  end
  TrimEvents(0)
  for i, unit in ipairs(HEALTH_HISTORY_STREAMS) do
    local guid = UnitGUID(unit)
    if guid and not HasInitializedTables(guid) then
      healthHistoryTimestamp[guid] = {}
      healthHistoryAmount[guid] = {}
      healthHistorySchool[guid] = {}
    end
  end
end

local healthHistoryEnabled = false
function IsHealthHistoryEnabled()
  return healthHistoryEnabled
end

function EnableHealthHistory()
  if IsHealthHistoryEnabled() then
    return
  end
  healthHistoryEnabled = true
  healthHistoryFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  healthHistoryFrame:RegisterEvent("GROUP_ROSTER_UPDATE")
  ResetHealthHistory()
end

function DisableHealthHistory()
  if not IsHealthHistoryEnabled() then
    return
  end
  healthHistoryEnabled = false
  healthHistoryFrame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  healthHistoryFrame:UnregisterEvent("GROUP_ROSTER_UPDATE")
end

local lastSwing = 0
local extraValues = {}
local function HealthHistoryEvent(self, event, time, subevent, ...)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local timestamp, subevent, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, i_12, i_13, i_14, i_15, i_16, i_17, i_18, i_19, i_20, i_21, i_22, i_23, i_24, i_25, i_26, i_27, i_28, i_29 = CombatLogGetCurrentEventInfo()
    if true then
      --prefix values
      local spellId
      local spellName
      local spellSchool
      local environmentalType
      local itemID
      local itemName
      local recapID
      local unconsciousOnDeath
      -- suffix values
      local amount
      local overkill
      local school
      local resisted
      local blocked
      local absorbed
      local critical
      local glancing
      local crushing
      local isOffHand
      local missType
      local amountMissed
      local overhealing
      local overEnergize
      local powerType
      local alternatePowerType
      local extraAmount
      local extraSpellId
      local extraSpellName
      local extraSchool
      local auraType
      local failedType
      local underscoreIndex = strfind(subevent, "_") or strlen(subevent)
      local prefix = strsub(subevent, 1, underscoreIndex - 1)
      local suffix = strsub(subevent, underscoreIndex)
      if true then
        local numBaseValues = 11
        extraValues[12] = i_12
        extraValues[13] = i_13
        extraValues[14] = i_14
        extraValues[15] = i_15
        extraValues[16] = i_16
        extraValues[17] = i_17
        extraValues[18] = i_18
        extraValues[19] = i_19
        extraValues[20] = i_20
        extraValues[21] = i_21
        extraValues[22] = i_22
        extraValues[23] = i_23
        extraValues[24] = i_24
        extraValues[25] = i_25
        extraValues[26] = i_26
        extraValues[27] = i_27
        extraValues[28] = i_28
        extraValues[29] = i_29
        local offset = 0
        if prefix == "RANGE" or prefix == "SPELL" or prefix == "DAMAGE" then
          spellId = extraValues[numBaseValues + 1]
          spellName = extraValues[numBaseValues + 2]
          spellSchool = extraValues[numBaseValues + 3]
          offset = 3
        elseif prefix == "ENCHANT" then
          spellName = extraValues[numBaseValues + 1]
          itemID = extraValues[numBaseValues + 2]
          itemName = extraValues[numBaseValues + 3]
          offset = 3
        elseif prefix == "UNIT" then
          recapID = extraValues[numBaseValues + 1]
          unconsciousOnDeath = extraValues[numBaseValues + 2]
          offset = 2
        elseif prefix == "ENVIRONMENTAL" then
          environmentalType = extraValues[numBaseValues + 1]
          offset = 1
        end
        if suffix == "_DAMAGE" or suffix == "_SHIELD" or suffix == "_SPLIT" then
          amount = extraValues[numBaseValues + offset + 1]
          overkill = extraValues[numBaseValues + offset + 2]
          school = extraValues[numBaseValues + offset + 3]
          resisted = extraValues[numBaseValues + offset + 4]
          blocked = extraValues[numBaseValues + offset + 5]
          absorbed = extraValues[numBaseValues + offset + 6]
          critical = extraValues[numBaseValues + offset + 7]
          glancing = extraValues[numBaseValues + offset + 8]
          crushing = extraValues[numBaseValues + offset + 9]
          isOffHand = extraValues[numBaseValues + offset + 10]
        elseif suffix == "_MISSED" or suffix == "_SHIELD_MISSED" then
          missType = extraValues[numBaseValues + offset + 1]
          isOffHand = extraValues[numBaseValues + offset + 2]
          amountMissed = extraValues[numBaseValues + offset + 3]
        elseif suffix == "_HEAL" then
          amount = extraValues[numBaseValues + offset + 1]
          overhealing = extraValues[numBaseValues + offset + 2]
          absorbed = extraValues[numBaseValues + offset + 3]
          critical = extraValues[numBaseValues + offset + 4]
        elseif suffix == "_ENERGIZE" then
          amount = extraValues[numBaseValues + offset + 1]
          overEnergize = extraValues[numBaseValues + offset + 2]
          powerType = extraValues[numBaseValues + offset + 3]
          alternatePowerType = extraValues[numBaseValues + offset + 4]
        elseif suffix == "_DRAIN" then
          amount = extraValues[numBaseValues + offset + 1]
          powerType = extraValues[numBaseValues + offset + 2]
          extraAmount = extraValues[numBaseValues + offset + 3]
        elseif suffix == "_LEECH" then
          amount = extraValues[numBaseValues + offset + 1]
          powerType = extraValues[numBaseValues + offset + 2]
          extraAmount = extraValues[numBaseValues + offset + 3]
        elseif suffix == "_INTERRUPT" then
          extraSpellId = extraValues[numBaseValues + offset + 1]
          extraSpellName = extraValues[numBaseValues + offset + 2]
          extraSchool = extraValues[numBaseValues + offset + 3]
        elseif suffix == "_DISPEL" then
          extraSpellId = extraValues[numBaseValues + offset + 1]
          extraSpellName = extraValues[numBaseValues + offset + 2]
          extraSchool = extraValues[numBaseValues + offset + 3]
          auraType = extraValues[numBaseValues + offset + 4]
        elseif suffix == "_DISPEL_FAILED" then
          extraSpellId = extraValues[numBaseValues + offset + 1]
          extraSpellName = extraValues[numBaseValues + offset + 2]
          extraSchool = extraValues[numBaseValues + offset + 3]
        elseif suffix == "_STOLEN" then
          extraSpellId = extraValues[numBaseValues + offset + 1]
          extraSpellName = extraValues[numBaseValues + offset + 2]
          extraSchool = extraValues[numBaseValues + offset + 3]
          auraType = extraValues[numBaseValues + offset + 4]
        elseif suffix == "_EXTRA_ATTACKS" then
          amount = extraValues[numBaseValues + offset + 1]
        elseif suffix == "_AURA_APPLIED" then
          auraType = extraValues[numBaseValues + offset + 1]
          amount = extraValues[numBaseValues + offset + 2]
        elseif suffix == "_AURA_REMOVED" then
          auraType = extraValues[numBaseValues + offset + 1]
          amount = extraValues[numBaseValues + offset + 2]
        elseif suffix == "_AURA_APPLIED_DOSE" then
          auraType = extraValues[numBaseValues + offset + 1]
          amount = extraValues[numBaseValues + offset + 2]
        elseif suffix == "_AURA_REMOVED_DOSE" then
          auraType = extraValues[numBaseValues + offset + 1]
          amount = extraValues[numBaseValues + offset + 2]
        elseif suffix == "_AURA_REFRESH" then
          auraType = extraValues[numBaseValues + offset + 1]
          amount = extraValues[numBaseValues + offset + 2]
        elseif suffix == "_AURA_BROKEN" then
          auraType = extraValues[numBaseValues + offset + 1]
        elseif suffix == "_AURA_BROKEN_SPELL" then
          extraSpellId = extraValues[numBaseValues + offset + 1]
          extraSpellName = extraValues[numBaseValues + offset + 2]
          extraSchool = extraValues[numBaseValues + offset + 3]
          auraType = extraValues[numBaseValues + offset + 4]
        elseif suffix == "_CAST_FAILED" then
          failedType = extraValues[numBaseValues + offset + 1]
        end
      end

      --if sourceGUID == UnitGUID("player") then
      --  if subevent == "SWING_DAMAGE" then
      --    ALERT(timestamp .. ": " .. subevent .. ": " .. (timestamp - lastSwing))
      --    lastSwing = timestamp
      --  end
      --end

      if not IsTrackedUnit(destGUID) then
        return
      end

      if prefix == "ENVIRONMENTAL" and school == 1 then
        return
      end
      if suffix == "_DAMAGE" or subevent == "SWING_MISSED" then
        --ALERT("==============")
        --ALERT("subevent: "..tostring(subevent))
        --ALERT("amount: "..tostring(amount))
        --ALERT("spellSchool: "..tostring(school or spellSchool))
        --ALERT("#healthHistoryTimestamp[destGUID]: "..tostring(#healthHistoryTimestamp[destGUID]))

        if not healthHistoryTimestamp[destGUID] then
          ResetHealthHistory()
        end
        table.insert(healthHistoryTimestamp[destGUID], GetTime())
        table.insert(healthHistoryAmount[destGUID], amount or 0)
        table.insert(healthHistorySchool[destGUID], max(1, school or spellSchool or 1))
        TrimEvents(30)
      end
    end
  elseif event == "GROUP_ROSTER_UPDATE" or  event == "PLAYER_ENTERING_WORLD" then
    ResetHealthHistory()
  elseif event == "PLAYER_SPECIALIZATION_CHANGED" then
    DisableHealthHistory()
  end
end
healthHistoryFrame:SetScript("OnEvent", HealthHistoryEvent)


local function UnitDamageReceived(unit, seconds, bitmask)
  if not IsHealthHistoryEnabled() then
    return "HealthHistory is not enabled"
  end
  bitmask = bitmask or 0xFF
  local sum = 0
  local guid = UnitGUID(unit)
  if not guid or not healthHistoryTimestamp[guid] then
    ResetHealthHistory()
    return 0
  end
  for j = 1, #healthHistoryTimestamp[guid] do
    local i = #healthHistoryTimestamp[guid] - j + 1
    local time = healthHistoryTimestamp[guid][i]
    if time < GetTime() - seconds then
      break
    end
    sum = sum + (bit.band(healthHistorySchool[guid][i], bitmask) > 0 and healthHistoryAmount[guid][i] or 0)
  end
  return sum
end

function UnitPhysicalDamageReceived(unit, seconds)
  return UnitDamageReceived(unit, seconds, 1)
end

function PlayerPhysicalDamageReceived(seconds)
  return UnitPhysicalDamageReceived("player", seconds)
end


local function TimeBetweenDmgReceived(window)
  if not damage_events then
    return 0
  end
  local history = damage_events[UnitGUID("player")]
  if not history or #history < 2 then
    return 0
  end
  local min_time = 10
  for i = 1, #history - 1 do
    local time = history[i][1]
    local dmg = history[i][2] or 0
    if dmg / UnitHealthMax("player") > 0.05 and time > GetTime() - window then
      min_time = min(min_time, history[i + 1][1] - time)
    end
  end
  return min_time
end

local function TimeToNextDmgReceived()
  if not damage_events then
    return 0
  end
  local history = damage_events[UnitGUID("player")]
  if #history < 2 then
    return 0
  end
  return max(0, TimeBetweenDmgReceived(30) - (GetTime() - history[#history][1]))
end

