local low_level_frame = CreateFrame("Frame", nil, UIParent)

local function on_event(self, event, ...)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()
    if source_guid == UnitGUID("player") then
      if string.find(sub_event, "SPELL_") == 1 then
        if sub_event == "SPELL_CAST_FAILED" and failed_type == "Not yet recovered" then
          --ALERT(string.format("%s combat cast spell", CombatCastCurrentSpell()))
        end
      end
    end
  end
end
low_level_frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
low_level_frame:SetScript("OnEvent", on_event)

function skill.IsLowLevelFarm()
  local _, type, _, difficulty = GetInstanceInfo()
  return type == "raid" and difficulty ~= "Mythic"
end

skill.low_level = {}
function skill.ShouldUseRunSpeed()
  return not UnitAffectingCombat("player") and IsPlayerMoving() and (not UnitExists("target") or not skill.UnitIsBoss("target"))
end
