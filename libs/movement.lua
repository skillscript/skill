_, skill = ...
if false then
local private = {}
function private.init()
  private.load()
end
private.frame = CreateFrame("Frame", nil, UIParent)
private.frame:SetScript("OnEvent", private.init)
private.frame:RegisterEvent("ADDON_LOADED")

-- WorldMapFrame:RemoveAllPinsByTemplate("EncounterJournalPinTemplate")

local PATHS = {
  [""] = 0
}
local WAYPOINTS = {
  ["mail"] = {["x"] = -4911.5, ["y"] = -974.5},
  ["bridge"] = {["x"] = -4936.0, ["y"] = -934.0},
  ["auction"] = {["x"] = -4961.5, ["y"] = -905.0},
}
local ORIENTATIONS = {
  ["mail"] = 128,
  ["auction"] = -45,
}
-- TODO: don't hard code
BINDINGS = {
  ["turnleft"] =     2 ^ 8 * 65,
  ["turnright"] =    2 ^ 8 * 68,
  ["movebackward"] = 2 ^ 8 * 83,
  ["moveforward"] =  2 ^ 8 * 87,
}


local function player_position()
  return UnitPosition("player")
end
function player_orientation()
  local facing = GetPlayerFacing() * 180 / math.pi
  if facing > 180 then
    facing = facing - 360
  elseif facing < -180 then
    facing = facing + 360
  end
  return facing
end
local function facing_direction_error(direction, precision)
  local orientation = player_orientation()
  return direction - orientation
end
local function facing_command(direction, precision)
  local orientation = player_orientation()
  local error = direction - orientation
  if error > precision then
    return "turnleft"
  elseif error < -precision then
    return "turnright"
  else
    return nil
  end
end

local function get_length(x, y)
  return math.sqrt(x * x + y * y)
end
local function get_distance(from_x, from_y, to_x, to_y)
  return get_length(to_x - from_x, to_y - from_y)
end
local function get_direction(from_x, from_y, to_x, to_y)
  local delta_x, delta_y = to_x - from_x, to_y - from_y
  local length = get_length(delta_x, delta_y)
  local facing = math.atan2(delta_y / length, delta_x / length) * 180 / math.pi
  if facing > 180 then
    facing = facing - 360
  elseif facing < -180 then
    facing = facing + 360
  end
  return facing
end

local function move_to(x, y, precision)
  -- TODO: remove this and set pixels to 0 correctly
  for p, _ in pairs(BINDINGS) do
    skill.set_pixel_value(p, 0)
  end

  local player_x, player_y, instance_id = player_position()
  local distance = get_distance(x, y, player_x, player_y)
  if distance < precision then
    return nil
  end
  local target_direction = get_direction(player_x, player_y, x, y)
  local face_first_distance = 10
  --ALERT("orientation: "..tostring(player_orientation()))
  --ALERT("target_direction: "..tostring(target_direction))
  --ALERT("facing_command: "..tostring(facing_command(target_direction, 5)))
  local f = facing_command(target_direction, 5)
  if f then
    skill.set_pixel_value(f, BINDINGS[f])
  end
  local facing_error = facing_direction_error(target_direction, 5)
  if distance < face_first_distance  and (facing_error > 10 or facing_error < -10)then
    -- do the strafe moving
    ALERT_ONCE("NYI strafing")
  else
    if facing_error > -45 and facing_error < 45 then
      skill.set_pixel_value("moveforward", BINDINGS["moveforward"])
    end
  end
end

function private.load()
  --private.frame:SetScript("OnUpdate", private.update)
end
--local destination = "mail"
--local current_waypoint = "bridge"
function private.update()
  --move_to(WAYPOINTS["mail"].x, WAYPOINTS["mail"].y, 1)
  --move_to(WAYPOINTS["bridge"].x, WAYPOINTS["bridge"].y, 1)

  --if skill.DistanceTo(current_waypoint) > 1 then
  --  skill.MoveTo(current_waypoint)
  --else
  --  if current_waypoint == destination then
  --    destination = destination == "mail" and "auction" or "mail"
  --    current_waypoint = "bridge"
  --  else
  --    current_waypoint = destination
  --  end
  --end

  if skill_settings.state == 42 then
    for p, _ in pairs(BINDINGS) do
      skill.set_pixel_value(p, 0)
    end
  end
end

function skill.DistanceTo(waypoint)
  local player_x, player_y = player_position()
  return get_distance(WAYPOINTS[waypoint].x, WAYPOINTS[waypoint].y, player_x, player_y)
end
function skill.MoveTo(waypoint, precision)
  precision = precision or 1
  move_to(WAYPOINTS[waypoint].x, WAYPOINTS[waypoint].y, precision)
end
local PATHS = {
  ["Tillers"] = {
    [1] = {["map"] = 870, ["x"] = -229.8, ["y"] = 519.6},
    [2] = {["map"] = 870, ["x"] = -253.9, ["y"] = 536.3},
    [3] = {["map"] = 870, ["x"] = -226.6, ["y"] = 604.3},
    [4] = {["map"] = 870, ["x"] = -175.6, ["y"] = 637.5},
    -- grid
    -- first row r2l
--    [5] = {["map"] = 870, ["x"] = -171.5, ["y"] = 638.1},
--    [6] = {["map"] = 870, ["x"] = -171.8, ["y"] = 642.5},
--    [7] = {["map"] = 870, ["x"] = -172.0, ["y"] = 646.7},
--    [8] = {["map"] = 870, ["x"] = -172.2, ["y"] = 650.6},
--    -- second row l2r
--    [9] = {["map"] = 870, ["x"] = -166.7, ["y"] = 649.3},
--    [10] = {["map"] = 870, ["x"] = -166.3, ["y"] = 645.6},
--    [11] = {["map"] = 870, ["x"] = -166.3, ["y"] = 641.2},
--    [12] = {["map"] = 870, ["x"] = -166.1, ["y"] = 636.8},
    -- third row r2l
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
    --[12] = {["map"] = 870, ["x"] = , ["y"] = },
  },
}
local current_path
local current_waypoint
function skill.FollowPath(path, precision)
  local player_x, player_y, instance_id = player_position()
  if path == current_path then
    local w = PATHS[path][current_waypoint]
    local d = get_distance(player_x, player_y, w.x, w.y)
    --ALERT("d: "..tostring(d))
    --ALERT("current_waypoint: "..tostring(current_waypoint))
    --ALERT("player_x: "..tostring(player_x))
    --ALERT("player_y: "..tostring(player_y))
    --ALERT("w.x: "..tostring(w.x))
    --ALERT("w.y: "..tostring(w.y))
    move_to(w.x, w.y, precision)
    if d < precision then
      if #PATHS[path] > current_waypoint then
        current_waypoint = current_waypoint + 1
      else
        current_path = nil
        ALERT("arrived")
      end
    end
  else
    local min_distance = 10000 --= get_distance(player_x, player_y, PATHS[path][1].x, PATHS[path][1].y)
    ALERT("player_x: "..tostring(player_x))
    ALERT("player_y: "..tostring(player_y))
    for i, w in pairs(PATHS[path]) do
      ALERT("w.x: "..tostring(w.x))
      ALERT("w.y: "..tostring(w.y))
      local d = get_distance(player_x, player_y, w.x, w.y)
      ALERT("d: "..tostring(d))
      if d < min_distance then
        min_distance = d
        current_waypoint = i
        current_path = path
        ALERT("current_waypoint: "..tostring(current_waypoint))
      end
    end
    ALERT("current_waypoint: "..tostring(current_waypoint))
    ALERT("current_path: "..tostring(current_path))
  end
end


function nav()
  local posX, posY, posZ, instanceId = UnitPosition("player")
  --  local directionX = posX - x
  --  local directionY = posY - y
  --ALERT("posX: "..tostring(posX))
  --ALERT("posY: "..tostring(posY))
  --ALERT("orientation: "..tostring(player_orientation()))
  --ALERT("GetPlayerFacing(): "..tostring(GetPlayerFacing()))
  --ALERT("(x, y): ("..tostring(pos.x)..", "..tostring(pos.y)..")")
end
end
