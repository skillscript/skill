_, skill = ...

local private = {}
local priority_selection = {}
priority_selection.gui = {}
priority_selection.gui.label = "Priority:"
priority_selection.gui.text = ""
local COLOR_BUTTON = {43 / 255 * 1.5, 43 / 255 * 1.5, 43 / 255 * 1.5, 1.0}
local COLOR_BUTTON_HIGHLIGHT = {43 / 255 * 2.0, 43 / 255 * 2.0, 43 / 255 * 2.0, 1.0}

local function first_key(t)
  for name, _ in pairs(t) do
    return name
  end
end
local function last_key(t)
  local result
  for name, _ in pairs(t) do
    result = name
  end
  return result
end

local function set_priority(priority_name)
  local class = PlayerClass()
  local spec = PlayerSpec()
  local key = class.."_"..spec
  skill_char_settings = skill_char_settings or {}
  skill_char_settings.priorities = skill_char_settings.priorities or {}
  priority_name = priority_name or skill_char_settings.priorities[key] or first_key(private.priorities)
  for name, func in pairs(private.priorities) do
    private.priority = func
    private.gui.string:SetText(name)
    skill_char_settings.priorities[key] = name
    if name == priority_name then
      break
    end
  end
end

function priority_selection.run()
  if private.priority then
    return private.priority()
  end
  ALERT_ONCE("Not a single priority available for priority selection.")
  return "nothing"
end

function private.dropdown_rebind_func(self, key)
  local alt = IsAltKeyDown() and "ALT-" or ""
  local ctrl = IsControlKeyDown() and "CTRL-" or ""
  local shift = IsShiftKeyDown() and "SHIFT-" or ""
  if key == "ESCAPE" then
    for binding, priority_name in pairs(skill_char_settings.priority_selection_bindings) do
      if priority_name == self.priority_name then
        SetBinding(binding)
        skill_char_settings.priority_selection_bindings[binding] = nil
        ALERT("Unbound '"..binding.."' from priority '"..self.priority_name.."'.")
        private.update_dropdown_text()
        break
      end
    end
  elseif key == "LSHIFT" or key == "RSHIFT" or key == "LCTRL" or key == "RCTRL" or key == "LALT" or key == "RALT" or key == "UNKNOWN" then
    return
  else
    local binding = alt..ctrl..shift..key
    local frame_name = private.binding_frames[self.priority_name]:GetName()
    SetBindingClick(binding, frame_name)
    SaveBindings(2)
    skill_char_settings.priority_selection_bindings[binding] = self.priority_name
    ALERT("Bound '"..binding.."' to priority '"..self.priority_name.."'.")
    private.update_dropdown_text()
  end
end
function private.update_dropdown_text()
  local i = 1
  for name, _ in pairs(private.priorities) do
    local suffix = ""
    for b, priority_name in pairs(skill_char_settings.priority_selection_bindings) do
      if priority_name == name then
        if suffix == "" then
          suffix = b
        else
          suffix = suffix..", "..b
        end
      end
    end
    if suffix ~= "" then
      suffix = " ["..suffix.."]"
    end
    local frame = _G["DropDownList1Button"..i]
    frame:SetText(name..suffix)
    i = i + 1
  end
end

function private.dropdown_info(frame, level, menu_list)
  local rebind_mode = false
  if InCombatLockdown() then
    ALERT_ONCE("|cFFFF0000Key binding not available during combat.|r ")
  else
    rebind_mode = IsAltKeyDown()
  end

  private.dropdown_list_texture = private.dropdown_list_texture or frame:CreateTexture()
  private.dropdown_list_texture:SetAllPoints(frame)
  private.dropdown_list_texture:SetColorTexture(unpack(rebind_mode and private.dropdown_rebind_color or skill.colors.background))

  --local backdrop = _G[frame:GetName().."MenuBackdrop"]
  --backdrop:SetBackdropColor(unpack(skill.colors.background))
  --backdrop:SetBackdropBorderColor(0,0,0,0)

  for name, _ in pairs(private.priorities) do
    private.dropdown_info_table.text = name
    if rebind_mode then
      private.dropdown_info_table.func = nil
      private.dropdown_info_table.keepShownOnClick = true
      private.dropdown_info_table.funcOnEnter = function(button)
        button.priority_name = name
        button:EnableKeyboard(true)
        button:SetScript("OnKeyDown", private.dropdown_rebind_func)
      end
      private.dropdown_info_table.funcOnLeave = function(button)
        button:EnableKeyboard(false)
        button:SetScript("OnKeyDown", nil)
      end
    else
      private.dropdown_info_table.func = function() set_priority(name) end
      private.dropdown_info_table.keepShownOnClick = false
      private.dropdown_info_table.funcOnEnter = nil
      private.dropdown_info_table.funcOnLeave = nil
    end
    UIDropDownMenu_AddButton(private.dropdown_info_table)
  end
  private.update_dropdown_text()
end
private.binding_frames = {}
private.dropdown_frame = CreateFrame("Frame", "priority_selection_dropdown", UIParent, "UIDropDownMenuTemplate")
private.dropdown_frame.initialize = private.dropdown_info
private.dropdown_frame.displayMode = "MENU"
private.dropdown_rebind_color = {1, 0, 0, 1}
private.font = CreateFont("skill")
private.font:SetFont("font", pixelScale(12))
private.dropdown_info_table = {}
private.dropdown_info_table.notCheckable = true
private.dropdown_info_table.fontObject = private.font
private.dropdown_info_table.padding = 2

function private.dropdown_show()
  local index = 0
  for _, func in pairs(private.priorities) do
    index = index + 1
    if func == private.priority then
      break
    end
  end
  --ToggleDropDownMenu(1, nil, private.dropdown_frame, private.gui, -15, 3 + 16 * index)
  ToggleDropDownMenu(1, nil, private.dropdown_frame, private.gui, -15, 14 + 16 * index)
end

function priority_selection.gui.click(gui, button)
  if button == "LeftButton" then
    private.dropdown_show()
  end
end

function priority_selection.gui.init(gui)
  private.gui = gui
  set_priority()
  for name, func in pairs(private.priorities) do
    local frame_name = "priority_selection_binding_frame_"..name
    private.binding_frames[name] = CreateFrame("Button", frame_name, private.dropdown_frame)
    private.binding_frames[name].priority_name = name
    private.binding_frames[name]:SetScript("OnClick", function()
      set_priority(name)
    end)
  end
  skill_char_settings = skill_char_settings or {}
  skill_char_settings.priority_selection_bindings = skill_char_settings.priority_selection_bindings or {}
end


function PrioritySelectionFor(priorities)
  private.priorities = priorities
  return priority_selection
end

function GetCurrentPriority()
  return private.priority
end
function SetPriority(priority)
  if not priority or private.priority == priority then
    return
  end
  for name, func in pairs(private.priorities) do
    if name == priority then
      ALERT("Setting priority to: "..tostring(name))
      set_priority(name)
      return
    end
  end
  ALERT("No such priority: "..tostring(priority))
end
function ShowPrioritySelectionDropdown()
  private.dropdown_show()
end
