_, skill = ...

function pushQ()
  ALERT("pushQ() is obsolete! Please use the action button queueing mechanism or EnqueueSpell().")
end
function tryQ()
end

local queue_frame = CreateFrame("Frame", nil, UIParent)
local queued_spell
local queued_unit
local allow_idle_until = 0
local MAX_IDLE_TIME = 0.1
local SUCCESS_TIMEOUT = 0.2
local last_success = 0

local function clear_queue()
  --ALERT(string.format("%s cleared", queued_spell or "nil"))
  queued_spell = nil
  queued_unit = nil
  queue_frame:SetScript("OnUpdate", nil)
end

local function on_update()
  if GetTime() > allow_idle_until then
    if not skill.IsGroundTargeted(queued_spell) then
      ALERT(string.format("%s no longer idle, you may want to increase MAX_IDLE_TIME", queued_spell or "nil"))
    end
    clear_queue()
  end
end

local function queue_spell(spell, unit, custom_idle_time)
  if not unit then
    local helpful = skill.CanCastOnFriends(spell)
    local harmful = skill.CanCastOnEnemies(spell)
    if harmful and helpful then
      ALERT_ONCE(string.format("Not queueing %s for unit == nil, because it's both a harmful and helpful spell.", spell))
      return
    elseif skill.IsGroundTargeted(spell) then
      queued_unit = "ground"
      ALERT_ONCE(string.format("Queueing %s on ground.", spell))
    elseif harmful and UnitExists("target") and not skill.IsUnitPlayerFriend("target") then
      queued_unit = "target"
      --ALERT_ONCE(string.format("Queueing %s on target.", spell))
    elseif helpful and RangeOfSpell(spell) <= 0 then
      queued_unit = "player"
      ALERT_ONCE(string.format("Queueing %s on player.", spell))
    else
      ALERT_ONCE(string.format("Not queueing %s for unit == nil, because heuristic isn't conclusive.", spell))
      return
    end
  else
    queued_unit = unit
  end
  queued_spell = spell
  local cast_time_left = skill.PlayerCastingTimeLeft()
  local start, duration = GetSpellCooldown(61304)
  local real_global_cd = start and max(0, start + duration - GetTime()) or 0
  allow_idle_until = GetTime() + max(cast_time_left, real_global_cd) + max(MAX_IDLE_TIME, custom_idle_time or 0)
  queue_frame:SetScript("OnUpdate", on_update)
end

local function on_event(self, event, ...)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()
    if source_guid == UnitGUID("player") then
      if string.find(sub_event, "SPELL_") == 1 then
        if sub_event == "SPELL_CAST_START" then
          if GetSpellInfo(spell) == GetSpellInfo(queued_spell) then
            --ALERT(string.format("%s queued spellcast start", spell))
            clear_queue()
          else
            --ALERT(string.format("%s normal spellcast start", spell))
          end
        elseif sub_event == "SPELL_CAST_FAILED" then
          --ALERT("failed_type: "..tostring(failed_type))
          --ALERT("spell: "..tostring(spell))
          --if (GetCD(spell) <= GlobalCD(0) and failed_type == "Not yet recovered") or failed_type == "Another action is in progress" then
          if failed_type == "Not yet recovered" or failed_type == "Another action is in progress" then
            --ALERT(string.format("%s combat cast spell", CombatCastCurrentSpell()))
            if GetSpellInfo(spell) == GetSpellInfo(CombatCastCurrentSpell()) then
              --ALERT(string.format("%s non-queued spell not recovered", spell))
            else
              if not queued_spell then
                if GetTime() > last_success + SUCCESS_TIMEOUT then
                  --ALERT(string.format("%s queued", spell))
                  queue_spell(spell)
                else
                  --ALERT(string.format("%s tried queueing in timeout", spell))
                end
              else
                --ALERT(string.format("%s repeating queueing", spell))
              end
            end
          else
            if GetSpellInfo(spell) == GetSpellInfo(queued_spell) then
              --ALERT(string.format("%s queuing fail: ", spell, failed_type))
              clear_queue()
            else
              --ALERT(string.format("%s normal fail", spell))
            end
          end
        elseif sub_event == "SPELL_CAST_SUCCESS" then
          last_success = GetTime()
          if GetSpellInfo(spell) == GetSpellInfo(queued_spell) then
            --ALERT(string.format("%s queued success", spell))
            clear_queue()
          else
            --ALERT(string.format("%s normal success", spell))
          end
        end
      end
    end
  --elseif event == "GENERIC_ERROR" then
  --  ALERT("GENERIC_ERROR")
  --elseif event == "UI_ERROR_MESSAGE" then
  --  ALERT("UI_ERROR_MESSAGE")
  --elseif event == "UI_INFO_MESSAGE" then
  --  ALERT("UI_INFO_MESSAGE")
  end
end
queue_frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
queue_frame:RegisterEvent("GENERIC_ERROR")
queue_frame:RegisterEvent("UI_ERROR_MESSAGE")
queue_frame:RegisterEvent("UI_INFO_MESSAGE")
queue_frame:SetScript("OnEvent", on_event)


-- "public"
function GetQueuedSpell()
  return queued_spell
end

function GetQueuedUnit()
  return queued_unit
end

function TryCastQueuedSpell()
  return TryCastSpellOnUnit(GetQueuedSpell(), GetQueuedUnit())
end

function EnqueueSpell(spell, unit, max_idle_time)
  queue_spell(spell, unit, max_idle_time)
end

function Dequeue()
  clear_queue()
end

function ToggleSpell(spell, unit, max_idle_time)
  if GetQueuedSpell() == spell and GetQueuedUnit() == unit then
    Dequeue()
  else
    EnqueueSpell(spell, unit, max_idle_time)
  end
end