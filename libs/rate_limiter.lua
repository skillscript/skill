local private = {}
private.MORE_POWERFUL_SPELL_MAX_SUSPENSION_DURATION = 15
private.NOT_IN_FRONT_SUSPENSION_DURATION = 1
private.LINE_OF_SIGHT_SUSPENSION_DURATION = 1
private.PATH_SUSPENSION_DURATION = 1
private.ALL_SPELLS = "ALL_SPELLS"
private.suspensions = {}
private.suspension_timeouts = {}

function private.suspension_count(spell, unit)
  local guid = UnitGUID(unit)
  if guid then
    private.suspensions[guid] = private.suspensions[guid] or {}
    return private.suspensions[guid][spell] or 0
  end
  return 0
end

function private.clear_suspended_spell_on_unit(spell, unit)
  local guid = UnitGUID(unit)
  if guid then
    private.suspensions[guid] = private.suspensions[guid] or {}
    private.suspensions[guid][spell] = nil
    private.suspensions[guid][private.ALL_SPELLS] = nil
    private.suspension_timeouts[guid] = private.suspension_timeouts[guid] or {}
    private.suspension_timeouts[guid][spell] = nil
    private.suspension_timeouts[guid][private.ALL_SPELLS] = nil
  end
end

function private.suspend_spell_on_unit(spell, unit, duration)
  local guid = UnitGUID(unit)
  if guid then
    private.suspensions[guid] = private.suspensions[guid] or {}
    private.suspensions[guid][spell] = (private.suspensions[guid][spell] or 0) + 1
    private.suspension_timeouts[guid] = private.suspension_timeouts[guid] or {}
    private.suspension_timeouts[guid][spell] = GetTime() + duration
  end
end

function private.suspend_unit(unit, duration)
  return private.suspend_spell_on_unit(private.ALL_SPELLS, unit, duration)
end

function private.on_event(_, event)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _, spell_name, _, failed_type = CombatLogGetCurrentEventInfo()
    local current_unit = CombatCastCurrentUnit()
    local current_spell = CombatCastCurrentSpell()
    if current_unit and current_spell then
      if sub_event == "SPELL_CAST_FAILED" and source_guid == UnitGUID("player") and spell_name == current_spell then
        if failed_type == "A more powerful spell is already active" then
          local count = private.suspension_count(current_spell, current_unit)
          local duration = min(private.MORE_POWERFUL_SPELL_MAX_SUSPENSION_DURATION, math.pow(count, 2))
          --ALERT(string.format("Suspending '%s' => '%s' for %d second(s).", current_spell, current_unit, duration))
          private.suspend_spell_on_unit(current_spell, current_unit, duration)
        elseif failed_type == "Target needs to be in front of you." then
          --ALERT(string.format("Suspending '%s' => '%s' for %d second(s).", current_spell, current_unit, private.NOT_IN_FRONT_SUSPENSION_DURATION))
          private.suspend_spell_on_unit(current_spell, current_unit, private.NOT_IN_FRONT_SUSPENSION_DURATION)
        elseif failed_type == "You are unable to move" then
          --ALERT(string.format("Suspending all spells on '%s' for %d second(s).", current_spell, current_unit, private.NOT_IN_FRONT_SUSPENSION_DURATION))
          private.suspend_spell_on_unit(current_spell, current_unit, private.MOVEMENT_SUSPENSION_DURATION)
        elseif failed_type == "No path Available" then
          --ALERT(string.format("Suspending all spells on '%s' for %d second(s).", current_spell, current_unit, private.NOT_IN_FRONT_SUSPENSION_DURATION))
          private.suspend_spell_on_unit(current_spell, current_unit, private.PATH_SUSPENSION_DURATION)
        elseif failed_type == "Target not in line of sight" then
          --ALERT(string.format("Suspending all spells on '%s' for %d second(s).", current_spell, current_unit, private.NOT_IN_FRONT_SUSPENSION_DURATION))
          private.suspend_unit(current_unit, private.LINE_OF_SIGHT_SUSPENSION_DURATION)
        end
      elseif sub_event == "SPELL_CAST_SUCCESS" then
        private.clear_suspended_spell_on_unit(current_spell, current_unit)
      end
    end
  end
end

private.frame = CreateFrame("Frame", "rate_limiter", UIParent)
private.frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
private.frame:SetScript("OnEvent", private.on_event)

rate_limiter = {}
function rate_limiter.is_suspended(spell, unit)
  if not spell or not unit or not UnitExists(unit) then
    return false
  end
  local guid = UnitGUID(unit)
  if not guid then
    return false
  end
  private.suspension_timeouts[guid] = private.suspension_timeouts[guid] or {}
  return (private.suspension_timeouts[guid][spell] or 0) > GetTime()
end
