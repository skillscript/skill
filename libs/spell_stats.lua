local ZERO = ""
local FIRST_NUMBER = "[^%d]*(%d+)"
local SECOND_NUMBER = "[^%d]*%d+[^%d]*(%d+)"
local THIRD_NUMBER = "[^%d]*%d+[^%d]*%d+[^%d]*(%d+)"
local FOURTH_NUMBER = "[^%d]*%d+[^%d]*%d+[^%d]*%d+[^%d]*(%d+)"
local FIFTH_NUMBER = "[^%d]*%d+[^%d]*%d+[^%d]*%d+[^%d]*%d+[^%d]*(%d+)"
local DAMAGE = {
  -- Priest
  ["Holy Fire"] = FIRST_NUMBER,
  ["Holy Nova"] = FIRST_NUMBER,
  ["Holy Word: Chastise"] = FIRST_NUMBER,
  ["Mind Blast"] = FIRST_NUMBER,
  ["Penance"] = FIRST_NUMBER,
  ["Power Word: Solace"] = FIRST_NUMBER,
  ["Purge the Wicked"] = FIRST_NUMBER,
  ["Schism"] = FIRST_NUMBER,
  ["Shadow Word: Death"] = FIRST_NUMBER,
  ["Shadow Word: Pain"] = FIRST_NUMBER,
  ["Shadowfiend"] = FIRST_NUMBER,
  ["Smite"] = FIRST_NUMBER,

  -- Monk
  ["Chi Wave"] = FIRST_NUMBER,
  ["Tiger Palm"] = FIRST_NUMBER,
  ["Blackout Kick"] = FIRST_NUMBER,
  ["Blackout Strike"] = FIRST_NUMBER,
  ["Fists of Fury"] = FIRST_NUMBER,

  -- Mage
  ["Fire Blast"] = FIRST_NUMBER,
  ["Ice Lance"] = FIRST_NUMBER,

  -- Paladin
  ["Wake of Ashes"] = FIRST_NUMBER,
  ["Templar's Verdict"] = FIRST_NUMBER,
  ["Holy Prism"] = FIRST_NUMBER,
}
local DOT_DAMAGE = {
  ["Shadow Word: Pain"] = SECOND_NUMBER,
  ["Purge the Wicked"] = SECOND_NUMBER,
}
local DOT_DURATION = {
  ["Shadow Word: Pain"] = THIRD_NUMBER,
  ["Purge the Wicked"] = THIRD_NUMBER,
}
local HEAL = {
  -- Monk
  ["Chi Wave"] = SECOND_NUMBER,
  ["Essence Font"] = FIFTH_NUMBER,
  ["Renewing Mist"] = ZERO,
  ["Soothing Mist"] = ZERO,
  ["Enveloping Mist"] = ZERO,
  ["Vivify"] = FIRST_NUMBER,
  ["Healing Elixir"] = FIRST_NUMBER,
  ["Expel Harm"] = FIRST_NUMBER,
  --["Refreshing Jade Wind"] = FIFTH_NUMBER,

  -- Shaman
  ["Healing Wave"] = FIRST_NUMBER,
  ["Chain Heal"] = FIRST_NUMBER,
  ["Healing Surge"] = FIRST_NUMBER,

  -- Priest
  ["Shadow Mend"] = FIRST_NUMBER,
  ["Renew"] = FIRST_NUMBER,
  ["Flash Heal"] = FIRST_NUMBER,
  ["Holy Nova"] = SECOND_NUMBER,
  ["Heal"] = FIRST_NUMBER,
  ["Flash Heal"] = FIRST_NUMBER,
  ["Penance"] = SECOND_NUMBER,
  ["Power Word: Radiance"] = THIRD_NUMBER,
  ["Holy Word: Serenity"] = FIRST_NUMBER,
  ["Holy Word: Sanctify"] = THIRD_NUMBER,
  ["Prayer of Healing"] = THIRD_NUMBER,
  ["Circle of Healing"] = THIRD_NUMBER,

  -- Paladin
  ["Holy Light"] = FIRST_NUMBER,
  ["Light of the Martyr"] = FIRST_NUMBER,
  ["Light of Dawn"] = THIRD_NUMBER,
  ["Holy Shock"] = SECOND_NUMBER,
  ["Flash of Light"] = FIRST_NUMBER,
  ["Word of Glory"] = FIRST_NUMBER,
  ["Glimmer of Light"] = THIRD_NUMBER,
  ["Holy Prism"] = FIFTH_NUMBER,

  -- Druid
  ["Regrowth"] = FIRST_NUMBER,
  ["Swiftmend"] = FIRST_NUMBER,
}
local HOT_HEAL = {
  --["Enveloping Mist"] = ZERO,
}
local HOT_DURATION = {
  ["Renewing Mist"] = SECOND_NUMBER,
  ["Enveloping Mist"] = SECOND_NUMBER,
}
local ABSORB = {
  ["Power Word: Shield"] = SECOND_NUMBER,
}
local DAMAGE_ABSORB = {
  ["Mind Blast"] = SECOND_NUMBER,
}
local RANGE = {
  -- Paladin
  ["Blinding Light"] = FIRST_NUMBER,
  ["Essence Font"] = SECOND_NUMBER,
}
local function MatchSpell(spell, attribute)
  if attribute and attribute[spell] then
    if attribute[spell] == ZERO then
      return 0
    end
    --local id = select(7, GetSpellInfo(spell))
    local id = SpellId(spell)
    if id then
      local description = GetSpellDescription(id)
      if description then
        local no_commas = string.gsub(description, ",", "") or ""
        local result = tonumber(string.match(no_commas, attribute[spell]))
        if spell == "Renew" then
          result = result / 6
        elseif spell == "Healing Elixir" then
          result = PlayerHealthMax() * result
        end
        return result
      else
        ALERT_ONCE(string.format("GetSpellDescription('|cFFFF0000%s|r') returned nil.", spell))
      end
    else
      ALERT_ONCE(string.format("SpellId('|cFFFF0000%s|r') returned nil.", spell))
    end
  elseif attribute ~= RANGE then
    ALERT_ONCE(string.format("No spell_stat attribute for spell '|cFFFF0000%s|r'.", spell))
  end
  return 0
end
function DamageOfSpell(spell)
  return MatchSpell(spell, DAMAGE)
end
function DotDamageOfSpell(spell)
  return MatchSpell(spell, DOT_DAMAGE)
end
function DotDurationOfSpell(spell)
  return MatchSpell(spell, DOT_DURATION)
end
function HealOfSpell(spell)
  return MatchSpell(spell, HEAL)
end
function HotHealOfSpell(spell)
  return MatchSpell(spell, HOT_HEAL)
end
function HotDurationOfSpell(spell)
  return MatchSpell(spell, HOT_DURATION)
end
function AbsorbOfSpell(spell)
  return MatchSpell(spell, ABSORB)
end
--local function CostOfSpell(spell, resource)
--  return MatchSpell(spell, ABSORB)
--end



local tooltip = CreateFrame("GameTooltip", "tooltip", nil, "GameTooltipTemplate")
tooltip:SetOwner(WorldFrame, "ANCHOR_NONE")
tooltip:AddFontStrings(tooltip:CreateFontString("$parentTextLeft1", nil, "GameTooltipText"), tooltip:CreateFontString("$parentTextRight1", nil, "GameTooltipText"));
local tooltip_lines_result = {}
local function tooltip_lines()
  wipe(tooltip_lines_result)
  for i = 1, select("#", tooltip:GetRegions()) do
    local region = select(i, tooltip:GetRegions())
    if region and region:GetObjectType() == "FontString" then
      table.insert(tooltip_lines_result, region:GetText())
    end
  end
  return tooltip_lines_result
end
local function spell_tooltip_lines(spell)
  local id = SpellBookId(spell)
  local type = id and "spell" or "pet"
  --id = id or PetBookId(spell)
  if id then
    tooltip:ClearLines()
    tooltip:SetSpellBookItem(id, type)
    local result = tooltip_lines()
    if result[1] ~= spell then
      ALERT_ONCE("Reading tooltip for "..tostring(result[1]).." instead of "..tostring(spell))
    end
    return result
  end
  return nil
end

function RangeOfSpell(spell)
  if spell == "target" then
    return 1000
  elseif spell == "Attack" or spell == "Auto Attack" then
    return 2
  end
  local module = GetModule()
  if module and module.spell and module.spell.range then
    local range = module.spell.range(spell)
    if range then
      if range > 0 then
        return range
      else
        ALERT_ONCE(string.format("Module skill.modules.%s.%s.spell.range returned a 0 value. Please return a positive or nil value.", PlayerClass(), PlayerSpec()))
      end
    end
  end
  local range = select(6, GetSpellInfo(spell))
  if range and range > 0 then
    return range
  end
  local lines = spell_tooltip_lines(spell)
  if lines and type(lines) == "table" then
    for i, line in ipairs(lines) do
      local melee = string.match(line, "^(Melee Range)$")
      if melee then
        return 2
      end
      local number = string.match(line, "^(%d+) yd range$")
      if number then
        return tonumber(number)
      end
    end
  end
  local match = MatchSpell(spell, RANGE)
  if match > 0 then
    return match
  end
  return 0
end
function CostOfSpell(spell, resource)
  if spell == "target" or spell == "Attack" then
    return 0
  end
  local lines = spell_tooltip_lines(spell)
  if lines and type(lines) == "table" then
    for i, line in ipairs(lines) do
      local number = string.match(line, "^(%d+) "..resource.."$")
      if number then
        return tonumber(number)
      end
    end
  end
  return 0
end
function GenerationOfSpell(spell, resource)
  if spell == "target" or spell == "Attack" then
    return 0
  end
  local lines = spell_tooltip_lines(spell)
  if lines and type(lines) == "table" then
    for _, line in ipairs(lines) do
      local number = string.match(line, "Generates (%d+) "..resource)
      if number then
        return tonumber(number)
      end
    end
  end
  return 0
end

--spell_stats_frame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")


