_, skill = ...
if false then
local private = {}
function private.init()
  private.load()
end
private.frame = CreateFrame("Button", "skill_task_confirm_button", UIParent)--, "SecureHandlerClickTemplate")
private.frame:SetScript("OnEvent", private.init)
private.frame:RegisterEvent("ADDON_LOADED")
private.current_task = nil

--private.task_list = 1
--private.task_list_index = 1

-- WorldMapFrame:RemoveAllPinsByTemplate("EncounterJournalPinTemplate")

local function confirm()
  ALERT("confirmed")
  --private.task_list_index = private.task_list_index + 1
  --if private.task_list_index < #private.task_list then
  --  SwitchTask(private.task_list[private.task_list_index])
  --else
  --  ALERT("end")
  --end
  if private.current_task then
    skill.set_pixel_value("task_move_x", 0)
    skill.set_pixel_value("task_move_y", 0)
    SwitchTask(private.current_task:finished())
  end
end
function private.load()
  SetOverrideBindingClick(private.frame, true, "6", "skill_task_confirm_button")
  private.frame:SetScript("OnClick", confirm)
end

function SwitchTask(task)
  if task then
    if not task.update or type(task.update) ~= "function" then
      ALERT("no update or not a function")
      return nil
    elseif not task.finished or type(task.finished) ~= "function" then
      ALERT("no finished or not a function")
      return nil
    end
    private.current_task = task
    private.frame:SetScript("OnUpdate", function() private.current_task.update(private.current_task) end)
  else
    ALERT("finished")
    private.frame:SetScript("OnUpdate", nil)
  end
  return nil
end

function RustMove(x, y)
  if private.current_task then
    return nil
  end
  --local floor_x = math.floor(x)
  --local floor_y = math.floor(y)
  --local x_pre_dot = 2 ^ 16 * floor_x
  --local x_post_dot = 2 ^ 16 * math.floor((x - floor_x) * 100)
  --local y_pre_dot = 2 ^ 16 * floor_y
  --local y_post_dot = 2 ^ 16 * math.floor((y - floor_y) * 100)
  return {
    ["x"] = x,
    ["y"] = y,
    ["update"] = function(self)
      --ALERT(string.format("pos: (%d, %d)", UnitPosition("player")))
      --ALERT(string.format("pos: %f.3", player_orientation()))
      ALERT(string.format("rmu: (%d, %d)", self.x, self.y))
      skill.set_pixel_value("task_move_x", self.x)
      skill.set_pixel_value("task_move_y", self.y)
      return nil
    end,
    ["finished"] = function(self)
      return nil
    end,
  }
end

function RustMoveList(list)
  if private.current_task then
    return nil
  end
  return {
    ["index"] = 1,
    ["list"] = list,
    ["update"] = function(self)
      ALERT("rmlu")
      return self.index <= #self.list and self.list[self.index].update(self.list[self.index]) or nil
    end,
    ["finished"] = function(self)
      ALERT("rmlu finihsed")
      ALERT("self.index: "..tostring(self.index))
      ALERT("#self.list: "..tostring(#self.list))
      self.index = self.index + 1
      return self.index <= #self.list and self or nil
    end,
  }
end

function ExecuteTask(task)
  if task and not private.current_task then
    SwitchTask(task)
  end
end

local function create_task()
  local result = {}
  -- start
  --table.insert(result, RustMove(658, 746))
  --table.insert(result, RustMove(666, 735))
  --table.insert(result, RustMove(698, 668))
  --table.insert(result, RustMove(691, 574))

  -- garrison
  table.insert(result, RustMove(644, 444))
  table.insert(result, RustMove(646, 501))
  table.insert(result, RustMove(714, 491))
  table.insert(result, RustMove(692, 426))
  return result
end
TEST_TASK_LIST = create_task()
end
