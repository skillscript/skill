--------------------------------------------------------------------------------------
zPets = {}
local zpetsFrame = CreateFrame("FRAME")
local zpetsFunctions = {
	registeredSpawnScripts = {},
	registeredDespawnScripts = {},
}
--------------------------------------------------------------------------------------
-------------------------------------zPets Data---------------------------------------
--------------------------------------------------------------------------------------
local zpetsData = {
	["CurrentPets"] = {},
	["TheoryCraftingData"] = {},
	["Fel Firebolt SpellID"] = 104318,
	["Demonic Consumption SpellID"] = 267215,
	["Demonic Tyrant Extension Time"] = 15,
}

zpetsData.PetDurations = {
	["Unkown"] = 15, -- Default Pet Duration
	
	--------------------------------
	-------- Warlock Pets ----------
	--------------------------------
	-- Grimoire of Service --
	["Felguard"] = 28,
	["Felhunter"] = 28,
	["Voidwalker"] = 28,
	["Succubus"] = 28,
	["Felhunter"] = 28,
	["Imp"] = 27,
	
	-- Base Demo Pets --
	["Dreadstalker"] = 12.25,
	["Wild Imp"] = 20,
	["Demonic Tyrant"] = 15,
	
	-- Demo Talent Pets --
	["Vilefiend"] = 15,
	
	-- Nether Portal --	
	["Vicious Hellhoud"] = 15,
	["Bilescourge"] = 15,
	["Darkhound"] = 15,	
	["Illidari Satyr"] = 15,
	["Shivarra"] = 15,
	["Ur'zul"] = 15,
	["Void Terror"] = 15,
	["Wrathguard"] = 15,
	["Eye of Gul'dan"] = 15,
}

zpetsData.PetEnergy = {
	["Unkown"] = 1, -- Default Pet Energy
	
	--------------------------------
	-------- Warlock Pets ----------
	--------------------------------	
	-- Base Demo Pets --
	["Wild Imp"] = 5,	
}

--------------------------------------------------------------------------------------
-------------------------------------zPets API----------------------------------------
--------------------------------------------------------------------------------------
function zPets.IsPetTypeActive(petType)	

	for key, petTable in pairs (zpetsData.CurrentPets) do
		if petTable.name == petType then
			return true
		end
	end
	
	return false
end
function zPets.NumPetTypeActive(petType)

	local result = 0
	for key, petTable in pairs (zpetsData.CurrentPets) do
		if petTable.name == petType then
			result = result + 1
		end
	end

	return result
end
--------------------------------------------------------------------------------------
function zPets.GetPetName(petGUID)	

	if zpetsData.CurrentPets[petGUID] then
		return zpetsData.CurrentPets[petGUID].name
	end
	
	return "Unkown"
end
--------------------------------------------------------------------------------------
function zPets.GetPetDurationInfo(petIdentifier) --Either Pet Name (Will return first one it finds, or Pet GUID)
	
	local petDuration = 0
	local petDurationRemaining = 0
	local petDespawnTime = 0
	local petTable = {}
	
	if zpetsData.CurrentPets[petIdentifier] then
		petTable = zpetsData.CurrentPets[petIdentifier]
	else
		local hasPetName = false
		for petGUID, petTable in pairs (zpetsData.CurrentPets) do
			if petTable.name == petIdentifier then
				petIdentifier = petGUID
				hasPetName = true
			end
		end
		if hasPetName then
			petTable = zpetsData.CurrentPets[petIdentifier]
		else		
			return petDuration, petDurationRemaining, petDespawnTime
		end
	end	
	
	local estimatedExpirationTime = 0
	
	if petTable.estimatedDespawnTime < petTable.despawnTime then
		estimatedExpirationTime = petTable.estimatedDespawnTime
	else
		estimatedExpirationTime = petTable.despawnTime
	end

	petDuration = GetTime() - petTable.spawnTime
	petDurationRemaining = estimatedExpirationTime - GetTime()
	
	return petDuration, petDurationRemaining, estimatedExpirationTime
end
--------------------------------------------------------------------------------------
function zPets.GetPetEnergy(petGUID)
	
	local petEnergy = 0
	
	if zpetsData.CurrentPets[petGUID] then
		petEnergy = zpetsData.CurrentPets[petGUID].currentEnergy
	end
	
	return petEnergy
end
--------------------------------------------------------------------------------------
function zPets.RegisterPetEvent(event, script)
	--'OnSpawn' will send petGUID, petTable as arguments	
	if event == "OnSpawn" then
		table.insert(zpetsFunctions.registeredSpawnScripts, script)
		
	--'OnDespawn' will send petGUID as a argument	
	elseif event == "OnDespawn" then
		table.insert(zpetsFunctions.registeredDespawnScripts, script)
	end
end
--------------------------------------------------------------------------------------
function zPets.PrintTheorycraftingData()
	print("---------------------------------------------------------")
	print("------Printing Collected Guardian Data-------")
	print("---------------------------------------------------------")
	for petName, petData in pairs (zpetsData.TheoryCraftingData) do		
		print(string.format("--%s--", petName))
		print(string.format("----| Initial Cast Start Low: %s", petData.lowestInitialCastTime))
		print(string.format("----| Initial Cast Start High: %s", petData.highestInitialCastTime))
		print(string.format("----| Recast Start Low: %s", petData.lowestRecastTime))
		print(string.format("----| Recast Start High: %s", petData.highestRecastTime))

	end
	print("---------------------------------------------------------")
end
--------------------------------------------------------------------------------------
-------------------------------zPets Run Time Code------------------------------------
--------------------------------------------------------------------------------------
function zpetsFunctions.AddPetToTracking(petGUID, petName)
	
	-- Get Default Pet Data
	local petDuration = zpetsData.PetDurations[petName] or zpetsData.PetDurations["Unkown"]
	local petMaxEnergy = zpetsData.PetEnergy[petName] or zpetsData.PetEnergy["Unkown"]
	
	-- Build petTable
	local petTable = {
		name = petName,
		spawnTime = GetTime(),
		despawnTime = GetTime() + petDuration,
		currentEnergy = petMaxEnergy,
		maxEnergy = petMaxEnergy,
		estimatedDespawnTime = GetTime() + petDuration,
		estimatedSpellCastComplete = GetTime(),
		lastSpellCastComplete = 0,
		previousSpellCastComplete = 0, 
		initialSpellCastStart = 0, 
		initialSpellCastComplete = 0, 
	}	
	
	-- Add petTable to zpetsData
	zpetsData.CurrentPets[petGUID] = petTable
	
	-- Send Event to registered scripts
	for key, script in pairs (zpetsFunctions.registeredSpawnScripts) do
		local localScript = script
		localScript(petGUID)
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.RemoveExpiredPetsFromTracking()

	local GUIDsForRemoval = {}
	for key, petTable in pairs (zpetsData.CurrentPets) do
		
		-- Find pets that have expired
		if GetTime() >= petTable.despawnTime then
			table.insert(GUIDsForRemoval, key)
		end
		
		-- Find Wild Imps that have reached 0 energy
		if petTable.currentEnergy <= 0 then
			table.insert(GUIDsForRemoval, key)
		end		
	end
	
	for key, GUIDForRemoval in pairs (GUIDsForRemoval) do		
		if zpetsData.CurrentPets[GUIDForRemoval] then			
			-- Send Event to registered scripts
			for key, script in pairs (zpetsFunctions.registeredDespawnScripts) do
				local localScript = script
				localScript(GUIDForRemoval)
			end
			
			zpetsFunctions.RemovePetFromTracking(GUIDForRemoval)
		end
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.RemoveSacrificedWildImpsFromTracking(spellName)
	
	local GUIDsForRemoval = {}
	
	if (spellName == "Implosion") or (spellName == "Summon Demonic Tyrant" and IsPlayerSpell(zpetsData["Demonic Consumption SpellID"])) then
		-- All active Wild Imps are food for the warlock god...you!
		for key, petTable in pairs (zpetsData.CurrentPets) do
			if petTable.name == "Wild Imp" then
				table.insert(GUIDsForRemoval, key)
			end
		end	
	end	
	
	if (spellName == "Power Siphon") then
		-- Remove the two lowest duration Wild Imps from tracking
		local impForDespawn1 = ""
		local impForDespawn2 = ""
		local impForDespawn1Duration = 120 -- Arbitrary high number
		local impForDespawn2Duration = 120
		
		for key, petTable in pairs (zpetsData.CurrentPets) do
			local petDuration, petDurationRemaining, estimatedExpirationTime = zPets.GetPetDurationInfo(key)
			
			if petTable.name == "Wild Imp" then				
				if petDurationRemaining < impForDespawn1Duration then
					impForDespawn2 = impForDespawn1
					impForDespawn2Duration = impForDespawn1Duration					
					impForDespawn1 = key
					impForDespawn1Duration = petDurationRemaining
				elseif (petDurationRemaining > impForDespawn1Duration) and (petDurationRemaining < impForDespawn2Duration) then
					impForDespawn2 = key
					impForDespawn2Duration = petDurationRemaining				
				end				
			end	
			
		end	
		
		if not (impForDespawn1 == "") then
			table.insert(GUIDsForRemoval, impForDespawn1)
		end
		
		if not (impForDespawn2 == "") then
			table.insert(GUIDsForRemoval, impForDespawn2)
		end
	end	
	
	for key, GUIDForRemoval in pairs (GUIDsForRemoval) do		
		if zpetsData.CurrentPets[GUIDForRemoval] then			
			-- Send Event to registered scripts
			for key, script in pairs (zpetsFunctions.registeredDespawnScripts) do
				local localScript = script
				localScript(GUIDForRemoval)
			end
			
			zpetsFunctions.RemovePetFromTracking(GUIDForRemoval)
		end
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.RemovePetFromTracking(GUIDForRemoval)

	zpetsData.CurrentPets[GUIDForRemoval] = nil
end
--------------------------------------------------------------------------------------
function zpetsFunctions.GUIDIsPlayerOrPet(testGUID)
	
	for key, petTable in pairs (zpetsData.CurrentPets) do
		if testGUID == key then
			return true, "guardian"
		end
	end
	
	if testGUID == UnitGUID("player") then
		return true, "player"
	end
	
	if testGUID == UnitGUID("pet") then
		return true, "pet"
	end
	
	return false	
end
--------------------------------------------------------------------------------------
function zpetsFunctions.UpdateUnkownGuardians(petGUID, petName)
	
	if not zpetsData.CurrentPets[petGUID] then
		return
	end
	
	local petTable = zpetsData.CurrentPets[petGUID]
	
	-- Update pet if the name was 'Unkown' on spawn (seems to happen sometimes)
	if (petTable.name == "Unkown") and (petName ~= "Unkown") then
		if zpetsData.PetDurations[petName] then
			print("zPets Debug: Updated Uknown Pet")
			petTable.name = petName
			petTable.despawnTime = petTable.SpawnTime + zpetsData.PetDurations[petName]
		end
	end	
end
--------------------------------------------------------------------------------------
function zpetsFunctions.UpdateWildImpEstimates(eventType, petGUID, spellID)

	if (eventType == "SPELL_CAST_START") and (spellID == zpetsData["Fel Firebolt SpellID"]) then	
		local petTable = zpetsData.CurrentPets[petGUID]		
		local hasteMod = ((UnitSpellHaste("player")/100)+1)
		local _, _, _, castTime = GetSpellInfo(zpetsData["Fel Firebolt SpellID"])
		local castTime = castTime/1000
		
		if zPets.IsPetTypeActive("Demonic Tyrant") then
			--print("tyrant active, pet cast fel firebolt")
			local _, tyrantRemaining = zPets.GetPetDurationInfo("Demonic Tyrant")
			
			if castTime > tyrantRemaining then	
				petTable.currentEnergy = petTable.currentEnergy - 1
				petTable.estimatedDespawnTime = GetTime() + (zPets.GetPetEnergy(petGUID)*castTime) + 0.01				
			else
				local freeTyrantTime = math.floor(tyrantRemaining / castTime)*castTime				
				petTable.estimatedDespawnTime = GetTime() + (zPets.GetPetEnergy(petGUID)*castTime) + freeTyrantTime + 0.01 - castTime
			end			
		else			
			--Estimated Despawn Time = Number of Casts Remaining * Current Cast time + average delay between cast ending and actual despawn
			petTable.estimatedDespawnTime = GetTime() + (zPets.GetPetEnergy(petGUID)*castTime) + 0.01				
		end
		
		--[[Track when spell cast should finish to see if the cast doesn't complete which would 
			indicate that the target has died and that the estimate should go back to real duration remaining]]--
		petTable.estimatedSpellCastComplete = GetTime() + castTime + 0.1	
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.UpdatePetData(eventType, petGUID, spellID)
	if (eventType == "SPELL_CAST_SUCCESS") then
		if zpetsData.CurrentPets[petGUID].lastSpellCastComplete > 0 then
			zpetsData.CurrentPets[petGUID].previousSpellCastComplete = zpetsData.CurrentPets[petGUID].lastSpellCastComplete
		end		
		zpetsData.CurrentPets[petGUID].lastSpellCastComplete = GetTime()
		
		if (zpetsData.CurrentPets[petGUID].initialSpellCastComplete == 0) then
			zpetsData.CurrentPets[petGUID].initialSpellCastComplete = GetTime()
		end
	end
	if (eventType == "SPELL_CAST_START") then
		if (zpetsData.CurrentPets[petGUID].initialSpellCastStart == 0) then
			zpetsData.CurrentPets[petGUID].initialSpellCastStart = GetTime()
		end
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.UpdatePetDurationsOnDemonicTyrant()
	
	for petGUID, petTable in pairs (zpetsData.CurrentPets) do
		
		-- Updated Despawn Time
		petTable.despawnTime = petTable.despawnTime + zpetsData["Demonic Tyrant Extension Time"]
		
		--[[ Wild Imps are different than other pets, they have their cost of Fel Firebold reduced to 0 while 
		Demonic Tyrant is active, so we'll estimate a new time based on energy remaining and time until Demonic Tyrant
		despawns. ]]--
		if petTable.name == "Wild Imp" then
			local hasteMod = ((UnitSpellHaste("player")/100)+1)
			local _, _, _, castTime = GetSpellInfo(zpetsData["Fel Firebolt SpellID"])
			local castTime = castTime/1000
			local estimatedDespawnTime = GetTime() + (zPets.GetPetEnergy(petGUID)*castTime) + 0.01 + zpetsData["Demonic Tyrant Extension Time"]

			if estimatedDespawnTime > petTable.despawnTime then
				petTable.estimatedDespawnTime = petTable.despawnTime
			else
			
			end			
		else
			petTable.estimatedDespawnTime = petTable.despawnTime
		end	
	end
	
end
--------------------------------------------------------------------------------------
function zpetsFunctions.CollectPetDataForTheorycrafting(petGUID, petName, eventType, timeStamp)
	
	zpetsFunctions.AddPetTypeToTheorycraftingData(petName)
	
	if eventType == "SPELL_CAST_START" then -- Track guardians with cast time spells
		-- Track the initial Cast Start variance		
		if (zpetsData.CurrentPets[petGUID].initialSpellCastStart > 0) and (zpetsData.CurrentPets[petGUID].initialSpellCastComplete == 0) then
			local initalCastPeriod = zpetsData.CurrentPets[petGUID].initialSpellCastStart - zpetsData.CurrentPets[petGUID].spawnTime
			
			if zpetsData.TheoryCraftingData[petName].lowestInitialCastTime > initalCastPeriod then
				zpetsData.TheoryCraftingData[petName].lowestInitialCastTime = initalCastPeriod
				--print(string.format("New Lowest Initial Cast Time for %s: %s", petName, initalCastPeriod))
			end
			if zpetsData.TheoryCraftingData[petName].highestInitialCastTime < initalCastPeriod then
				zpetsData.TheoryCraftingData[petName].highestInitialCastTime = initalCastPeriod
				--print(string.format("New Highest Initial Cast Time for %s: %s", petName, initalCastPeriod))
			end
		end
		
		-- Track the subsequent Cast Start variance
		if (zpetsData.CurrentPets[petGUID].initialSpellCastComplete > 0) then
			local castPeriod = GetTime() - zpetsData.CurrentPets[petGUID].lastSpellCastComplete
			
			if zpetsData.TheoryCraftingData[petName].lowestRecastTime > castPeriod then
				zpetsData.TheoryCraftingData[petName].lowestRecastTime = castPeriod
				--print(string.format("New Lowest Recast Time for %s: %s", petName, castPeriod))
			end
			if zpetsData.TheoryCraftingData[petName].highestRecastTime < castPeriod then
				zpetsData.TheoryCraftingData[petName].highestRecastTime = castPeriod
				--print(string.format("New Highest Recast Time for %s: %s", petName, castPeriod))
			end 
		end
	end
	
	if eventType == "SPELL_CAST_SUCCESS" then -- Track guardians with instant cast spells
		if (zpetsData.CurrentPets[petGUID].initialSpellCastStart == 0) then
			if zpetsData.CurrentPets[petGUID].previousSpellCastComplete > 0 then
				-- This is a subsequent cast, do math accordingly 
				local castPeriod = GetTime() - zpetsData.CurrentPets[petGUID].previousSpellCastComplete
			
				if zpetsData.TheoryCraftingData[petName].lowestRecastTime > castPeriod then
					zpetsData.TheoryCraftingData[petName].lowestRecastTime = castPeriod
					--print(string.format("New Lowest Recast Time for %s: %s", petName, castPeriod))
				end
				if zpetsData.TheoryCraftingData[petName].highestRecastTime < castPeriod then
					zpetsData.TheoryCraftingData[petName].highestRecastTime = castPeriod
					--print(string.format("New Highest Recast Time for %s: %s", petName, castPeriod))
				end
			else
				-- This is the first successful instant cast
				local initalCastPeriod = GetTime() - zpetsData.CurrentPets[petGUID].spawnTime
			
				if zpetsData.TheoryCraftingData[petName].lowestInitialCastTime > initalCastPeriod then
					zpetsData.TheoryCraftingData[petName].lowestInitialCastTime = initalCastPeriod
					--print(string.format("New Lowest Initial Cast Time for %s: %s", petName, initalCastPeriod))
				end
				if zpetsData.TheoryCraftingData[petName].highestInitialCastTime < initalCastPeriod then
					zpetsData.TheoryCraftingData[petName].highestInitialCastTime = initalCastPeriod
					--print(string.format("New Highest Initial Cast Time for %s: %s", petName, initalCastPeriod))
				end
			end		
		end
	end

end
--------------------------------------------------------------------------------------
function zpetsFunctions.AddPetTypeToTheorycraftingData(petName)
	if not (zpetsData.TheoryCraftingData[petName]) then
		local theorycraftingTable = {
			lowestInitialCastTime = 15,
			highestInitialCastTime = 0,
			lowestRecastTime = 15,
			highestRecastTime = 0,		
		}
		
		zpetsData.TheoryCraftingData[petName] = theorycraftingTable
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.CheckIfWildImpCastFailed()
	
	--[[ Checking to see if the current time hast past when the Wild Imp should have been able to finish their cast,
	This would mean their target likely died or is out of range and that the Wild Imp is no longer casting ]]--
	for key, petTable in pairs (zpetsData.CurrentPets) do
		if petTable.name == "Wild Imp" then
			if (GetTime() >= petTable.estimatedSpellCastComplete) and (petTable.estimatedDespawnTime < petTable.despawnTime) then
				petTable.estimatedDespawnTime = petTable.despawnTime
			end
		end
	end
end
--------------------------------------------------------------------------------------
function zpetsFunctions.OnUpdate()
	if true then
		return
	end
	-- Upkeep Functions
	zpetsFunctions.RemoveExpiredPetsFromTracking()
	zpetsFunctions.CheckIfWildImpCastFailed()
end
--------------------------------------------------------------------------------------
function zpetsFunctions.OnEvent()
	if true then return	end
	
	local timeStamp, eventType, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, targetGUID, targetName, destFlags, arg11, spellID, spellName, destRaidFlags, spellDamage, arg16, arg17, arg18, arg19, arg20 = CombatLogGetCurrentEventInfo()
	local isPlayerOrPet, GUIDType = zpetsFunctions.GUIDIsPlayerOrPet(sourceGUID)
	
	if not isPlayerOrPet then -- We don't care about anyone besides ourselves, so lets just ignore other people. 
		return
	end
	
	-----------------------------------------------
	------------Add Pet To Tracking----------------
	-----------------------------------------------
	if (eventType == "SPELL_SUMMON") and not (targetGUID == UnitGUID("pet")) then
		zpetsFunctions.AddPetToTracking(targetGUID, targetName)
	end	
	
	-- -- Update any guardians that may have spawned with "Unkown" as a name
	if GUIDType == "guardian" then
		zpetsFunctions.UpdateUnkownGuardians(sourceGUID, petName)
	end
	
	-- Update Pet Data
	if GUIDType == "guardian" then
		zpetsFunctions.UpdatePetData(eventType, sourceGUID, spellID)
	end
	-----------------------------------------------
	-----------Warlock Special Cases---------------
	-----------------------------------------------	
		
	-- Update Durations Based on Demonic Tyrant
	if spellName == "Summon Demonic Tyrant" and eventType == "SPELL_CAST_SUCCESS" then
		zpetsFunctions.UpdatePetDurationsOnDemonicTyrant()
	end
	
	-- Will handle pet clean up on these spells being cast since their despawn isn't tracked in the combat log
	if eventType == "SPELL_CAST_SUCCESS" then
		zpetsFunctions.RemoveSacrificedWildImpsFromTracking(spellName) -- This will act on specific spells that could despawn Wild Imps
	end
	
	-- Manage Pet Duration Special Cases
	if sourceName == "Wild Imp" then	
		zpetsFunctions.UpdateWildImpEstimates(eventType, sourceGUID, spellID)
		
		if eventType == "SPELL_CAST_SUCCESS" and not zPets.IsPetTypeActive("Demonic Tyrant") then
			zpetsData.CurrentPets[sourceGUID].currentEnergy = zpetsData.CurrentPets[sourceGUID].currentEnergy - 1
		end
	end
	-----------------------------------------------
	------Data Collection For Theorycrafting-------
	-----------------------------------------------

	if (GUIDType == "guardian") then
		zpetsFunctions.CollectPetDataForTheorycrafting(sourceGUID, sourceName, eventType, timeStamp)
	end	
	
end
--------------------------------------------------------------------------------------
function zpetsFunctions.DeepCopy(orig)

    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[zpetsFunctions.DeepCopy(orig_key)] = zpetsFunctions.DeepCopy(orig_value)
        end
        setmetatable(copy, zpetsFunctions.DeepCopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
--------------------------------------------------------------------------------------
zpetsFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
zpetsFrame:SetScript("OnEvent", zpetsFunctions.OnEvent)
zpetsFrame:SetScript("OnUpdate", zpetsFunctions.OnUpdate)
