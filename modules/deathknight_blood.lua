_, skill = ...
local damageLog = {}
local lastFive = 0
local excludedSpellIds = {}
local function trackDamageNew(event, _, subEvent, _, _, _, _, _, destGUID, _, _, _, ...)
  --ALERT("tdn")
  --ALERT(select(2, CombatLogGetCurrentEventInfo()))
  --ALERT(select(8, CombatLogGetCurrentEventInfo()))
  --local params = {CombatLogGetCurrentEventInfo()}
  --local event, _, subEvent, _, _, _, _, _, destGUID, _, _, _ = params
  if destGUID then
  ALERT(destGUID)
  end
  --if select(8, params) == UnitGUID("player") then
  --  ALERT("ITSME")
  --end
  if (destGUID ~= UnitGUID("player")) then
    ALERT("Not player")
    return;
  end

  if (subEvent == "SPELL_HEAL") then
    -- print debug messages if spell heal
    --if (aura_env.config.debug and select(1, ...) == 45470) then
    --  local amt = select(4, ...);
    --  local est = math.floor(aura_env.estimated + 0.5);

    --  print('|cffdff442Expected:|r', est, '|cffdff442Got:|r', amt);
    --  print('|cffdff442Off By:|r', string.format("%.2f", (100 * amt / est) - 100), "%");
    --end
    if subEvent then
      ALERT(subEvent)
    end
  elseif (subEvent == "SWING_DAMAGE") then
    return {GetTime(), (select(1, ...))}
    --aura_env.AddDamageEvent(select(1, ...), subevent);
  elseif (subEvent == "SPELL_ABSORBED") then
    if (select(9, ...)) then
      -- spell absorbed, check the spellId
      local spellId = select(1, ...);
      if (excludedSpellIds[spellId]) then
        return nil;
      end
      return {GetTime(), (select(11, ...))}
      --aura_env.AddDamageEvent(select(11, ...), subEvent);
      --return;
    end

    -- melee absorbed
    return {GetTime(), (select(8, ...))}
    --aura_env.AddDamageEvent(select(8, ...), subEvent);
  else
    -- SPELL_ and RANGE_
    -- check for excluded spellids before moving the offset
    local spellId = select(1, ...);
    if excludedSpellIds[spellId] then
      return nil;
    end
    return {GetTime(), (select(4, ...))}
    --aura_env.AddDamageEvent(select(4, ...), subEvent);
  end
end
function trackDamage(event, time, subevent, ...)
  --ALERT("td called")
  --ALERT(time)
  --ALERT(subevent)
  --ALERT(select(6, ...) )
  local res = nil
  local eType = select(2, ...)
  --if not (eType == "SPELL_PERIODIC_DAMAGE" or eType == "SPELL_DAMAGE" or eType == "SWING_DAMAGE") then return res end

  if select(6, ...) ~= UnitGUID("player") then return res end
  --ALERT("destName " .. tostring(destGUID))
  --ALERT("playerId " .. tostring(UnitGUID("player"))
  local amount, overkill, school, resisted, blocked, absorbed = select(14, ...)
  --ALERT(amount)
  --set selection offset to amount for baseline SWING_DAMAGE
  local offset = 10

  --handle SPELL_ABSORBED events
  if subevent == "SPELL_ABSORBED" then
    --if a spell gets absorbed, there are 3 additional parameters regarding which spell got absorbed, so move the offset 3 more places
    if GetSpellInfo((select(offset, ...))) == (select(offset + 1, ...)) then
      offset = offset + 3
    end

    --absorb value is 7 places further
    offset = offset + 7
    --table.insert(damageLog, {GetTime(), (select(offset, ...))})
    res = {GetTime(), (select(offset, ...))}

    --handle regular XYZ_DAMAGE events
  elseif subevent:find("_DAMAGE") then
    --don't include environmental damage (like falling etc)
    if not subevent:find("ENVIRONMENTAL") then
      --move offset by 3 places for spell info for RANGE_ and SPELL_ prefixes
      if subevent:find("SPELL") then
        offset = offset + 3
      elseif subevent:find("RANGE") then
        offset = offset + 3
      end
      --ALERT((select(offset, ...)))
      res = {GetTime(), (select(offset, ...))}
    end
  end
  return res
end


local function newLogByTrim(log, seconds)
  local cutoff = GetTime() - seconds
  local newLog = {}
  for i=1, #log do
    if log[i][1] > cutoff then
      table.insert(newLog, log[i])
    end
  end
  return newLog
end

local function sumLogValues(log)
  local res = 0
  local buf
  for i=1, #log do
    buf = log[i][2]
    if buf ~= nil then
      res = res + buf
    end
  end
  return res
end

function trackDeathStrikeHealableEvents(self, event, ...)
  -- tracking damage
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    found = trackDamage(event, CombatLogGetCurrentEventInfo())
    if found ~= nil then
      table.insert(damageLog, found)
    end
    --safety first
    --ALERT("last five: "..tostring(lastFive))
    if #damageLog > 10000 then
      damageLog = {}
    end
  end
end


local function IsOffGlobal(name)
  return
  name == "Icebound Fortitude" or
  name == "Will to Survive" or
  name == "Healthstone" or
  name == "Mind Freeze"
end

local function IsStun(spell)
  return spell == "Asphyxiate"
end

local function IsDisorient(spell)
  return false
end

local function IsIncapacitate(spell)
  return false
end

local function IsSilence(spell)
  return false
end

local function IsInterrupt(spell)
  return spell == "Mind Freeze"
end

local function runesInterpolated()
  local runeAmount = 0
  local lowestRuneCD = 9999
  local partialRune = 0
  for i = 1, 6 do
    local start, duration, runeReady = GetRuneCooldown(i)
    local remaining = start + duration - GetTime()
    if runeReady == true then
      runeAmount = runeAmount + 1
    elseif lowestRuneCD >= remaining then
      lowestRuneCD = remaining
      partialRune = 1 - remaining / duration
    end
  end

  return runeAmount + partialRune
end

local function deathStrikeHeal(dmg)
  local voraciousSelected = IsTalentSelected("Voracious")
  local voraciousMod = voraciousSelected and 1.2 or 1
  local minimumHealPercentage = voraciousSelected and 0.105 or 0.07
  local hemoMod = 1 + 0.08 * PlayerAuraStacks("Hemostasis")
  local vampiricBloodMod = PlayerAuraDuration("Vampiric Blood") > 0 and 1.35 or 1
  local versatilityMod = 1 + ((GetCombatRatingBonus(29) + GetVersatilityBonus(30)) / 100)
  local baseHealMod = voraciousMod * versatilityMod * vampiricBloodMod * hemoMod
  local voraciousMinMod = vampiricBloodMod * hemoMod
  if voraciousSelected then
    return max(dmg * 0.25 * baseHealMod, minimumHealPercentage * UnitHealthMax("player") * voraciousMinMod)
  else
    return max(dmg * 0.25, minimumHealPercentage * UnitHealthMax("player")) * baseHealMod
  end

end

function getBloodLastFive()
  return lastFive
end
function currentDeathStrikeHeal()
  return deathStrikeHeal(lastFive)
end

------------------------------------------------------------------------
local spellTarget = nil
local spellName = nil
local function SafeTryCastSpellOnUnit(spell, unit)
  local can = CanCastSpellOnUnit(spell, unit)
  if can then
    spellTarget = unit
    spellName = spell
  end
  return can
end
local function SafeTryCastSpellOnTarget(spell)
  local can = CanCastSpellOnUnit(spell, "target")
  if can then
    spellTarget = "target"
    spellName = spell
  end
  return can
end

local function SafeTryCastSpellOnPlayer(spell)
  local can = CanCastSpellOnUnit(spell, "player")
  if can then
    spellTarget = "player"
    spellName = spell
  end
  return can
end
------------------------------------------------------------------------
local function GetRunicPower()
  return UnitPower("player", Enum.PowerType.RunicPower)
end
------------------------------------------------------------------------
local function tryMarrowrend()

  return (runesInterpolated() >= 2 and SafeTryCastSpellOnTarget("Marrowrend")) or nil
end

local function tryDeathStrike()
  return (GetRunicPower() >= 45 and SafeTryCastSpellOnTarget("Death Strike")) or nil
end

local function tryBloodBoil()
  local result = IsUnitInRange("Marrowrend", "target") and GetCharges("Blood Boil") >= 1 and SafeTryCastSpellOnPlayer("Blood Boil")
  return result
end

local function tryHeartStrike()
  return (runesInterpolated() >= 1 and SafeTryCastSpellOnTarget("Heart Strike")) or nil
end

------------------------------------------------------------------------
local function keepBoneShield(minStacks)
  local bsStacks = PlayerAuraStacks("Bone Shield")
  local bsDur = PlayerAuraDuration("Bone Shield")
  if ((bsStacks < minStacks and runesInterpolated() >= 2) or bsDur <= 5.15 and runesInterpolated() >= 3) then
    return tryMarrowrend()
  end
end

local function keepBloodPlague()
  local bpDur = TargetMyAuraDuration("Blood Plague")
  return (bpDur <= 2 and tryBloodBoil()) or nil
end

local function dumpBloodBoil()
  return (GetCharges("Blood Boil") >= 1 and GetChargeCD("Blood Boil") < HastedSeconds(1.5) and tryBloodBoil()) or nil
end

local function expectedHeartStrikeRP(expectedTargets)
  return 10 + 5 + (PlayerAuraDuration("Death and Decay") >= 0.15 and IsTalentSelected("Heartbreaker") and max(expectedTargets, 5) * 2 or 0)
end

local function dndHeartStrike(expectedTargets)
  local rpGain = expectedHeartStrikeRP(expectedTargets)
  if PlayerAuraDuration("Death and Decay") >= 0.15
      and GetRunicPower() + rpGain <= 125 then
    return tryHeartStrike()
  end
end

local function dumpIntoMR()
  if runesInterpolated() >= 4.0 and GetRunicPower() <= 100 then
    return keepBoneShield(7)
  end
end

local function defensiveDS(heal, currentHealthLEQ)
  if currentDeathStrikeHeal() / UnitHealthMax("player") >= heal
      and UnitHealth("player") / UnitHealthMax("player") <= currentHealthLEQ then
    return tryDeathStrike()
  end
end

local function offGlobal()
end

local function GetRunes()
  return UnitPower("player", Enum.PowerType.Runes)
end
local function GetRunicPower()
  return UnitPower("player", Enum.PowerType.RunicPower)
end

local function PreventBoneShieldDropping()
  return
  PlayerAuraDuration("Bone Shield") < GlobalCD() + GlobalCDMax() and
  SafeTryCastSpellOnTarget("Marrowrend")
end
local function HandleDeathStrike()
  return
  GetRunicPower() >= 45 and
  PlayerAuraDuration("Blood Shield") < GlobalCD() + GlobalCDMax() and
  SafeTryCastSpellOnTarget("Death Strike") or
  nil
end

function urgentDeathStrike()
  local rp = GetRunicPower()
  if defensiveDS(0.17, 0.7) or -- basically take any empowered heal below 2/3
      rp >= 60 and defensiveDS(0.17, 0.85) then
    return "2"
  end
  return "1"
end

local function semiAuto()
  local recentHPLossRelative = UnitHealthLost("player", 5)/UnitHealthMax("player")
  local hpRelative = UnitHealth("player")/UnitHealthMax("player")
  local rp = GetRunicPower()
  local rpMissing = UnitPowerMax("player") - rp
  local runes = runesInterpolated()
  local enemiesWithin8 =  skill.NumEnemiesWithin(8)
  local result =
    -- urgent defense
    defensiveDS(0.30, 0.7) or
    defensiveDS(0.25, 0.6) or
    defensiveDS(0.20, 0.5) or -- basically take any empowered heal below half
    rp >= 90 and defensiveDS(0.25, 0.70) or
    hpRelative < 0.4 and SafeTryCastSpellOnTarget("Blooddrinker") or

    -- queue
    SafeTryCastSpellOnUnit(GetQueuedSpell(), GetQueuedUnit()) or

    -- urgent offense (threat)
    enemiesWithin8 >= 3 and hpRelative > 0.8 and keepBloodPlague() or
    enemiesWithin8 >= 3 and hpRelative > 0.8 and dumpBloodBoil() or

    -- basic defense
    keepBoneShield(enemiesWithin8 >= 3 and 4 or 2) or
    rp >= 85 and defensiveDS(0.20, 0.75) or -- threshold with DS legendary
    rp >= 115 and defensiveDS(0.14, 0.75) or -- use hemo stacks near cap
    hpRelative < 0.75 and SafeTryCastSpellOnTarget("Blooddrinker") or
    -- keepHemostasis()...

    -- resource management
    -- --
    rp < 45 and runesInterpolated() >= 1.8 and dndHeartStrike(enemiesWithin8) or
    -- -- using rp
    rpMissing <= 20 and tryDeathStrike() or
    -- -- making best use of excess runes
    runes > 2.8 and  tryHeartStrike() or
    runes > 2.8 and rpMissing <= 20 and keepBoneShield(enemiesWithin8 >= 3 and 7 or 5) or
    runes >= 1 and rpMissing <= expectedHeartStrikeRP(enemiesWithin8) and tryHeartStrike() or
    keepBloodPlague() or
    --runes > -- combine expectedHeartStrikeRP, rp, runes, hpRelative, cooldowns, etc to decide how to spend runes
    dumpBloodBoil() or
    --]]
    "nothing"
  return result
end

local private = {}
private.priorities = {}
function private.priorities.default()
  damageLog = newLogByTrim(damageLog, 5)
  lastFive = sumLogValues(damageLog)
  local in_melee_range = IsUnitInRange("Death Strike", "target")
  local result =
  --skill.DoNothingOutOfCombat() or
  --skill.DoNothingIfTargetOutOfCombat() or
    skill.DoNothingIfCasting() or
    skill.DoNothingIfMounted() or
    (in_melee_range and not IsCurrentSpell(6603) and "start_attack") or   -- skill.StartAttack() or
    --TryInterruptTarget() or
    semiAuto()

  if result ~= "nothing" and  result ~= "start_attack"then
    --ALERT("trying " .. spellName .. spellTarget)
    return TryCastSpellOnUnit(spellName, spellTarget)
    --return "nothing"
  else
    return result
  end
end

local function range(spell)
  if spell == "Blood Boil" then
    return 5
  end
end

local module = {
  --["run"] = private.priority_selection.run,
  ["interrupts"] = {
    "Mind Freeze",
    "Asphyxiate",
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["spells"] = {["range"] = range},
}

function module.run()
  return private.priority_selection.run()
end

local frame
function module.load()
  frame = CreateFrame("Frame", "deathknight_blood_frame")
  frame:SetScript("OnEvent", trackDeathStrikeHealableEvents)
  frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
  frame:SetScript("OnEvent", nil)
  frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
end

skill.modules.deathknight.blood = module
