_, skill = ...

local function IsOffGlobal(name)
  return name == "Mind Freeze"
end

local function IsStun(name)
  return name == "Asphyxiate"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return name == "Mind Freeze"
end

local function DefaultOffGlobal()
  return
  "nothing"
end

local function GetRunicPower()
  return UnitPower("player", Enum.PowerType.RunicPower)
end
local function GetRunes()
  return UnitPower("player", Enum.PowerType.Runes)
end


local function HandleFrostwyrmsFury()

end
local function HandleRemorselessWinter()
  return
    GetRunes() >= 1 and
    TryCastSpellOnTarget("Remorseless Winter") or
    nil
end
local function HandleHowlingBlast()
  return GetRunes() >= 1 and PlayerAuraDuration("Rime") > 0 and TryCastSpellOnTarget("Howling Blast") or nil
end
local function HandleKillingMachine()
  return GetRunes() >= 2 and PlayerAuraDuration("Killing Machine") > 0 and TryCastSpellOnTarget("Obliterate") or nil
end
local function Handle4RuneObliterate()
  return GetRunes() >= 4 and TryCastSpellOnTarget("Obliterate") or nil
end
local function Handle75PowerFrostStrike()
 return GetRunicPower() >=75 and TryCastSpellOnTarget("Frost Strike") or nil
end
local function HandleHornOfWinter()

end
local function HandleFrostStrike()
  return GetRunicPower() >=25 and TryCastSpellOnTarget("Frost Strike") or nil
end
local function HandleChainsOfIce()
  return
    GetRunes() >= 1 and
    PlayerAuraStacks("Cold Heart") >= 20 and
    TryCastSpellOnTarget("Chains of Ice") or
    nil
end

local function HandleDeathStrike()
  return
    PlayerHealthPercent() < 0.5 and
    GetRunicPower() >= 35 and
    TryCastSpellOnTarget("Death Strike") or
    nil
end
local function HandleDarkSuccor()
  return
    PlayerHealthPercent() < 0.7 and
    PlayerAuraDuration("Dark Succor") > GlobalCD() and
    TryCastSpellOnTarget("Death Strike") or
    nil
end
local function HandleDroppingDarkSuccor()
  return
    PlayerHealthPercent() < 1 and
    PlayerAuraDuration("Dark Succor") > GlobalCD() and
    PlayerAuraDuration("Dark Succor") < 3 and
    TryCastSpellOnTarget("Death Strike") or
    nil
end
local function HandleHealing()
  return
    HandleDroppingDarkSuccor() or
    HandleDarkSuccor() or
    HandleDeathStrike() or
    nil
end
local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function HandlePillarOfFrost()
  local sum_enemy_hp = 0
  for guid, unit in pairs(skill.EnemiesWithin(8)) do
    sum_enemy_hp = sum_enemy_hp + UnitHealth(unit)
  end
  local sum_party_hp = UnitHealthMax("player") * 0.5
  local num_party_members = 1
  for i, unit in ipairs(PARTY) do
    if UnitExists(unit) then
      sum_party_hp = sum_party_hp + UnitHealthMax(unit)
      num_party_members = num_party_members + 1
    end
  end
  local sum_party_dps = sum_party_hp / num_party_members * 0.2
  --ALERT("skill.NumEnemiesWithin(8): "..tostring(skill.NumEnemiesWithin(8)))
  --ALERT("sum_party_dps: "..tostring(sum_party_dps))
  --ALERT("sum_enemy_hp: "..tostring(sum_enemy_hp))
  --ALERT("sum_party_xx: "..tostring(sum_party_dps * 20))
  if sum_enemy_hp > sum_party_dps * 15 then
    return TryCastSpellOnPlayer("Pillar of Frost") or nil
  end
end
local function HandleEmpowerRuneWeapon()
  if GetRunicPower() > 60 and GetRunes() > 3 then
    return
  end
  local sum_enemy_hp = 0
  for guid, unit in pairs(skill.EnemiesWithin(8)) do
    sum_enemy_hp = sum_enemy_hp + UnitHealth(unit)
  end
  local sum_party_hp = UnitHealthMax("player") * 0.5
  local num_party_members = 1
  for i, unit in ipairs(PARTY) do
    if UnitExists(unit) then
      sum_party_hp = sum_party_hp + UnitHealthMax(unit)
      num_party_members = num_party_members + 1
    end
  end
  local sum_party_dps = sum_party_hp / num_party_members * 0.2
  --ALERT("skill.NumEnemiesWithin(8): "..tostring(skill.NumEnemiesWithin(8)))
  --ALERT("sum_party_dps: "..tostring(sum_party_dps))
  --ALERT("sum_enemy_hp: "..tostring(sum_enemy_hp))
  --ALERT("sum_party_xx: "..tostring(sum_party_dps * 20))
  if sum_enemy_hp > sum_party_dps * 20 then
    return TryCastSpellOnPlayer("Empower Rune Weapon") or nil
  end
end
local function KeepUpRemorselessWinter()
  return
    IsTalentSelected("Gathering Storm") and
    --PlayerAuraDuration("Remorseless Winter") <= GlobalCD() and
    GetRunes() >= 1 and
    TryCastSpellOnPlayer("Remorseless Winter") or
    nil

end


local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (skill.UnitIsBoss("target") or target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
local private = {}
private.priorities = {}
function private.priorities.low_level_farm()
  return
  skill.DoNothingIfMounted() or
  (skill.UnitIsBoss("target") and not UnitIsFriend("player", "target") and skill.IsUnitAlive("target") and TryCastSpellOnPlayer("Empower Rune Weapon")or nil) or
  (skill.UnitIsBoss("target") and not UnitIsFriend("player", "target") and skill.IsUnitAlive("target") and TryCastSpellOnPlayer("Pillar of Frost") or nil) or
  initiateLowLevelCombatWithSpell("Howling Blast") or
  skill.AcquireTarget() or
  skill.DoNothingOutOfCombat() or
  skill.StartAttack() or
  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or

  (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(8) > 1 and KeepUpRemorselessWinter()) or
  (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(8) > 1 and HandleHowlingBlast()) or

  HandleChainsOfIce() or
  --HandleFrostwyrmsFury() or
  HandleRemorselessWinter() or
  HandleHowlingBlast() or
  Handle4RuneObliterate() or
  Handle75PowerFrostStrike() or
  HandleKillingMachine() or
  --HandleEmperorRuneWeapon() or
  HandleHornOfWinter() or
  HandleFrostStrike() or
  skill.TryUseConcentratedFlame() or

  skill.AcquireTarget() or
  "nothing"
end
function private.priorities.default()
  return
    torghast.torghast_stuff() or
    skill.common_priority_start() or
    skill.AcquireTarget() or
    skill.StartAttack() or

    HandleHealing() or
    --KeepUpRemorselessWinter() or
    HandlePillarOfFrost() or
    HandleEmpowerRuneWeapon() or
    HandleChainsOfIce() or
    --HandleFrostwyrmsFury() or
    --HandleRemorselessWinter() or
    HandleHowlingBlast() or
    Handle4RuneObliterate() or
    Handle75PowerFrostStrike() or
    HandleKillingMachine() or
    --HandleEmperorRuneWeapon() or
    HandleHornOfWinter() or
    HandleFrostStrike() or

    "nothing"
end

local function range(spell)
  if spell == "Howling Blast" then
    return 20
  end
end

local module = {
  ["interrupts"] = {
    "Mind Freeze",
    "Asphyxiate",
  },
  ["spell"] = {
    ["range"] = range,
  },
  ["save_interrupt_for"]= {},
  ["range_override"] = {},
  ["override_range"] = {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["range_override"] = {
    ["Power Siphon"] = 100,
    ["Felstorm"] = 100,
  },
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.deathknight.frost = module