_, skill = ...

local function IsOffGlobal(name)
  return
  name == "Icebound Fortitude" or
          name == "Every Man for Himself" or
          name == "Healthstone" or
          name == "Mind Freeze"
end

local function IsStun(spell)
  return spell == "Asphyxiate"
end

local function IsDisorient(spell)
  return false
end

local function IsIncapacitate(spell)
  return false
end

local function IsSilence(spell)
  return false
end

local function IsInterrupt(spell)
  return spell == "Mind Freeze"
end

local function runesInterpolated()
  local runeAmount = 0
  local lowestRuneCD = 9999
  local partialRune = 0
  for i = 1, 6 do
    local start, duration, runeReady = GetRuneCooldown(i)
    local remaining = start + duration - GetTime()
    if runeReady == true then
      runeAmount = runeAmount + 1
    elseif lowestRuneCD >= remaining then
      lowestRuneCD = remaining
      partialRune = 1 - remaining / duration
    end
  end

  return runeAmount + partialRune
end

local function runesIn(seconds)
  local runeAmount = 0
  for i = 1, 6 do
    local start, duration, runeReady = GetRuneCooldown(i)
    local remaining = start + duration - GetTime()
    if runeReady == true or remaining - seconds <= 0 then
      runeAmount = runeAmount + 1
    end
  end
  return runeAmount
end

local function runesWastedIn(seconds)
  local runeAmount = 0
  for i = 1, 6 do
    local start, duration, runeReady = GetRuneCooldown(i)
    local remaining = start + duration - GetTime()
    if runeReady == true or remaining - seconds <= 0 then
      runeAmount = runeAmount + 1
    end
  end
  return runeAmount
end


function deathStrikeHeal(dmg)
  local hemoMod = 1 + 0.08 * PlayerAuraStacks("Hemostasis")
  local vampiricBloodMod = PlayerAuraDuration("Vampiric Blood") and 1.3 or 1
  return max(dmg * 0.25, 0.07 * UnitHealthMax("player")) * hemoMod * vampiricBloodMod
end

------------------------------------------------------------------------
local spellTarget = nil
local spellName = nil
local function SafeTryCastSpellOnUnit(spell, unit)
  local can = CanCastSpellOnUnit(spell, unit)
  if can then
    spellTarget = unit
    spellName = spell
  end
  return can
end
local function SafeTryCastSpellOnTarget(spell)
  local can = CanCastSpellOnUnit(spell, "target")
  if can then
    spellTarget = "target"
    spellName = spell
  end
  return can
end

local function SafeTryCastSpellOnPlayer(spell)
  local can = CanCastSpellOnUnit(spell, "player")
  if can then
    spellTarget = "player"
    spellName = spell
  end
  return can
end
------------------------------------------------------------------------
local function GetRunicPower()
  return UnitPower("player", Enum.PowerType.RunicPower)
end

local function GetRunes()
  return UnitPower("player", Enum.PowerType.Runes)
end
------------------------------------------------------------------------
local function tryFesteringStrike()
  return (runesInterpolated() >= 2 and
  (SafeTryCastSpellOnTarget("Festering Strike")))
    or nil
end

local function tryDeathStrike()
  return (GetRunicPower() >= 35 and SafeTryCastSpellOnTarget("Death Strike")) or nil
end

local function tryOutbreak()
  return (runesInterpolated() >= 1 and SafeTryCastSpellOnTarget("Outbreak")) or nil
end

local function tryDeathCoil()
  return ((GetRunicPower() >= 40 or PlayerMyAuraDuration("Sudden Doom") >= 0.15)
          and SafeTryCastSpellOnTarget("Death Coil")) or nil
end

local function tryEpidemic()
  return ((GetRunicPower() >= 30 or PlayerMyAuraDuration("Sudden Doom") >= 0.15)
          and SafeTryCastSpellOnTarget("Epidemic")) or nil
end

local function tryScourgeStrike()
  if runesInterpolated() >= 1 then
    if IsTalentSelected("Clawing Shadows") then
      return SafeTryCastSpellOnTarget("Clawing Shadows")
    end
    return SafeTryCastSpellOnTarget("Scourge Strike")
  end
end
------------------------------------------------------------------------
local function keepFesteringWound(minStacks)
  local fsStacks = TargetMyAuraStacks("Festering Wound")
  local fsDur = TargetMyAuraDuration("Festering Wound")
  if ((fsStacks < minStacks and runesInterpolated() >= 2) or fsDur <= 5.15 and runesInterpolated() >= 3) then
    return tryFesteringStrike()
  end
end

local function keepVirulentPlague()
  local bpDur = TargetMyAuraDuration("Virulent Plague")
  return (bpDur <= 2 and tryOutbreak()) or nil
end

local function dndHeartStrike(expectedTargets)
  local rpGain = expectedHeartStrikeRP(expectedTargets)
  if PlayerAuraDuration("Death and Decay") >= 0.15
          and GetRunicPower() + rpGain <= 125 then
    return tryHeartStrike()
  end
end

local function defensiveDS(heal, currentHealthLEQ)
  if man_mode == "off"
          and deathStrikeHeal(UnitHealthLost("player", 5)) / UnitHealthMax("player") >= heal
          and UnitHealth("player") / UnitHealthMax("player") <= currentHealthLEQ then
    return tryDeathStrike()
  end
end
local function freeDS()
  return PlayerMyAuraDuration("Dark Succor") > 0.15 and tryDeathStrike()
end


local function offGlobal()
end

function unholyGcdPressureAoe()
  local timeToSpendRunicPower = GetRunicPower()/30 * GlobalCDMax()
  local timeToSpendRunes = min(0, runesIn(timeToSpendRunicPower) - 3) *  GlobalCDMax()
  return timeToSpendRunicPower + timeToSpendRunes
end


local function rpSpender()
  local enemiesWithin8 =  skill.NumEnemiesWithin(8)
  local runes = runesInterpolated()
  local runesNextGlobal = runesIn(GlobalCDMax())
  local rp = GetRunicPower()
  local rpMissing = UnitPowerMax("player") - rp
  local runeThreshold = (PlayerMyAuraDuration("Death and Decay") > 0.15 and enemiesWithin8 >= 3) and 2 or 4
  if (runesNextGlobal < runeThreshold and PlayerMyAuraDuration("Runic Corruption") < 0.15) or rpMissing <= 25 then
    if enemiesWithin8 >= 3 then
      return tryEpidemic()
    else
      return tryDeathCoil()
    end
  end
end


--- define min / max values for resources as base features...

local function HandleScourgeStrike()
  local enemiesWithin8 =  skill.NumEnemiesWithin(8)
  local fwStacks = TargetMyAuraStacks("Festering Wound")
  local rp = GetRunicPower()
  local rpMissing = UnitPowerMax("player") - rp
  local runes = runesInterpolated()
  local runesNextGlobal = runesIn(GlobalCDMax())
  local runesAtDnD = runesIn(GetCD("Death and Decay"))
  if rpMissing >= 12 then
    if enemiesWithin8 >= 3 and PlayerMyAuraDuration("Death and Decay") > 0.15 then
      return tryScourgeStrike()
    end
    if fwStacks > 1 and runesNextGlobal >= 4 then -- or PlayerMyAuraDuration("Rune of Hysteria") > 0.15
      return tryScourgeStrike()
    end
  end
end

local function HandleFesteringStrike()
  local enemiesWithin8 = skill.NumEnemiesWithin(8)
  local fwStacks = TargetMyAuraStacks("Festering Wound")
  local rp = GetRunicPower()
  local rpMissing = UnitPowerMax("player") - rp
  local runes = runesInterpolated()
  local runesNextGlobal = runesIn(GlobalCDMax())
  local runesAtDnD = runesIn(GetCD("Death and Decay"))
  local apocalypseBeforeDnD = GetCD("Death and Decay") >= GetCD("Apocalypse")
  if rpMissing >= 20 then
    if enemiesWithin8 >= 3 then
      if PlayerMyAuraDuration("Death and Decay") < 0.15 then --runesAtDnD >= (apocalypseBeforeDnD and 3 or 5) then
      -- build up stacks outside DnD window
        return keepFesteringWound(4)
      elseif runes >= 5 and PlayerMyAuraDuration("Death and Decay") >= GlobalCDMax() * 3 then
        -- dont waste runes or globals even with DnD
        return keepFesteringWound(2)
      end
    else -- enemiesWithin8 < 3
      if runesNextGlobal >= 5 then
        return keepFesteringWound(4)
      elseif runes >= 4 then
        return keepFesteringWound(3)
      elseif runes >= 4 then
        return keepFesteringWound(1)
      end
    end
  end
end


local function semiAuto()
  local recentHPLossRelative = UnitHealthLost("player", 5)/UnitHealthMax("player")
  local hpRelative = UnitHealth("player")/UnitHealthMax("player")
  local rp = GetRunicPower()
  local rpMissing = UnitPowerMax("player") - rp
  local runes = runesInterpolated()
  local runesNextSecond = runesIn(1)
  local inMeleeRange = IsUnitInRange("Death Strike", "target")
  local enemiesWithin8 =  skill.NumEnemiesWithin(8)
  local fwStacks = TargetMyAuraStacks("Festering Wound")
  local result =

  -- urgent defense
          --defensiveDS(0.3, 0.5) or
          --defensiveDS(0.2, 0.35) or

          -- queue
          SafeTryCastSpellOnUnit(GetQueuedSpell(), GetQueuedUnit()) or
          hpRelative < 0.9 and freeDS() or
          inMeleeRange and keepVirulentPlague() or
          rpSpender() or
          HandleFesteringStrike() or
          HandleScourgeStrike() or
          "nothing"
  return result
end

local private = {}
private.priorities = {}
function private.priorities.default()
  local in_melee_range = IsUnitInRange("Death Strike", "target")
  local result =
  --skill.DoNothingOutOfCombat() or
  --skill.DoNothingIfTargetOutOfCombat() or
  ((UnitIsDead("target") or UnitIsGhost("target")) and "nothing" or nil)  or
  skill.DoNothingIfCasting() or
          skill.DoNothingIfMounted() or
          (in_melee_range and not IsCurrentSpell(6603) and "start_attack") or   -- skill.StartAttack() or
          --TryInterruptTarget() or
          semiAuto()

  if result ~= "nothing" and  result ~= "start_attack"then
    --ALERT("trying " .. spellName .. spellTarget)
    return TryCastSpellOnUnit(spellName, spellTarget)
    --return "nothing"
  else
    return result
  end
end

local function range(spell)
  if spell == "Blood Boil" then
    return 5
  end
end

local module = {
  --["run"] = private.priority_selection.run,
  ["interrupts"] = {
    "Mind Freeze",
    "Asphyxiate",
  },
  ["extra_spells"] = {
    "Flicker",
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["spells"] = {["range"] = range},
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.deathknight.unholy = module
