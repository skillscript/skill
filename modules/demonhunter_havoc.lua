_, skill = ...

local function IsOffGlobal(name)
  return name == "Disrupt"
end

local function IsStun(name)
  return name == "Chaos Nova" or name == "Fel Eruption"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return name == "Disrupt" or name == "Chaos Nova"
end

local function DefaultOffGlobal()
  return
  "nothing"
end

local function range(spell)
  if spell == "Chaos Nova" then
    return 8
  elseif spell == "Glaive Tempest" then
    return 8
  elseif spell == "Immolation Aura" then
    return 5
  elseif spell == "Annihilation" then
    return 5
  elseif spell == "Death Sweep" then
    return 8
  elseif spell == "Blade Dance" then
    return 8
  end
end

local function TryEyeBeam()
  return
    not IsPlayerMoving() and
    skill.IsUsable("Eye Beam") and
    TryCastSpellOnTarget("Eye Beam") or
    nil
end
local function TryDeathSweep()
  return
    PlayerAuraDuration("Metamorphosis") > GlobalCD() and
    skill.IsUsable("Death Sweep") and
    TryCastSpellOnTarget("Death Sweep") or
    nil
end
local function TryGlaiveTempest()
  return
    IsTalentSelected("Glaive Tempest") and
    skill.IsUsable("Glaive Tempest") and
    TryCastSpellOnTarget("Glaive Tempest") or
    nil
end
local function TryFelBlade()
  return
    IsTalentSelected("Fel Blade") and
    skill.IsUsable("Fel Blade") and
    TryCastSpellOnTarget("Fel Blade") or
    nil
end
local function TryBladeDance()
  return
    skill.IsUsable("Blade Dance") and
    TryCastSpellOnPlayer("Blade Dance") or
    nil
end
local function TryChaosStrike()
  return
    skill.IsUsable("Chaos Strike") and
    TryCastSpellOnTarget("Chaos Strike") or
    nil
end
local function TryAnnihilation()
  return
    PlayerAuraDuration("Metamorphosis") > GlobalCD() and
    skill.IsUsable("Annihilation") and
    TryCastSpellOnTarget("Annihilation") or
    nil
end

local private = {}
private.priorities = {}
function private.priorities.default()
  return
    (skill.PlayerSpellCastName() == "Eye Beam" and "nothing" or nil) or
    torghast.torghast_stuff() or
    skill.common_priority_start() or
    skill.AcquireTarget() or
    skill.StartAttack() or

    skill.TryPurgeAnythingFromTarget() or
    (skill.NumEnemiesWithin(8) >= 2 and TryDeathSweep() or nil) or
    TryEyeBeam() or
    TryGlaiveTempest() or
    TryEyeBeam() or
    TryCastSpellOnTarget("Immolation Aura") or
    TryFelBlade() or
    (skill.NumEnemiesWithin(8) >= 3 and TryBladeDance() or nil) or
    TryAnnihilation() or
    TryChaosStrike() or
    TryCastSpellOnTarget("Demon's Bite") or

    "nothing"
end
function private.priorities.daily()
  return skill.dailies()
end
local module = {
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Consume Magic"] = {
        "Magic",
      },
    },
  },
  ["interrupts"] = {
    "Disrupt",
    "Fel Eruption",
    "Chaos Nova",
  },
  ["extra_spells"] = {
    "Annihilation",
    "Death Sweep",
  },
  ["save_interrupt_for"]= {
    ["Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Empowered Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
      ["Harrow"] = "don't interrupt",
    },
    ["Empowered Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
    },
  },
  ["range_override"] = {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.demonhunter.havoc = module
