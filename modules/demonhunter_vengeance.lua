_, skill = ...
local private = {}

local function IsOffGlobal(name)
  return name == "Disrupt"
end

local function IsStun(name)
  skill.DoNothingIfMounted()
  return name == ""
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return name == "Sigil of Silence"
end

local function IsInterrupt(name)
  return name == "Disrupt"
end

local function DefaultOffGlobal()
  return
  "nothing"
end

local function GetRangeOverride(spell)
  if spell == "Eye Beam" then
    return 10
  elseif spell == "Soul Cleave" then
    return 8
  elseif spell == "Fracture" then
    return 8
  elseif spell == "Throw Glaive" then
    return 30
  elseif spell == "Sigil of Silence" then
    return 10
  elseif spell == "Infernal Strike" then
    return 5
  elseif spell == "Immolation Aura" then
    return 5
  end
end
local function GetFury()
  return UnitPower("player", Enum.PowerType.Fury)
end
local function GetFuryMax()
  return UnitPowerMax("player", Enum.PowerType.Fury)
end
local function GetSoulFragments()
  return PlayerAuraStacks("Soul Fragments")
end
local function GetSoulFragmentsMax()
  return 5
end

local function TrySoulCleave()
  return
    GetFury() >= 30 and
    TryCastSpellOnTarget("Soul Cleave") or
    nil
end
local function TrySpiritBomb()
  return
    IsTalentSelected("Spirit Bomb") and
    GetFury() >= 30 and
    GetSoulFragments() > 0 and
    TryCastSpellOnPlayer("Spirit Bomb") or
    nil
end

function private.DontCapInfernalStrike()
  local spell = "Infernal Strike"
  return
    GetCharges(spell) >= GetChargesMax(spell) - 1 and
    GetChargeCD(spell) < GlobalCDMax() + GlobalCD() and
    IsTargetInRange("Soul Cleave") and
    TryCastSpellOnPlayer(spell) or
    nil
end
function private.HandleFieryBrand()
  local c = UnitClassification("target")
  -- "worldboss", "rareelite", "elite", "rare", "normal", "trivial", or "minus"
  return
    PlayerHealthPercent() < 0.9 and
    (c == "worldboss" or c == "rareelite" or c == "elite") and
    UnitGUID("targettarget") == UnitGUID("player") and
    TryCastSpellOnTarget("Fiery Brand") or
    nil
end
function private.TryFelDevastation()
  return
    not IsPlayerMoving() and
    skill.IsUsable("Fel Devastation") and
    TryCastSpellOnTarget("Fel Devastation") or
    nil
end
function private.TryFracture()
  return
    IsTalentSelected("Fracture") and
    skill.IsUsable("Fracture") and
    GetSoulFragments() < GetSoulFragmentsMax() - 2 and
    TryCastSpellOnTarget("Fracture") or
    nil
end
function private.TryImmolationAura()
  return
    GetSoulFragments() <= GetSoulFragmentsMax() - 1 and
    skill.NumEnemiesWithin(5) > 0 and
    TryCastSpellOnPlayer("Immolation Aura") or
    nil
end
function private.TrySigilOfFlame()
  return
    IsTargetInRange("Soul Cleave") and
    TryCastSpellOnPlayer("Sigil of Flame") or
    nil
end
function private.TryThrowGlaive()
  return
    TryCastSpellOnTarget("Throw Glaive") or
    nil
end

private.priorities = {}
function private.priorities.default()
  return
    (skill.PlayerSpellCastName() == "Fel Devastation" and "nothing" or nil) or
    torghast.torghast_stuff() or
    (UnitAuraDuration("player", "Fae Empowered Elixir", "MAW") > 0 and not skill.UnitIsBoss("target") and UnitName("target") ~= "Ashen Phylactery" and UnitName("target") ~= "Mawrat" and TryCastSpellOnTarget("The Hunt") or nil) or
    skill.common_priority_start() or
    skill.AcquireTarget() or
    skill.StartAttack() or

    (PlayerHealthPercent() < 0.9 and GetCharges("Demon Spikes") >= 2 and TryCastSpellOnPlayer("Demon Spikes") or nil) or
    (PlayerHealthPercent() < 0.6 and PlayerAuraDuration("Demon Spikes") <= 0  and TryCastSpellOnTarget("Fiery Brand") or nil) or
    (PlayerHealthPercent() < 0.4 and PlayerAuraDuration("Demon Spikes") <= 0 and TargetMyAuraDuration("Fiery Brand") <= 0 and GetCharges("Demon Spikes") >= 1 and TryCastSpellOnPlayer("Demon Spikes") or nil) or
    (PlayerHealthPercent() < 0.4 and PlayerAuraDuration("Demon Spikes") <= 0 and TargetMyAuraDuration("Fiery Brand") <= 0 and TryCastSpellOnPlayer("Metamorphosis") or nil) or

    skill.TryPurgeAnythingFromTarget() or

    private.DontCapInfernalStrike() or
    private.HandleFieryBrand() or
    (GetSoulFragments() >= 4 and TrySpiritBomb() or nil) or
    skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    private.TryFelDevastation() or
    (GetFury() > GetFuryMax() - 25 and TrySoulCleave() or nil) or
    private.TryFracture() or
    private.TryImmolationAura() or
    private.TrySigilOfFlame() or
    private.TryThrowGlaive() or

    "nothing"
end
function private.priorities.daily()
  return skill.dailies()
end

local module = {
  ["interrupts"] = {
    "Disrupt",
  },
  ["spell"] = {
    ["purge"] = {
      ["Consume Magic"] = {
        "Magic",
      },
    },
  },
  ["extra_spells"] = {
    "Annihilation",
    "Death Sweep",
  },
  ["save_interrupt_for"]= {
    ["Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Empowered Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Disrupt",
    },
    ["Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
      ["Harrow"] = "don't interrupt",
    },
    ["Empowered Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
    },
  },
  ["extra_spells"] = {},
  ["range_override"] = {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}
function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end
skill.modules.demonhunter.vengeance = module
