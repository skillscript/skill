_, skill = ...

local function IsOffGlobal(name)
  return name == "Skull Bash"
end

local function IsStun(name)
  return name == "Mighty Bash"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return name == "Skull Bash"
end

local function GetCP()
  return UnitPower("player", Enum.PowerType.ComboPoints)
end

local function GetCPMax()
  return UnitPowerMax("player", Enum.PowerType.ComboPoints)
end

local function GetEnergy()
  return UnitPower("player", Enum.PowerType.Energy)
end

local function GetEnergyMax()
  return UnitPowerMax("player", Enum.PowerType.Energy)
end

local function PredictedDamage(spell, unit)
  local amount = 0
  local attack_power = UnitAttackPower("player")
  local CR_VERSATILITY = 29
  local armor_multiplier = 0.7
  local cp = GetCP()
  local multiplier = (1 + GetCombatRating(CR_VERSATILITY) / 8500)
                   * armor_multiplier
  if spell == "Ferocious Bite" then
    multiplier = multiplier * (1 + GetMasteryEffect() / 100)
    multiplier = 1 + max(GetEnergy() - 25, 25) / 25;
    amount = (cp) * 0.36 * attack_power
  end
  return amount * multiplier
end

local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function HandleBerserk()
  local sum_enemy_hp = 0
  for guid, unit in pairs(skill.EnemiesWithin(8)) do
    sum_enemy_hp = sum_enemy_hp + UnitHealth(unit)
  end
  local sum_party_hp = UnitHealthMax("player") * 0.5
  local num_party_members = 1
  for i, unit in ipairs(PARTY) do
    if UnitExists(unit) then
      sum_party_hp = sum_party_hp + UnitHealthMax(unit)
      num_party_members = num_party_members + 1
    end
  end
  local sum_party_dps = sum_party_hp / num_party_members * 0.2
  --ALERT("skill.NumEnemiesWithin(8): "..tostring(skill.NumEnemiesWithin(8)))
  --ALERT("sum_party_dps: "..tostring(sum_party_dps))
  --ALERT("sum_enemy_hp: "..tostring(sum_enemy_hp))
  --ALERT("sum_party_xx: "..tostring(sum_party_dps * 20))
  if sum_enemy_hp > sum_party_dps * 20 then
    return TryCastSpellOnPlayer("Berserk") or nil
  end

end
local function HandleInstakillFerociousBite()
  return
    GetEnergy() >= 25 and
    TargetHealth() < PredictedDamage("Ferocious Bite", "target") and
    TryCastSpellOnTarget("Ferocious Bite") or
    nil
end
local function HandleFerociousBite()
  return
    GetCP() >= 5 and
    GetEnergy() >= 25 and
    TargetMyAuraDuration("Rip") > GlobalCD() and
    TryCastSpellOnTarget("Ferocious Bite") or
    nil
end
local function HandleShred()
  return
    GetCP() < 5 and
    (GetEnergy() >= 40 or PlayerMyAuraDuration("Clearcasting") > 0.2) and
    TryCastSpellOnTarget("Shred") or
    nil
end
local function HandlePredatorySwiftness()
  if PlayerHealthPercent() < 0.9 then
    return
      GetEnergy() + 20 < GetEnergyMax() and
      PlayerAuraDuration("Predatory Swiftness") > GlobalCD() and
      TryCastSpellOnPlayer("Regrowth") or
      nil
  elseif PlayerHealthPercent() < 0.5 then
    return
      PlayerAuraDuration("Predatory Swiftness") > GlobalCD() and
      TryCastSpellOnPlayer("Regrowth") or
      nil
  elseif not skill.IsPlayerInCombat() and PlayerHealthPercent() < 0.8 then
    return
      PlayerAuraDuration("Predatory Swiftness") > GlobalCD() and
      TryCastSpellOnPlayer("Regrowth") or
      nil
  end
end
local function PrimalWrathDirectDamage()
  return
    skill.NumEnemiesWithin(11) >= 3 and
    IsTalentSelected("Primal Wrath") and
    GetCP() >= 5 and
    GetEnergy() >= 20 and
    TryCastSpellOnTarget("Primal Wrath") or
    nil
end

local function PrimalWrathDebuff()
  return
    skill.NumEnemiesWithin(11) >= 2 and
    IsTalentSelected("Primal Wrath") and
    GetCP() >= 5 and
    GetEnergy() >= 20 and
    TargetMyAuraDuration("Rip") <= 3  and
    TryCastSpellOnTarget("Primal Wrath") or
    nil
end

local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end

--- WantX = does X make sense, knowing the overall rotation, without considering specific alternatives
local function WantBrutalSlash(energy, comboPoints)
  return
    energy >= 25 and
    comboPoints < 5 and
    IsTalentSelected("Brutal Slash")
end
local function HandleBrutalSlash()
  return
    WantBrutalSlash(GetEnergy(), GetCP()) and
    TryCastSpellOnTarget("Ferocious Bite")
    or nil
end
local function HandleFerociousBite()
  return
    GetCP() >= 5 and
    skill.NumEnemiesWithin(11) <= 2 and
    GetEnergy() >= 50 and
    GetEnergy() >= 150 - TargetMyAuraDuration("Rip") * 10 and
    TargetMyAuraDuration("Rip") > GlobalCD() and
    TryCastSpellOnTarget("Ferocious Bite") or
    nil
end

local function HandleThrash()
  return
    GetCP() < 5 and
    skill.NumEnemiesWithin(8) >= 4 and
    (GetEnergy() >= 40 or PlayerMyAuraDuration("Clearcasting") > 0.2) and
    TargetMyAuraDuration("Thrash") <= 4 and
    TryCastSpellOnPlayer("Thrash") or
    nil
end
local function HandleSwipe()
  return
    GetCP() < 5 and
    skill.NumEnemiesWithin(8) > 1 and
    (GetEnergy() >= 35 or PlayerMyAuraDuration("Clearcasting") > 0.2) and
    TryCastSpellOnPlayer("Swipe") or
    nil
end
local function HandleTigersFury()
  return
    ((GetEnergy() < 90 and GetCD("Tiger's Fury") < 29 and skill.NumEnemiesWithin(12) >= 1) or
    (PlayerAuraDuration("Berserk") > 0 and PlayerAuraDuration("Berserk") < 15 and GetEnergy() < GetEnergyMax() - 50 - 10)) and TryCastSpellOnPlayer("Tiger's Fury") or
    nil
end

local function HandleRip()
  return
    skill.NumEnemiesWithin(13) <= 1 and
    GetCP() >= 5 and
    GetEnergy() >= 20 and
    (TargetMyAuraDuration("Rip") <= 4) and
    TryCastSpellOnTarget("Rip") or
    nil
end

local function HandleRake()
  return
    GetCP() < 5 and
    GetEnergy() >= 35 and
    TargetMyAuraDuration("Rake") <= 4 and
    TryCastSpellOnTarget("Rake") or
    nil
end

local function SelfRegrowth()
  return
    UnitHealth("player") / UnitHealthMax("player") < 0.90 and
    PlayerMyAuraDuration("Predatory Swiftness") >= GlobalCD() + 0.2 and
    TryCastSpellOnPlayer("Regrowth") or
    nil
end



local private = {}
private.priorities = {}
function private.priorities.vintorez()
  if IsTalentSelected("Guardian Affinity") and PlayerAuraDuration("Bear Form") > 0 then
    return skill.modules.druid.guardian.run()
  end

  return
    skill.DoNothingIfMounted() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfCasting() or
    SelfRegrowth() or
    TryCastSpellOnUnit(GetQueuedSpell(), GetQueuedUnit()) or
    HandleTigersFury() or
    PrimalWrathDebuff() or
    HandleRip() or
    PrimalWrathDirectDamage() or
    HandleFerociousBite() or

    HandleRake() or
    HandleThrash() or
    HandleSwipe() or
    HandleBrutalSlash() or
    HandleShred() or

    "nothing"
end
function private.priorities.jurei()
  if IsTalentSelected("Guardian Affinity") and PlayerAuraDuration("Bear Form") > 0 then
    return skill.modules.druid.guardian.run()
  end

  return
    torghast.torghast_stuff() or
    skill.common_priority_start() or
    skill.DoNothingIfCasting() or
    skill.AcquireTarget() or
    skill.StartAttack() or

    HandlePredatorySwiftness() or

    HandleBerserk() or
    HandleTigersFury() or
    HandleRip() or
    HandleInstakillFerociousBite() or
    HandleRake() or
    HandleThrash() or
    HandleFerociousBite() or
    HandleBrutalSlash() or
    HandleShred() or

    "nothing"
end
function private.priorities.low_level_farm()
  return
  skill.DoNothingIfCasting() or
  skill.TryLoot() or
  (PlayerAuraDuration("Travel Form") > 0 and "nothing") or
  initiateLowLevelCombatWithSpell("Moonfire") or
  (not UnitAffectingCombat("player") and skill.UnitIsBoss("target") and TryCastSpellOnTarget("Shred") or nil) or
  (UnitAffectingCombat("player") and PlayerAuraDuration("Cat Form") <= 0 and TryCastSpellOnPlayer("Cat Form")) or
  (not UnitAffectingCombat("player") and PlayerAuraDuration("Prowl") <= 0 and TryCastSpellOnPlayer("Prowl")) or
  (not UnitExists("target") and skill.AcquireTarget() or nil) or
  skill.DoNothingOutOfCombat() or
  skill.StartAttack() or
  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or
  --(skill.IsUnitBoss("target") and (TryCastSpellOnPlayer("Time Warp") or TryCastSpellOnPlayer("Combustion"))) or
  HandlePredatorySwiftness() or

  (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(8) > 1 and TryCastSpellOnTarget("Thrash")) or
  (skill.UnitIsBoss("target") and HandleBerserk()) or
  (skill.UnitIsBoss("target") and HandleTigersFury()) or
  (not skill.UnitIsBoss("target") and GetCP() >= 5 and TryCastSpellOnTarget("Ferocious Bite")) or
  HandleRip() or
  HandleInstakillFerociousBite() or
  HandleRake() or
  HandleThrash() or
  HandleFerociousBite() or
  HandleBrutalSlash() or
  HandleShred() or
  skill.TryUseConcentratedFlame() or
  initiateLowLevelCombatWithSpell() or
  skill.AcquireTarget() or
  "nothing"
end
function private.priorities.daily()
  return
  skill.HandleJustWingingIt() or
  skill.Aspirant() or

  "nothing"
end

local module = {
  ["interrupts"] = {
    "Mighty Bash",
    "Skull Bash",
  },
  ["spell"] = {
    ["purge"] = {
      ["Soothe"] = {
        "", -- Enrage uses an empty string https://wow.gamepedia.com/API_AuraUtil.FindAuraByName
      },
    },
    ["dispel"] = {
      ["Remove Corruption"] = {
        "Curse",
        "Poison",
      },
    },
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.druid.feral = module
