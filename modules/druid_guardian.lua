_, skill = ...
local private = {}
private.priorities = {}

local function IsOffGlobal(name)
  return name == "Skull Bash"
end

local function IsStun(name)
  return name == "Mighty Bash"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return name == "Incapacitating Roar"
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return name == "Skull Bash"
end

local function defensiveMeasures()
  local rage = UnitPower("player", 1)
  local relHP = UnitHealth("player") / UnitHealthMax("player")
  local frCharges = GetCharges("Frenzied Regeneration")
  local keepRage = 0
  local man_mode = "off"
  if man_mode == "on" then
    keepRage = 45
  end
  if relHP < 0.2 and IsTalentSelected("Renewal") then
    return TryCastSpellOnPlayer("Renewal") or nil
  end
  local noFR = PlayerMyAuraDuration("Frenzied Regeneration") <= 0
  if noFR and rage >= keepRage + 10 then
    if frCharges == 2 and relHP < 0.6 then
      return TryCastSpellOnPlayer("Frenzied Regeneration") or nil
    elseif frCharges == 1 and relHP < 0.45 then
      return TryCastSpellOnPlayer("Frenzied Regeneration") or nil
    end
  end

  if not skill.NumEnemiesWithin(8) then
    return nil
  end
  if rage >= 55 and GetCD("Ironfur") <= 0 and PlayerAuraStacks("Ironfur") == 0 and relHP <= 0.85 then
    return TryCastSpellOnPlayer("Ironfur") or nil
  elseif rage >= 85 and GetCD("Ironfur") <= 0 then
    return TryCastSpellOnPlayer("Ironfur") or nil
  else
    return nil
  end
end

local function HandleGalacticGuardian()
  return
    IsSpellInRange("Mangle") and
    PlayerAuraDuration("Galactic Guardian") > GlobalCD() and
    TryCastSpellOnTarget("Moonfire") or
    nil
end

function private.priorities.default()
  local num_enemies = skill.NumEnemiesWithin(12)
  local cleave_mode = nil
  if num_enemies >= 5 then
    cleave_mode = "aoe"
  elseif num_enemies >= 3 then
    cleave_mode = "cleave"
  else
    cleave_mode = "off"
  end

  return
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Moonfire")) or
    skill.DoNothingIfMounted() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfTargetOutOfCombat() or
    skill.DoNothingIfCasting() or

    skill.TryInterruptTarget() or
    defensiveMeasures() or

    TryCastSpellOnTarget("Mangle") or
    TryCastSpellOnTarget("Thrash") or
    HandleGalacticGuardian() or
    (cleave_mode == "cleave" and TryCastSpellOnTarget("Mangle") or nil) or
    TryCastSpellOnTarget("Swipe") or
    "nothing"
end

function private.priorities.semi()
  local num_enemies = skill.NumEnemiesWithin(12)

  return
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Moonfire")) or
    skill.DoNothingIfMounted() or
      skill.DoNothingOutOfCombat() or
      --skill.DoNothingIfTargetOutOfCombat() or
      skill.DoNothingIfCasting() or

      --skill.TryInterruptTarget() or
      defensiveMeasures() or

      num_enemies >= 3 and TryCastSpellOnTarget("Thrash") or
      num_enemies >= 4 and TryCastSpellOnTarget("Swipe") or
      TryCastSpellOnTarget("Mangle") or
      TryCastSpellOnTarget("Thrash") or
      HandleGalacticGuardian() or
      TryCastSpellOnTarget("Swipe") or
      "nothing"
end

function yolo()
  local rage = UnitPower("player", 1)
  if rage >= 80 and PlayerAuraDuration("Ironfur") <= 0 then
    return TryCastSpellOnPlayer("Ironfur") or nil
  else
    return nil
  end
end
function private.priorities.test()
  return
    yolo() or
    TryCastSpellOnTarget("Mangle") or
    TryCastSpellOnTarget("Thrash") or
    TryCastSpellOnTarget("Swipe") or
    "nothing"
end

local function range(spell)
  if spell == "Incapacitating Roar" then
    return 8
  end
end

local module = {
  ["interrupts"] = {
    --"Skull Bash",
    --"Incapacitating Roar",
  },
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Soothe"] = {
        "",  -- Enrage uses an empty string https://wow.gamepedia.com/API_AuraUtil.FindAuraByName
      },
    },
    ["dispel"] = {
      ["Remove Corruption"] = {
        "Curse",
        "Poison",
      },
    },
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end

local frame = nil
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.druid.guardian = module
