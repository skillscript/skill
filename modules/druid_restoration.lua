_, skill = ...

local function IsOffGlobal(name)
  return
    name == ""
end

local function IsStun(name)
  return name == "Axe Toss"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return false
end

local function DefaultOffGlobal()
  return
  "nothing"
end

local function ShapeshiftDuration()
  return PlayerAuraDuration("Moonkin Form") + PlayerAuraDuration("Cat Form") + PlayerAuraDuration("Bear Form")
end
local function HandleRejuvenation()
  if ShapeshiftDuration() > 0 then
    return nil
  end
  for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      UnitHealthDeficit(unit) > 0 and
      UnitMyAuraDuration(unit, "Rejuvenation") < 5 and
      TryCastSpellOnUnit("Rejuvenation", unit) or
      nil
    if result then
      return result
    end
  end
end
local function HandleWildGrowth()
  if IsPlayerMoving() then
    return
  end
  local deficit = 0
  --ALERT("=========")
  for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    --ALERT("unit: "..tostring(UnitName(unit)))
    if UnitExists(unit) and UnitIsFriend(unit, "player") and IsUnitInRange("Wild Growth", unit) then
      deficit = deficit + min(0.1, 1 - UnitHealthPercent(unit))
    end
  end
  --ALERT("deficit: "..tostring(deficit))
  if deficit >= 0.3 then
    return TryCastSpellOnPlayer("Wild Growth")
  end
end
local function KeepLifebloomOnTank()
  local tank = TryGetTank()
  return
    tank and
    UnitMyAuraDuration(tank, "Lifebloom") <= GlobalCD() and
    TryCastSpellOnUnit("Lifebloom", tank) or
    nil
end
local function KeepStarfireOnTarget()
  return
    TargetMyAuraDuration("Sunfire") <= GlobalCDMax() * 2 and
    TryCastSpellOnTarget("Sunfire") or
    nil
end
local function KeepMoonfireOnTarget()
  return
    TargetMyAuraDuration("Moonfire") <= GlobalCDMax() * 2 and
    TryCastSpellOnTarget("Moonfire") or
    nil
end
local function HandleStarsurge()
  return
    PlayerAuraDuration("Moonkin Form") > 0 and
    TryCastSpellOnTarget("Starsurge") or
    nil
end
local function HandleLunarStrike()
  return
    PlayerAuraDuration("Moonkin Form") > 0 and
    PlayerAuraDuration("Lunar Empowerment") > GlobalCD() + GlobalCDMax() and
    TryCastSpellOnTarget("Lunar Strike") or
    nil
end

local function TrySwiftmendOn(unit, ifBelowHealthPercent, maxOverhealPercent)
  local hot_duration = max(UnitMyAuraDuration(unit, "Wild Growth"), max(UnitMyAuraDuration(unit, "Rejuvenation"), UnitMyAuraDuration(unit, "Regrowth")))
  return
    hot_duration > GlobalCD() and
    skill.TryHealUnit("Swiftmend", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
local function HandleSwiftmend()
  for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      TrySwiftmendOn(unit, 0.3, 0.3) or
      nil
    if result then
      return result
    end
  end
end

local function IsTreeOfLife()
  return PlayerAuraDuration("Incarnation: Tree of Life") >= GlobalCD() or PlayerAuraDuration("Incarnation") >= GlobalCD()
end

local function HandleRegrowth()
  if IsPlayerMoving() and not IsTreeOfLife() then
    return
  end
  for i, unit in ipairs(GroupCombatCastUnits()) do
    local result =
      UnitExists(unit) and
      UnitHealthPercent(unit) < 0.8 and
      TryCastSpellOnUnit("Regrowth", unit) or
      nil
    if result then
      return result
    end
  end
end

local function HandleShiftToMoonkin()
  local tank = TryGetTank() or "player"
  local hots = UnitMyAuraDuration(tank, "Lifebloom")
  return
    PlayerAuraDuration("Moonkin Form") <= 0 and
    hots > 5 and
    TryCastSpellOnPlayer("Moonkin Form") or
    nil
end

local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function GivePartySecondsToLive(seconds)
  local result
  for i, unit in ipairs(PARTY) do
    if UnitExists(unit) and UnitHealth(unit) + UnitHealthChangedPerSecond(unit, 5) * seconds <= 0 then
      result =
        ((not IsPlayerMoving() or IsTreeOfLife()) and TryCastSpellOnUnit("Regrowth", unit) or nil) or
        TryCastSpellOnUnit("Swiftmend", unit) or
        TryCastSpellOnPlayer("Incarnation: Tree of Life") or
        nil
      if result then
        return result
      end
    end
  end
end
local function MaybeAssistTank()
  if UnitExists("target") then
    return
  end
  local tank = TryGetTank()
  if not tank then
    return
  end
  ALERT("tank: "..tostring(tank))
  local target = tank.."target"
  local result = TryCastSpellOnUnit("target", target)
  ALERT("result: "..tostring(result))
  return result
end
local function DoNothingIfMounted()
  local is_mounted = IsMounted() or PlayerAuraDuration("Travel Form") > 0
  return is_mounted and "nothing" or nil
end

local function MaxDpsOnGlobal()
  --ALERT("=========")
  --ALERT("UnitHealthLost(): "..tostring(UnitHealthLost("player", 10)))
  --ALERT("UnitHealthGained(): "..tostring(UnitHealthGained("player", 10)))
  --ALERT("UnitIncomingDps(): "..tostring(UnitIncomingDps("player", 10)))
  --ALERT("UnitHealthLost2(): "..tostring(UnitHealthLost2("player", 10)))
  --ALERT("UnitHealthGained2(): "..tostring(UnitHealthGained2("player", 10)))
  --ALERT("UnitHealthChangedPerSecond(): "..tostring(UnitHealthChangedPerSecond("player", 10)))
  local units = HighestDeficitPercentGroupUnits()
  local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
  return
    skill.common_priority_start() or
    skill.DoNothingIfCasting() or

    skill.TryHealUnit("Wild Growth", units[1], 0.3, 1.0) or
    TryDispelAnythingFromParty() or
    skill.TryHealUnit("Wild Growth", units[1], 0.5, 1.0) or
    HandleSwiftmend() or
    KeepLifebloomOnTank() or
    GivePartySecondsToLive(3) or
    (prioritize_aoe and HandleWildGrowth() or nil) or
    HandleRejuvenation() or
    skill.TryHealUnit("Wild Growth", units[1], 0.85, 0.0) or
    HandleWildGrowth() or
    HandleRegrowth() or
    GivePartySecondsToLive(6) or

    --skill.DoNothingOutOfCombat() or
    skill.DoNothingIfTargetOutOfCombat() or
    (not IsPlayerMoving() and PlayerManaPercent() > 0.5 and TryCastSpellOnTarget("Wrath")) or

    "nothing"
end


local private = {}
private.priorities = {}
function private.priorities.jurei()
  local units = HighestDeficitPercentGroupUnits()
  local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
  return
    skill.common_priority_start() or
    skill.DoNothingIfCasting() or

    skill.TryHealUnit("Regrowth", units[1], 0.3, 1.0) or
    TryDispelAnythingFromParty() or
    skill.TryHealUnit("Regrowth", units[1], 0.5, 1.0) or
    HandleSwiftmend() or
    KeepLifebloomOnTank() or
    GivePartySecondsToLive(3) or
    (prioritize_aoe and HandleWildGrowth() or nil) or
    HandleRejuvenation() or
    skill.TryHealUnit("Regrowth", units[1], 0.85, 0.0) or
    HandleWildGrowth() or
    HandleRegrowth() or
    GivePartySecondsToLive(6) or

    --skill.DoNothingOutOfCombat() or
    skill.DoNothingIfTargetOutOfCombat() or
    (not IsPlayerMoving() and PlayerManaPercent() > 0.5 and TryCastSpellOnTarget("Wrath")) or

    "nothing"

end
function private.priorities.default()
  return MaxDpsOnGlobal()
end
function private.priorities.dungeonDefensive()
  return MaxDpsOnGlobal()
end
local function range()
  return nil
end

local module = {
  ["run"] = rotation,
  ["interrupts"] = {},
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Soothe"] = {
        "",  -- Enrage uses an empty string https://wow.gamepedia.com/API_AuraUtil.FindAuraByName
      },
    },
    ["dispel"] = {
      ["Nature's Cure"] = {
        "Magic",
        "Curse",
        "Poison",
      },
    },
  },
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.druid.restoration = module
