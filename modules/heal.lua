private = {}
private.stats = {
  ["spell"] = {
    ["target unit"] = {
      ["unit guid"] = {
        ["heal"] = 0,
        ["overheal"] = 0,
      }
    }
  }
}
function private.update_heal_stats()
  wipe(private.stats)
  for _, unit in ipairs(GroupCombatCastUnits()) do
    private.atonement_durations[unit] = UnitMyAuraDuration(unit, "Atonement")
  end
  for spell, _ in pairs(atonement_spells) do
    local heal, overheal, efficiency = private.get_heal_stat(DamageOfSpell(spell) * 0.5, GlobalCD() + GetCastTime(spell) + 0.1)
    private.spell_heal_stats[spell] = private.spell_heal_stats[spell] or {}
    private.spell_heal_stats[spell]["heal"] = heal
    private.spell_heal_stats[spell]["overheal"] = overheal
    private.spell_heal_stats[spell]["efficiency"] = efficiency
  end
  --local heal, overheal, efficiency = private.get_heal_stat(DamageOfSpell("Power Word: Radiance") * 0.5, GlobalCD() + GetCastTime("Power Word: Radiance") + 0.1)
  --private.spell_heal_stats["Power Word: Radiance"]["heal"] = heal
  --private.spell_heal_stats["Power Word: Radiance"]["overheal"] = overheal
  --private.spell_heal_stats["Power Word: Radiance"]["efficiency"] = efficiency
end
function private.get_heal_stat(amount, after_seconds)
  local master_multiplier = GetMasteryEffect() / 100
  local heal = 0
  local overheal = 0
  for unit, duration in pairs(private.atonement_durations) do
    if duration > after_seconds then
      local deficit = UnitHealthMax(unit) - UnitHealth(unit)
      local effective = min(deficit, amount * master_multiplier)
      heal = heal + effective
      overheal = overheal + deficit - effective
    end
  end
  return heal, overheal, heal > 0 and overheal / heal or 0
end
function private.get_heal_stat(spell, src, dst, effect)

end

