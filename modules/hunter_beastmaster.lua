_, skill = ...

local private = {}

local function IsOffGlobal(name)
  return
    name == "Healthstone" or
    name == "Counter Shot"
end

local function IsStun(name)
  return name == "Intimidation"
end

local function IsDisorient(name)
  return name == "Blinding Light"
end

local function IsIncapacitate(name)
  return name == "Repentance"
end

local function IsSilence(name)
  return name == "Avenger's Shield"
end

local function IsInterrupt(name)
  return name == "Counter Shot"
end

local function DefaultOffGlobal()
  return "nothing"
end

local function GetFocus()
  return UnitPower("player", Enum.PowerType.Focus)
end
local function GetFocusMax()
  return UnitPowerMax("player", Enum.PowerType.Focus)
end
local pet_index = 1
local function HandlePetHealth()
  if UnitIsDead("pet") then
    return TryCastSpellOnUnit("Revive Pet", "pet")
  end
  if UnitExists("pet") then
    for i = 1, 5 do
      local spell, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo("Call Pet "..i)
      local name = select(2, GetCallPetSpellInfo(spellID))
      if name == UnitName("pet") then
        pet_index = i
        break
      end
    end
    return
      UnitHealthPercent("pet") < 0.8 and
      TryCastSpellOnUnit("Revive Pet", "pet") or
      nil
  else
    return TryCastSpellOnPlayer("Call Pet "..pet_index)
  end
end
local function DontClip(spell, percent)
  return
    GetCD(spell) > GlobalCD() and
    GetCD(spell) < GlobalCDMax() * max(0, min(1, percent))
    and "nothing"
end

local function HandleFlayedShot()
  return TryCastSpellOnTarget("Flayed Shot") or nil
end

local last_white_damage_timestamp = 0
local function StartAttack()
  if UnitExists("target") and skill.IsUnitInCombatWithPlayer("target") and last_white_damage_timestamp + 4 < GetTime() then
    return "start_attack"
  end
end
local whiteDamageFrame = CreateFrame("Frame", nil, UIParent)
local function WhiteDamageEvent(self, event)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local timestamp, subevent = CombatLogGetCurrentEventInfo()
    if subevent == "RANGE_DAMAGE" then
      last_white_damage_timestamp = GetTime()
    end
  end
end
whiteDamageFrame:SetScript("OnEvent", WhiteDamageEvent)

local trackingEnabled = false
local function MaxDpsOnGlobal()
  local cleave_mode = skill.NumEnemiesWithin(30) >= 3
  if not trackingEnabled then
    whiteDamageFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    trackingEnabled = true
  end
  return
    skill.common_priority_start() or

    StartAttack() or

    HandlePetHealth() or
    TryCastSpellOnTarget("Kill Shot") or
    TryCastSpellOnTarget("Flayed Shot")  or
    TryCastSpellOnTarget("Death Chakram") or
    (cleave_mode == "cleave" and UnitAuraDuration("player", "Beast Cleave") < GlobalCD() + GlobalCDMax() and TryCastSpellOnTarget("Multi-Shot")) or
    TryCastSpellOnTarget("Kill Command") or
    TryCastSpellOnTarget("A Murder of Crows") or
    (GetFocus() + (GlobalCD() + GlobalCDMax()) * GetPowerRegen() >= GetFocusMax() and TryCastSpellOnTarget("Cobra Shot") or nil) or
    skill.TryUseConcentratedFlame() or
    TryCastSpellOnPlayer("Bestial Wrath") or
    (GetFocus() + (GlobalCD() + GlobalCDMax()) * GetPowerRegen() + (GetChargeCD("Barbed Shot") <= GlobalCD() and 20 / 8 * GlobalCDMax() or 0) >= GetFocusMax() and TryCastSpellOnTarget("Cobra Shot") or nil) or
    TryCastSpellOnTarget("Barbed Shot") or
    (GetFocus() > 35 + 30 - GetPowerRegen() and TryCastSpellOnTarget("Cobra Shot")) or
    "nothing"
end

function HunterEngage()

end

private.priorities = {}
function private.priorities.default()
  return MaxDpsOnGlobal()
end

local module = {
  ["run"] = rotation,
  ["interrupts"] = {
    "Counter Shot",
    "Intimidation",
  },
  ["extra_spells"] = {
    "Flayed Shot",
    "Death Chakram",
  },
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.hunter.beast_mastery = module
