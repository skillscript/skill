_, skill = ...

local private = {}
private.frame = CreateFrame("Frame", nil, UIParent)

local module = {}
module.spell = {}
function module.spell.range(spell)
  return nil
end
module.spell.purge = {
  [""] = {},
}
module.spell.dispel = {
  [""] = {},
}

function module.is_off_global(name)
  return
    name == "Counter Shot" or
    name == "Pain Suppression"
end
function module.is_stun(name)
  return false
end
function module.is_disorient(name)
  -- TODO: Add IsFear()
  return name == "Psychic Scream"
end
function module.is_incapacitate(name)
  return false
end
function module.is_silence(name)
  return false
end
function module.is_interrupt(name)
  return false
end


local function GetFocus()
  return UnitPower("player", Enum.PowerType.Focus)
end
local function GetFocusMax()
  return UnitPowerMax("player", Enum.PowerType.Focus)
end

local function TryShield(unit)
  return
    (UnitAuraDuration(unit, "Weakened Soul") <= GlobalCD() or PlayerAuraDuration("Rapture") > GlobalCD()) and
    TryCastSpellOnUnit("Power Word: Shield", unit) or
    nil
end

local function TryDotIfWorth(guid)
  local unit = GuidToUnit(guid)
  if not unit then
    return --TryScanForGuid(guid) or nil
  end
  local duration = DotDurationOfSpell(private.dot)
  if UnitMyAuraDuration(unit, private.dot) > duration * 0.3 then
    return
  end
  local instant_damage = DamageOfSpell(private.dot)
  local duration_damage = DotDamageOfSpell(private.dot)
  local smite_damage = DamageOfSpell("Smite")
  local min_extra_dmg = math.floor(duration / HastedSeconds(1.5)) * smite_damage

  local projected_damage = (instant_damage + duration_damage + min_extra_dmg) * 0.8
  local maximum_damage = math.min(projected_damage, UnitHealth(unit))

  --ALERT("unit: "..tostring(unit))
  --ALERT("instant_damage: "..tostring(instant_damage))
  --ALERT("duration_damage: "..tostring(duration_damage))
  --ALERT("smite_damage: "..tostring(smite_damage))
  --ALERT("min_extra_dmg: "..tostring(min_extra_dmg))
  --ALERT("projected_damage: "..tostring(projected_damage))
  --ALERT("maximum_damage: "..tostring(maximum_damage))
  --ALERT("TargetHealth(): "..tostring(TargetHealth()))
  if maximum_damage > UnitHealth(unit) then
    return
  end
  return TryCastSpellOnUnit(private.dot, unit)
end
local function in_dot_range_and_in_combat(unit)
  return IsUnitInRange(private.dot, unit) and (UnitAffectingCombat(unit) or skill.ShouldAlwaysAttackUnit(unit))
end
local function MaybeDotAllEnemies()
  local enemies = skill.GetEnemies(in_dot_range_and_in_combat)
  --ALERT("#enemies: "..tostring(#enemies))
  for guid, _ in pairs(enemies) do
    --ALERT("guid: "..tostring(guid))
    --ALERT("GuidToUnit(guid): "..tostring(GuidToUnit(guid)))
    local result = TryDotIfWorth(guid) or nil
    if result then
      return result
    end
  end
end

local function TrySchism()
  return
    not IsPlayerMoving() and
    IsTalentSelected("Schism") and
    TryCastSpellOnTarget("Schism") or
    nil
end

local function TrySmite()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Smite") or
    nil
end

local function TryPowerWordSolace()
  return
    IsTalentSelected("Power Word: Solace") and
    TryCastSpellOnTarget("Power Word: Solace") or
    nil
end

local function TryInstantKill()
  local target_health = TargetHealth()
  local pws_damage = DamageOfSpell("Power Word: Solace")
  local dot_damage = DamageOfSpell(private.dot)
  return
    (DamageOfSpell("Power Word: Solace") > target_health and TryPowerWordSolace()) or
    (DamageOfSpell("Penance") > target_health and TryCastSpellOnTarget("Penance")) or
    (DamageOfSpell(private.dot) > target_health and TryCastSpellOnTarget(private.dot)) or
    nil
end

local function DoNothingIfMounted()
  return
    IsMounted() and
    "nothing" or nil
end

local function IsInGroup()
  return UnitExists("party1")
end
function private.KeepPowerWordFortitudeUp()
  if UnitLevel("player") < 22 then
    return
  end
  for _, unit in ipairs(SKILL_PARTY) do
    if UnitExists(unit) and UnitIsFriend("player", unit) and UnitAuraDuration(unit, "Power Word: Fortitude") < 20 * 60 and IsUnitInRange("Power Word: Fortitude", unit) and not UnitIsDeadOrGhost(unit) then
      return TryCastSpellOnUnit("Power Word: Fortitude", unit)
    end
  end
end

local function StayAlive()
  return
    (PlayerHealthPercent() < 0.1 and TryCastSpellOnPlayer("Desperate Prayer")) or
    (PlayerHealthPercent() < 0.2 and TryCastSpellOnPlayer("Pain Suppression")) or
    (PlayerHealthPercent() < 0.3 and skill.TrySelfHealWithConcentratedFlame()) or
    (PlayerHealthPercent() < 0.4 and TryCastSpellOnPlayer("Penance")) or
    (not IsPlayerMoving() and PlayerHealthPercent() < 0.5 and TryCastSpellOnPlayer("Shadow Mend")) or
    nil
end
local function existing_alive_friend(unit)
  return UnitExists(unit) and not UnitIsDeadOrGhost(unit) and UnitIsFriend(unit, "player")
end
local function below_20_percent_health(unit)
  return UnitHealthPercent(unit) < 0.2
end
local function TryShadowMendOnUnit(unit)
  return
    not IsPlayerMoving() and
    TryCastSpellOnUnit("Shadow Mend", unit) or
    nil
end
local function ApplyAtonement(unit)
  return
    (UnitAuraDuration(unit, "Weakened Soul") <= GlobalCD() and TryCastSpellOnUnit("Power Word: Shield", unit)) or
    TryShadowMendOnUnit(unit) or
    nil
end
local function KeepAtonementOnTank()
  local tank = TryGetTank()
  return
    tank and
    UnitAffectingCombat(tank) and
    UnitMyAuraDuration(tank, "Atonement") <= GlobalCD() and
    ApplyAtonement(tank) or
    nil
end
local function KeepwPwsOnTank()
  local tank = TryGetTank()
  return
    tank and
    UnitAffectingCombat(tank) and
    UnitAuraDuration(tank, "Weakened Soul") <= GlobalCD() and
    UnitAuraAbsorb(tank, "Power Word: Shield") <= 0 and
    TryCastSpellOnUnit("Power Word: Shield", tank) or
    nil
end
local function GiveUnitSecondsToLive(unit, seconds)
  if UnitHealth(unit) + UnitAbsorb(unit) - UnitHealAbsorb(unit) + (UnitHealthChangedPerSecond(unit, 5) + UnitAbsorbChangePerSecond(unit, 5)) * seconds <= 0 then
    return
      ((UnitAuraDuration(unit, "Weakened Soul") <= GlobalCD() or PlayerAuraDuration("Rapture") > GlobalCD()) and UnitAuraAbsorb("Power Word: Shield") <= AbsorbOfSpell("Power Word: Shield") * 0.2 and TryCastSpellOnUnit("Power Word: Shield", unit)) or
      TryCastSpellOnUnit("Penance", unit) or
      TryShadowMendOnUnit(unit) or
      (UnitAuraDuration(unit, "Weakened Soul") > GlobalCD() and UnitAuraAbsorb("Power Word: Shield") <=AbsorbOfSpell("Power Word: Shield") * 0.2 and TryCastSpellOnPlayer("Rapture")) or
      nil
  end
end
--local function loh_event()
--
--end
--local loh_frame = CreateFrame("loh_frame")
--loh_frame:RegisterEvent("UNIT_AURA")
--loh_frame:SetScript("OnEvent", loh_event)
--local step = 0
--function loh()
--  if UnitExists("pet") and UnitName("pet") == "Make Loh Go!" then
--    if GetMinimapZoneText() == "Darkwood Shoal" then
--
--    end
--  end
--end


local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (skill.UnitIsBoss("target") or target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
local function low_level_farm()
  return
  --(not UnitAffectingCombat("player") and IsTalentSelected("Hunter's Mark") and TargetMyAuraDuration("Hunter's Mark") <= 0 and skill.IsUnitBoss("target") and not UnitIsFriend("player", "target") and UnitIsAlive("target") and TryCastSpellOnTarget("Hunter's Mark")) or
  (skill.UnitIsBoss("target") and not UnitIsFriend("player", "target") and skill.IsUnitAlive("target") and TryCastSpellOnPlayer("Trueshot")) or
  initiateLowLevelCombatWithSpell("Arcane Shot") or
  skill.DoNothingIfCasting() or
  skill.DoNothingOutOfCombat() or
  --skill.StartAttack() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or

  (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(40) > 1 and TryCastSpellOnTarget("Multi-Shot")) or
  --(skill.IsUnitBoss("target") and not UnitIsFriend("player", "target") and UnitIsAlive("target") and TryCastSpellOnPlayer("Trueshot")) or

  (GetFocus() >= GetFocusMax() and TryCastSpellOnTarget("Arcane Shot")) or
  (skill.UnitIsBoss("target") and GetFocus() < 75 and TryCastSpellOnTarget("Rapid Fire")) or
  (PlayerAuraDuration("Precise Shots") > GlobalCD() and GetFocus() >= 30 and TryCastSpellOnTarget("Arcane Shot")) or
  (PlayerAuraDuration("Lock and Load") > GlobalCD() and TryCastSpellOnTarget("Aimed Shot")) or
  (skill.UnitIsBoss("target") and GetFocus() >= 30 and not IsPlayerMoving() and TryCastSpellOnTarget("Aimed Shot")) or
  (GetFocus() < 85 and skill.TryUseConcentratedFlame()) or
  (GetFocus() > 15 and TryCastSpellOnTarget("Arcane Shot")) or
  TryCastSpellOnTarget("Steady Shot") or


  skill.AcquireTarget() or
  "nothing"
end
local function default()
  return
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Multi-Shot")) or
    skill.DoNothingIfCasting() or

    skill.DoNothingIfDrinking() or
    DoNothingIfMounted() or
    TryCastQueuedSpell() or

    --skill.KeepRepurposedFelFocuserUp() or
    skill.DoNothingIfTargetOutOfCombat() or

    (GetFocus() >= GetFocusMax() and TryCastSpellOnTarget("Arcane Shot")) or
    (IsTalentSelected("Hunter's Mark") and TargetMyAuraDuration("Hunter's Mark") <= 0 and TryCastSpellOnTarget("Hunter's Mark")) or
    (GetFocus() < 75 and TryCastSpellOnTarget("Rapid Fire")) or
    (PlayerAuraDuration("Precise Shots") > GlobalCD() and GetFocus() >= 30 and TryCastSpellOnTarget("Arcane Shot")) or
    (PlayerAuraDuration("Lock and Load") > GlobalCD() and TryCastSpellOnTarget("Aimed Shot")) or
    (GetFocus() >= 30 and not IsPlayerMoving() and TryCastSpellOnTarget("Aimed Shot")) or
    (GetFocus() < 85 and skill.TryUseConcentratedFlame()) or
    (GetFocus() > 65 and TryCastSpellOnTarget("Arcane Shot")) or
    TryCastSpellOnTarget("Steady Shot") or
    "nothing"
end

local private = {}
private.priorities = {}
function private.priorities.default()
  return default()
end
function private.priorities.low_level_farm()
  return low_level_farm()
end

module.priorities = nil
module.gui = {}
module.interrupts = {
  "Counter Shot",
  "Intimidation",
}

function module.run()
  if module.priorities and module.priorities.priority then
    return module.priorities.priority()
  end
  ALERT_ONCE("No priority_selection in module.")
  return nil
end

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.hunter.marksmanship = module
