_, skill = ...

local last_spell = 0
local tracker_frame = CreateFrame("Frame", nil, UIParent)
local function WhiteDamageEvent(self, event)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, subevent, _, _, _, _, _, _, _, _, _, _, spell = CombatLogGetCurrentEventInfo()
    if subevent == "SPELL_CAST_SUCCESS" then
      last_spell = spell
    end
  end
end
tracker_frame:SetScript("OnEvent", WhiteDamageEvent)

local function range(spell)
  if spell == "Frost Nova" then
    return 8
  end
  return nil
end

local function HandleCombustion()
  return nil
end
local function HandleRadiantSpark()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Radiant Spark") or
    nil
end
local function HandlePyroblast()
  local should_cast_pyroblast =
    PlayerAuraDuration("Hot Streak!") > GlobalCD() or
    (IsTalentSelected("Searing Touch") and
     TargetHealth() < 0.3 and
     PlayerAuraDuration("Heating Up") > GlobalCD() and
     skill.PlayerSpellCastName() == "Scorch")
  return
    should_cast_pyroblast and
    TryCastSpellOnTarget("Pyroblast") or
    nil
end
local function HandleLivingBomb()
  return
    skill.NumEnemiesWithin(30) > 4 and
    TryCastSpellOnTarget("Living Bomb") or
    nil
end
local function HandleDragonsBreath()
  return
    skill.NumEnemiesWithin(10) >= 4 and
    TryCastSpellOnPlayer("Dragon's Breath") or
    nil
end
local function HandleFlamestrike()
  local should_cast_pyroblast =
    PlayerAuraDuration("Hot Streak!") > GlobalCD() or
    (IsTalentSelected("Searing Touch") and
     TargetHealth() < 0.3 and
     PlayerAuraDuration("Heating Up") > GlobalCD() and
     skill.PlayerSpellCastName() == "Scorch")
  return
    should_cast_pyroblast and
    skill.NumEnemiesWithin(10) >= 4 and
    TryCastSpellOnPlayer("Flamestrike") or
    nil
end
local function HandleArcaneExplosion()
  return
    skill.NumEnemiesWithin(10) >= 5 and
    TryCastSpellOnPlayer("Arcane Explosion") or
    nil
end
local function HandleFireblast()
  --ALERT("GlobalCD(): "..tostring(GlobalCD()))
  --ALERT("GlobalCDMax() / 2: "..tostring(GlobalCDMax() / 2))
  --ALERT("CanCastSpellOnUnit(FB): "..tostring(CanCastSpellOnUnit("Fire Blast", "target")))
  local should_cast_fire_blast = PlayerAuraDuration("Heating Up") > 0
  if IsTalentSelected("Searing Touch") then
    should_cast_fire_blast = should_cast_fire_blast or (PlayerAuraDuration("Heating Up") <= 0 and skill.PlayerSpellCastName() == "Scorch")
  end
    --(not IsTalentSelected("Searing Touch") and PlayerAuraDuration("Heating Up") > 0 and (not current_spellcast or current_spellcast ~= "Scorch")) or
    --(IsTalentSelected("Searing Touch") and PlayerAuraDuration("Heating Up") <= 0 and current_spellcast == "Scorch")
  --ALERT("should_cast_fire_blast: "..tostring(should_cast_fire_blast))
  return
    should_cast_fire_blast and
    GlobalCD() < GlobalCDMax() / 2 and
    TryCastSpellOnTarget("Fire Blast") or
    nil
end
local function HandleSearingTouch()
  return
    IsTalentSelected("Searing Touch") and
    TargetHealthPercent() < 0.3 and
    TryCastSpellOnTarget("Scorch") or
    nil
end
local function HandleFireball()
  if IsPlayerMoving() then
    return
  end
  return
    TryCastSpellOnTarget("Fireball") or
    nil
end
local function HandleBlazingBarrier()
  return
    TryCastSpellOnPlayer("Blazing Barrier") or
    nil
end
local function HandleScorch()
  return
    TryCastSpellOnTarget("Scorch") or
    nil
end
local function TryCastFireball()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Fireball") or
    nil
end
local function GenerateHeatingUp()
  if PlayerAuraDuration("Heating Up") > 0 or PlayerAuraDuration("Hot Streak!") > GlobalCD() then
    return nil
  end
  return
    (IsTalentSelected("Searing Touch") and TargetHealthPercent() <= 0.3 and TryCastSpellOnTarget("Scorch") or nil) or
    TryCastSpellOnTarget("Phoenix Flames") or
    TryCastFireball() or
    nil

end
local function GenerateHotStreak()
  return
    PlayerAuraDuration("Heating Up") > GlobalCD() and
    TryCastSpellOnTarget("Fire Blast") or
    nil

end
local function KeepArcaneIntellectUp()
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if UnitAuraDuration(unit, "Arcane Intellect") < 20 * 60 and IsUnitInRange("Arcane Intellect", unit) then
      return
        TryCastSpellOnUnit("Arcane Intellect", unit) or
        nil
    end
  end
  return nil
end

local trackingEnabled = false

local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (skill.UnitIsBoss("target") or target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
local function update_ttl(gui)
end
local function update_stagger(gui)
end
local GUI = {
  { ["label"] = "waypoint:", ["text"] = "0", ["update"] = update_ttl, ["click"] = nil, ["init"] = nil },
  { ["label"] = "stagger:", ["text"] = "0", ["update"] = update_stagger, ["click"] = nil, ["init"] = nil },
}
local private = {}
private.priorities = {}
function private.priorities.low_level_farm()
  return
  initiateLowLevelCombatWithSpell("Scorch") or
  skill.DoNothingOutOfCombat() or
  skill.StartAttack() or
  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or
  (skill.UnitIsBoss("target") and (TryCastSpellOnPlayer("Time Warp") or TryCastSpellOnPlayer("Combustion"))) or
  HandleFlamestrike() or
  HandleDragonsBreath() or
  HandlePyroblast() or
  HandleLivingBomb() or
  --HandleDragonsBreath() or
  HandleFireblast() or
  HandleSearingTouch() or
  HandleFireball() or
  skill.TryUseConcentratedFlame() or
  HandleScorch() or
  initiateLowLevelCombatWithSpell() or
  skill.AcquireTarget() or
  "nothing"
end
function private.priorities.default()
  if not trackingEnabled then
    tracker_frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    trackingEnabled = true
  end
  return
    (not UnitAffectingCombat("player") and not IsMounted() and TryCastSpellOnPlayer("Blazing Barrier") or nil) or
    --(not UnitAffectingCombat("player") and not IsMounted() and KeepArcaneIntellectUp() or nil) or
    skill.common_priority_start() or
    skill.StartAttack() or
    skill.AcquireTarget() or
    TryCastQueuedSpell() or

    HandleCombustion() or
    HandleRadiantSpark() or
    GenerateHeatingUp() or

    HandleFlamestrike() or
    HandleDragonsBreath() or
    HandleArcaneExplosion() or

    GenerateHotStreak() or
    HandlePyroblast() or
    HandleLivingBomb() or

    TryCastSpellOnPlayer("Blazing Barrier") or
    TryCastSpellOnTarget("Scorch") or

    "nothing"
end
function private.priorities.daily()
  return
  skill.HandleJustWingingIt() or
  skill.Aspirant() or

  "nothing"
end

local function IsInterrupt(spell)
  return spell == "Counterspell"
end
local function update_dir(gui)
  --gui.string:SetText(string.format("%.3f", player_orientation()))
end
local GUI = {
  { ["label"] = "direction:", ["text"] = "0", ["update"] = update_dir, ["click"] = nil, ["init"] = nil },
}
local module = {
  ["gui"] = GUI,
  ["interrupts"] = {
    "Counterspell",
  },
  ["is_interrupt"] = IsInterrupt,

  ["extra_spells"] = {
    "Radiant Spark",
  },
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Spellsteal"] = {
        "Magic",
      },
    },
    ["dispel"] = {
      ["Remove Curse"] = {
        "Curse",
      },
    },
  },
}


function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.mage.fire = module
