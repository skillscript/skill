_, skill = ...

local function inRange()
  res = IsSpellInRange("Ice Lance")
  if res == 1 then
    return true
  else
    return false
  end
end

local lastSpell = ""
local trackList = {["Frostbolt"] = true, ["Glacial Spike"] = true}

local function trackLastSpell(self, event, ...)
  if true then return	end
  -- ALERT("tracking events")
  if event == "UNIT_SPELLCAST_SUCCEEDED" or event == "UNIT_SPELLCAST_STOP" or event == "UNIT_SPELLCAST_START" then
    local unit, spell, rank, lineID, spellID = ...
    --ALERT(spell)
    if unit == "player" then --  and (trackList[spell]) then
      if event == "UNIT_SPELLCAST_SUCCEEDED" then

        if spell == "Fire Blast" then
          lastFireBlast = GetTime()
        end
        lastSpell = spell
      elseif event == "UNIT_SPELLCAST_START" then

      elseif event == "UNIT_SPELLCAST_STOP" then

      end
    end
  elseif event == "PLAYER_ENTERING_WORLD" or event == "PLAYER_SPECIALIZATION_CHANGED" then
    if PlayerClass() == "mage" and PlayerSpec() == "frost" then
      spellTracker:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
      spellTracker:RegisterEvent("UNIT_SPELLCAST_START")
      spellTracker:RegisterEvent("UNIT_SPELLCAST_STOP")
    else
      spellTracker:UnregisterEvent("UNIT_SPELLCAST_SUCCEEDED")
      spellTracker:UnregisterEvent("UNIT_SPELLCAST_START")
      spellTracker:UnregisterEvent("UNIT_SPELLCAST_STOP")
    end
  end
end

------------------------------------------------------------------------

local function tryFrostbolt()
  if IsSpellInRange("Frostbolt")
  then return "Frostbolt"
  end
end

local function tryEbonbolt()
  if IsSpellInRange("Ebonbolt") and GetCD("Ebonbolt") <= 0.15
  then return "Ebonbolt"
  end
end

local function tryGlacialSpike()
  if Try("Glacial Spike") and IsTalentSelected("Glacial Spike")
  then return "Glacial Spike"
  end
end

local function tryIceLance()
  return Try("Ice Lance")
end

local function tryFlurry()
 return Try("Flurry")
end

------------------------------------------------------------------------
local function gsWillBeReady()
  local icicleCount = UnitAuraStacks("player", "Icicles")
  local e, spell, t = s.castingInfo()

  if icicleCount >= 4 and spell == "Frostbolt"
  or (icicleCount >= 5 and spell ~= "Glacial Spike")
  then
    return true
  end
end

local function instantFlurry()
  if tryFlurry() and PlayerAuraDuration("Brain Freeze") >= 0.15 then
    return "Flurry"
  end
end


local function lowIcicleFlurry()
  local icicleCount = UnitAuraStacks("player", "Icicles")

  if tryFlurry() and icicleCount <= 2 and PlayerAuraDuration("Brain Freeze") >= 0.15 then
    return "Flurry"
  end
end

local function lowTimeFlurry()
    if tryFlurry() and PlayerAuraDuration("Brain Freeze") >= 0.15 and PlayerAuraDuration("Brain Freeze") <= 3 then
        return "Flurry"
    end
end

local function flurryAfterGS()
    local castingName = UnitCastingInfo("player")
    local e, sp, t = s.castingInfo()
    t = t or 0
    if (lastSpell == "Glacial Spike" and t == 0 or sp == "Glacial Spike" and t <= 0.25) then
        return instantFlurry()
    end
    if sp == "Glacial Spike" then
        return "nothing"
    end
end

local function ffLance()
    if tryIceLance() and PlayerAuraDuration("Fingers of Frost") >= 0.15 then
        return "Ice Lance"
    end
end

local function wcLance()
    if tryIceLance() and TargetMyAuraDuration("Winter's Chill") >= 0.35 and lastSpell ~= "Ice Lance" then
        --print("wcLance")
        return "Ice Lance"
    end
end


local function wcGlacialSpike()
  local icicleDur, icicleCount = PlayerAuraDuration("Icicles")
  if tryGlacialSpike() and icicleDur >= 0.15 and gsWillBeReady() and PlayerAuraDuration("Brain Freeze") >= 3.15 then
    return "Glacial Spike"
  end
end

local function splittingIceGS()
    local icicleDur, icicleCount = PlayerAuraDuration("Icicles")
    if tryGlacialSpike() and icicleDur >= 0.15 and gsWillBeReady() and IsTalentSelected("Splitting Ice") then
        return "Glacial Spike"
    end
end

local function ebBeforeGS()
  if gsWillBeReady() and not instantFlurry() then
    return tryEbonbolt()
  end
end

local function safeFrostbolt()
  if lastSpell ~= "Flurry"  and GlobalCD() <= 0.1 then
    return tryFrostbolt()
  end
end

local function wintersReach()
    local e, sp, t = s.castingInfo()
    if PlayerAuraDuration("Winter's Reach") >= 0.15 and sp ~= "Flurry" then
        return tryFlurry()
    end
end

local stopCastTimeStamp = 0
local function skillstop()
    if GetTime() - stopCastTimeStamp > 1 then
        stopCastTimeStamp = GetTime()
    end
end
local function canstop()
    if GetTime() - stopCastTimeStamp > 1
    --or GetTime() - stopCastTimeStamp <= 0.17
    then
        return "stopcasting"
    end
end

local function stoplocked()
    if GetTime() - stopCastTimeStamp < 1
    --or GetTime() - stopCastTimeStamp <= 0.17
    then
        return "stoplocked"
    end
end

local function stopcasting()
    local e, sp, t = s.castingInfo()
    local icicleDur = PlayerAuraDuration("Icicles")
    local icicleCount = UnitAuraStacks("player", "Icicles")

    if canstop() and
            sp == "Glacial Spike" and PlayerAuraDuration("Fingers of Frost") >= 0.25
            or sp == "Ebonbolt" and PlayerAuraDuration("Brain Freeze") >= 3.25
            or (sp ~= nil and sp ~= "Glacial Spike") and icicleDur and icicleCount == 5 and PlayerAuraDuration("Brain Freeze") >= 3.25
    then
        skillstop()
        return "stopcasting"
    end
end

local signedGCD = 0
local lastGCDEnd = 0
local function updateSGCD()
    if GlobalCD() >= 0 and signedGCD >= 0 and lastGCDEnd > 0 then
        signedGCD = 0
        lastGCDEnd = 0
    elseif GlobalCD() == 0 and signedGCD == 0 and lastGCDEnd == 0 then
        lastGCDEnd = GetTime()
    elseif GlobalCD() == 0 then
        signedGCD = GetTime() - lastGCDEnd
    end
end

local function waitFrame()
    local e, sp, t = s.castingInfo()
    local icicleDur = PlayerAuraDuration("Icicles")
    local icicleCount = UnitAuraStacks("player", "Icicles")

    if (sp ~= nil and sp ~= "Glacial Spike") and icicleDur >= 0.15 and icicleCount == 5
    then
        return "nothing"
    end
end

local function offGlobal()
end

local function singleTarget()
    local result = --waitFrame() or
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Ice Lance")) or
        ((GlobalCD() > 0.15 or castingLeft > 0.4) and stopcasting()) or
        (castingLeft < 0.4 and flurryAfterGS()) or
        ((GlobalCD() > 0.15 or castingLeft > 0.2) and "nothing") or
        -- wintersReach() or
        --ffLance() or
        --wcLance() or
        --ebBeforeGS() or
        --wcGlacialSpike() or
        --wintersReach() or
        --lowIcicleFlurry() or

        --safeFrostbolt() or
        "nothing"
    return result
end

local function cleave()
    local e, sp, t = s.castingInfo()
    local castingLeft = t or 0
    local result = --waitFrame() or
    ((GlobalCD() > 0.15 or castingLeft > 0.4) and stopcasting()) or
            (castingLeft < 0.4 and flurryAfterGS()) or
            ((GlobalCD() > 0.15 or castingLeft > 0.2) and "nothing") or
            -- wintersReach() or
            ffLance() or
            wcLance() or
            ebBeforeGS() or
            splittingIceGS() or
            wcGlacialSpike() or
            lowIcicleFlurry() or

            safeFrostbolt() or
            "nothing"
    return result
end

local function LevelingPriorityNotMoving()
  if not IsPlayerMoving() then
    return
      TryCastSpellOnTarget("Frostbolt") or
      "nothing"
  end
end
local function LevelingPriority()
  local fireblast_dmg = DamageOfSpell("Fire Blast")
  local ice_lance_dmg = DamageOfSpell("Ice Lance")
  --ALERT("ice_lance_dmg: "..tostring(ice_lance_dmg))
  --ALERT("TargetHealth(): "..tostring(TargetHealth()))
  --ALERT("ice_lance_dmg > TargetHealth(): "..tostring(ice_lance_dmg > TargetHealth()))
  --ALERT("TryCastSpellOnTarget(\"Ice Lance\"): "..tostring(TryCastSpellOnTarget("Ice Lance")))
  return
  --ALERT("test: "..(TryCastSpellOnPlayer("Hymnal of the 7th Legion") or "nil")) or
  --TryCastSpellOnPlayer("Hymnal of the 7th Legion") or
  --TryCastSpellOnPlayer("Flash of Light") or
  --"nothing" or
    skill.DoNothingIfDrinking() or
    --skill.AcquireTarget() or
    skill.StartAttack() or
    --skill.DoNothingOutOfCombat() or

    --(fireblast_dmg > TargetHealth() and TryCastSpellOnTarget("Fire Blast")) or
    (ice_lance_dmg > TargetHealth() and TryCastSpellOnTarget("Ice Lance") or nil) or
    LevelingPriorityNotMoving() or
    --(skill.IsTargetInCombat() and TryCastSpellOnTarget("Fire Blast") or nil) or
    (skill.IsTargetInCombat() and TryCastSpellOnTarget("Ice Lance") or nil) or

    "nothing"
end

local private = {}
private.priorities = {}
function private.priorities.default()
  if true then
    return
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Ice Lance")) or
    "nothing"
  end
  if (UnitLevel("player") < MAX_PLAYER_LEVEL_TABLE[GetAccountExpansionLevel()]) then
    return LevelingPriority()
  end
  if cleave_mode == "off" then
    return singleTarget()
  elseif cleave_mode == "cleave" then
    return cleave()
  end
end

local function GetRangeOverride(spell)
  if spell == "Ice Lance" then
    return 40
  end
end
local function update_dir(gui)
  gui.string:SetText(string.format("%.3f", player_orientation()))
end
local GUI = {
  { ["label"] = "direction:", ["text"] = "0", ["update"] = update_dir, ["click"] = nil, ["init"] = nil },
}
local module = {
  ["run"] = rotation,
  ["gui"] = GUI,
  ["spell"] = {
    ["purge"] = {
      ["Spellsteal"] = {
        "Magic",
      },
    },
    ["dispel"] = {
      ["Remove Curse"] = {
        "Curse",
      },
    },
  },
}

local spellTracker = CreateFrame("Frame", nil, UIParent)
spellTracker:SetFrameStrata("TOOLTIP")
spellTracker:Show()

spellTracker:SetScript("OnEvent", trackLastSpell)
spellTracker:RegisterEvent("PLAYER_ENTERING_WORLD")
spellTracker:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.mage.frost = module
