_, skill = ...

local function GetRangeOverride(name)
  if name == "Chi Wave" then
    return 40
  elseif name == "Spinning Crane Kick" then
    return 8
  elseif name == "Spear Hand Strike" then
    return 5
  elseif name == "Touch of Death" then
    return 5
  end
end

local function IsOffGlobal(name)
  return name == "Spear Hand Strike"
end

local function IsStun(name)
  return name == "Leg Sweep"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsInterrupt(name)
  return name == "Spear Hand Strike"
end

local function min_single_target_dps()
  local armor = select(2, UnitArmor("target"))
  --local player_level = UnitLevel("player")
  local dr = 0.3
  --if player_level < 60 then
  --  dr = armor / (armor + 400 + 85 * player_level)
  --elseif player_level < 80 then
  --  dr = armor / (armor + 400 + 85 * (player_level + 4.5 * (player_level - 59)))
  --elseif player_level < 85 then
  --  dr = armor / (armor + 400 + 85 * player_level + (4.5 * (player_level - 59)) + (20 * (player_level - 80)) )
  --else
  --  dr = armor / (armor + (467.5 * UnitLevel("target") - 22167.5))
  --end
  --ALERT("armor: "..tostring(armor))
  --ALERT("dr: "..tostring(dr))
  local armor_multiplier = 1 - dr
  return (DamageOfSpell("Tiger Palm") / 5 + DamageOfSpell("Blackout Strike") * 2) * armor_multiplier
end
local function GetEnergy()
  return UnitPower("player", Enum.PowerType.Energy)
end
local function StaggerAmount()
  return
    tonumber(UnitAuraMatch("player", "Light Stagger", "Stagger Remaining: (%d+)") or 0) +
    tonumber(UnitAuraMatch("player", "Moderate Stagger", "Stagger Remaining: (%d+)") or 0) +
    tonumber(UnitAuraMatch("player", "Heavy Stagger", "Stagger Remaining: (%d+)") or 0)
end
local function EnergyRequiredForNextKegSmash()
  return tonumber(math.max(0, 40 + 10 - (1 + GetHaste() / 100) * 10 * GetCD("Keg Smash")))
end
local function TryChiWaveOn(unit)
  return
  IsTalentSelected("Chi Wave") and
  TryCastSpellOnUnit("Chi Wave", unit) or
  nil
end
local function TryBlackoutKick()
  return
    TryCastSpellOnTarget("Blackout Kick") or
    nil
end
local function TryKegSmash()
  return
    GetEnergy() >= 40 and
    TryCastSpellOnTarget("Keg Smash") or
    nil
end
local function TryTigerPalm()
  return
    GetEnergy() >= 25 + EnergyRequiredForNextKegSmash() and
    TryCastSpellOnTarget("Tiger Palm") or
    nil
end
local function TryRushingJadeWind()
  return
    IsTalentSelected("Rushing Jade Wind") and
    TryCastSpellOnTarget("Rushing Jade Wind") or
    nil
end
local function TryExpelHarm()
  return
    IsUsableSpell("Expel Harm") and
    GetEnergy() > 15 + EnergyRequiredForNextKegSmash() and
    --(GetCharges("Expel Harm") or 0) > 0 and <= Charges don't work for Expel Harm
    PlayerHealthPercent() < 0.33 and
    TryCastSpellOnPlayer("Expel Harm") or
    nil
end
local function RemoveFatalStagger()
  local missing = StaggerAmount() / 10 + UnitHealthLost("player", 5) * 2
  local seconds_to_live = missing > 0 and PlayerHealthMax() / missing or 1000
  local large_hit = PlayerHealthMax() * 0.4
  return
    GetCharges("Purifying Brew") > 0 and
    (seconds_to_live < 3 or StaggerAmount() > large_hit * 0.8) and
    TryCastSpellOnPlayer("Purifying Brew") or
    nil
end
local function RemoveLightStagger(ifBelowHealthPercent)
  ifBelowHealthPercent = ifBelowHealthPercent or 0.5
  return
    PlayerHealthPercent() < ifBelowHealthPercent and
    GetCharges("Purifying Brew") >= 2 and
    PlayerAuraDuration("Light Stagger") > 0 and
    TryCastSpellOnPlayer("Purifying Brew") or
    nil
end
local function RemoveModerateStagger()
  return
    GetCharges("Purifying Brew") >= 2 and
    PlayerAuraDuration("Moderate Stagger") > 0 and
    TryCastSpellOnPlayer("Purifying Brew") or
    nil
end
local function RemoveLargeStagger()
  local large_hit = PlayerHealthMax() * 0.2
  return
    GetCharges("Purifying Brew") >= 2 and
    StaggerAmount() > large_hit * 0.8 and
    TryCastSpellOnPlayer("Purifying Brew") or
    nil
end
local function DumpPurifyingBrewCharges()
  local time_til_cap = math.max(0, HastedSeconds(12) * (GetChargesMax("Ironskin Brew") - GetCharges("Ironskin Brew")) - (HastedSeconds(12) - GetChargeCD("Ironskin Brew")))
  --ALERT("GetChargesMax(\"Ironskin Brew\")"..tostring(GetChargesMax("Ironskin Brew")))
  return
    PlayerAuraDuration("Stagger") > 0 and
    GetCharges("Purifying Brew") >= GetChargesMax("Purifying Brew") - 1 and
    time_til_cap <= 4 + GetCD("Keg Smash") and
    TryCastSpellOnPlayer("Purifying Brew") or
    nil
end
local function IncreaseIronskinBrewDuration()
  return
    UnitAffectingCombat("player") and
    PlayerHealthPercent() > 0.75 and
    GetCharges("Ironskin Brew") >= GetChargesMax("Ironskin Brew") - 1 and
    --time_til_cap <= 4 + GetCD("Keg Smash") and
    PlayerAuraDuration("Ironskin Brew") < 21 - 7 and
    TryCastSpellOnPlayer("Ironskin Brew") or
    nil
end
local function DumpIronskinBrewCharges()
  local time_til_cap = math.max(0, HastedSeconds(12) * (GetChargesMax("Ironskin Brew") - GetCharges("Ironskin Brew")) - (HastedSeconds(12) - GetChargeCD("Ironskin Brew")))
  return
    UnitAffectingCombat("player") and
    GetCharges("Ironskin Brew") >= GetChargesMax("Ironskin Brew") - 1 and
    time_til_cap <= 4 + GetCD("Keg Smash") and
    TryCastSpellOnPlayer("Ironskin Brew") or
    nil
end
local function HandleHealingElixir()
  if not IsTalentSelected("Healing Elixir") then
    return nil
  end
  return
    (GetCharges("Healing Elixir") >= GetChargesMax("Healing Elixir") and PlayerHealthPercent() < 0.8 and TryCastSpellOnPlayer("Healing Elixir")) or
    (GetCharges("Healing Elixir") > 0 and PlayerHealthPercent() < 0.3 and TryCastSpellOnPlayer("Healing Elixir")) or
    nil
end
local function HandleCelestialBrew()
  return
    PlayerHealthPercent() < 0.75 and
    TryCastSpellOnPlayer("Celestial Brew") or
    nil
end
local function HandleTouchOfDeath()
  return
    IsUsableSpell("Touch of Death") and
    TryCastSpellOnTarget("Touch of Death") or
    nil
end
local function LevelingPriority()
  return
end

local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (skill.UnitIsBoss("target") or target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
local function low_level_farm()
  return
  (skill.UnitIsBoss("target") and not UnitIsFriend("player", "target") and skill.IsUnitAlive("target") and TryCastSpellOnPlayer("Hyper Organic Light Originator")) or
  initiateLowLevelCombatWithSpell("Keg Smash") or
  skill.DoNothingOutOfCombat() or
  skill.StartAttack() or
  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or

  (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(8) > 1 and (PlayerAuraDuration("Rushing Jade Wind") < HastedSeconds(9 * 0.3) and TryRushingJadeWind())) or
  (PlayerAuraDuration("Rushing Jade Wind") < HastedSeconds(9 * 0.3) and TryRushingJadeWind()) or

  TryKegSmash() or
  skill.TryUseConcentratedFlame() or

  --RemoveFatalStagger() or
  TryExpelHarm() or
  --RemoveLargeStagger() or
  IncreaseIronskinBrewDuration() or
  --DumpPurifyingBrewCharges() or
  --DumpIronskinBrewCharges() or
  (GetEnergy() >= 20 + EnergyRequiredForNextKegSmash() and TryDispelAnythingFromUnit("player")) or

  TryBlackoutStrike() or
  TryCastSpellOnTarget("Breath of Fire") or

  (GetEnergy() > EnergyRequiredForNextKegSmash() and TryTigerPalm()) or

  skill.AcquireTarget() or
  "nothing"
end
local private = {}
private.priorities = {}
function private.priorities.default()
  return
  skill.common_priority_start() or
  skill.AcquireTarget() or
  skill.StartAttack() or

  (IsTalentSelected("Chi Wave") and PlayerHealthMax() - PlayerHealth() > 2 * HealOfSpell("Chi Wave") and TryChiWaveOn("player") or nil) or
  (UnitAffectingCombat("player") and IsTalentSelected("Dampen Harm") and PlayerHealthPercent() < 0.5 and TryCastSpellOnPlayer("Dampen Harm") or nil) or
  (UnitAffectingCombat("player") and PlayerAuraDuration("Dampen Harm") <= 0 and PlayerHealthPercent() < 0.4 and TryCastSpellOnPlayer("Fortifying Brew") or nil) or

  HandleHealingElixir() or

  HandleCelestialBrew() or
  TryKegSmash() or
  TryBlackoutKick() or
  TryCastSpellOnTarget("Breath of Fire") or
  HandleTouchOfDeath() or
  (PlayerAuraDuration("Rushing Jade Wind") < HastedSeconds(9 * 0.3) and TryRushingJadeWind() or nil) or
  TryChiWaveOn("target") or

  TryExpelHarm() or
  RemoveLightStagger(0.5) or
  RemoveModerateStagger() or
  (GetEnergy() >= 20 + EnergyRequiredForNextKegSmash() and TryDispelAnythingFromUnit("player")) or


  (GetEnergy() > EnergyRequiredForNextKegSmash() and TryTigerPalm()) or

  "nothing"
end

local function update_ttl(gui)
  gui.string:SetText(string.format("%.3f", math.max(TargetHealth() / min_single_target_dps(), UnitExists("target") and UnitHealthChangedPerSecond("target", 5) or 0)))
end
local function update_stagger(gui)
  gui.string:SetText(string.format("%.0f", StaggerAmount()))
end
local GUI = {
  { ["label"] = "target ttl:", ["text"] = "0", ["update"] = update_ttl, ["click"] = nil, ["init"] = nil },
  { ["label"] = "stagger:", ["text"] = "0", ["update"] = update_stagger, ["click"] = nil, ["init"] = nil },
}
local function gui()
  return GUI
end

local function range(spell)
  if spell == "Leg Sweep" then
    return 5
  elseif spell == "Breath of Fire" then
    return 5
  elseif spell == "Rushing Jade Wind" then
    return 5
  end
end

local function update_ttl(gui)
  gui.string:SetText(string.format("%.3f", math.max(TargetHealth() / min_single_target_dps(), UnitExists("target") and UnitHealthChangedPerSecond("target", 5) or 0)))
end
local function update_stagger(gui)
  gui.string:SetText(string.format("%.0f", StaggerAmount()))
end
local GUI = {
  { ["label"] = "target ttl:", ["text"] = "0", ["update"] = update_ttl, ["click"] = nil, ["init"] = nil },
  { ["label"] = "stagger:", ["text"] = "0", ["update"] = update_stagger, ["click"] = nil, ["init"] = nil },
}

local module = {
  ["run"] = run,
  ["gui"] = GUI,
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Detox"] = {
        "Poison",
        "Disease",
      },
    },
  },
  ["interrupts"] = {
    "Spear Hand Strike",
    "Leg Sweep",
  },
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.monk.brewmaster = module
