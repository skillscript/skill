_, skill = ...
local mistweaver = {}


local function IsOffGlobal(name)
  return name == "Spear Hand Strike"
end

local function IsStun(name)
  return name == "Leg Sweep"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsInterrupt(name)
  return name == "Spear Hand Strike"
end

function mistweaver.TryTouchOfDeath()
  local target_time_to_live = math.max(TargetHealth() / min_single_target_dps(), UnitHealthChangedPerSecond("target", 5))
  --ALERT("target_time_to_live: "..tostring(target_time_to_live))
  --ALERT("min_single_target_dps(): "..tostring(min_single_target_dps()))
  return
  target_time_to_live > 8 * 2 and
  TryCastSpellOnTarget("Touch of Death") or
  nil
end

function mistweaver.TryChiWave(unit)
  return
    IsTalentSelected("Chi Wave") and
    skill.TryHealUnit("Chi Wave", unit) or
    nil
end

function mistweaver.TrySpinningCraneKick()
  return
    TryCastSpellOnTarget("Spinning Crane Kick") or
    nil
end

function mistweaver.TryRisingSunKick()
  return
    TryCastSpellOnTarget("Rising Sun Kick") or
    nil
end

function mistweaver.TryBlackoutKick()
  return
    TryCastSpellOnTarget("Blackout Kick") or
    nil
end

function mistweaver.TryTigerPalm()
  return
    TryCastSpellOnTarget("Tiger Palm") or
    nil
end

function mistweaver.TryCracklingJadeLightning()
  return
    not IsPlayerMoving() and
    skill.PlayerSpellChannelName() ~= "Crackling Jade Lightning" and
    TryCastSpellOnTarget("Crackling Jade Lightning") or
    nil
end

function mistweaver.TryEfficientAoEHeal(spell, heal_to_overheal_fraction)
  heal_to_overheal_fraction = heal_to_overheal_fraction or 2
  local sum_heal = 0
  local sum_overheal = 0
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do

    local ef_heal = HealOfSpell(spell) * skill.UnitHealReceivedMultiplier(unit)
    local heal = IsUnitInRange(spell, unit) and ef_heal or 0
    local overheal = max(0, heal - UnitHealthAndHealAbsorbDeficit(unit))
    sum_heal = sum_heal + heal
    sum_overheal = sum_overheal + overheal
  end
  return
    sum_heal > sum_overheal * heal_to_overheal_fraction and
    TryCastSpellOnPlayer(spell) or
    nil
end

function mistweaver.TryEfficientEssenceFont()
  return
    mistweaver.TryEfficientAoEHeal("Essence Font") or
    nil
end

function mistweaver.TryRefreshingJadeWind()
  if not IsTalentSelected("Refreshing Jade Mist") then
    return nil
  end
  return
    mistweaver.TryEfficientAoEHeal("Refreshing Jade Mist") or
    nil
end

function mistweaver.TryRenewingMistOnHighestDeficitPercentUnit(if_below_health_percent)
  local spell = "Renewing Mist"
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      UnitMyAuraDuration(unit, spell) < HotDurationOfSpell(spell) * 0.3 and
      skill.TryHealUnit(spell, unit, if_below_health_percent) or
      nil
    if result then
      return result
    end
  end
  return nil
end

function mistweaver.TryEnvelopingMist(if_below_health_percent)
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      UnitMyAuraDuration(unit, "Enveloping Mist") <= HotDurationOfSpell("Enveloping Mist") * 0.3 and
      skill.TryHealUnit("Enveloping Mist", unit, if_below_health_percent) or
      nil
    if result then
      return result
    end
  end
  return nil
end

function mistweaver.TryHealHighestDeficitGroupUnits(spell, if_below_health_percent, max_overheal_percent)
  if not spell then
    return nil
  end
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result = skill.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent) or nil
    if result then
      return result
    end
  end
  return nil
end

function mistweaver.soothing_mist_target()
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    if PlayerMyAuraDuration("Soothing Mist") > GlobalCD() then
      return unit
    end
  end
  return nil
end

function mistweaver.HandleSoothingMistsTarget()
  local soothing_mist_unit = mistweaver.soothing_mist_target()
  if not soothing_mist_unit or IsPlayerMoving() then
    return nil
  end
  if UnitHealthAndHealAbsorbDeficitPercent(soothing_mist_unit) < 0.05 then
    return
      TryMacro("/stopcasting") or
      nil
  end
  local soothing_mist_unit_deficit = UnitHealthAndHealAbsorbDeficit(soothing_mist_unit)
  ALERT("soothing_mist_unit_deficit: "..tostring(soothing_mist_unit_deficit))
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    ALERT("unit: "..tostring(unit))
    ALERT("UnitHealthAndHealAbsorbDeficit(unit): "..tostring(UnitHealthAndHealAbsorbDeficit(unit)))
    if unit ~= soothing_mist_unit_deficit and UnitHealthAndHealAbsorbDeficit(unit) / 2 > soothing_mist_unit_deficit then
      return
        TryMacro("/stopcasting") or
        nil
    end
  end
  return
    (UnitMyAuraDuration(soothing_mist_unit, "Enveloping Mist") <= GlobalCD() and skill.TryHealUnit("Enveloping Mist", soothing_mist_unit, 0.9, 0.0)) or
    skill.TryHealUnit("Vivify", soothing_mist_unit, 0.9, 0.0) or
    skill.TryHealUnit("Vivify", soothing_mist_unit, 0.7, 0.2) or
    "nothing"
end

function mistweaver.TryUseRisingMist()
  if not IsTalentSelected("Rising Mist") then
    return nil
  end
  return
    nil
end

function mistweaver.HandleMinimalHealing()
  local units = HighestDeficitPercentGroupUnits()
  local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
  local tank = skill.TryGetFirstTankUnit()
  return

    skill.DispelPurificationProtocol() or
    skill.DispelPhantasmalParasite() or
    TryDispelAnythingFromParty() or
    (prioritize_aoe and mistweaver.TryEfficientEssenceFont() or nil) or
    (prioritize_aoe and mistweaver.TryRefreshingJadeWind() or nil) or

    --mistweaver.TryUseRisingMist() or
    (tank and skill.will_cap_stacks("Renewing Mist") and UnitMyAuraDuration(tank, "Renewing Mist") <= HotDurationOfSpell("Renewing Mist") * 0.3 and TryCastSpellOnUnit("Renewing Mist", tank)) or
    (skill.will_cap_stacks("Renewing Mist") and mistweaver.TryRenewingMistOnHighestDeficitPercentUnit()) or
    --mistweaver.HandleSoothingMistsTarget() or
    --(skill.PlayerSpellChannelName() ~= "Soothing Mist" and mistweaver.TryHealHighestDeficitGroupUnits("Soothing Mist", 0.7)) or

    (tank and UnitMyAuraDuration(tank, "Enveloping Mist") <= GlobalCD() and skill.TryHealUnit("Enveloping Mist", tank, 0.8)) or
    mistweaver.TryEnvelopingMist(0.6) or
    mistweaver.TryHealHighestDeficitGroupUnits("Vivify", 0.8, 0.0) or
    mistweaver.TryRenewingMistOnHighestDeficitPercentUnit(0.9) or
    --mistweaver.TryHealHighestDeficitGroupUnits("Vivify", 0.7, 0.2) or

    mistweaver.TryEfficientEssenceFont() or
    mistweaver.TryRefreshingJadeWind() or

    nil
end

function mistweaver.maximum_damage()
  return
    (skill.NumEnemiesWithin(5) >=3 and mistweaver.TrySpinningCraneKick()) or
    mistweaver.TryRisingSunKick() or
    mistweaver.free_damage() or
    nil
end
function mistweaver.free_damage()
  return
    mistweaver.TryBlackoutKick() or
    mistweaver.TryTigerPalm() or
    mistweaver.TryCracklingJadeLightning() or
    nil
end
function mistweaver.TryHealingElixir()
  local spell = "Healing Elixir"
  if not IsTalentSelected(spell) then
    return nil
  end
  return
    UnitHealthAndHealAbsorbDeficit("player") > HealOfSpell(spell) * skill.UnitHealReceivedMultiplier("player") and
    TryCastSpellOnPlayer(spell) or
    nil
end
function mistweaver.stay_alive()
  return
    skill.TryHealUnit("Expel Harm", "player", 1.0, 0.0) or
    (skill.will_cap_stacks("Healing Elixir") and mistweaver.TryHealingElixir()) or
    (PlayerHealthPercent() < 0.4 and mistweaver.TryHealingElixir()) or
    nil
end
function mistweaver.save_group()
  return
    nil
end

mistweaver.priorities = {}
function mistweaver.priorities.test()
  ALERT("skill.PlayerSpellCastName(): "..tostring(skill.PlayerSpellCastName()))
  ALERT("UnitMyAuraDuration(unit, spell): "..tostring(UnitMyAuraDuration("player", "Enveloping Mist")))
  --ALERT("HotDurationOfSpell(spell) * 0.3: "..tostring(HotDurationOfSpell("Enveloping Mist") * 0.3))
  return
    mistweaver.TryEnvelopingMist(1.0) or
    nil
end
function mistweaver.priorities.default()
  return
    TryCastQueuedSpell() or
    --mistweaver.HandleSoothingMistsTarget() or
    (IsInInstance() and not UnitAffectingCombat("player") and not skill.IsMounted() and mistweaver.HandleMinimalHealing() or nil) or
    skill.common_priority_start() or
    mistweaver.stay_alive() or
    mistweaver.save_group() or

    TryDispelFromParty("Infectious Rain", 3) or
    mistweaver.HandleMinimalHealing() or

    skill.AcquireTarget() or
    skill.StartAttack() or

    (PlayerManaPercent() > 0.4 and mistweaver.maximum_damage()) or
    mistweaver.free_damage() or

--    (skill.NumEnemiesWithin(8) >= 3 and not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
--    (GetCharges("Crusader Strike") >= GetChargesMax("Crusader Strike") and holy.TryCrusaderStrike() or nil) or
--    (skill.NumEnemiesWithin(8) >= 2 and not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() * 2 + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
--    (PlayerManaPercent() < 0.1 and "nothing") or
--    (IsTalentSelected("Judgment of Light") and TargetMyAuraDuration("Judgment of Light") <= 0 and TryCastSpellOnTarget("Judgment") or nil) or
--    holy.TryCrusaderStrike() or
--    (GetCharges("Crusader Strike") >= GetChargesMax("Crusader Strike") and holy.TryCrusaderStrike() or nil) or
--    holy.TryOffensiveHolyPrism() or
--    (UnitName("target") ~= "Executor Tarvold" and GetCharges("Crusader Strike") >= 1 and GetChargeCD("Crusader Strike") <= GlobalCDMax() and paladin.GetHolyPower() >= paladin.GetHolyPowerMax() and holy.TryShieldOfTheRighteous() or nil) or
--    TryCastSpellOnTarget("Judgment") or
--    holy.TryOffensiveHolyShock() or
--    (not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() * 2 + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or

    "nothing"
end

local function update_ttl(gui)
  gui.string:SetText(string.format("%.3f", math.max(TargetHealth() / min_single_target_dps(), UnitHealthChangedPerSecond("target", 5))))
end
local GUI = {
  { ["label"] = "ttl:", ["text"] = "0", ["update"] = update_ttl, ["click"] = nil, ["init"] = nil },
}
local function gui()
  return GUI
end

local function range(spell)
  if spell == "Roll" then
    return 1
  elseif spell == "Leg Sweep" then
    return 5
  elseif spell == "Spinning Crane Kick" then
    return 8
  end
  return nil
end

local module = {
  ["gui"] = GUI,
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Detox"] = {
        "Magic",
        "Poison",
        "Disease",
      },
    },
  },
  ["interrupts"] = {
    "Leg Sweep",
  },
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return mistweaver.priority_selection.run()
end
function module.load()
  mistweaver.priority_selection = PrioritySelectionFor(mistweaver.priorities)
  AddGuiElement(mistweaver.priority_selection.gui)
end
function module.unload()
end

skill.modules.monk.mistweaver = module
