_, skill = ...

local function GetRangeOverride(name)
  if name == "Chi Wave" then
    return 40
  elseif name == "Spinning Crane Kick" then
    return 8
  elseif name == "Spear Hand Strike" then
    return 5
  elseif name == "Touch of Death" then
    return 5
  end
end

local function IsOffGlobal(name)
  return name == "Spear Hand Strike"
end

local function IsStun(name)
  return name == "Leg Sweep"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsInterrupt(name)
  return name == "Spear Hand Strike"
end

local function min_single_target_dps()
  local armor = select(2, UnitArmor("target"))
  --local player_level = UnitLevel("player")
  local dr = 0.3
  --if player_level < 60 then
  --  dr = armor / (armor + 400 + 85 * player_level)
  --elseif player_level < 80 then
  --  dr = armor / (armor + 400 + 85 * (player_level + 4.5 * (player_level - 59)))
  --elseif player_level < 85 then
  --  dr = armor / (armor + 400 + 85 * player_level + (4.5 * (player_level - 59)) + (20 * (player_level - 80)) )
  --else
  --  dr = armor / (armor + (467.5 * UnitLevel("target") - 22167.5))
  --end
  --ALERT("armor: "..tostring(armor))
  --ALERT("dr: "..tostring(dr))
  local armor_multiplier = 1 - dr
  return (DamageOfSpell("Tiger Palm") / 5 + DamageOfSpell("Blackout Kick") * 2) * armor_multiplier
end
local function GetEnergy()
  return UnitPower("player", Enum.PowerType.Energy)
end
local function GetEnergyMax()
  return UnitPowerMax("player", Enum.PowerType.Energy)
end
local function GetChi()
  return UnitPower("player", Enum.PowerType.Chi)
end
local function GetChiMax()
  return UnitPowerMax("player", Enum.PowerType.Chi)
end
local function TryStormEarthAndFire()
  local target_time_to_live = math.max(TargetHealth() / min_single_target_dps(), UnitHealthChangedPerSecond("target", 5))
  return
    target_time_to_live > 15 * 1.5 and
    TryCastSpellOnTarget("Storm, Earth, and Fire") or
    nil
end
local function TryTouchOfDeath()
  local target_time_to_live = math.max(TargetHealth() / min_single_target_dps(), UnitHealthChangedPerSecond("target", 5))
  --ALERT("target_time_to_live: "..tostring(target_time_to_live))
  --ALERT("min_single_target_dps(): "..tostring(min_single_target_dps()))
  return
  --target_time_to_live > 8 * 2 and
  --TryCastSpellOnTarget("Touch of Death") or
  nil
end
local function TryWhirlingDragonPunch()
  return
    GetCD("Fists of Fury") > 0 and
    GetCD("Rising Sun Kick") > 0 and
    TryCastSpellOnTarget("Whirling Dragon Punch") or
    nil
end
local function TryChiWaveOn(unit)
  return
    IsTalentSelected("Chi Wave") and
    TryCastSpellOnUnit("Chi Wave", unit) or
    nil
end
local function TrySpinningCraneKick()
  return
    GetChi() >= 2 and
    TryCastSpellOnTarget("Spinning Crane Kick") or
    nil
end
local function TryRisingSunKick()
  return
    GetChi() >= 2 and
    TryCastSpellOnTarget("Rising Sun Kick") or
    nil
end
local function TryFistsOfFury()
  return
    GetChi() >= 3 and
    TargetHealth() * 0.8 > DamageOfSpell("Fists of Fury") and
    TryCastSpellOnTarget("Fists of Fury") or
    nil
end
local function TryBlackoutKick()
  return
    GetChi() >= 2 and
    TryCastSpellOnTarget("Blackout Kick") or
    nil
end
local function TryTigerPalm()
  return
    GetEnergy() >= 50 and
    GetChi() <= GetChiMax() - 2 and
    TryCastSpellOnTarget("Tiger Palm") or
    nil
end
local function TryFistOfTheWhiteTiger()
  return
    IsTalentSelected("Fist of the White Tiger") and
    GetEnergy() >= 40 and
    GetChi() <= GetChiMax() - 3 and
    TryCastSpellOnTarget("Fist of the White Tiger") or
    nil
end
local private = {}
private.priorities = {}
function private.priorities.default()
  return
    torghast.torghast_stuff() or
    skill.common_priority_start() or
    skill.AcquireTarget() or
    skill.StartAttack() or
    (UnitAuraDuration("player", "Bad Karma", "MAW") > 0 and TryCastSpellOnTarget("Provoke") or nil) or

    skill.DoNothingIfCasting() or

    (PlayerHealthMax() - PlayerHealth() > 2 * HealOfSpell("Chi Wave") and TryChiWaveOn("player") or nil) or
    (GetEnergy() >= 30 and PlayerHealthPercent() < 0.25 and not IsPlayerMoving() and TryCastSpellOnPlayer("Vivify", "player")) or

    skill.TryInterruptTarget() or
    (GetEnergy() >= 20 and TryDispelAnythingFromUnit("player")) or
    TryCastQueuedSpell() or
    skill.AcquireTarget() or
    skill.StartAttack() or
    TryTouchOfDeath() or
    (TargetMyAuraDuration("Touch of Death") <= 0 and TryStormEarthAndFire()) or
    (not IsPlayerMoving() and TryCastSpellOnTarget("Faeline Stomp") or nil) or
    TryFistOfTheWhiteTiger() or
    TryFistsOfFury() or
    (skill.NumEnemiesWithin(8) >= 3 and TrySpinningCraneKick()) or
    TryRisingSunKick() or
    TryChiWaveOn("target") or
    TryTigerPalm() or
    (GetChi() > GetChiMax() - 2 and TryBlackoutKick()) or

    "nothing"
end

local function update_ttl(gui)
  gui.string:SetText(string.format("%.3f", math.max(TargetHealth() / min_single_target_dps(), UnitHealthChangedPerSecond("target", 5))))
end
local GUI = {
  { ["label"] = "ttl:", ["text"] = "0", ["update"] = update_ttl, ["click"] = nil, ["init"] = nil },
}
local function gui()
  return GUI
end

local function range(spell)
  if spell == "Roll" then
    return 0
  elseif spell == "Flying Serpent Kick" then
    return 0
  elseif spell == "Leg Sweep" then
    return 5
  elseif spell == "Spinning Crane Kick" then
    return 8
  elseif spell == "Storm, Earth, and Fire" then
    return 8
  elseif spell == "Zen Pilgrimage" then
    return 0
  end
end

local module = {
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Detox"] = {
        "Poison",
        "Disease",
      },
    },
  },
  ["interrupts"] = {
    "Spear Hand Strike",
    "Leg Sweep",
  },
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.monk.windwalker = module
