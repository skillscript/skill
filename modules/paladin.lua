_, skill = ...

local private = {}
paladin = skill.modules.paladin
paladin.range_check = LibStub("LibRangeCheck-2.0")

paladin.spell = {}
function paladin.spell.range(spell)
  if spell == "Consecration" then
    return 5
  elseif spell == "Blinding Light" then
    return 8
  elseif spell == "Shield of the Righteous" then
    return 4
  end
end

function paladin.GetHolyPower()
  return UnitPower("player", Enum.PowerType.HolyPower)
end

function paladin.GetHolyPowerMax()
  return 5
end

function paladin.IsMounted()
  return skill.IsMounted() and PlayerAuraDuration("Divine Steed") <= 0
end
function private.DoNothingIfMounted()
  return
    paladin.IsMounted() and
    "nothing" or nil
end

function paladin.TryLayOnHandsOn(unit)
  return
    UnitAuraDuration(unit, "Forbearance") <= 0 and
    TryCastSpellOnUnit("Lay on Hands", unit) or
    nil
end

function paladin.TryDivineShield()
  return
    PlayerAuraDuration("Forbearance") <= 0 and
    UnitName("target") ~= "Oryphrion" and
    TryCastSpellOnPlayer("Divine Shield") or
    nil
end

function private.TryActivateAura(aura)
  return
    PlayerAuraDuration(aura) <= 0 and
    TryCastSpellOnPlayer(aura) or
    nil
end
private.chosen_aura = "Devotion Aura"
private.aura_timeout = 0
private.AURA_TIMEOUT = 10
function paladin.HandleAuras()
  if IsMounted() or (IsResting() and not UnitAffectingCombat("player")) then
    return
      private.TryActivateAura("Crusader Aura") or
      nil
  end
  if IsPlayerMoving() or skill.IsCasting() then
    return nil
  end
  local _, range_max = paladin.range_check:GetRange("target")
  local range = range_max or 100
  local should_activate_chosen_aura =
    not UnitExists("target") or
    range >= 30
  return
    (should_activate_chosen_aura and paladin.TryActivateChosenAura() or nil) or
    (GetTime() > private.aura_timeout and paladin.TryActivateChosenAura() or nil) or
    nil
end
function paladin.TryActivateChosenAura()
  return
    not IsMounted() and
    private.TryActivateAura(private.chosen_aura) or
    nil
end
function paladin.GetHolyPowerCost(spell)
  return CostOfSpell(spell, "Holy Power")
end
function paladin.GetHolyPowerGenerated(spell)
  return GenerationOfSpell(spell, "Holy Power") * (PlayerAuraDuration("Holy Avenger") > GlobalCD() + GetCastTime(spell) and 3 or 1)
end
function paladin.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    paladin.GetHolyPowerCost(spell) >= paladin.GetHolyPower() and
    skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end


function private.CausesForbearance(name)
  return
    name == "Lay on Hands" or
    name == "Blessing of Protection" or
    name == "Blessing of Spellwarding" or
    name == "Divine Shield"
end

function paladin.TrySaveUnitWithWordOfGlory(unit)
  return
  IsUsableSpell("Word of Glory") and
  skill.TryHealUnit("Word of Glory", unit, 0.1) or
  nil
end

function paladin.save_group()
  local units = HighestDeficitPercentGroupUnits()
  for _, unit in ipairs(units) do
    local result =
    skill.TryHealUnit("Word of Glory", unit, 0.15) or
    (skill.IsUnitTank(unit) and skill.TryHealUnit("Lay on Hands", unit, 0.21, 1.0) or nil) or
    (skill.IsUnitHealer(unit) and skill.TryHealUnit("Lay on Hands", unit, 0.20, 1.0) or nil) or
    (skill.IsUnitDamager(unit) and skill.TryHealUnit("Lay on Hands", unit, 0.19, 1.0) or nil) or
    -- TODO: Use Blessing of Sacrifice for dps spikes.
    nil
    if result then
      return result
    end
  end
  return nil
end

function paladin.TrySaveUnitWithBlessingOfProtection(unit)
  return
  --private.ShouldReceiveBlessingOfProtection(unit) and
  --TryCastSpellOnUnit("Blessing of Protection", unit) or
  nil
end

function paladin.TryLayOnHandsOnUnitBelowFivePercent(unit)
  return skill.TryHealUnit("Lay on Hands", unit, 0.05, 1.0)
end

function paladin.TryLayOnHandsOnUnitBelowTenPercent(unit)
  return skill.TryHealUnit("Lay on Hands", unit, 0.1, 1.0)
end

function private.ForbearanceAllowsCastSpellOnUnit(spell, unit)
  return not private.CausesForbearance(spell) or UnitAuraDuration(unit, "Forbearance") <= 0
end

function private.TryCastSpellOnUnit(spell, unit)
  return private.ForbearanceAllowsCastSpellOnUnit(spell, unit) and private.try_cast_spell_on_unit(spell, unit) or nil
end

function paladin.can_efficiently_spend_holy_power(spell)
  if PlayerAuraDuration("Divine Purpose") > GlobalCD() then
    return false
  end
  local can_efficiently_spend_holy_power = GetModule()["can_efficiently_spend_holy_power"]
  return
    (paladin.GetHolyPower() < paladin.GetHolyPowerMax()) or
    (can_efficiently_spend_holy_power and can_efficiently_spend_holy_power()) or
    (not IsUnitInRange("Auto Attack", "target") and not IsPlayerMoving())
end

function paladin.TryHammerOfWrath()
  return
  (paladin.GetHolyPower() < paladin.GetHolyPowerMax() - 1 or not IsUnitInRange("Auto Attack", "target")) and
  TryCastSpellOnTarget("Hammer of Wrath") or
  nil
end


function private.HealOfSpell(spell)
  return
    (spell == "Lay on Hands" and PlayerHealthMax() or nil) or
    private.heal_of_spell(spell) or
    nil
end

function paladin.load()
  private.try_cast_spell_on_unit = TryCastSpellOnUnit
  TryCastSpellOnUnit = private.TryCastSpellOnUnit
  private.heal_of_spell = HealOfSpell
  HealOfSpell = private.HealOfSpell
  private.do_nothing_if_mounted = skill.DoNothingIfMounted
  skill.DoNothingIfMounted = private.DoNothingIfMounted

  private.frame:RegisterEvent("UNIT_AURA")
  private.frame:SetScript("OnEvent", private.on_event)
end

function paladin.unload()
  TryCastSpellOnUnit = private.try_cast_spell_on_unit
  HealOfSpell = private.heal_of_spell
  skill.DoNothingIfMounted = private.do_nothing_if_mounted

  private.frame:UnregisterEvent("UNIT_AURA")
  private.frame:SetScript("OnEvent", nil)
end
local AURAS =  {
  "Devotion Aura",
  "Retribution Aura",
  "Concentration Aura",
}
private.frame = CreateFrame("Frame", nil, UIParent)
function private.on_event(self, event, ...)
  if event == "UNIT_AURA" then
    if PlayerMyAuraDuration("Crusader Aura") > 0 then
      private.aura_timeout = GetTime() + private.AURA_TIMEOUT
    else
      for _, aura in ipairs(AURAS) do
        if PlayerMyAuraDuration(aura) > 0 then
          private.chosen_aura = aura
          break
        end
      end
    end
  elseif event == "PLAYER_REGEN_DISABLED" then
    private.aura_timeout = GetTime() + 5
  end
end

function paladin.ascension()
  if not UnitExists("pet") then
    return nil
  end
  local map = C_Map.GetBestMapForUnit("player")
  if not map or not C_Map.GetMapInfo(map).name == "Ascension Coliseum" then
    return nil
  end
  local char = UnitName("pet")
  if char == "Kleia" then
    return
      "ACTIONBUTTON1"
  elseif char == "Pelagos" then
    return
      (not UnitExists("target") and "nothing") or
      "ACTIONBUTTON3"
  end
  --ALERT("char: "..tostring(char))
  return nil
end
