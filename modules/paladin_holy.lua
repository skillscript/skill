_, skill = ...

holy = {}

function FREEDOM()
  local tank = skill.TryGetFirstTankUnit()
  return
    (tank and PlayerAuraDuration("Wretched Phlegm") > 0 and TryCastSpellOnTarget(tank) or nil) or
    nil
end

local function range(spell)
  return skill.modules.paladin.spell.range(spell)
end

local function IsOffGlobal(name)
  return
    name == "Lay on Hands" or
    name == "Will to Survive" or
    name == "Healthstone" or
    name == "Rebuke"
end


local function IsStun(name)
  return name == "Hammer of Justice"
end

local function IsDisorient(name)
  return name == "Blinding Light"
end

local function IsIncapacitate(name)
  return name == "Repentance"
end

local function IsSilence(name)
  return name == "Avenger's Shield"
end

local function IsInterrupt(name)
  return name == "Rebuke" or name == "Avenger's Shield"
end


function holy.find_beacon_target(kind)
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if UnitMyAuraDuration(unit, "Beacon of "..kind) > 0 then
      return unit
    end
  end
  return nil
end

holy.manual_beacon = {
  ["Light"] = nil,
  ["Faith"] = nil,
}
function holy.choose_beacon_target_for_role(role, n)
  local x = 1
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if skill.IsUnitRole(unit, role) then
      if x >= n then
        return unit
      else
        x = x + 1
      end
    end
  end
  return nil
end

holy.beacon_targets = {}
function holy.by_beacon_priority(a, b)
  local a_role = UnitGroupRolesAssigned(a)
  local b_role = UnitGroupRolesAssigned(b)
  if a_role == b_role then
    local a_name = UnitName(a)
    local b_name = UnitName(b)
    if not a_name then
      return false
    elseif not b_name then
      return true
    end
    return a_name < b_name
  elseif a_role == "TANK" then
    return true
  elseif b_role == "TANK" then
    return false
  elseif a_role == "HEALER" then
    return true
  elseif b_role == "HEALER" then
    return false
  end
  return false
end
function holy.calculate_beacon_targets()
  table.wipe(holy.beacon_targets)
  if IsTalentSelected("Beacon of Virtue") then
    return
  end
  for _, unit in ipairs(GroupCombatCastUnits()) do
    table.insert(holy.beacon_targets, unit)
  end
  if #holy.beacon_targets == 0 then
    table.insert(holy.beacon_targets, "player")
  end
  table.sort(holy.beacon_targets, holy.by_beacon_priority)
  if GetMinimapZoneText() == "Proving Grounds" then
    table.wipe(holy.beacon_targets)
    for _, unit in ipairs(GroupCombatCastUnits()) do
      if UnitName(unit) == "Oto the Protector" then
        table.insert(holy.beacon_targets, unit)
        break
      end
    end
    table.insert(holy.beacon_targets, "player")
  end
  --ALERT("===========")
  --for i, unit in ipairs(holy.beacon_targets) do
  --  ALERT(i..": "..UnitGroupRolesAssigned(unit)..": "..UnitName(unit))
  --end
end
function holy.KeepUpBeaconOf(kind, unit)
  local duration = UnitMyAuraDuration(unit, "Beacon of Light") + UnitMyAuraDuration(unit, "Beacon of Faith")
  return
    duration <= 10 * 60 and
    TryCastSpellOnUnit("Beacon of "..kind, unit) or
    nil
end
function holy.HandleBeaconOf(kind)
  local unit = holy.manual_beacon[kind]
  if not unit then
    unit = holy.beacon_targets[kind == "Light" and 1 or 2]
  end
  return
    unit and
    skill.IsUnitAlive(unit) and
    holy.KeepUpBeaconOf(kind, unit) or
    nil
end
function holy.HandleBeacons()
  if skill_char_settings["manual_beacons"] then
    return nil
  end
  if paladin.IsMounted() or IsTalentSelected("Beacon of Virtue") then
    return nil
  end
  return
    holy.HandleBeaconOf("Light") or
    (IsTalentSelected("Beacon of Faith") and GetNumGroupMembers() > 1 and holy.HandleBeaconOf("Faith") or nil) or
    nil
end

function holy.KeepUpGlimmerOfLight()
  --for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
  --  local range_max = skill.UnitRangeMax(unit)
  --  local deficit = min(lod_healing, UnitHealthAndHealAbsorbDeficit(unit))
  --  lod_deficit = lod_deficit + ((range_max and range_max <= 5) and deficit or 0)
  --end
  return nil
end

local function PredictedHealing(spell, unit)
  if not unit then
    return 0
  end
  local range_max = skill.UnitRangeMax(unit)
  local range_multiplier = 1 + (min(35, 40 - (range_max or 40))) / 35 * GetMasteryEffect() / 100
  return HealOfSpell(spell) * range_multiplier
end

function holy.TryEfficientLightOfDawn()
  local lod_deficit = 0
  local lod_healing = PredictedHealing("Light of Dawn", "player")
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local range_max = skill.UnitRangeMax(unit)
    local deficit = min(lod_healing, UnitHealthAndHealAbsorbDeficit(unit))
    lod_deficit = lod_deficit + ((range_max and range_max <= 5) and deficit or 0)
  end
  return
    lod_deficit > HealOfSpell("Word of Glory") * 1.5 and
    skill.TryHealUnit("Light of Dawn", "player") or
    nil
end
local function dmg()
  if UnitIsFriend("player", "target") then
    return
  end
  return
    TryCastSpellOnTarget("Judgment") or
    TryCastSpellOnTarget("Crusader Strike") or
    (PlayerMana() > PlayerManaMax() * 0.5 and TryCastSpellOnTarget("Holy Shock") or nil) or
    skill.TryUseConcentratedFlame() or
    TryCastSpellOnTarget("Consecration") or
    nil
end
local PARTY = {
  "party1",
  "party2",
  "party3",
  "party4",
}
local function HandleAvengingCrusader()
  if PlayerAuraDuration("Avenging Crusader") > GlobalCD() then
    return dmg()
  else
    local sum_enemy_hp = 0
    for guid, unit in pairs(skill.EnemiesWithin(8)) do
      --ALERT("unit: "..tostring(unit))
      sum_enemy_hp = sum_enemy_hp + UnitHealth(unit)
    end
    local sum_party_hp = UnitHealthMax("player") * 0.5
    local num_party_members = 1
    for i, unit in ipairs(PARTY) do
      if UnitExists(unit) then
        sum_party_hp = sum_party_hp + UnitHealthMax(unit)
        num_party_members = num_party_members + 1
      end
    end
    local sum_party_dps = sum_party_hp / num_party_members * 0.2
    --ALERT("skill.NumEnemiesWithin(8): "..tostring(skill.NumEnemiesWithin(8)))
    --ALERT("sum_party_dps: "..tostring(sum_party_dps))
    --ALERT("sum_enemy_hp: "..tostring(sum_enemy_hp))
    --ALERT("sum_party_xx: "..tostring(sum_party_dps * 20))
    if sum_enemy_hp > sum_party_dps * 20 then
      return TryCastSpellOnPlayer("Avenging Wrath") or nil
    end
  end
end

function holy.save_group()
  return
    paladin.save_group() or
    nil
end
function holy.stay_alive()
  if not skill.IsUnitAlive("player") then
    return nil
  end
  return
    (PlayerHealthPercent() < 0.2 and TryCastSpellOnPlayer("Divine Shield")) or
    (PlayerAuraDuration("Divine Shield") <= 0 and GetCD("Divine Shield") > GlobalCD() and (
      (PlayerHealthPercent() < 0.1 and TryCastSpellOnPlayer("Lay on Hands")) or
      nil
    )) or
    (not IsPlayerMoving() and PlayerHealthPercent() < 0.1 and TryCastSpellOnUnit("Flash of Light", "player") or nil) or
    nil
end

function holy.TryHealWithoutOverheal(spell, unit)
  return
    IsUsableSpell(spell) and
    skill.TryHealUnit(spell, unit, 0.9, 0.0) or
    nil
end
function holy.NoOverhealWordOfGlory(unit)
  return holy.TryHealWithoutOverheal("Word of Glory", unit)
end
function holy.NoOverhealLightOfDawn(unit)
  return holy.TryHealWithoutOverheal("Light of Dawn", unit)
end
function holy.NoOverhealHolyLight(unit)
  return
    not IsPlayerMoving() and
    holy.TryHealWithoutOverheal("Holy Light", unit) or
    nil
end
function holy.NoOverhealFlashOfLight(unit)
  return
    not IsPlayerMoving() and
    holy.TryHealWithoutOverheal("Flash of Light", unit) or
    nil
end
function holy.NoOverhealHolyShock(unit)
  return holy.TryHealWithoutOverheal("Holy Shock", unit)
end
function holy.NoOverhealLightOfTheMartyr(unit)
  if UnitGUID(unit) == UnitGUID("player") or not skill.IsUnitPlayerFriend(unit) then
    return nil
  end
  return holy.TryHealWithoutOverheal("Light of the Martyr", unit)
end

holy.heal_funcs = {}
function holy.get_heal_func(spell, ifBelowHealthPercent, maxOverhealPercent)
  holy.heal_funcs[spell] = holy.heal_funcs[spell] or {}
  holy.heal_funcs[spell][ifBelowHealthPercent] = holy.heal_funcs[spell][ifBelowHealthPercent] or {}
  if not holy.heal_funcs[spell][ifBelowHealthPercent][maxOverhealPercent] then
    holy.heal_funcs[spell][ifBelowHealthPercent][maxOverhealPercent] = function(unit)
      return
        (not (spell == "Light of the Martyr" and UnitGUID(unit) == UnitGUID("player"))) and
        IsUsableSpell(spell) and
        skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent) or
        nil
    end
  end
  return holy.heal_funcs[spell][ifBelowHealthPercent][maxOverhealPercent]
end

function holy.TryFlashOfLight(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    not IsPlayerMoving() and
    skill.TryHealUnit("Flash of Light", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function holy.TryInfusedFlashOfLight(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraDuration("Infusion of Light") > GlobalCD() + GetCastTime("Flash of Light") and
    holy.TryFlashOfLight(unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function holy.TryHolyLight(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    not IsPlayerMoving() and
    skill.TryHealUnit("Holy Light", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function holy.TryInfusedHolyLight(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraDuration("Infusion of Light") > GlobalCD() + GetCastTime("Holy Light") and
    holy.TryHolyLight(unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function holy.TryLightOfTheMartyr(unit, ifPlayerHealthAbove, ifBelowHealthPercent, maxOverhealPercent)
  return
    UnitGUID(unit) ~= UnitGUID("player") and
    PlayerHealthPercent() > ifPlayerHealthAbove and
    skill.TryHealUnit("Light of the Martyr", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end

function holy.UnitBeaconDuration(unit)
  return max(UnitMyAuraDuration(unit, "Beacon of Light"), max(UnitMyAuraDuration(unit, "Beacon of Virtue"), UnitMyAuraDuration(unit, "Beacon of Faith")))

end
function holy.HealTankBelow50Percent(tank)
  if not tank or UnitHealthPercent(tank) > 0.5 then
    return nil
  end
  return
    skill.TryHealUnit("Word of Glory", tank) or
    holy.TryInfusedFlashOfLight(tank) or
    skill.TryHealUnit("Holy Shock", tank) or
    holy.TryLightOfTheMartyr(tank, 0.5, 0.3) or
    nil
end
function holy.HandleAoeHeal()
  return
    nil or
    nil
end
function holy.HealHighestDeficitTargets(ifBelowHealthPercent, maxOverhealPercent)
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      (paladin.GetHolyPower() < paladin.GetHolyPowerMax() and skill.TryHealUnit("Holy Shock", ifBelowHealthPercent, maxOverhealPercent) or nil) or
      (paladin.GetHolyPower() < paladin.GetHolyPowerMax() and GetCD("Holy Shock") > GetCastTime("Holy Light") and holy.UnitBeaconDuration(unit) > GetCastTime("Holy Light") and holy.TryHolyLight(unit, ifBelowHealthPercent, maxOverhealPercent) or nil) or
      holy.TryEfficientLightOfDawn() or
      skill.TryHealUnit("Word of Glory", ifBelowHealthPercent, maxOverhealPercent) or
      skill.TryHealUnit("Holy Shock", ifBelowHealthPercent, maxOverhealPercent) or
      (GetCD("Holy Shock") > GetCastTime("Holy Light") and holy.TryHolyLight(unit, ifBelowHealthPercent, maxOverhealPercent) or nil) or
      (GetCD("Holy Shock") > GetCastTime("Holy Light") and holy.TryLightOfTheMartyr(unit, 0.5, ifBelowHealthPercent, maxOverhealPercent) or nil) or
      nil
    if result then
      ALERT("--------------")
      ALERT("ifBelowHealthPercent:"..tostring(ifBelowHealthPercent))
      ALERT("maxOverhealPercent:"..tostring(maxOverhealPercent))
      ALERT("result:"..tostring(unit))
      return result
    end
  end
  return nil
end

local function infused(spell)
  if not (spell == "Flash of Light" or spell == "Holy Light") then
    ALERT_ONCE(string.format("Trying to cast an infused '%s'.", spell))
    return nil
  end
  return
    PlayerAuraDuration("Infusion of Light") > GlobalCD() + GetCastTime(spell) and
    spell or
    nil
end
local function generating(spell, unit)
  --paladin.GetHolyPower() + private.HolyPowerGenerated(spell, unit) <= paladin.GetHolyPowerMax()
end
function holy.HolyPowerGenerated(spell, unit)
  if spell == "Flash of Light" or spell == "Holy Light" then
    return holy.UnitBeaconDuration(unit) > GlobalCD() + GetCastTime(spell) and (PlayerAuraDuration("Holy Avenger") > GlobalCD() + GetCastTime(spell) and 3 or 1) or 0
  end
  return paladin.GetHolyPowerGenerated(spell)
end
--local function generating(spell)
--  local generated = paladin.GetHolyPowerGenerated(spell)
--  if generated <= 0 then
--    ALERT_ONCE(string.format("Trying to cast a Holy Power generating spell: '%s'.", spell))
--    return nil
--  end
--  generated = generated * (PlayerAuraDuration("Holy Avenger") > GlobalCD() + GetCastTime(spell) and 3 or 1)
--  return nil
--end
function holy.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent, max_holy_power_wasted, min_holy_power_generated)
  if not spell then
    return nil
  end
  if spell == "Light of the Martyr" and UnitGUID(unit) == UnitGUID("player") then
    return nil
  end
  local generated
  if min_holy_power_generated then
    generated = holy.HolyPowerGenerated(spell, unit)
    if generated < min_holy_power_generated then
      return nil
    end
  end
  if max_holy_power_wasted then
    generated = generated or holy.HolyPowerGenerated(spell, unit)
    local wasted = max(0, paladin.GetHolyPower() + generated - paladin.GetHolyPowerMax())
    if wasted > max_holy_power_wasted then
      return nil
    end
  end
  --ALERT("++:"..tostring(spell))
  --ALERT("if_below_health_percent: "..tostring(if_below_health_percent))
  --ALERT("max_overheal_percent: "..tostring(max_overheal_percent))
  --ALERT("max_holy_power_wasted: "..tostring(max_holy_power_wasted))
  --ALERT("min_holy_power_generated: "..tostring(min_holy_power_generated))
  return skill.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent)
end

function holy.TryHealHighestDeficitGroupUnits(spell, if_below_health_percent, max_overheal_percent)
  if not spell then
    return nil
  end
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result = holy.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent) or nil
    if result then return result end
  end
  return nil
end
local new = false
function holy.HandleMinimalHealing()
  if new then
    local units = HighestDeficitPercentGroupUnits()
    -- Generating Holy Power, no waste, no overheal
    for _, unit in ipairs(units) do
      local result =
      holy.TryHealUnit(infused("Flash of Light"), unit, 0.6, 0.0, 0, 1) or
      holy.TryHealUnit(infused("Holy Light"), unit, 0.9, 0.0, 0, 1) or
      holy.TryHealUnit("Holy Shock", unit, 0.9, 0.0, 0) or
      nil
      if result then
        return result
      end
    end
    -- Spending at max Holy Power
    if paladin.GetHolyPower() >= paladin.GetHolyPowerMax() then
      for _, unit in ipairs(units) do
        local result =
        holy.TryEfficientLightOfDawn() or
        skill.TryHealUnit("Word of Glory", unit, 0.9, 0.0) or
        skill.TryHealUnit("Word of Glory", unit, 0.9, 0.2) or
        nil
        if result then
          return result
        end
      end
    end
    -- Generating Holy Power, no waste, low overheal
    for _, unit in ipairs(units) do
      local result =
      holy.TryHealUnit(infused("Flash of Light"), unit, 0.6, 0.2, 0, 1) or
      holy.TryHealUnit(infused("Holy Light"), unit, 0.9, 0.2, 0, 1) or
      holy.TryHealUnit("Holy Shock", unit, 0.9, 0.2, 0) or
      nil
      if result then
        return result
      end
    end
    -- Generating Holy Power, no waste, high overheal
    for _, unit in ipairs(units) do
      local result =
      holy.TryHealUnit(infused("Flash of Light"), unit, 0.6, 0.5, 0, 1) or
      holy.TryHealUnit(infused("Holy Light"), unit, 0.9, 0.5, 0, 1) or
      holy.TryHealUnit("Holy Shock", unit, 0.9, 0.5, 0) or
      nil
      if result then
        return result
      end
    end
    -- Spending
    if skill.IsUsable("Word of Glory") or skill.IsUsable("Light of Dawn") then
      for _, unit in ipairs(units) do
        local result =
        holy.TryEfficientLightOfDawn() or
        skill.TryHealUnit("Word of Glory", unit, 0.9, 0.0) or
        skill.TryHealUnit("Word of Glory", unit, 0.9, 0.3) or
        skill.TryHealUnit("Word of Glory", unit, 0.9, 0.5) or
        nil
        if result then
          return result
        end
      end
    end
    -- Wasting Holy Power, no overheal
    for _, unit in ipairs(units) do
      local result =
      holy.TryHealUnit(infused("Flash of Light"), unit, 0.6, 0.0) or
      holy.TryHealUnit(infused("Holy Light"), unit, 0.9, 0.0) or
      holy.TryHealUnit("Holy Shock", unit, 0.9) or
      nil
      if result then
        return result
      end
    end
    for _, unit in ipairs(units) do
      local result =
      -- Wasting Holy Power, overheal
      holy.TryHealUnit(infused("Flash of Light"), unit, 0.6, 0.5) or
      holy.TryHealUnit(infused("Holy Light"), unit, 0.9, 0.5) or
      holy.TryHealUnit("Holy Shock", unit, 0.9, 0.5) or
      nil
      if result then
        return result
      end
    end
    -- Non-generating, no overheal
    for _, unit in ipairs(units) do
      local result =
      holy.TryHealUnit("Flash of Light", unit, 0.5, 0.0) or
      holy.TryHealUnit("Holy Light", unit, 0.9, 0.0) or
      nil
      if result then
        return result
      end
    end
    --for _, unit in ipairs(units) do
    --  local result =
    --  -- Non-generating, overheal
    --  private.TryHealUnit("Flash of Light", unit, 0.5, 0.2, paladin.GetHolyPowerMax(), 0) or
    --  private.TryHealUnit("Holy Light", unit, 0.9, 0.2, paladin.GetHolyPowerMax(), 0) or
    --  private.TryHealUnit("Flash of Light", unit, 0.5, 0.5, paladin.GetHolyPowerMax(), 0) or
    --  private.TryHealUnit("Holy Light", unit, 0.9, 0.5, paladin.GetHolyPowerMax(), 0) or
    --  nil
    --  if result then
    --    return result
    --  end
    --end
    return nil
  elseif true then
    local units = HighestDeficitPercentGroupUnits()
    local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
    return
      holy.TryHealHighestDeficitGroupUnits("Holy Shock", 0.9, 0.0) or
      skill.DispelPurificationProtocol() or
      skill.DispelPhantasmalParasite() or
      TryDispelAnythingFromParty() or
      (prioritize_aoe and holy.TryEfficientLightOfDawn() or nil) or
      holy.TryHealHighestDeficitGroupUnits("Word of Glory", 0.9, 0.0) or
      (PlayerManaPercent() > 0.7 and holy.TryHealHighestDeficitGroupUnits(infused("Flash of Light"), 0.9, 0.0) or nil) or
      holy.TryEfficientLightOfDawn() or
      holy.TryHealHighestDeficitGroupUnits("Holy Shock", 0.8, 0.3) or
      holy.TryHealHighestDeficitGroupUnits("Word of Glory", 0.8, 0.3) or
      (PlayerManaPercent() < 0.3 and holy.TryHealHighestDeficitGroupUnits(infused("Holy Light"), 0.9, 0.0) or nil) or
      holy.TryHealHighestDeficitGroupUnits("Holy Shock", 0.9, 0.7) or
      (skill.NumEnemiesWithin(8) >= 2 and not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() * 2 + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
      (PlayerManaPercent() > 0.1 and holy.TryHealHighestDeficitGroupUnits(infused("Flash of Light"), 0.7, 0.4) or nil) or
      --(PlayerManaPercent() > 0.1 and holy.TryHealHighestDeficitGroupUnits(PlayerHealthPercent() > (IsPlayerMoving() and 0.5 or 0.7) and "Light of the Martyr" or nil, 0.7, 0.0) or nil) or
      (PlayerManaPercent() > 0.1 and holy.TryHealHighestDeficitGroupUnits(PlayerHealthPercent() > 0.7 and "Light of the Martyr" or nil, 0.7, 0.0) or nil) or
      (PlayerManaPercent() > 0.1 and holy.TryHealHighestDeficitGroupUnits("Flash of Light", 0.8, 0.0) or nil) or
      (PlayerManaPercent() < 0.1 and holy.TryHealHighestDeficitGroupUnits("Holy Light", 0.8, 0.0) or nil) or
      nil
  else
    local tank = skill.TryGetFirstTankUnit()
  return
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealHolyShock, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealHolyShock, skill.IsUnitTank, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or

    ForUniqueFilteredCombatCastUnits(holy.NoOverhealWordOfGlory, skill.IsUnitTank, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
    (tank and PlayerAuraDuration("Infusion of Light") > GlobalCD() + GetCastTime("Flash of Light") and skill.TryHealUnit("Flash of Light", tank, 0.5, 0.0) or nil) or
    holy.TryEfficientLightOfDawn() or
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealWordOfGlory, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or


    (tank and skill.TryHealUnit("Flash of Light", tank, 0.5, 0.0) or nil) or
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealHolyLight, skill.IsUnitTank, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealHolyLight, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or

    ForUniqueFilteredCombatCastUnits(holy.NoOverhealLightOfTheMartyr, skill.IsUnitTank, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
    ForUniqueFilteredCombatCastUnits(holy.NoOverhealLightOfTheMartyr, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or

    nil
  end
end
function holy.TryOffensiveHolyShock()
  return
  paladin.GetHolyPower() < paladin.GetHolyPowerMax() and
  TryCastSpellOnTarget("Holy Shock") or
  nil
end
function holy.TryCrusaderStrike()
  return
  paladin.GetHolyPower() < paladin.GetHolyPowerMax() and
  TryCastSpellOnTarget("Crusader Strike") or
  nil
end
function holy.TryShieldOfTheRighteous()
  return
    TryCastSpellOnTarget("Shield of the Righteous") or
    nil
end
function holy.TryOffensiveHolyPrism()
  return
    IsTalentSelected("Holy Prism") and
    TryCastSpellOnTarget("Holy Prism") or
    nil
end

function holy.range_heal_multiplier(unit)
  local range_max = skill.UnitRangeMax(unit)
  return 1 + (min(35, 40 - (range_max or 40))) / 35 * GetMasteryEffect() / 100
end
function holy.beacons_effect(targeted_unit, amount, after_seconds, result)
  local is_beacon_of_faith_selected = IsTalentSelected("Beacon of Faith")
  local beacon_multiplier = 0.5 * (is_beacon_of_faith_selected and 0.7 or 1.0)
  -- TODO: Optimizations maybe.
  --local is_beacon_of_virtue_selected = IsTalentSelected("Beacon of Virtue")
  --local beacon_of_virtue_cd = GetCD("Beacon of Virtue")
  --local beacon_of_virtue_cd_max = GetCDMax("Beacon of Virtue")
  --local max_beacon_of_virtues = beacon_of_faith_cd > 0 and beacon_of_faith_cd <
  local num_beacons = 0
  local num_beacons_max = max(is_beacon_of_faith_selected and 2 or 1, IsTalentSelected("Beacon of Virtue") and 4 or 1)
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if UnitGUID(targeted_unit) ~= UnitGUID(unit) then
      if holy.UnitBeaconDuration(unit) > after_seconds then
        result[unit] = result[unit] + amount * beacon_multiplier
        num_beacons = num_beacons + 1
        if num_beacons >= num_beacons_max then
          break
        end
      end
    end
  end
  return result
end
function holy.holy_shock_effect(targeted_unit, result)
  local amount = HealOfSpell("Holy Shock") * holy.range_heal_multiplier(targeted_unit)
  result[targeted_unit] = amount
  local holy_shock_cd = GetCD("Holy Shock")
  if IsTalentSelected("Glimmer of Light") then
    local glimmer_of_light_heal = HealOfSpell("Glimmer of Light")
    for _, unit in ipairs(GroupCombatCastUnits()) do
      local glimmer_of_light = UnitMyAuraDuration(unit, "Glimmer of Light") > holy_shock_cd and glimmer_of_light_heal or 0
      result[targeted_unit] = result[targeted_unit] + glimmer_of_light * holy.range_heal_multiplier(unit)
    end
  end
  result = holy.beacons_effect(targeted_unit, amount, holy_shock_cd, result)
  return result
end
function holy.holy_light_effect(targeted_unit, result)
  local heal = HealOfSpell("Holy Light") * holy.range_heal_multiplier(targeted_unit)
  result[targeted_unit] = heal
  result = holy.beacons_effect(targeted_unit, heal, GetCastTime("Holy Light"), result)
  return result
end
function holy.flash_of_light_effect(targeted_unit, result)
  local heal = HealOfSpell("Flash of Light") * holy.range_heal_multiplier(targeted_unit)
  result[targeted_unit] = heal
  result = holy.beacons_effect(targeted_unit, heal, GetCastTime("Flash of Light"), result)
  return result
end
function holy.word_of_glory_effect(targeted_unit, result)
  if IsUsableSpell("Word of Glory") then
    local heal = HealOfSpell("Word of Glory") * holy.range_heal_multiplier(targeted_unit)
    result[targeted_unit] = heal
    result = holy.beacons_effect(targeted_unit, heal, GetCastTime("Word of Glory"), result)
  end
  return result
end
function holy.light_of_dawn(targeted_unit, result)
  if IsUsableSpell("Light of Dawn") then
    local heal = HealOfSpell("Light of Dawn") * holy.range_heal_multiplier(targeted_unit)
    result[targeted_unit] = heal
    ALERT_ONCE("NYI light_of_dawn")
    result = holy.beacons_effect(targeted_unit, heal, GetCastTime("Light of Dawn"), result)
  end
  return result
end
function holy.light_of_the_marty_effect(targeted_unit, result)
  if UnitGUID(targeted_unit) == UnitGUID("player") then
    return result
  end
  local heal = HealOfSpell("Light of the Martyr") * holy.range_heal_multiplier(targeted_unit)
  result[targeted_unit] = heal
  -- TODO: Consider damage reduction on the player.
  result["player"] = -heal * 0.5
  return result
end
function holy.lay_on_hands_effect(targeted_unit, result)
  -- No range bonus nor beacon effect.
  result[targeted_unit] = HealOfSpell("Lay on Hands")
  return result
end
function holy.holy_prism_effect(targeted_unit, result)
  result[targeted_unit] = HealOfSpell("Holy Prism") * holy.range_heal_multiplier(targeted_unit)
  return result
end
function holy.holy_prism_damage_effect(targeted_unit, result)
  -- TODO: Figure out a way to use Holy Prism offensively at range.
  local _, range_max = range_check:GetRange(targeted_unit)
  if range_max and range_max <= 10 then
    local num = 0
    for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
      -- TODO: Double check healing fraction.
      local amount = DamageOfSpell("Holy Prism") * 0.93 * holy.range_heal_multiplier(unit)
      _, range_max = range_check:GetRange(unit)
      if range_max and range_max < 15 then
        result[unit] = amount
      end
      num = num + 1
      if num >= 5 then
        break
      end
    end
  end
  return result
end
function holy.UnitDamageTakenMultiplier(unit, spell)
  -- match damagereceived search
  local result = 1.0
  if UnitMyAuraDuration(unit, "Judgment") > GlobalCD() and (spell == "Crusader Strike" or spell == "Holy Shock") then
    result = result * 1.3
  end
  return result
end
function holy.crusader_strike_damage_effect(targeted_unit, result)
  if PlayerAuraDuration("Avenging Crusader") > GlobalCD() then
    -- TODO: Research real multiplier.
    local armor_multiplier = 0.66
    local heal = DamageOfSpell("Crusader Strike") * 2.5 * armor_multiplier * holy.UnitDamageTakenMultiplier(targeted_unit, "Crusader Strike")
    for unit, amount in pairs(holy.avenging_crusader_heal(heal)) do
      result[unit] = amount
    end
  end
  return result
end
function holy.judgment_damage_effect(targeted_unit, result)
  if PlayerAuraDuration("Avenging Crusader") > GlobalCD() then
    local heal = DamageOfSpell("Judgment") * 2.5 * holy.UnitDamageTakenMultiplier(targeted_unit, "Judgment")
    for unit, amount in pairs(holy.avenging_crusader_heal(heal)) do
      result[unit] = amount
    end
  end
  return result
end
local avenging_crusader_heal = {}
function holy.avenging_crusader_heal(amount)
  wipe(avenging_crusader_heal)
  local num = 0
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local range_max = skill.UnitRangeMax(unit)
    if range_max and range_max < 30 then
      avenging_crusader_heal[unit] = amount
    end
    num = num + 1
    if num >= 3 then
      break
    end
  end
  return avenging_crusader_heal
end

local heal_effects_table = {}
function holy.effect_func(func, unit)
  ALERT("unit: "..tostring(unit))
  wipe(heal_effects_table)
  heal_effects_table = func(unit, heal_effects_table)
  return heal_effects_table
end
holy.heal_effects = {
  ["Holy Shock"] = holy.holy_shock_effect,
  ["Holy Light"] = holy.holy_light_effect,
  ["Flash of Light"] = holy.flash_of_light_effect,
  ["Word of Glory"] = holy.word_of_glory_effect,
  ["Light of Dawn"] = holy.light_of_dawn,
  ["Light of the Martyr"] = holy.light_of_the_marty_effect,
  ["Lay on Hands"] = holy.lay_on_hands_effect,
  ["Holy Prism"] = holy.holy_prism_effect,
}
holy.damage_effects = {
  ["Holy Prism"] = holy.holy_prism_damage_effect,
  ["Crusader Strike"] = holy.crusader_strike_damage_effect,
  ["Judgment"] = holy.judgment_damage_effect,
}

holy.atonement_units = {}
holy.spell_heal_stats = {}
holy.atonement_durations = {}
holy.heal_stats = {}
holy.update_heal_stats_cache_time = 0
function holy.update_heal_stats(heal_effects, damage_effects)
  if holy.update_heal_stats_cache_time < GetTime() then
    if not type(heal_effects) == table then
      ALERT_ONCE("Passed non-table to update_heal_stats().")
      return
    end
    for spell, unit_to_effects in pairs(holy.heal_stats) do
      for targeted_unit, effects in pairs(unit_to_effects) do
        for affected_unit, amount in pairs(effects) do
          holy.heal_stats[spell][targeted_unit][affected_unit] = 0
        end
      end
    end
    for _, unit in ipairs(UniqueExistingCombatCastUnits()) do
      if skill.IsUnitPlayerFriend(unit) then
        for spell, spell_func in pairs(heal_effects) do
          -- TODO: Error handling.
          -- The negation needs to be kept.
          if not IsHarmfulSpell(spell) then
            local result = holy.effect_func(spell_func, unit)
            for affected_unit, amount in pairs(result) do
              holy.heal_stats[spell] = holy.heal_stats[spell] or {}
              holy.heal_stats[spell][unit] = holy.heal_stats[spell][unit] or {}
              holy.heal_stats[spell][unit][affected_unit] = amount
            end
          end
        end
      else
        for spell, spell_func in pairs(damage_effects) do
          if not IsHelpfulSpell(spell) then
            local result = holy.effect_func(spell_func, unit)
            for affected_unit, amount in pairs(result) do
              holy.heal_stats[spell] = holy.heal_stats[spell] or {}
              holy.heal_stats[spell][unit] = holy.heal_stats[spell][unit] or {}
              holy.heal_stats[spell][unit][affected_unit] = amount
            end
          end
        end
      end
    end
    holy.update_heal_stats_cache_time = GetTime()
  end
  return holy.heal_stats
end
function holy.get_heal_stats()
  holy.update_heal_stats(holy.heal_effects, holy.damage_effects)
  return holy.heal_effects
end

function test_update_heal_stats()
  local _, range_max = range_check:GetRange("target")
  ALERT("range: "..tostring(range_max))
  return holy.update_heal_stats(holy.heal_effects, holy.damage_effects)
end


holy.priorities = {  }
function holy.priorities.default()
  return
    --paladin.HandleAuras() or
    (not UnitAffectingCombat("player") and not paladin.IsMounted() and holy.HandleMinimalHealing() or nil) or
    (not UnitAffectingCombat("player") and not IsPlayerMoving() and skill.TryLoot() or nil) or
    TryCastQueuedSpell() or
    skill.DoNothingIfMounted() or
    holy.HandleBeacons() or
    --skill.TryPreventFallDamage() or
    skill.KeepRepurposedFelFocuserUp() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfDrinking() or
    skill.DoNothingIfTargetOutOfCombat() or
    skill.TryUseHealthstone(0.33) or
    holy.stay_alive() or
    holy.save_group() or
    skill.DoNothingIfCasting(0) or

    --TryDispelHighestStackedAuraFromGroup() or
    --TryDispelFromParty("Burst", 3) or
    --TryDispelFromParty("Lingering Doubt", 3) or

    --(not skill.IsUnitTank("party1") and TryDispelUnit("party1", "Phantasmal Parasite") or nil) or
    --(not skill.IsUnitTank("party2") and TryDispelUnit("party1", "Phantasmal Parasite") or nil) or
    --(not skill.IsUnitTank("party3") and TryDispelUnit("party1", "Phantasmal Parasite") or nil) or
    --(not skill.IsUnitTank("party4") and TryDispelUnit("party1", "Phantasmal Parasite") or nil) or
    (PlayerAuraStacks("Infectious Rain") >= 3 and TryCastSpellOnPlayer("Phial of Serenity") or nil) or
    TryDispelFromParty("Infectious Rain", 3) or
    FREEDOM() or
    holy.HandleMinimalHealing() or
    holy.KeepUpGlimmerOfLight() or

    skill.AcquireTarget() or
    skill.StartAttack() or
    --private.DumpHolyPower() or
    --KeepHolyShockOnCd() or
    --(paladin.GetHolyPower() >= 5 and private.TryShieldOfTheRighteous() or nil) or
    --(paladin.GetHolyPower() < paladin.GetHolyPowerMax() and KeepOnCd("Crusader Strike") or nil) or
    paladin.TryHammerOfWrath() or
    (skill.NumEnemiesWithin(8) >= 3 and not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
    (GetCharges("Crusader Strike") >= GetChargesMax("Crusader Strike") and holy.TryCrusaderStrike() or nil) or
    (skill.NumEnemiesWithin(8) >= 2 and not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() * 2 + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
    (PlayerManaPercent() < 0.1 and "nothing") or
    (IsTalentSelected("Judgment of Light") and TargetMyAuraDuration("Judgment of Light") <= 0 and TryCastSpellOnTarget("Judgment") or nil) or
    holy.TryCrusaderStrike() or
    (GetCharges("Crusader Strike") >= GetChargesMax("Crusader Strike") and holy.TryCrusaderStrike() or nil) or
    holy.TryOffensiveHolyPrism() or
    (UnitName("target") ~= "Executor Tarvold" and GetCharges("Crusader Strike") >= 1 and GetChargeCD("Crusader Strike") <= GlobalCDMax() and paladin.GetHolyPower() >= paladin.GetHolyPowerMax() and holy.TryShieldOfTheRighteous() or nil) or
    TryCastSpellOnTarget("Judgment") or
    holy.TryOffensiveHolyShock() or
    (not IsPlayerMoving() and TargetMyAuraDuration("Consecration") <= GlobalCDMax() * 2 + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
    "nothing"
end
function holy.priorities.mage_tower()
  return
    "nothing"
end
function holy.priorities.daily()
  return
  skill.dailies() or
  "nothing"
end
function holy.priorities.debug()
  --ALERT("skill.TryHealUnit(\"Lay on Hands\", \"player\", 0.45, 1.0): "..tostring(skill.TryHealUnit("Lay on Hands", "player", 0.75, 1.0)))
  --local yolo = UnitExists("target") and PlayerHealthPercent() < 0.75 and TryCastSpellOnUnit("Divine Protection", "player")
  --ALERT("yolo: "..tostring(yolo))
  local rofl = skill.TryHealUnit("Lay on Hands", "player", 0.5, 1.0)
  --ALERT("rofl: "..tostring(rofl))
  return
    rofl or
    TryCastSpellOnUnit("Light of the Martyr", "target") or
    "nothing"
end
holy.priorities.ascension = paladin.ascension

local module = {
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Cleanse"] = {
        "Magic",
        "Disease",
        "Poison",
      },
    },
  },
  ["interrupts"] = {
    "Hammer of Justice",
    "Blinding Light",
  },
  ["save_interrupt_for"]= {
    ["Dungeoneer's Training Dummy"] = {
      ["don't cast"] = "Hammer of Justice",
    },
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_diwsorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

--local frame_time_min = 1000 * 1000
--local frame_time_max = 0
--local frame_time_cur = 0
--function holy.frame_time_update(self)
--  self.string:SetText(string.format("[%d, %d, %d]", frame_time_min, frame_time_cur, frame_time_max))
--end
--holy.frame_time = { ["label"] = "frame_time:", ["text"] = "", ["update"] = holy.frame_time_update, ["click"] = nil, ["init"] = nil }


function holy.init_manual_beacons(gui)
  if skill_char_settings["manual_beacons"] == nil then
    skill_char_settings["manual_beacons"] = false
  end
  gui.string:SetText(tostring(skill_char_settings["manual_beacons"]))
end
function holy.toggle_manual_beacons(gui)
  skill_char_settings["manual_beacons"] = not skill_char_settings["manual_beacons"]
  gui.string:SetText(tostring(skill_char_settings["manual_beacons"]))
end
holy.toggle_manual_beacons_gui = { ["label"] = "Manual Beacons:", ["text"] = "", ["update"] = nil, ["click"] = holy.toggle_manual_beacons, ["init"] = holy.init_manual_beacons }

local frame_time_debug = false
function module.run()
  if frame_time_debug then
    debugprofilestart()
  end
  local result = holy.priority_selection.run()
  if frame_time_debug then
    local time_elapsed = debugprofilestop() * 1000
    frame_time_cur = time_elapsed
    frame_time_min = min(frame_time_min, time_elapsed)
    frame_time_max = max(frame_time_max, time_elapsed)
  end
  return result
end
function module.load()
  paladin.load()
  holy.priority_selection = PrioritySelectionFor(holy.priorities)
  AddGuiElement(holy.priority_selection.gui)
  AddGuiElement(holy.toggle_manual_beacons_gui)
  --AddGuiElement(holy.frame_time)
  --local multiplier = PlayerAuraDuration("Rule of Law") > GlobalCD() + GetCastTime(spell) and 1.5 or 1
end
function module.unload()
  paladin.unload()
end

skill.modules.paladin.holy = module

function holy.on_event(self, event, ...)
  if event == "PLAYER_ENTERING_WORLD" then
    local class = PlayerClass()
    local spec = PlayerSpec()
    if class == "paladin" and spec == "holy" then
      holy.frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
      holy.frame:RegisterEvent("PLAYER_ROLES_ASSIGNED")
      holy.calculate_beacon_targets()
    end
  elseif event == "PLAYER_ROLES_ASSIGNED" then
    holy.calculate_beacon_targets()
  elseif event == "COMBAT_LOG_EVENT_UNFILTERED" then
    if combat_log.unit_spell_cast_success("player", "Beacon of Light") then
      local info = combat_log.GetCurrentEventInfo()
      local is_manual_cast = UnitGUID(holy.beacon_targets[1]) ~= info.destGUID
      holy.manual_beacon["Light"] = is_manual_cast and GuidToUnit(info.destGUID) or nil
      --if is_manual_cast then
      --  holy.manual_beacon["Light"] = GuidToUnit(info.destGUID)
      --else
      --  holy.manual_beacon["Light"] = nil
      --end
    elseif combat_log.unit_spell_cast_success("player", "Beacon of Faith") then
      if holy.beacon_targets[2] then
        local info = combat_log.GetCurrentEventInfo()
        local is_manual_cast = UnitGUID(holy.beacon_targets[2]) ~= info.destGUID
        holy.manual_beacon["Faith"] = is_manual_cast and GuidToUnit(info.destGUID) or nil
      end
    end
  end
end
holy.frame = CreateFrame("Frame", nil, UIParent)
holy.frame:SetScript("OnEvent", holy.on_event)
holy.frame:RegisterEvent("PLAYER_ENTERING_WORLD")
