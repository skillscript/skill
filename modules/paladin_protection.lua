_, skill = ...
local protection = {}

local function IsOffGlobal(name)
  return
    name == "Shield of the Righteous" or
    name == "Lay on Hands" or
    name == "Ardent Defender" or
    name == "Guardian of Ancient Kings" or
    name == "Will to Survive" or
    name == "Healthstone" or
    name == "Blessing of Sacrifice" or
    name == "Hand of Reckoning" or
    name == "Bastion of Light" or
    name == "Rebuke"
end

local function IsStun(name)
  return name == "Hammer of Justice"
end

local function IsDisorient(name)
  return name == "Blinding Light"
end

local function IsIncapacitate(name)
  return name == "Repentance"
end

local function IsSilence(name)
  return name == "Avenger's Shield"
end

local function IsInterrupt(name)
  return name == "Rebuke" or name == "Avenger's Shield"
end

local function DoNothingIfTargetSoftCCd()
  local is_soft_ccd =
    TargetAuraDuration("Repentance") > 0 or
    TargetAuraDuration("Polymorph") > 0
  return
    is_soft_ccd and
    "nothing" or
    nil
end

function protection.HandleArdentDefender()
  local health_percent = PlayerHealthPercent()
  return
    (health_percent < 0.1 and TryCastSpellOnPlayer("Ardent Defender") or nil) or
    (health_percent < 0.4 and PlayerAuraDuration("Guardian of Ancient Kings") < 0.1 and TryCastSpellOnPlayer("Ardent Defender") or nil) or
    nil
end

function protection.HandleGuardianOfAncientKings()
  return
    nil
end

function MaxCastsAfter(spell, seconds)
--  seconds = seconds - GlobalCD(0)
--  local recharge = GetChargeCD(spell) - GlobalCD(0)
--  if recharge < 0 then
--    if GetCharges(spell) + 1 < GetChargesMax() then
--      recharge = recharge + GetChargeCDMax(spell)
--    end
--  end
--  seconds = seconds + GlobalCDMax() * GetCharges(spell)
--  while seconds > 0 do
--
--  end
end
function MaxCastsAfter(spell, seconds)
  local after_global = seconds - GlobalCD(0)
  local charges = GetCharges(spell)
  local after_global_cast = charges > 0 and 1 or 0
  local charge_cd_max = GetChargeCDMax(spell)
  local after_recharge = charges > 1 and max(0, after_global - GetChargeCD(spell)) or 0
  local after_recharge_cast = after_recharge > (after_global_cast * GlobalCDMax()) and 1 or 0
  local consecutive_casts = max(0, floor(after_recharge / charge_cd_max))
  ALERT("=========")
  ALERT("after_global: "..tostring(after_global))
  ALERT("after_recharge: "..tostring(after_recharge))
  ALERT("after_global_cast: "..tostring(after_global_cast))
  ALERT("after_recharge_cast: "..tostring(after_recharge_cast))
  ALERT("consecutive_casts: "..tostring(consecutive_casts))
  return after_global_cast + after_recharge_cast + consecutive_casts
end
--local function GetChargesAfter(spell, after)
--  local charge_cd = GetChargeCD(spell)
--  local recharging = GetChargesMax(spell) - GetCharges(spell)
--  local full_charges_remaining = GlobalCDMax() * max(0, recharging - 1)
--  local full_charges = floor(after / full_charges_remaining)
--  local times = charge_cd + full
--
--  if charge_cd - after <= 0 then
--
--  end
--  return GetCharges("Judgment") + full_charges
--end
--local hpga_QUEUE = {
--  [0] = "Judgment"
--}
function protection.HolyPowerGeneratedBySpellAfter(spell, seconds)
  local instant = GetCharges(spell)
  local cd = GetChargeCD(spell)
  local cd_max = GetChargeCDMax(spell)
  local fraction = cd > 0 and seconds - cd > 0 and 1 or 0
  local full_seconds = math.floor(math.max(0, seconds - cd) / cd_max)
  return instant + fraction + full_seconds
end
function protection.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent)
  if not spell or not unit then
    return nil
  end
  if spell == "Word of Glory" then
    local missing_health_multiplier = 1
    if UnitGUID(unit) == UnitGUID("player") or IsTalentSelected("Hand of the Protector") then
      missing_health_multiplier = (1 - UnitHealth(unit) / UnitHealthMax(unit)) * 2.5
    end
    if max_overheal_percent then
      local deficit = UnitHealthAndHealAbsorbDeficit(unit)
      local heal_received = HealOfSpell(spell) * UnitHealReceivedMultiplier(unit)
      local overheal = max(0, heal_received - deficit)
      local overhealPercent = overheal / heal_received
      if overhealPercent > maxOverhealPercent then
        return nil
      end
    end
  end

  return skill.TryHealUnit(spell, unit, if_below_health_percent, max_overheal_percent)
end

function protection.TimeUntilNextShieldOfTheRighteous()
  if PlayerAuraDuration("Divine Purpose") > GlobalCD() then
    return GlobalCD()
  end
  local generators = GetCharges("Hammer of the Righteous") + GetCharges("Blessed Hammer") + GetCharges("Judgment") + GetCharges("Avenger's Shield")
  local missing_holy_power = max(0, 3 - paladin.GetHolyPower())
  if generators >= missing_holy_power then
    return GlobalCD() + missing_holy_power * GlobalCDMax()
  end

  return
    (PlayerAuraDuration("Divine Purpose") > GlobalCD() and GlobalCD() or nil) or
    (generators >= missing_holy_power and missing_holy_power * GlobalCDMax() or nil) or
    1337
end
function protection.TryWordOfGloryOn(unit, ifShieldOfTheRighteousLongerThan, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraDuration("Shield of the Righteous") >= ifShieldOfTheRighteousLongerThan and
    skill.TryHealUnit("Word of Glory", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function protection.ShiningLightDuration()
  return PlayerAuraDuration(327510)
end
function protection.HandleWordOfGloryOnSelf()
  local deficit_percent = UnitHealthAndHealAbsorbDeficit("player") / PlayerHealthMax()
  local sotr_duration = PlayerAuraDuration("Shield of the Righteous")
  local holy_power = paladin.GetHolyPower()
  --local holy_power_percent = holy_power / paladin.GetHolyPowerMax()
  local cost = CostOfSpell("Word of Glory", "Holy Power")

  return
    (((sotr_duration <= GlobalCD() and deficit_percent > 0.7) or (sotr_duration > GlobalCD() + GlobalCDMax() * (cost - holy_power))) and protection.TryWordOfGloryOn("player", 0, 1.0, 0.0) or nil) or
    (protection.ShiningLightDuration() > GlobalCD() and protection.TryWordOfGloryOn("player", 0, 0.5, 0.1) or nil) or
    nil
end
function protection.HandleWordOfGloryOnOthers()
  for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      protection.TryWordOfGloryOn(unit, 4.5 + GlobalCD(), 0.6, 0.0) or
      (protection.ShiningLightDuration() > GlobalCD() and protection.TryWordOfGloryOn(unit, 0, 0.5, 0.1) or nil) or
      nil
    if result then
      return result
    end
  end
  return nil
end
function protection.HandleDivineToll()
  local ringing_clarity_active = soulbinds.is_active("Ringing Clarity")
  local hits = min(5, skill.NumEnemiesWithin(20) + (ringing_clarity_active and 3 or 0))
  return
    protection.should_cast_divine_toll and
    (paladin.GetHolyPowerMax() - paladin.GetHolyPower() < hits and GetCD("Divine Toll") <= GlobalCD() and paladin.GetHolyPower() >= 3 and TryCastSpellOnTarget("Shield of the Righteous") or nil) or
    (paladin.GetHolyPowerMax() - paladin.GetHolyPower() >= hits and TryCastSpellOnTarget("Divine Toll")) or
    nil
end
function protection.HandleSeraphim()
  if not IsTalentSelected("Seraphim") then
    return nil
  end
  local sotr_duration = PlayerAuraDuration("Shield of the Righteous")
  local hp =
    protection.HolyPowerGeneratedBySpellAfter("Judgment", sotr_duration) +
    protection.HolyPowerGeneratedBySpellAfter("Divine Toll", sotr_duration) +
    protection.HolyPowerGeneratedBySpellAfter("Avenger's Shield", sotr_duration) +
    protection.HolyPowerGeneratedBySpellAfter("Hammer of the Righteous", sotr_duration)
  return
    hp >= 3 and
    IsUnitInRange("Shield of the Righteous", "target") and
    TryCastSpellOnPlayer("Seraphim") or
    nil
end
function protection.AvengingWrath()
  return
    PlayerAuraDuration("Seraphim") > 10 and
    TryCastSpellOnPlayer("Avenging Wrath") or
    nil
end
protection.no_incoming_physical_damage = false
function protection.KeepUpShieldOfTheRighteous()
  --local cost = 3
  --local required_holy_power = cost - paladin.GetHolyPower()
  --local directly_available_holy_power_per_global =
  --  GetCharges("Judgment") * protection.HolyPowerGeneratedBy("Judgment") +
  --  GetCharges("Hammer of Righteousness") * protection.HolyPowerGeneratedBy("Hammer of Righteousness")
  --local directly_available_holy_power_in = min(required_holy_power, directly_available_holy_power_per_global) * GlobalCDMax()
  --if directly_available_holy_power_in
  --local three_holy_power_in = max(0, cost - paladin.GetHolyPower()) * GlobalCDMax()
  local sotr_duration = PlayerAuraDuration("Shield of the Righteous")
  local hpgencd = math.min(GetCD("Judgment"), math.min(GetCD("Avenger's Shield"), GetCD("Hammer of the Righteous")))
  local should_cast =
    (sotr_duration > 4.5 * 2 and paladin.GetHolyPower() == paladin.GetHolyPowerMax() and hpgencd <= 0) or
    (sotr_duration < 4.5 * 2 and paladin.GetHolyPower() == paladin.GetHolyPowerMax()) or
    (sotr_duration < 4.5 and paladin.GetHolyPower() <= paladin.GetHolyPowerMax()) or
    sotr_duration <= 0 or
    false
  return
    should_cast and
    TryCastSpellOnTarget("Shield of the Righteous") or
    nil
end


function protection.TryWordOfGloryOnHighestDeficitPercentUnit(ifShieldOfTheRighteousLongerThan, ifBelowHealthPercent, maxOverhealPercent)
  if PlayerAuraDuration("Shield of the Righteous") < ifShieldOfTheRighteousLongerThan then
    return nil
  end
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      protection.TryWordOfGloryOn(unit, ifBelowHealthPercent, maxOverhealPercent) or
      nil
    if result then
      return result
    end
  end
  return nil
end

local next_print = 0
function protection.TryAvengersShield()
  local threshold = 0.4
  local best_interrupt = BestInterrupt(threshold)
  if best_interrupt and best_interrupt == "Avenger's Shield" then
    local _, ttf = TargetCast()
    if ttf - threshold > GetCD("Avenger's Shield") + GlobalCDMax() then
      if next_print < GetTime() then
        next_print = GetTime() + 2
        ALERT("Saving AS for interrupt!")
      end
      return nil
    end
  end
  return
    (paladin.GetHolyPower() + protection.HolyPowerGeneratedBy("Avenger's Shield") <= paladin.GetHolyPowerMax() or not protection.can_spend_holy_power()) and
    TryCastSpellOnTarget("Avenger's Shield") or
    nil
end

function protection.TryHammerOfTheRighteous()
  return
    (paladin.GetHolyPower() + protection.HolyPowerGeneratedBy("Hammer of the Righteous") <= paladin.GetHolyPowerMax() or not protection.can_spend_holy_power()) and
    TryCastSpellOnTarget("Hammer of the Righteous") or
    nil
end

function protection.TryJudgment()
  return
    --TargetMyAuraDuration("Judgment") <= GlobalCD() and
    (paladin.GetHolyPower() + protection.HolyPowerGeneratedBy("Judgment") <= paladin.GetHolyPowerMax() or not protection.can_spend_holy_power()) and
    TryCastSpellOnTarget("Judgment") or
    nil
end

function protection.TryHammerOfWrath()
  return
    (paladin.GetHolyPower() + protection.HolyPowerGeneratedBy("Hammer of Wrath") <= paladin.GetHolyPowerMax() or not protection.can_spend_holy_power()) and
    TryCastSpellOnTarget("Hammer of Wrath") or
    nil
end

function protection.HolyPowerGeneratedBy(spell)
  if spell == "Avenger's Shield" then
    return 1
  elseif spell == "Hammer of the Righteous" then
    return 1
  elseif spell == "Judgment" then
    local sanctified_wrath = IsTalentSelected("Sanctified Wrath") and 1 or 0
    local sanctified_wrath_duration = sanctified_wrath * PlayerAuraDuration("Avenging Wrath")
    return sanctified_wrath_duration > GlobalCD() and 2 or 1
  elseif spell == "Hammer of Wrath" then
    return 1
  end
  ALERT_ONCE(spell.." doesn't generate Holy Power.")
end

function protection.can_spend_holy_power()
  if PlayerAuraDuration("Divine Purpose") > GlobalCD() then
    return false
  end
  if IsUnitInRange("Shield of the Righteous", "target") then
    return true
  end
  local sum_deficit = 0
  for _, unit in ipairs(GroupCombatCastUnits()) do
    sum_deficit = sum_deficit + UnitHealthDeficit(unit)
  end
  return
    (PlayerAuraDuration("Shield of the Righteous") < 4.5 * 0.3) or
    (PlayerAuraDuration("Shining Light") <= GlobalCD() and sum_deficit > 0)
end

function protection.stay_alive()
  return
    protection.HandleArdentDefender() or
    protection.HandleGuardianOfAncientKings() or
    skill.TryHealUnit("Lay on Hands", "player", 0.1, 0.8) or
    nil
end
function protection.save_group()
  for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
    protection.TryWordOfGloryOn(unit, 0, 0.2, 0.5) or
    skill.TryHealUnit("Lay on Hands", unit, 0.1, 0.8) or
    nil
    if result then
      return result
    end
  end
  return nil
end
protection.priorities = {}

function protection.priorities.default()
  return
    paladin.HandleAuras() or
    protection.stay_alive() or
    protection.save_group() or
    skill.common_priority_start() or
    skill.AcquireTarget() or
    skill.StartAttack() or

    DoNothingIfTargetSoftCCd() or

    protection.HandleWordOfGloryOnSelf() or
    protection.KeepUpShieldOfTheRighteous() or
    protection.HandleWordOfGloryOnOthers() or
    protection.HandleSeraphim() or
    protection.AvengingWrath() or

    --MaybeTryGotAK() or
    --MaybeTryArdentDefender() or

    --protection.DumpHolyPower() or

    (PlayerMyAuraDuration("Consecration") <= 0 and TryCastSpellOnTarget("Consecration") or nil) or
    (PlayerAuraDuration("Consecration") < GlobalCDMax() + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or
    (PlayerAuraDuration("Consecration") < GlobalCDMax() + GlobalCD() and not IsPlayerMoving() and TryCastSpellOnPlayer("Consecration") or nil) or
    protection.HandleDivineToll() or
    (skill.NumEnemiesWithin(8) > 2 and protection.TryAvengersShield() or nil) or
    (TargetMyAuraDuration("Judgment") <= GlobalCD() and protection.TryJudgment()) or
    protection.TryHammerOfWrath() or
    protection.TryAvengersShield() or
    protection.TryHammerOfTheRighteous() or
    protection.TryJudgment() or
    (GetCD("Judgment") > GlobalCDMax() + GlobalCD() and GetCD("Avenger's Shield") > GlobalCDMax() + GlobalCD() and GetCD("Hammer of the Righteous") > GlobalCDMax() + GlobalCD() and TryCastSpellOnTarget("Consecration") or nil) or

    "nothing"
end

function protection.priorities.new()

end
local function range(spell)
  if spell == "Blessed Hammer" then
    return 8
  end
  return skill.modules.paladin.spell.range(spell)
end
local module = {
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Cleanse Toxins"] = {
        "Disease",
        "Poison",
      },
    },
  },
  ["interrupts"] = {
    "Avenger's Shield",
    "Rebuke",
    "Hammer of Justice",
    "Blinding Light",
  },
  ["save_interrupt_for"]= {
    ["Target Name"] = {
      ["Enemy Spell"] = "Interrupt Spell",
    },
    ["Dungeoneer's Training Dummy"] = {
      ["don't cast"] = "Hammer of Justice",
    },
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

local range_check = LibStub("LibRangeCheck-2.0")
function protection.range_update(self)
  self.string:SetText(string.format("[%d, %d]", range_check:GetRange("target")))
end
--protection.range = { ["label"] = "range:", ["text"] = "", ["update"] = protection.range_update, ["click"] = nil, ["init"] = nil }

local function wog()
  return HealOfSpell("Word of Glory") * (1 + (1 - PlayerHealthPercent()) * 2.5)
end
function protection.wog_update(self)
  local missing = UnitHealthAndHealAbsorbDeficit("player")
  local healed = wog()
  local overheal = max(0, healed - missing)
  self.string:SetText(string.format("%d, %d", healed, overheal))
end
--protection.wog = { ["label"] = "wog:", ["text"] = "", ["update"] = protection.wog_update, ["click"] = nil, ["init"] = nil }

function protection.toggle_divine_toll(self)
  protection.should_cast_divine_toll = not (protection.should_cast_divine_toll or false)
  self.string:SetText(tostring(protection.should_cast_divine_toll))
end
function protection.init_divine_toll(self)
  protection.should_cast_divine_toll = true
  self.string:SetText(tostring(protection.should_cast_divine_toll))
end
protection.range = { ["label"] = "divine toll:", ["text"] = "", ["update"] = nil, ["click"] = protection.toggle_divine_toll, ["init"] = protection.init_divine_toll }

function module.run()
  return protection.priority_selection.run()
end
function module.load()
  paladin.load()
  protection.priority_selection = PrioritySelectionFor(protection.priorities)
  AddGuiElement(protection.priority_selection.gui)
  AddGuiElement(protection.range)
  AddGuiElement(protection.wog)
end
function module.unload()
  paladin.unload()
end

skill.modules.paladin.protection = module
