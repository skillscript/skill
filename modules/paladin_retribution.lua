_, skill = ...

local retribution = {}
paladin = skill.modules.paladin or {}

local function range(spell)
  if spell == "Divine Storm" then
    return 5
  elseif spell == "Wake of Ashes" then
    return 5
  end
  return skill.modules.paladin.spell.range(spell)
end

local function IsOffGlobal(name)
  return
    name == "Lay on Hands" or
    name == "Will to Survive" or
    name == "Avenging Wrath" or
    name == "Healthstone" or
    name == "Rebuke" or
    name == "Hand of Reckoning" or
    name == "Shield of the Righteous" or
    name == "Holy Avenger" or
    name == "Blessing of Sacrifice"
end

local function IsStun(name)
  return name == "Hammer of Justice"
end

local function IsDisorient(name)
  return name == "Blinding Light"
end

local function IsIncapacitate(name)
  return name == "Repentance"
end

local function IsInterrupt(name)
  return name == "Rebuke"
end

function retribution.TargetTimeToLive(window)
  --local target_name = UnitName("target")
  --if target_name == "Burden of Destiny" or target_name == "Frostbound Devoted" or target_name == "Glacial Spike" then
  --  return 1
  --end
  local dps = skill.GetNumAliveGroupDamageDealers() * 6000
  local ttl = UnitHealth("target") / dps
  return ttl
end
function retribution.TryBladeOfJustice()
  return
    (paladin.GetHolyPower() <= 3 or not IsUnitInRange("Templar's Verdict", "target")) and
    IsUsableSpell("Blade of Justice") and
    TryCastSpellOnTarget("Blade of Justice") or
    nil
end

function retribution.HandleDivineToll()
  local ringing_clarity_active = soulbinds.is_active("Ringing Clarity")
  local hits = min(5, skill.NumEnemiesWithin(20) + (ringing_clarity_active and 3 or 0))
  --ALERT("paladin.GetHolyPower(): "..tostring(paladin.GetHolyPower()))
  --ALERT("hits: "..tostring(hits))
  --ALERT("paladin.GetHolyPowerMax() - paladin.GetHolyPower() >= hits: "..tostring(paladin.GetHolyPowerMax() - paladin.GetHolyPower() >= hits))
  return
    (ringing_clarity_active and GetCD("Divine Toll") <= GlobalCD() + GlobalCDMax() and retribution.TryFinish() or nil) or
    paladin.GetHolyPowerMax() - paladin.GetHolyPower() >= hits and
    TargetMyAuraDuration("Judgment") <= GlobalCD() and
    TryCastSpellOnTarget("Divine Toll") or
    nil
end

function retribution.TryHammerOfWrath()
  return
    (paladin.GetHolyPower() < paladin.GetHolyPowerMax() - 1 or not IsUnitInRange("Auto Attack", "target")) and
    TryCastSpellOnTarget("Hammer of Wrath") or
    nil
end

function retribution.TryJudgment()
  return
    TargetMyAuraDuration("Judgment") <= GlobalCD() and
    (paladin.GetHolyPower() < paladin.GetHolyPowerMax() - 1 or not IsUnitInRange("Auto Attack", "target")) and
    TryCastSpellOnTarget("Judgment") or
    nil
end

local function TryConsecration()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Consecration") or
    nil
end

local function KeepCrusaderStrikeOnCd()
  return
    GetCharges("Crusader Strike") >= 1 and
    GetChargeCD("Crusader Strike") <= GlobalCD() + GlobalCDMax() and
    TryCastSpellOnTarget("Crusader Strike") or
    nil
end

local function ChooseFinisher()
  if not skill.IsAoeDamageAllowed() or skill.ShouldFocusSingleTarget() then
    if IsUsableSpell("Templar's Verdict") then
      return "Templar's Verdict"
    elseif IsUsableSpell("Shield of the Righteous") then
      return "Shield of the Righteous"
    end
  elseif skill.ShouldDoAoeDamage() then
    if IsUsableSpell("Divine Storm") then
      return "Divine Storm"
    elseif IsUsableSpell("Shield of the Righteous") then
      return "Shield of the Righteous"
    end
    return "Divine Storm"
  else
    local result = skill.NumEnemiesWithin(6) > 2 and "Divine Storm" or "Templar's Verdict"
    if IsUsableSpell(result) then
      return result
    elseif IsUsableSpell("Shield of the Righteous") then
      return "Shield of the Righteous"
    end
  end
end

local function GetFriendlyDps()
  return 0
--  end
--  local friendlyDps = 0
--  local combat = Details:GetCurrentCombat()
--  if combat then
--    if combat:GetCombatTime() < 20 then
--      combat = Details:GetCombat(DETAILS_SEGMENTID_OVERALL)
--    end
--    local actorContainer = combat:GetContainer (DETAILS_ATTRIBUTE_DAMAGE)
--    local numActors = 0
--    for _, actor in actorContainer:ListActors() do
--      if actor:IsGroupPlayer() then
--        friendlyDps = friendlyDps + actor.total / actor:Tempo()
--        numActors = numActors + 1
--      end
--    end
--    if numActors < GetNumGroupMembers() then
--      friendlyDps = friendlyDps / numActors * max(1, GetNumGroupMembers() - 1) -- Subtract 1 for lower healer+Tank dmg.
--    end
--  end
--  return friendlyDps
end

function retribution.TryEmpyreanPower()
  return
    -- TODO: Proper aoe switches.
    cleave_mode ~= "no_aoe" and
    PlayerAuraDuration("Empyrean Power") > GlobalCD() and
    TryCastSpellOnTarget("Divine Storm") or
    nil
end
function retribution.can_finish()
  return skill.IsUsable("Templar's Verdict") or skill.IsUsable("Divine Storm") or skill.IsUsable("Word of Glory")
end
function retribution.TryFinish()
  -- TODO: Maybe implement Justicar's Vengeance.
  return
    retribution.can_finish() and
    TryCastSpellOnTarget(ChooseFinisher()) or
    nil
end


local function KeepWoAOnCd()
  return
    paladin.GetHolyPower() <= 2 and
    UnitHealth("target") > DamageOfSpell("Wake of Ashes") + DamageOfSpell("Templar's Verdict") and
    retribution.TryWakeOfAshes() or
    nil
end


local function SelfBuff(buff)
  local name, _, _, _, _, duration, expirationTime = UnitBuff("player", buff)
  if duration then
    local remaining = expirationTime - GetTime()
    return max(0, remaining)
  end
  return 0
end

local function DontWasteHolyPower()
  return
    paladin.GetHolyPower() >= 5 and
    retribution.TryFinish() or
    nil
end

local function UnitHealReceivedMultiplier(unit)
  return 1
end

function retribution.TryWakeOfAshes()
  return
    IsUnitInRange("Blade of Justice", "target") and
    TryCastSpellOnTarget("Wake of Ashes") or
    nil
end
function retribution.TryHealRole(spell, role, ifBelowHealthPercent, maxOverhealPercent)
  -- TODO: Using TryGetFirstRoleUnit() is wrong.
  local unit = skill.TryGetFirstRoleUnit(role)
  return
    unit and
    skill.IsUnitAlive(unit) and
    skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end

local function isOverflowing()
  return false
end

function retribution.TryHealUnitWithWordOfGlory(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    skill.TryHealUnit("Word of Glory", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end

local function TrySelflesslyHealUnit(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraDuration("Selfless Healer") > GlobalCD() and
    PlayerAuraStacks("Selfless Healer") >= 4 and
    skill.TryHealUnit("Flash of Light", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end

local function TrySelflesslyHealRole(role, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraStacks("Selfless Healer") >= 4 and
    PlayerAuraDuration("Selfless Healer") > GlobalCD() and
    retribution.TryHealRole("Flash of Light", role, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end

function retribution.ShouldReceiveBlessingOfProtection(unit)
  return false
  -- TODO: Fix health history.
  --local interval = 1.5
  --local physicalDamageInInterval = UnitPhysicalDamageReceived(unit, interval)
  --local historySize = #healthHistoryTimestamp
  --local secondHitTime = historySize > 2 and healthHistoryTimestamp[guid][historySize - 1] or 0
  ---- BoP if received > 50% dmg in at least two physical hits.
  --return physicalDamageInInterval / UnitHealthMax(unit) > 0.5 and secondHitTime > GetTime() - interval
end


local function IsEmptyGlobal()
  local global_max = GlobalCDMax() + GlobalCD()
  return
    (not IsUnitInRange("Judgment", "target") or GetCD("Judgment") > global_max) and
    (not IsUnitInRange("Blade of Justice", "target") or GetCD("Blade of Justice") > global_max) and
    (not IsUnitInRange("Crusader Strike", "target") or GetCD("Crusader Strike") > global_max) and
    (not IsUnitInRange("Wake of Ashes", "target") or GetCD("Wake of Ashes") > global_max) and
    (not IsUnitInRange("Hammer of Wrath", "target") or not IsUsableSpell("Hammer of Wrath") or GetCD("Hammer of Wrath") > global_max)
end

function retribution.TryLayOnHandsOnUnitBelowFivePercent(unit)
  return skill.TryHealUnit("Lay on Hands", unit, 0.05, 1.0)
end
function retribution.TrySelflesslyHealUnitWithoutOverheal(unit)
  return TrySelflesslyHealUnit(unit, 1.0, 0.0)
end
function retribution.UseFreeGlobal()
  if not IsEmptyGlobal() then
    return nil
  end
  return
    paladin.TryActivateChosenAura() or
    TrySelflesslyHealUnit("player", 1.0, 0.3) or
    TrySelflesslyHealRole("HEALER", 1.0, 0.1) or
    TrySelflesslyHealRole("TANK", 0.5, 0.1) or
    TrySelflesslyHealRole("DAMAGER", 0.7, 0.1) or
    ForUniqueFilteredCombatCastUnits(retribution.TrySelflesslyHealUnitWithoutOverheal, UnitExists, skill.IsUnitPlayerFriend, skill.IsUnitAlive) or
    nil
end

--function private.TryDumpWordOfGloryOnUnit(unit)
--  return
--    private.TryHealUnitWithWordOfGlory(unit, 0.5, 0.1) or
--    nil
--end
--
--function private.HandleFinisherOutOfRange()
--  if IsUnitInRange("Templar's Verdict", "target") then
--    return nil
--  end
--  return
--    ForUniqueFilteredCombatCastUnits(private.TryDumpWordOfGloryOnUnit, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend, skill.IsUnitTank) or
--    ForUniqueFilteredCombatCastUnits(private.TryDumpWordOfGloryOnUnit, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
--    nil
--end
local function TryAvengingWrathIfWorth()
  if not UnitExists("target") or GetCD("Avenging Wrath") then
    return
  end
  local avengingWrathDuration = 20
  if IsTalentSelected("Inquisition") and PlayerAuraDuration("Inquisition") < avengingWrathDuration + GlobalCD() + GlobalCDMax() then
    return
  end
  return
    skill.GetEnemiesHealth(8) > GetFriendlyDps() * avengingWrathDuration and
    TryCastSpellOnPlayer("Avenging Wrath") or
    nil
end

function retribution.stay_alive()
  if not skill.IsUnitAlive("player") then
    return nil
  end
  return
    (GetJailersTowerLevel() > 40 and TrySelflesslyHealUnit("player", 0.8, 1.0) or nil) or
    (GetJailersTowerLevel() > 40 and retribution.TryHealUnitWithWordOfGlory("player", 0.8, 1.0) or nil) or
    (GetJailersTowerLevel() > 40 and PlayerHealthPercent() < 0.5 and kyrian.TryRefreshPhials() or nil) or
    (GetJailersTowerLevel() > 40 and PlayerHealthPercent() < 0.4 and TryCastSpellOnPlayer("Lay on Hands") or nil) or
    (PlayerHealthPercent() < 0.3 and TryCastSpellOnPlayer("Shield of Vengeance") or nil) or
    TrySelflesslyHealUnit("player", 0.2, 0.9) or
    (PlayerHealthPercent() < 0.2 and paladin.TryDivineShield()) or
    (PlayerAuraDuration("Divine Shield") <= 0 and (
      (PlayerHealthPercent() < 0.1 and TryCastSpellOnPlayer("Lay on Hands")) or
      (PlayerHealthPercent() < 0.2 and GetCD("Lay on Hands") > GlobalCD() and skill.TrySelfHealWithConcentratedFlame()) or
      nil
    )) or
    (not IsPlayerMoving() and PlayerHealthPercent() < 0.1 and TryCastSpellOnUnit("Flash of Light", "player") or nil) or
    nil
end
function retribution.stay_alive_raid()
  if not skill.IsUnitAlive("player") then
    return nil
  end
  return
    (PlayerHealthPercent() < 0.3 and TryCastSpellOnPlayer("Shield of Vengeance") or nil) or
    TrySelflesslyHealUnit("player", 0.6, 0.1) or
    TrySelflesslyHealUnit("player", 0.2, 0.9) or
    skill.TryUseHealthstone(0.33) or
    kyrian.TryUsePhialOfSerenity(0.3) or
    (PlayerHealthPercent() < 0.2 and TryCastSpellOnPlayer("Divine Shield") or nil) or
    (PlayerAuraDuration("Divine Shield") <= 0 and PlayerHealthPercent() < 0.1 and TryCastSpellOnPlayer("Lay on Hands") or nil) or
    retribution.TryHealUnitWithWordOfGlory("player", 0.5, 0.0) or
    retribution.TryHealUnitWithWordOfGlory("player", 0.3, 0.5) or
    (not IsPlayerMoving() and PlayerHealthPercent() < 0.1 and TryCastSpellOnUnit("Flash of Light", "player") or nil) or
    nil
end
function retribution.save_raid()
  local max_cd = GlobalCDMax() + GlobalCD()
  local bla =
    PlayerAuraDuration("Selfless Healer") > GlobalCD() and
    PlayerAuraStacks("Selfless Healer") >= 4 and
    paladin.GetHolyPower() < 3 and
    GetCD("Crusader Strike") > max_cd and
    GetCD("Blade of Justice") > max_cd and
    GetCD("Judgment") > max_cd
  if bla then
    for i, unit in ipairs(HighestDeficitPercentGroupUnits()) do
      if UnitHealthPercent(unit) > 0.4 then
        return nil
      end
      local result = skill.TryHealUnit("Flash of Light", unit, 0.4, 0.1)
      if result then
        return result
      end
    end
  end
  return nil
end
function retribution.save_group()
  if GetNumGroupMembers() <= 1 then
    return nil
  end
  return
    TrySelflesslyHealRole("TANK", 0.3, 0.9) or
    TrySelflesslyHealRole("HEALER", 0.1, 0.9) or
    TrySelflesslyHealRole("DAMAGER", 0.1, 0.9) or
    paladin.save_group() or
    nil
end

retribution.priorities = {}
function retribution.priorities.default()
  return
    torghast.torghast_stuff() or
    retribution.stay_alive() or
    retribution.save_group() or
    paladin.HandleAuras() or
    (not UnitAffectingCombat("player") and TrySelflesslyHealUnit("player", 0.9, 0.1) or nil) or
    (not UnitAffectingCombat("player") and retribution.TryHealUnitWithWordOfGlory("player", 0.5, 0.0) or nil) or
    (not UnitAffectingCombat("player") and not paladin.IsMounted() and kyrian.TryRefreshPhials() or nil) or
    skill.common_priority_start() or
    skill.StartAttack() or
    skill.AcquireTarget() or

    TrySelflesslyHealUnit("player", 0.5, 0.0) or
    retribution.TryHealUnitWithWordOfGlory("player", 0.3, 0.0) or
    (TargetMyAuraDuration("Final Reckoning") <= 0 and TryDispelAnythingFromUnit("player") or nil) or
    (TargetMyAuraDuration("Final Reckoning") > GlobalCD() and retribution.TryEmpyreanPower() or nil) or
    (UnitAuraDuration("player", "Mark of Conk-quest") > GlobalCD() and retribution.TryFinish() or nil) or
    (PlayerAuraDuration("Avenging Wrath") > GlobalCD() and UnitAuraDuration("player", "Badge of the Mad Paragon", "MAW") > GlobalCD() and retribution.TryHammerOfWrath() or nil) or
    (UnitAuraDuration("player", "Soulforged Censer", "MAW") > 0 and TargetMyAuraDuration("Consecration") <= 0 and skill.NumEnemiesWithin(8) > 1 and TryConsecration() or nil) or
    (UnitAuraDuration("player", "Soulforged Censer", "MAW") > 0 and TargetMyAuraDuration("Consecration") <= 0 and TargetHealthPercent() < 0.2 and TryConsecration() or nil) or
    (TargetMyAuraDuration("Judgment") > 0 and TargetMyAuraDuration("Judgment") < GlobalCD() + GlobalCDMax() and retribution.TryFinish() or nil) or
    DontWasteHolyPower() or
    (paladin.GetHolyPower() <= 2 and GetCD("Blade of Justice") > GlobalCDMax() + GlobalCD() and retribution.TryWakeOfAshes()) or
    KeepWoAOnCd() or
    retribution.TryHammerOfWrath() or
    retribution.TryJudgment() or
    retribution.HandleDivineToll() or
    retribution.TryBladeOfJustice() or
    retribution.TryEmpyreanPower() or
    retribution.TryHealUnitWithWordOfGlory("player", 0.5, 0.0) or
    (paladin.TryActivateChosenAura() or nil) or
    KeepCrusaderStrikeOnCd() or
    TrySelflesslyHealUnit("player", 0.7, 0.2) or
    retribution.TryHealUnitWithWordOfGlory("player", 0.7, 0.2) or
    kyrian.TryUsePhialOfSerenity(0.3) or
    TryCastSpellOnTarget("Crusader Strike") or
    retribution.TryHealUnitWithWordOfGlory("player", 0.1, 1.0) or
    retribution.TryFinish() or
    TryConsecration() or
    TrySelflesslyHealUnit("player", 0.5, 1.0) or
    kyrian.TryRefreshPhials() or
    kyrian.TryDismissSteward() or
    retribution.UseFreeGlobal() or
    TrySelflesslyHealUnit("player", 0.1, 0.5) or
    --private.HandleFinisherOutOfRange() or
    "nothing"
end
local conduits = {}
function conduits.is_active(name)
  if name == "Expurgation" then
    return false
  elseif name == "Templar's Vindication" then
    return true
  end
  return false
end
local runeforge = {}
function runeforge.is_active(name)
  if name == "Final Verdict" then
    return false
  end
  return false
end
simcraft = {}
function simcraft.talent_finisher_conditions()
  return
    (not IsTalentSelected("Execution Sentence") or GetCD("Execution Sentence") > GlobalCDMax() * 6 + GlobalCD() or GetCD("Execution Sentence") > GlobalCDMax() * 5 + GlobalCD() and paladin.GetHolyPower() >= 4 or retribution.TargetTimeToLive(16) < 8 or not IsTalentSelected("Seraphim") and GetCD("Execution Sentence") > GlobalCDMax() * 2 + GlobalCD()) and
    (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") > GlobalCDMax() * 6 + GlobalCD() or GetCD("Final Reckoning") > GlobalCDMax() * 5 + GlobalCD() and paladin.GetHolyPower() >= 4 or not IsTalentSelected("Seraphim") and GetCD("Execution Sentence") > GlobalCDMax() * 2 + GlobalCD()) and
    --(not IsTalentSelected("Seraphim") or GetCD("Seraphim") % GlobalCDMax() + paladin.GetHolyPower() > 3 or IsTalentSelected("Final Reckoning") or IsTalentSelected("Execution Sentence") or skill.IsKyrian()) and
    true
end
function simcraft.should_cast_divine_toll()
  -- !debuff.judgment.up&
  -- (!talent.seraphim.enabled|buff.seraphim.up)&
  -- (!raid_event.adds.exists|raid_event.adds.in>30|raid_event.adds.up)&
  -- (holy_power<=2|holy_power<=4&(cooldown.blade_of_justice.remains>gcd*2|debuff.execution_sentence.up|debuff.final_reckoning.up))&
  -- (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains>gcd*10)&
  -- (!talent.execution_sentence.enabled|cooldown.execution_sentence.remains>gcd*10|target.time_to_die<8)&
  -- (cooldown.avenging_wrath.remains|cooldown.crusade.remains)
  return
    TargetMyAuraDuration("Judgment") <= 0 and
    (not IsTalentSelected("Seraphim") or PlayerAuraDuration("Seraphim") > GlobalCD()) and
    (paladin.GetHolyPower() <= 2 or (paladin.GetHolyPower() <= 4 and (GetCD("Blade of Justice") > GlobalCDMax() * 2 + GlobalCD() or TargetMyAuraDuration("Execution Sentence") > GlobalCD() or TargetMyAuraDuration("Final Reckoning")))) and
    (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") > GlobalCDMax() * 10) and
    (not IsTalentSelected("Execution Sentence") or GetCD("Final Reckoning") > GlobalCDMax() * 10 or retribution.TargetTimeToLive(16) < 8) and
    (GetCD("Avenging Wrath") > GlobalCD() or GetCD("Avenging Wrath") > GlobalCD())
end
function simcraft.should_cast_hammer_of_wrath()
  return
    --runeforge.is_active("The Mad Paragon") or
    --(runeforge.is_active("Vanguard's Momentum") and IsTalentSelected("Execution Sentence")) or
    --(skill.IsVenthyr() and GetCD("Ashen Hallow") > 120) or
    false
end
function simcraft.should_cast_blade_of_justice()
  -- holy_power<=3&
  -- talent.blade_of_wrath.enabled&
  -- ((talent.final_reckoning.enabled&debuff.final_reckoning.remains>gcd*2)|
  --  (talent.execution_sentence.enabled&!talent.final_reckoning.enabled&(debuff.execution_sentence.up|cooldown.execution_sentence.remains=0)))
  return
    paladin.GetHolyPower() <= 3 and
    IsTalentSelected("Blade of Wrath") and
    ((IsTalentSelected("Final Reckoning") and GetCD("Final Reckoning") > GlobalCDMax() * 2 + GlobalCD()) or
     (IsTalentSelected("Execution Sentence") and not IsTalentSelected("Final Reckoning") and (TargetMyAuraDuration("Execution Sentence") > GlobalCD() or GetCD("Execution Sentence") <= 0)))
end
function simcraft.should_cast_wake_of_ashes()
  -- (holy_power=0|holy_power<=2&(cooldown.blade_of_justice.remains>gcd*2|debuff.execution_sentence.up|target.time_to_die<8|debuff.final_reckoning.up))&
  -- (!raid_event.adds.exists|raid_event.adds.in>20|raid_event.adds.up)&
  -- (!talent.execution_sentence.enabled|cooldown.execution_sentence.remains>15|target.time_to_die<8)&
  -- (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains>15|target.time_to_die<8)&
  -- (cooldown.avenging_wrath.remains|cooldown.crusade.remains)
  local ttl = retribution.TargetTimeToLive(16)
  return
  (paladin.GetHolyPower() == 0 or (paladin.GetHolyPower() <= 2 and (GetCD("Blade of Justice") > GlobalCDMax() * 2 + GlobalCD() or TargetMyAuraDuration("Execution Sentence") > GlobalCD() or ttl < 8 or TargetMyAuraDuration("Final Reckoning") > GlobalCD()))) and
  (not IsTalentSelected("Execution Sentence") or GetCD("Execution Sentence") > 15 or ttl < 8) and
  (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") > 15 or ttl < 8) and
  (GetCD("Avenging Wrath") > GlobalCD() or GetCD("Crusade") > GlobalCD()) and
  true
end
function simcraft.should_cast_crusader_strike()
  -- cooldown.crusader_strike.charges_fractional>=1.75&
  -- (holy_power<=2|
  --  holy_power<=3&cooldown.blade_of_justice.remains>gcd*2|
  --  holy_power=4&cooldown.blade_of_justice.remains>gcd*2&cooldown.judgment.remains>gcd*2)
  local hp = paladin.GetHolyPower()
  return
    (GetChargesMax("Crusader Strike") - GetCharges("Crusader Strike") == 1 and GetChargeCD("Crusader Strike") / GetChargeCDMax("Crusader Strike") >= 0.25) and
    ((hp <= 2) or
     (hp <= 3 and GetCD("Blade of Justice") > GlobalCDMax() * 2 + GlobalCD()) or
     (hp == 4 and GetCD("Blade of Justice") > GlobalCDMax() * 2 + GlobalCD() and GetCD("Judgment") > GlobalCDMax() * 2 + GlobalCD()))
end
function simcraft.should_cast_avenging_wrath()
  --(
  --  holy_power>=4&time<5|
  --  holy_power>=3&(time>5|runeforge.the_magistrates_judgment)|
  --  talent.holy_avenger.enabled&cooldown.holy_avenger.remains=0
  --)&(
  --  !talent.seraphim.enabled|
  --  cooldown.seraphim.remains>0|
  --  talent.sanctified_wrath.enabled
  --)
  return
    (
      (paladin.GetHolyPower() >= 3) or
      IsTalentSelected("Holy Avenger") and GetCD("Holy Avenger") <= 0
    ) and
    (
      not IsTalentSelected("Seraphim") or
      GetCD("Seraphim") > GlobalCD() or
      IsTalentSelected("Sanctified Wrath")
    )
end
function simcraft.should_cast_final_reckoning()
  -- (holy_power>=4&time<8|holy_power>=3&time>=8)&
  -- cooldown.avenging_wrath.remains>gcd&time_to_hpg=0&
  -- (!talent.seraphim.enabled|buff.seraphim.up)&
  -- (!raid_event.adds.exists|raid_event.adds.up|raid_event.adds.in>40)
  if not IsUnitInRange("Crusader Strike", "target") then
    return false
  end
  return
    (paladin.GetHolyPower() >= 3) and
    (GetCD("Avenging Wrath") > GlobalCD()) and
    (not IsTalentSelected("Seraphim") or PlayerAuraDuration("Seraphim") > GlobalCD())
end
function simcraft.cooldowns()
  -- (holy_power>=4&time<5|holy_power>=3&(time>5|runeforge.the_magistrates_judgment)|talent.holy_avenger.enabled&cooldown.holy_avenger.remains=0)&(!talent.seraphim.enabled|cooldown.seraphim.remains>0|talent.sanctified_wrath.enabled)
  return
    ((IsUnitInRange("Crusader Strike", "target") and (not IsTalentSelected("Execution Sentence") or GetCD("Execution Sentence") < 52 and retribution.TargetTimeToLive(30) > 15)) and TryCastSpellOnPlayer("Shield of Vengeance")) or
    skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    (simcraft.should_cast_avenging_wrath() and TryCastSpellOnPlayer("Avenging Wrath")) or
    --(simcraft.should_cast_crusade() and TryCastSpellOnPlayer("Crusade")) or
    --(simcraft.should_cast_holy_avenger() and TryCastSpellOnPlayer("Holy Avenger")) or
    (simcraft.should_cast_final_reckoning() and TryCastSpellOnPlayer("Final Reckoning")) or
    nil
end
function simcraft.should_cast_seraphim()
  -- (cooldown.avenging_wrath.remains>15|cooldown.crusade.remains>15|talent.final_reckoning.enabled)&
  -- (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains<=gcd*3&(!raid_event.adds.exists|raid_event.adds.in>40|raid_event.adds.in<gcd|raid_event.adds.up))&
  -- (!talent.execution_sentence.enabled|cooldown.execution_sentence.remains<=gcd*3|talent.final_reckoning.enabled)&
  -- (!covenant.kyrian|cooldown.divine_toll.remains<9)|
  -- (target.time_to_die<15&target.time_to_die>5)
  if not IsUnitInRange("Blade of Justice", "target") then
    return false
  end
  return
    (GetCD("Avenging Wrath") > 15 or GetCD("Avenging Wrath") > 15 or IsTalentSelected("Final Reckoning")) and
    (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") <= GlobalCDMax() * 3 + GlobalCD()) and
    (not IsTalentSelected("Execution Sentence") or GetCD("Execution Sentence") < GlobalCDMax() * 3 + GlobalCD() or IsTalentSelected("Final Reckoning")) and
    (not skill.IsKyrian() or GetCD("Divine Toll") < 9) or
    (retribution.TargetTimeToLive(30) < 15 and retribution.TargetTimeToLive(10) > 5)
end
function simcraft.should_cast_execution_sentence()
  -- (buff.crusade.down&cooldown.crusade.remains>10|buff.crusade.stack>=3|cooldown.avenging_wrath.remains>10)&
  -- (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains>10)&
  -- target.time_to_die>8
  return
    (UnitName("target") ~= "Spiked Ball" and UnitName("target") ~= "Formless Mass") and
    (PlayerAuraDuration("Crusade") <= 0 and GetCD("Crusade") > 10 or PlayerAuraStacks("Crusade") >= 3 or GetCD("Avenging Wrath") > 10) and
    (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") > 10) and
    (retribution.TargetTimeToLive(16) > 8)
end
function simcraft.should_cast_divine_storm()
  -- (
  --  spell_targets.divine_storm=2&!(runeforge.final_verdict&talent.righteous_verdict.enabled&conduit.templars_vindication.enabled)|
  --  spell_targets.divine_storm>2|
  --  buff.empyrean_power.up&debuff.judgment.down&buff.divine_purpose.down
  -- )&
  -- !buff.vanquishers_hammer.up&
  -- (
  --  (!talent.crusade.enabled|cooldown.crusade.remains>gcd*3)&
  --  (!talent.execution_sentence.enabled|cooldown.execution_sentence.remains>gcd*6|cooldown.execution_sentence.remains>gcd*5&holy_power>=4|target.time_to_die<8|!talent.seraphim.enabled&cooldown.execution_sentence.remains>gcd*2)&
  --  (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains>gcd*6|cooldown.final_reckoning.remains>gcd*5&holy_power>=4|!talent.seraphim.enabled&cooldown.final_reckoning.remains>gcd*2)&
  --  (!talent.seraphim.enabled|cooldown.seraphim.remains%gcd+holy_power>3|talent.final_reckoning.enabled|talent.execution_sentence.enabled|covenant.kyrian)|
  --  (talent.holy_avenger.enabled&cooldown.holy_avenger.remains<gcd*3|buff.holy_avenger.up|buff.crusade.up&buff.crusade.stack<10)
  -- )
  return
    (skill.NumEnemiesWithin(8) == 2 and not (runeforge.is_active("Final Verdict") and IsTalentSelected("Righteous Verdict") and conduits.is_active("Templar's Vindication")) or
     skill.NumEnemiesWithin(8) > 2 or
     PlayerAuraDuration("Empyrean Power") > GlobalCD() and TargetMyAuraDuration("Judgment") and PlayerAuraDuration("Divine Purpose") <= 0
    ) and
    --(not PlayerAuraDuration("Vanquisher's Hammer") > GlobalCD()) and
    (
      simcraft.should_cast_templars_verdict()
    )
end
function simcraft.should_cast_templars_verdict()
  -- (!talent.crusade.enabled|cooldown.crusade.remains>gcd*3)&
  -- (!talent.execution_sentence.enabled|cooldown.execution_sentence.remains>gcd*6|(cooldown.execution_sentence.remains>gcd*5&holy_power>=4)|target.time_to_die<8|(!talent.seraphim.enabled&cooldown.execution_sentence.remains>gcd*2))&
  -- (!talent.final_reckoning.enabled|cooldown.final_reckoning.remains>gcd*6|cooldown.final_reckoning.remains>gcd*5&holy_power>=4|!talent.seraphim.enabled&cooldown.final_reckoning.remains>gcd*2)&
  -- (!talent.seraphim.enabled|cooldown.seraphim.remains%gcd+holy_power>3|talent.final_reckoning.enabled|talent.execution_sentence.enabled|covenant.kyrian)|
  -- (talent.holy_avenger.enabled&cooldown.holy_avenger.remains<gcd*3|buff.holy_avenger.up|buff.crusade.up&buff.crusade.stack<10)
  return
    --(not IsTalentSelected("Crusade") or GetCD("Crusade") > GlobalCDMax() * 3 + GlobalCD()) and
    (not IsTalentSelected("Execution Sentence") or GetCD("Execution Sentence") > GlobalCDMax() * 6 + GlobalCD() or GetCD("Execution Sentence") > GlobalCDMax() * 5 + GlobalCD() and paladin.GetHolyPower() >= 4 or retribution.TargetTimeToLive(16) < 8 or not IsTalentSelected("Seraphim") and GetCD("Execution Sentence") > GlobalCDMax() * 2 + GlobalCD()) and
    (not IsTalentSelected("Final Reckoning") or GetCD("Final Reckoning") > GlobalCDMax() * 6 + GlobalCD() or GetCD("Final Reckoning") > GlobalCDMax() * 5 + GlobalCD() and paladin.GetHolyPower() >= 4 or not IsTalentSelected("Seraphim") and GetCD("Execution Sentence") > GlobalCDMax() * 2 + GlobalCD()) --and
    --(not IsTalentSelected("Seraphim") or GetCD("Seraphim") % GlobalCDMax() + paladin.GetHolyPower() > 3 or IsTalentSelected("Final Reckoning") or IsTalentSelected("Execution Sentence") or skill.IsKyrian()) or
    --(IsTalentSelected("Holy Avenger") and GetCD("Holy Avenger") < GlobalCDMax() * 3 + GlobalCD() or PlayerAuraDuration("Holy Avenger") > GlobalCD() and PlayerAuraStacks("Crusade") < 10) or
end
function simcraft.finishers()
  return
    (simcraft.should_cast_seraphim() and TryCastSpellOnPlayer("Seraphim")) or
    (simcraft.should_cast_execution_sentence() and TryCastSpellOnTarget("Execution Sentence")) or
    (not skill.ShouldFocusSingleTarget() and simcraft.should_cast_divine_storm() and IsUnitInRange("Crusader Strike", "target") and TryCastSpellOnTarget("Divine Storm")) or
    ((simcraft.should_cast_templars_verdict() or not simcraft.should_cast_execution_sentence() or not simcraft.should_cast_final_reckoning()) and TryCastSpellOnTarget("Templar's Verdict")) or
    nil
end
function simcraft.generators()
  return
    ((paladin.GetHolyPower() == 5 or PlayerAuraDuration("Holy Avenger") > GlobalCD() or TargetMyAuraDuration("Final Reckoning") > GlobalCD() or TargetMyAuraDuration("Execution Sentence") > GlobalCD()) and simcraft.finishers() or nil) or
    (simcraft.should_cast_divine_toll() and TryCastSpellOnTarget("Divine Toll") or nil) or
    (simcraft.should_cast_hammer_of_wrath() and TryCastSpellOnTarget("Hammer of Wrath") or nil) or
    --((TargetMyAuraDuration("Judgment") <= 0 and PlayerAuraDuration("Holy Avenger") > GlobalCD()) and TryCastSpellOnTarget("Judgment") or nil) or
    ((paladin.GetHolyPower() <= 2 and IsTalentSelected("Execution Sentence") and TargetMyAuraDuration("Execution Sentence") > GlobalCD() and TargetMyAuraDuration("Execution Sentence") < GlobalCDMax() * 2 + GlobalCD()) and retribution.TryWakeOfAshes() or nil) or
    (simcraft.should_cast_blade_of_justice() and TryCastSpellOnTarget("Blade of Justice") or nil) or
    --((!debuff.judgment.up&talent.seraphim.enabled&(holy_power>=1&runeforge.the_magistrates_judgment|holy_power>=2)) and TryCastSpellOnTarget("Judgment") or nil) or
    ((TargetMyAuraDuration("Judgment") <= 0 and IsTalentSelected("Seraphim") and paladin.GetHolyPower() >= 2) and TryCastSpellOnTarget("Judgment") or nil) or
    (simcraft.should_cast_wake_of_ashes() and retribution.TryWakeOfAshes() or nil) or
    ((paladin.GetHolyPower() >= 3 and PlayerAuraDuration("Crusade") > GlobalCD() and PlayerAuraStacks("Crusade") < 10) and simcraft.finishers() or nil) or
    --((not skill.IsVenthyr() and paladin.GetHolyPower() <= 3 and conduits.is_active("Expurgation")) and TryCastSpellOnTarget("Blade of Justice") or nil) or
    (TargetMyAuraDuration("Judgment") <= 0 and TryCastSpellOnTarget("Judgment") or nil) or
    (TryCastSpellOnTarget("Hammer of Wrath") or nil) or
    (paladin.GetHolyPower() <= 3 and TryCastSpellOnTarget("Blade of Justice") or nil) or
    ((TargetHealthPercent() <= 0.2 or PlayerAuraDuration("Avenging Wrath") > GlobalCD() or PlayerAuraDuration("Crusade") > GlobalCD() or PlayerAuraDuration("Empyrean Power") > GlobalCDMax()) and simcraft.finishers() or nil) or
    (simcraft.should_cast_crusader_strike() and TryCastSpellOnTarget("Crusader Strike") or nil) or
    ((TargetMyAuraDuration("Consecration") <= 0 and skill.NumEnemiesWithin(8) >= 2) and TryConsecration() or nil) or
    simcraft.finishers() or
    (TargetMyAuraDuration("Consecration") <= 0 and TryConsecration() or nil) or
    TryCastSpellOnTarget("Crusader Strike") or
    TryConsecration() or
    nil
end
function retribution.priorities.raid()
  return
    retribution.stay_alive_raid() or
    TryCastQueuedSpell() or
    skill.DoNothingIfMounted() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfDrinking() or
    skill.DoNothingIfTargetOutOfCombat() or
    skill.TryUseHealthstone(0.33) or
    skill.TryInterruptTarget() or
    skill.StartAttack() or
    skill.AcquireTarget() or
    retribution.save_raid() or
    (TargetCast() == "Song of Dissolution" and TryCastSpellOnTarget("Rebuke") or nil) or
    --(false)

    simcraft.cooldowns() or
    simcraft.generators() or
    nil
end
function retribution.priorities.daily()
  return
  skill.dailies() or
  "nothing"
end
retribution.priorities.ascension = paladin.ascension

--function retribution.priorities.ascension()
--  return kyrian.ascension()
--end

local module = {
  ["spell"] = {
    ["range"] = range,
    ["dispel"] = {
      ["Cleanse Toxins"] = {
        "Disease",
        "Poison",
      },
    },
  },
  ["interrupts"] = {
    "Rebuke",
    "Blinding Light",
    "Hammer of Justice",
  },
  ["save_interrupt_for"]= {
    ["Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Empowered Flameforge Enforcer"] = {
      ["Fanning the Flames"] = "don't interrupt",
    },
    ["Coldheart Agent"] = {
      ["Terror"] = "Rebuke",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Rebuke",
    },
    ["Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Mawsworn Guard"] = {
      ["Massive Strike"] = "don't interrupt",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Rebuke",
    },
    ["Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
      ["Harrow"] = "don't interrupt",
    },
    ["Empowered Mawsworn Flametender"] = {
      ["Inner Flames"] = "Hammer of Justice",
    },
    ["Dungeoneer's Training Dummy"] = {
      ["don't cast"] = "Hammer of Justice",
    },
    ["Patrician Cromwell"] = {
      ["Mass Slow"] = "don't interrupt",
    },
    ["The Grand Malleare"] = {
      ["Withering Roar"] = "Rebuke",
    },
  },
  ["override_range"] = range,
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return retribution.priority_selection.run()
end
function module.load()
  paladin.load()
  EnableHealthHistory()
  retribution.priority_selection = PrioritySelectionFor(retribution.priorities)
  AddGuiElement(retribution.priority_selection.gui)
  retribution.frame:SetScript("OnEvent", retribution.on_event)
end
function module.unload()
  paladin.unload()
  retribution.frame:SetScript("OnEvent", nil)
end
retribution.frame = CreateFrame("Frame", nil, UIParent)

function retribution.on_event(self, event, ...)
  if event == "PLAYER_REGEN_DISABLED" then
    retribution.wake_of_ashes_timeout = GetTime() + GlobalCDMax() * 2
  end
end

skill.modules.paladin.retribution = module
