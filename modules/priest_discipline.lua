_, skill = ...
priest = priest or {}
local private = {}

private.frame = CreateFrame("Frame", nil, UIParent)







--function private.init()
--  private.load()
--end
--private.frame = CreateFrame("Frame", nil, UIParent)
--private.frame:SetScript("OnEvent", private.init)
--private.frame:RegisterEvent("ADDON_LOADED")



local module = {}
module.spell = {}
function module.spell.range(spell)
  if spell == "Holy Nova" then
    return 8
  elseif spell == "Ascended Nova" then
    return 8
  elseif spell == "Ascended Blast" then
    return 40
  elseif spell == "Psychic Scream" then
    return 8
  end
end
module.spell.purge = {
  ["Dispel Magic"] = {
    "Magic",
  },
}
module.spell.dispel = {
  ["Purify"] = {
    "Magic",
    "Disease",
  },
}
module["extra_spells"] = {
  "Ascended Nova",
  "Ascended Blast",
}
module["interrupts"] = {}


function module.is_off_global(name)
  return
    name == "Fade" or
    name == "Desperate Prayer" or
    name == "Leap of Faith" or
    name == "Pain Suppression"
end
function module.is_stun(name)
  return false
end
function module.is_disorient(name)
  -- TODO: Add IsFear()
  return name == "Psychic Scream"
end
function module.is_incapacitate(name)
  return false
end
function module.is_silence(name)
  return false
end
function module.is_interrupt(name)
  return false
end

local function TryShield(unit)
  return
    private.can_shield_unit(unit) and
    TryCastSpellOnUnit("Power Word: Shield", unit) or
    nil
end

local function TryDotIfWorth(guid)
  local unit = GuidToUnit(guid)
  if not unit then
    return --TryScanForGuid(guid) or nil
  end
  local duration = DotDurationOfSpell(private.dot)
  if UnitMyAuraDuration(unit, private.dot) > duration * 0.3 then
    return
  end
  local instant_damage = DamageOfSpell(private.dot)
  local duration_damage = DotDamageOfSpell(private.dot)
  local smite_damage = DamageOfSpell("Smite")
  local min_extra_dmg = math.floor(duration / HastedSeconds(1.5)) * smite_damage

  local projected_damage = (instant_damage + duration_damage + min_extra_dmg) * 0.8
  local maximum_damage = math.min(projected_damage, UnitHealth(unit))

  --ALERT("unit: "..tostring(unit))
  --ALERT("instant_damage: "..tostring(instant_damage))
  --ALERT("duration_damage: "..tostring(duration_damage))
  --ALERT("smite_damage: "..tostring(smite_damage))
  --ALERT("min_extra_dmg: "..tostring(min_extra_dmg))
  --ALERT("projected_damage: "..tostring(projected_damage))
  --ALERT("maximum_damage: "..tostring(maximum_damage))
  --ALERT("TargetHealth(): "..tostring(TargetHealth()))
  if maximum_damage > UnitHealth(unit) then
    return
  end
  return TryCastSpellOnUnit(private.dot, unit)
end
local function in_dot_range_and_in_combat(unit)
  return IsUnitInRange(private.dot, unit) and (UnitAffectingCombat(unit) or skill.ShouldAlwaysAttackUnit(unit))
end
local function MaybeDotAllEnemies()
  local enemies = skill.GetEnemies(in_dot_range_and_in_combat)
  --ALERT("#enemies: "..tostring(#enemies))
  for guid, _ in pairs(enemies) do
    --ALERT("guid: "..tostring(guid))
    --ALERT("GuidToUnit(guid): "..tostring(GuidToUnit(guid)))
    local result = TryDotIfWorth(guid) or nil
    if result then
      return result
    end
  end
end

local function TrySchism()
  local result =
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Schism") or
    private.TryUseTrinkets() or
    nil
  if result then
    return result
  end
  return nil
end

function private.TryUseTrinkets()
  return
    skill.TryUseEquippedTrinketOnUnit("Sinful Aspirant's Badge of Ferocity", "player") or
    skill.TryUseEquippedTrinketOnUnit("Wakener's Frond", "player") or
    skill.TryUseEquippedTrinketOnUnit("Soulletting Ruby", "target") or
    nil
end
local function UseSchismDebuff()
  if TargetMyAuraDuration("Schism") > 3 * HastedSeconds(1.5) + HastedSeconds(2) then
    return
      private.TryUseTrinkets() or
      (PlayerAuraDuration("Scales of Trauma") <= 0 and private.TryMindBlast() or nil) or
      priest.TrySadisticShadowWordDeath() or
      priest.TryLethalShadowWordDeath() or
      TryCastSpellOnTarget("Penance") or
      TryCastSpellOnTarget("Power Word: Solace") or
      nil
  elseif TargetMyAuraDuration("Schism") > GlobalCD() then
    return
      private.TryUseTrinkets() or
      priest.TrySadisticShadowWordDeath() or
      priest.TryLethalShadowWordDeath() or
      TryCastSpellOnTarget("Penance") or
      TryCastSpellOnTarget("Power Word: Solace") or
      (PlayerAuraDuration("Scales of Trauma") <= 0 and private.TryMindBlast() or nil) or
      nil
  else
    return nil
  end
end

function private.TryMindBlast()
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Power Overwhelming") > 0) and
    TryCastSpellOnTarget("Mind Blast") or
    nil
end

function private.TrySmite()
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Power Overwhelming") > 0) and
    TryCastSpellOnTarget("Smite") or
    nil
end

function private.TryPowerWordSolace()
  return
    TryCastSpellOnTarget("Power Word: Solace") or
    nil
end

local function TryInstantKill()
  local target_health = TargetHealth()
  local pws_damage = DamageOfSpell("Power Word: Solace")
  local dot_damage = DamageOfSpell(private.dot)
  return
    (DamageOfSpell("Power Word: Solace") > target_health and private.TryPowerWordSolace()) or
    (DamageOfSpell("Penance") > target_health and TryCastSpellOnTarget("Penance")) or
    (DamageOfSpell(private.dot) > target_health and TryCastSpellOnTarget(private.dot)) or
    nil
end

local function DoNothingIfMounted()
  return
    IsMounted() and
    "nothing" or nil
end

local function IsInGroup()
  return UnitExists("party1")
end
function KeepPowerWordFortitudeUp()
  for _, unit in ipairs(SKILL_PARTY) do
    if UnitExists(unit) and UnitIsFriend("player", unit) and UnitAuraDuration(unit, "Power Word: Fortitude") < 20 * 60 and IsUnitInRange("Power Word: Fortitude", unit) and not UnitIsDeadOrGhost(unit) then
      return TryCastSpellOnUnit("Power Word: Fortitude", unit)
    end
  end
end

function private.stay_alive()
  local player_health_percent = PlayerHealthPercent()
  return
    (player_health_percent < 0.15 and TryCastSpellOnPlayer("Power Word: Barrier") or nil) or
    (player_health_percent < 0.25 and TryCastSpellOnPlayer("Pain Suppression") or nil) or
    (player_health_percent < 0.30 and TryCastSpellOnPlayer("Desperate Prayer") or nil) or
    (player_health_percent < 0.35 and kyrian.TryUsePhialOfSerenity()) or
    (player_health_percent < 0.40 and TryShield("player")) or
    (player_health_percent < 0.20 and TryCastSpellOnPlayer("Penance") or nil) or
    (player_health_percent < 0.15 and not IsPlayerMoving() and TryCastSpellOnPlayer("Shadow Mend") or nil) or
    nil
end

function private.stay_alive_mage_tower()
  local player_health_percent = PlayerHealthPercent()
  return
    (player_health_percent < 0.30 and TryCastSpellOnPlayer("Desperate Prayer") or nil) or
    (player_health_percent < 0.40 and UnitAuraAbsorb("player", "Power Word: Shield") < PlayerHealthMax() * 0.1 and TryShield("player")) or
    (player_health_percent < 0.40 and TryCastSpellOnPlayer("Penance")) or
    (player_health_percent < 0.45 and PlayerAuraDuration("Weakened Soul") > 0 and PlayerAuraDuration("Weakened Soul") <= 0 and not IsPlayerMoving() and TryCastSpellOnPlayer("Shadow Mend") or nil) or
    (player_health_percent < 0.15 and not IsPlayerMoving() and TryCastSpellOnPlayer("Shadow Mend") or nil) or
    nil
end
local function existing_alive_friend(unit)
  return UnitExists(unit) and not UnitIsDeadOrGhost(unit) and UnitIsFriend(unit, "player")
end
local function below_20_percent_health(unit)
  return UnitHealthPercent(unit) < 0.2
end
local function TryShadowMendOnUnit(unit)
  return
    not IsPlayerMoving() and
    TryCastSpellOnUnit("Shadow Mend", unit) or
    nil
end
function private.can_shield_unit(unit)
  return UnitAuraDuration(unit, "Weakened Soul") <= GlobalCD() or PlayerAuraDuration("Rapture") > GlobalCD()
end

local function GiveUnitSecondsToLive(unit, seconds)
  -- TODO: Maybe implement absorb change per second.
  if UnitHealth(unit) + UnitAbsorb(unit) - UnitHealAbsorb(unit) + (UnitHealthChangedPerSecond(unit, 5)) * seconds <= 0 then
    local can_shield_unit = private.can_shield_unit(unit)
    local current_pws_absorb_is_small = UnitAuraAbsorb("Power Word: Shield") <= AbsorbOfSpell("Power Word: Shield") * 0.2
    return
      (can_shield_unit and current_pws_absorb_is_small and TryShield(unit)) or
      TryCastSpellOnUnit("Penance", unit) or
      TryShadowMendOnUnit(unit) or
      (not private.can_shield_unit(unit) and current_pws_absorb_is_small and TryCastSpellOnTarget("Rapture")) or
      nil
  end
end

private.ttl_funcs = {}
function private.get_ttl_func(seconds)
  if not private.ttl_funcs[seconds] then
    private.ttl_funcs[seconds] = function(unit)
      return GiveUnitSecondsToLive(unit, seconds) or nil
    end
  end
  return private.ttl_funcs[seconds]
end
local function GivePartySecondsToLive(seconds)
  return
    ForUniqueFilteredCombatCastUnits(private.get_ttl_func(seconds), existing_alive_friend) or
    nil
end
local function HandlePowerWordRadiance()
  if IsPlayerMoving() then
    return nil
  end
  local deficit = 0
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if ((UnitMyAuraDuration(unit, "Atonement") < GlobalCDMax() * 3 or UnitHealthPercent(unit) < 0.7)) and UnitIsFriend(unit, "player") and IsUnitInRange("Power Word: Radiance", unit) then
      deficit = deficit + min(0.15, 1 - UnitHealthPercent(unit))
    end
  end
  if deficit >= 0.4 then
    return TryCastSpellOnPlayer("Power Word: Radiance")
  end
end
local function HandleShadowfiend()
  local deficit = 0
  for _, unit in ipairs(GroupCombatCastUnits()) do
    local deficit_percent = (1 - UnitHealthPercent(unit))
    local atonement_duration = UnitMyAuraDuration(unit, "Atonement")
    if min(deficit_percent, atonement_duration) > 0.25 and UnitIsFriend(unit, "player") then
      deficit = deficit + min(0.5, deficit_percent)
    end
  end
  if deficit >= 0.75 then
    return TryCastSpellOnTarget("Shadowfiend")
  end
end
local function HandlePowerWordShield()
  for _, unit in ipairs(GroupCombatCastUnits()) do
    local result =
      UnitHealthPercent(unit) < 0.7 and
      UnitMyAuraDuration(unit, "Atonement") < 2 and
      TryShield(unit) or
      nil
    if result then
      return result
    end
  end
end
local function TrySchismForLowHpAtonement()
  if not IsTalentSelected("Schism") then
    return nil
  end
  for _, unit in ipairs(GroupCombatCastUnits()) do
    local result =
      UnitHealthPercent(unit) < 0.7 and
      UnitMyAuraDuration(unit, "Atonement") > 6 and
      TryCastSpellOnTarget("Schism") or
      nil
    if result then
      return result
    end
  end
end
local function TryDotIfWorthInGroup(guid)
  local unit = GuidToUnit(guid)
  if not unit then
    return --TryScanForGuid(guid) or nil
  end
  local duration = DotDurationOfSpell(private.dot)
  if UnitMyAuraDuration(unit, private.dot) > duration * 0.3 then
    return
  end
  local incoming_dps = UnitHealthChangedPerSecond(unit, 5)
  local time_to_live = TargetHealth() / incoming_dps
  if duration * 0.9 < time_to_live then
    return TryCastSpellOnUnit(private.dot, unit)
  end
end
local function MaybeDotAllEnemiesInGroup()
  local enemies = skill.GetEnemies(in_dot_range_and_in_combat)
  for guid, _ in pairs(enemies) do
    local result = TryDotIfWorthInGroup(guid) or nil
    if result then
      return result
    end
  end
end
local function greatest_common_denominator(a, b)
  return b ~= 0 and greatest_common_denominator(b, a % b) or math.abs(a)
end
function least_common_multiple(m, n)
  return (m ~= 0 and n ~= 0) and m * n / greatest_common_denominator(m, n) or 0
end
local function single_target_dps()
  local global = HastedSeconds(1.5)
  local schism = IsTalentSelected("Schism") and 1 or 0
  local schism_multiplier = 1 + (schism * 0.4)
  -- Shadow Word: Death
  -- Mind Blast
  -- Power Word: Solace
  -- Penance
  local schism_smite_uptime = schism * (9 - 3 * global - HastedSeconds(2)) / 24
  local solace = IsTalentSelected("Power Word: Solace") and 1 or 0

  local schism_dmg = schism * schism_multiplier * DamageOfSpell("Schism")
  local solace_dmg = solace * schism_multiplier * DamageOfSpell("Power Word: Solace")
  local mind_blast_dmg = DamageOfSpell("Mind Blast")
  local shadow_word_death_dmg = DamageOfSpell("Shadow Word: Death")
  local penance_dmg = schism_multiplier * DamageOfSpell("Penance")
  local smite_dmg = SpellId("Smite") and (schism_multiplier * schism_smite_uptime * DamageOfSpell("Smite") + (1 - schism_smite_uptime ) * DamageOfSpell("Smite")) or 0
  local dot_dmg = DamageOfSpell(private.dot) + DotDamageOfSpell(private.dot) * (1 + GetHaste() / 100)

  local rotation_time = 360
  local x = rotation_time * schism_dmg / 24

  local schism_dps = schism_dmg / 24
  local solace_dps = solace_dmg / HastedSeconds(15)
  local mind_blast_dps = mind_blast_dmg / HastedSeconds(15)
  local shadow_word_death_dps = shadow_word_death_dmg / HastedSeconds(20)
  local penance_dps = penance_dmg / 9
  local smite_dps = smite_dmg / GetCDMax("Smite")
  local dot_dps = dot_dmg / DotDurationOfSpell(private.dot)
  return schism_dps + solace_dps + mind_blast_dps + shadow_word_death_dps + penance_dps + smite_dps + dot_dps
end
local function holy_nova_dps()
  return DamageOfSpell("Holy Nova") / HastedSeconds(1.5)
end
local function TryGetTarget()
  local tank = TryGetTank()
  if not UnitExists("target") and tank and UnitAffectingCombat(tank) and UnitExists(tank.."target") and not UnitIsDeadOrGhost(tank.."target") then
    local result = TryCastSpellOnUnit("target", tank.."target")
    if result then
      return result
    end
  end
  return nil
end
function private.TryPenance(unit)
  return
    TryCastSpellOnUnit("Penance", unit) or
    nil
end
function private.TryPainSuppression(unit)
  return
    TryCastSpellOnUnit("Pain Suppression", unit) or
    nil
end
function private.HandleRapture()
  return
    PlayerAuraDuration("Rapture") > GlobalCD() and
    ALERT_ONCE("NYI: Rapture") or
    nil
end
private.atonement_units = {}
private.spell_heal_stats = {}
private.atonement_durations = {}
local atonement_spells = {
  ["Schism"] = true,
  ["Penance"] = true,
  ["Mind Blast"] = true,
  ["Smite"] = true,
  ["Power Word: Solace"] = true,
}
function private.update_heal_stats()
  wipe(private.atonement_units)
  for _, unit in ipairs(GroupCombatCastUnits()) do
    private.atonement_durations[unit] = UnitMyAuraDuration(unit, "Atonement")
  end
  for spell, _ in pairs(atonement_spells) do
    local heal, overheal, efficiency = private.get_heal_stat(DamageOfSpell(spell) * 0.5, GlobalCD() + GetCastTime(spell) + 0.1)
    private.spell_heal_stats[spell] = private.spell_heal_stats[spell] or {}
    private.spell_heal_stats[spell]["heal"] = heal
    private.spell_heal_stats[spell]["overheal"] = overheal
    private.spell_heal_stats[spell]["efficiency"] = efficiency
  end
  --local heal, overheal, efficiency = private.get_heal_stat(DamageOfSpell("Power Word: Radiance") * 0.5, GlobalCD() + GetCastTime("Power Word: Radiance") + 0.1)
  --private.spell_heal_stats["Power Word: Radiance"]["heal"] = heal
  --private.spell_heal_stats["Power Word: Radiance"]["overheal"] = overheal
  --private.spell_heal_stats["Power Word: Radiance"]["efficiency"] = efficiency
end
function private.get_heal_stat(amount, after_seconds)
  local master_multiplier = GetMasteryEffect() / 100
  local heal = 0
  local overheal = 0
  for unit, duration in pairs(private.atonement_durations) do
    if duration > after_seconds then
      local deficit = UnitHealthMax(unit) - UnitHealth(unit)
      local effective = min(deficit, amount * master_multiplier)
      heal = heal + effective
      overheal = overheal + deficit - effective
    end
  end
  return heal, overheal, heal > 0 and overheal / heal or 0
end

local sorted_by_efficiency = {}
local function by_efficiency(a, b)
  return a.efficiency < b.efficiency
end
function private.HandleAoeHeal()
  if private.spell_heal_stats['Schism']['efficiency'] >= 4 / 5 then
    return
    TryCastSpellOnTarget("Schism") or
    nil
  end
  table.sort(private.spell_heal_stats, by_efficiency)
  for spell, stats in pairs(sorted_by_efficiency) do
    if CanCastSpellOnUnit(spell, "target") and stats.efficiency > 0.5 then
      return
        TryCastSpellOnTarget(spell) or nil
      --return
      --  spell == "Power Word: Radiance" and
      --  TryCastSpellOnPlayer(spell) or
      --  TryCastSpellOnTarget(spell) or
      --  nil
    end
  end
end
function private.HandleBoonOfTheAscended()
  if PlayerAuraDuration("Boon of the Ascended") <= GlobalCD() then
    return nil
  end
  return
    (skill.NumEnemiesWithin(8) >= 5 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
    TryCastSpellOnTarget("Ascended Blast") or
    (skill.NumEnemiesWithin(8) > 0 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
    nil
end

local function GroupPriority()
  local tank = skill.TryGetFirstTankUnit()
  private.update_heal_stats()
  tank = tank or "party1"
  return
    (PlayerHealthPercent() < 0.5 and TryShield("player")) or

    private.HandleBoonOfTheAscended() or

    skill.DoNothingIfCasting() or
    (tank and UnitHealthDeficit(tank) > HealOfSpell("Penance") * 2 and TryCastSpellOnUnit("Penance", tank)) or
    ForUniqueFilteredCombatCastUnits(private.TryPenance, existing_alive_friend, below_20_percent_health) or
    ForUniqueFilteredCombatCastUnits(private.TryPainSuppression, existing_alive_friend, below_20_percent_health) or
    GivePartySecondsToLive(3) or
    (UnitAffectingCombat(tank) and PlayerMyAuraDuration("Power Word: Shield") <= 0 and TryShield(tank)) or
    private.HandleRapture() or

    (tank and UnitHealthPercent(tank) < 0.4 and TryShadowMendOnUnit(tank)) or

    TryDispelAnythingFromParty() or
    skill.TryPurgeAnythingFromTarget() or

    GivePartySecondsToLive(6) or
    (tank and UnitHealthPercent(tank) < 0.8 and TryCastSpellOnTarget("Penance")) or
    private.HandleAoeHeal() or
    TrySchismForLowHpAtonement() or
    HandlePowerWordRadiance() or
    (tank and UnitHealthPercent(tank) < 0.6 and TryCastSpellOnTarget("Mind Blast")) or
    HandlePowerWordShield() or

    (PlayerManaPercent() > 0.33 and MaybeDotAllEnemiesInGroup()) or

    (skill.NumEnemiesWithin(8) * holy_nova_dps() > single_target_dps() and TryCastSpellOnPlayer("Holy Nova")) or
    (PlayerManaPercent() < 0.9 and private.TryPowerWordSolace() or nil) or
    private.TrySmite() or
    private.TryPowerWordSolace() or
    TryCastSpellOnTarget("Penance") or
    --UseFreeGlobals() or

    nil
end
local function SoloPriority()
  return
    (PlayerHealthPercent() < 0.8 and TryShield("player")) or
    private.stay_alive() or

    skill.TryPurgeAnythingFromTarget() or
    TryDispelAnythingFromUnit("player") or

--    (skill.NumEnemiesWithin(10) * holy_nova_dps() > single_target_dps() and TryCastSpellOnPlayer("Holy Nova")) or
    skill.TryUseConcentratedFlame() or
    (GetCD("Penance") <= max(0,9 - 2 * HastedSeconds(1.5)) and TrySchism() or nil) or
    TryCastSpellOnTarget("Penance") or
    private.TryPowerWordSolace() or

    --TryInstantKill() or
    MaybeDotAllEnemies() or
    TryDotIfWorth(UnitGUID("target")) or
    private.TrySmite() or

    nil
end
local function singleTargetGlobal()
  return
    --(not UnitAffectingCombat("player") and TryCastSpellOnPlayer("Weather-Beaten Fishing Hat")) or
    --"nothing" or
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
    --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget(private.dot)) or

    skill.DoNothingIfCasting() or
    skill.DoNothingIfDrinking() or
    DoNothingIfMounted() or
    TryCastQueuedSpell() or

    skill.KeepRepurposedFelFocuserUp() or
    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp()) or
    --skill.DoNothingOutOfCombat() or
    skill.DoNothingIfTargetOutOfCombat() or


    (IsInGroup() and GroupPriority() or SoloPriority()) or




--    (skill.NumEnemiesWithin(8) > 9 and TryCastSpellOnPlayer("Holy Nova") or nil) or
--    TryDotIfWorth() or
--    (not IsPlayerMoving() and TrySchism() or nil) or
--    (MyTargetAuraDuration("Concentrated Flame") < GlobalCD() and skill.TryUseConcentratedFlame() or nil) or
--    private.TryPowerWordSolace() or
--    (skill.NumEnemiesWithin(8) > 3 and TryCastSpellOnPlayer("Holy Nova") or nil) or
--    (not IsPlayerMoving() and TryCastSpellOnTarget("Smite") or nil) or
--    (MyTargetAuraDuration("Purge the Wicked") <= 4 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or
--    (skill.NumEnemiesWithin(8) > 1 and TryCastSpellOnPlayer("Holy Nova") or nil) or
--    (IsPlayerMoving() and TryCastSpellOnTarget("Penance") or nil) or
--    (PlayerAuraDuration("Weakened Soul") <= 0 and TryCastSpellOnPlayer("Power Word: Shield") or nil) or
--    TryCastSpellOnPlayer("Holy Nova") or
    "nothing"
end

local priorities = {}
function priorities.default()
  return
    private.common() or

    GroupPriority() or

    "nothing"
end
--function priorities.new()
--  return
--    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp() or nil) or
--    skill.common_priority_start() or
--    private.common() or
--
--
--    "nothing"
--end
local function TryFinish()
  local target_health = TargetHealth()
  return
    (target_health < DamageOfSpell("Smite") and private.TrySmite() or nil) or
    (UnitAuraDuration("player", "Catharstick", "MAW") <= 0 and target_health < DamageOfSpell(private.dot) and TryCastSpellOnTarget(private.dot) or nil) or
    (target_health < DamageOfSpell("Holy Nova") and TryCastSpellOnTarget("Holy Nova") or nil) or
    (target_health < DamageOfSpell("Power Word: Solace") and TryCastSpellOnTarget("Power Word: Solace") or nil) or
    (torghast.IsTargetMawrat() and UnitAuraDuration("player", "Rat-Corpse Bag", "MAW") <= 0 and priest.TryLethalShadowWordDeath() or nil) or
    nil
end

function UnitTimeToPercent(unit, percent)
  if not UnitExists(unit) then
    return 60 * 60
  end
  local real_dps = GetTime() - UnitOldestHealthChangedTime(unit, 10) > 5 and -UnitHealthChangedPerSecond(unit, 10) or 0
  local dps = max(real_dps, single_target_dps())
  local health_remaining = max(0, UnitHealth(unit) - (UnitHealthMax(unit) * percent))
  return (dps > 0) and (health_remaining / dps) or (60 * 60)
end
local function UnitTimeToLive(unit)
  return UnitTimeToPercent(unit, 0)
end

function private.IsKissOfDeathEquipped()
  return IsEquippedItem(173246)
end
function private.ShouldUseSadisticShadowWordDeath()
  if private.IsKissOfDeathEquipped() then
    return UnitTimeToPercent("target", 0.2) > HastedSeconds(20) - 8
  else
    return UnitTimeToPercent("target", 0.2) > HastedSeconds(20) and TargetMyAuraDuration("Schism") > GlobalCD()
  end
end
function private.HandleAngelicFeather()
  return
    not UnitAffectingCombat("player") and
    IsTalentSelected("Angelic Feather") and
    IsPlayerMoving() and
    PlayerAuraDuration("Lifted Spirit") <= 0 and
    PlayerAuraDuration("Unburden") <= 0 and
    PlayerAuraDuration("Angelic Feather") <= 0.2 and
    GetCharges("Angelic Feather") > (private.angelic_feathers_to_keep or 3) and
    TryCastSpellOnPlayer("Angelic Feather") or
    nil
end
function private.common()
  return
    TryCastQueuedSpell() or
    DoNothingIfMounted() or
    skill.DoNothingIfDrinking() or
    skill.KeepRepurposedFelFocuserUp() or
    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp() or nil) or
    private.HandleAngelicFeather() or
    skill.TryLoot() or
    private.stay_alive() or
    skill.DoNothingOutOfCombat() or
    TryDispelAnythingFromUnit("player") or
    skill.DoNothingIfTargetOutOfCombat() or
    nil
end
function priorities.solo()
  return
    skill.TryGather() or
    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp() or nil) or
    private.common() or
    skill.StartAttack() or
    skill.AcquireTarget() or

    GiveUnitSecondsToLive("player", 4) or
    kyrian.HandleBoonOfTheAscended() or
    skill.DoNothingIfChanneling() or
    UseSchismDebuff() or
    (PlayerHealthPercent() < 0.70 and TryShield("player")) or
    skill.TryPurgeAnythingFromTarget() or
    (UnitTimeToLive("target") > 9 - GlobalCDMax() and TrySchism() or nil) or
    TryFinish() or
    (not IsPlayerMoving() and skill.NumEnemiesWithin(10) > 3 and TryCastSpellOnTarget("Mind Sear") or nil) or
    priest.TryLethalShadowWordDeath() or
    (private.ShouldUseSadisticShadowWordDeath() and priest.TrySadisticShadowWordDeath() or nil) or
    private.TryMindBlast() or
    TryCastSpellOnTarget("Penance") or
    private.TryPowerWordSolace() or
    private.TrySmite() or

    "nothing"
end
function priorities.debug()
  return
    "nothing"
end
function priorities.daily()
  return
    skill.HandleJustWingingIt() or
    skill.Aspirant() or

    "nothing"
end
--function priorities.fishing()
--  return
--    TryCastSpellOnPlayer("Weather-Beaten Fishing Hat") or
--
--    "nothing"
--end
function HandleMawrats()
  if not torghast.IsTargetMawrat() then
    return nil
  end
  return
    (PlayerAuraDuration("Torment: Fracturing Forces") > 0 and "nothing" or nil) or
    private.TrySmite() or
    (UnitAuraDuration("player", "Catharstick") <= 0 and TryCastSpellOnTarget(private.dot) or nil) or
    nil
end
function HandleVolatilePhantasm()
  if UnitAuraDuration("player", "Volatile Phantasm", "MAW") <= 0 then
    return nil
  end
  return
    not IsPlayerMoving() and
    skill.NumEnemiesWithin(8) > 0 and
    TryCastSpellOnPlayer("Fade") or
    nil
end
function priorities.torghast()
  local catharstick_amount = tonumber(UnitAuraMatch("player", "Catharstick", "(%d+) damage will be added.") or 0)
  local catharstick_percent = catharstick_amount / PlayerHealthMax()
  local scales_of_trauma_amount = tonumber(UnitAuraMatch("player", "Scales of Trauma", "Your next Mind Blast deals an additional (%d+) damage.") or 0)
  local scales_of_trauma_percent = scales_of_trauma_amount / 2 / PlayerHealthMax()
  return
    torghast.torghast_stuff() or
    (not UnitAffectingCombat("player") and not IsMounted() and kyrian.TryRefreshPhials() or nil) or
    (not UnitAffectingCombat("player") and not (UnitAuraDuration("player", "Parliament Stone", "MAW") > 0 or UnitAuraDuration("player", "Strigidium", "MAW") > 0) and kyrian.TryDismissSteward() or nil) or
    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp() or nil) or
    skill.common_priority_start() or
    skill.DoNothingIfCasting() or
    private.common() or
    skill.AcquireTarget() or

    GiveUnitSecondsToLive("player", 4) or
    (PlayerMyAuraDuration("Atonement") <= 0 and PlayerHealthPercent() < 1 and catharstick_amount <= 0 and TryShield("player") or nil) or
    (PlayerMyAuraDuration("Atonement") <= 0 and PlayerHealthPercent() < 1 and not IsPlayerMoving() and skill.TryHealUnit("Shadow Mend", "player", 0.8, 0.1) or nil) or
    (PlayerMyAuraDuration("Atonement") <= 0 and PlayerHealthPercent() < 1 and catharstick_amount <= 0 and PlayerHealthPercent() < 0.8 and IsPlayerMoving() and TryShield("player") or nil) or
    (PlayerHealthPercent() < 0.5 and TryShield("player") or nil) or
    (UnitName("target") ~= "Watchers of Death" and PlayerHealthPercent() < 0.6 and TryCastSpellOnPlayer("Penance") or nil) or
    skill.TryPurgeAnythingFromTarget() or
    skill.DoNothingIfChanneling() or
    (UnitAuraDuration("player", "Death Harvester", "MAW") > 0 and not (UnitAuraDuration("player", "Rat-Corpse Bag", "MAW") > 0 and torghast.IsTargetMawrat()) and priest.TryLethalShadowWordDeath() or nil) or
    private.HandleBoonOfTheAscended() or
    HandleMawrats() or
    HandleVolatilePhantasm() or
    TryFinish() or
    UseSchismDebuff() or
    (UnitName("target") ~= "Watchers of Death" and UnitAuraDuration("player", "Scales of Trauma", "MAW") > 0 and scales_of_trauma_percent < 1 and skill.TryHealUnit("Shadow Mend", "player", 1, 0) or nil) or
    (UnitName("target") ~= "Watchers of Death" and UnitAuraDuration("player", "Scales of Trauma", "MAW") > 0 and scales_of_trauma_percent < 1 and UnitName("targettarget") == UnitName("player") and skill.TryHealUnit("Shadow Mend", "targettarget", 1, 0) or nil) or
    (not IsPlayerMoving() and IsTalentSelected("Divine Star") and skill.NumEnemiesWithin(10) >= 2 and TryCastSpellOnPlayer("Divine Star") or nil) or
    (not IsPlayerMoving() and skill.NumEnemiesWithin(8) > 3 and TryCastSpellOnTarget("Mind Sear") or nil) or
    (not UnitExists("party1") and UnitTimeToLive("target") > 30 and TryCastSpellOnPlayer("Power Infusion") or nil) or
    (catharstick_amount <= 0 and UnitTimeToLive("target") > 5 and TargetMyAuraDuration(private.dot) <= 0 and TryCastSpellOnTarget(private.dot) or nil) or
    (UnitTimeToLive("target") > 10 and TrySchism() or nil) or
    (PlayerAuraDuration("Power Overwhelming") > GlobalCD() and PlayerAuraDuration("Power Infusion") > GlobalCD() and TryCastSpellOnTarget("Mind Blast") or nil) or
    (scales_of_trauma_amount > 0 and scales_of_trauma_percent >= 1 and scales_of_trauma_amount < TargetHealth() and private.TryMindBlast() or nil) or
    (catharstick_amount > 0 and catharstick_percent >= 1 and catharstick_amount < TargetHealth() and TryCastSpellOnTarget(private.dot) or nil) or
    (PlayerManaPercent() < 0.7 and private.TryPowerWordSolace() or nil) or
    private.TrySmite() or
    (catharstick_amount <= 0 and TargetMyAuraDuration(private.dot) <= 0 and TryCastSpellOnTarget(private.dot) or nil) or
    TryCastSpellOnTarget("Penance") or
    private.TryPowerWordSolace() or
    (not IsPlayerMoving() and TryCastSpellOnTarget("Mind Sear") or nil) or
    (catharstick_amount <= 0 and TryCastSpellOnTarget(private.dot) or nil) or
    TryCastSpellOnTarget("Holy Nova") or

    "nothing"
end
function dot()
  ALERT("dot: "..tostring(private.dot))
end
local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (target_name == "Vazeel'fazag" or target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
function priorities.low_level_farm()
  local is_boss = skill.UnitIsBoss("target")
  return
  initiateLowLevelCombatWithSpell(private.dot) or
  (not UnitAffectingCombat("player") and skill.UnitIsBoss("target") and TryCastSpellOnTarget(private.dot) or nil) or
  skill.DoNothingIfMounted() or
  (skill.ShouldUseRunSpeed() and PlayerAuraDuration("Angelic Feather") <= 0.3 and TryCastSpellOnPlayer("Angelic Feather")) or
  skill.AcquireTarget() or
  skill.DoNothingIfCasting() or
  skill.KeepRepurposedFelFocuserUp() or
  (skill.UnitIsBoss("target") and not UnitIsFriend("player", "target") and skill.IsUnitAlive("target") and TryCastSpellOnTarget("Shadowfiend") or nil) or
  skill.DoNothingOutOfCombat() or
  skill.DoNothingIfTargetOutOfCombat() or
  initiateLowLevelCombatWithSpell(private.dot) or
  skill.StartAttack() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or

    (not skill.UnitIsBoss("target") and skill.NumEnemiesWithin(8) > 1 and TryCastSpellOnPlayer("Holy Nova")) or
    skill.DoNothingIfTargetOutOfCombat() or
    (is_boss and skill.TryUseConcentratedFlame()) or
    TryCastSpellOnTarget("Penance") or
    TryCastSpellOnTarget("Power Word: Solace") or
    (TargetMyAuraDuration(private.dot) <= 0 and TryCastSpellOnTarget(private.dot)) or
    singleTargetGlobal() or
    nil
end
function private.handle_target_and_focus()
  local target = UnitName("target")
  local focus = UnitName("focus")
  local tugar = "Tugar Bloodtotem"
  local jormog = "Jormog the Behemoth"
  if target == tugar then
    return
      (not focus or focus == tugar) and
      UnitName("mouseover") == jormog and
      TryMacro("/focus mouseover") or
      nil
  elseif target == jormog then
    if TargetAuraStacks("Fel Hardened Scales") > 3 then
      return
        TryMacro("/target Tugar") or
        nil
    end
    return
      (not focus or focus == jormog) and
      UnitName("mouseover") == tugar and
      TryMacro("/focus mouseover") or
      nil
  elseif target == "Bile Spitter Egg" or target == "Bile Spitter" then

  else
    return
      TryMacro("/target Tugar") or
      nil
  end
end
function private.handle_fel_burst()
  local name = "Tugar Bloodtotem"
  local unit = UnitName("target") == name and "target"
  unit = UnitName("focus") == name and "focus" or unit
  unit = UnitName("mouseover") == name and "mouseover" or unit
  if not unit then
    return nil
  end
  local cast_remaining = skill.UnitSpellCastRemaining(unit, "Fel Burst")
  local cd = GetCD("Psychic Scream")
  if cast_remaining <= 0 then
    if not IsPlayerMoving() and cd > GlobalCDMax() + 3 then
      return
        (PlayerAuraDuration("Masochism") <= GlobalCDMax() + GlobalCD() and TryCastSpellOnPlayer("Shadow Mend") or nil) or
        skill.TryHealUnit("Shadow Mend", "player", 0.7, 0.5) or
        nil
    end
    return nil
  end
  if cd < cast_remaining and skill.UnitRangeMax(unit) <= 5 then
    return TryCastSpellOnPlayer("Psychic Scream") or nil
  end
  local health = UnitHealthPercent(unit)
  local r = PlayerAuraDuration("Rapture")
  local dp = PlayerAuraDuration("Desperate Prayer")
  local ps = PlayerAuraDuration("Pain Suppression")
  local pwb = PlayerAuraDuration("Power Word: Barrier")
  local absorb = PlayerAuraDuration("Rapture") > GlobalCD() and 1400 or 700
  return
    (r <= 0 and TryCastSpellOnPlayer("Desperate Prayer") or nil) or
    (dp <= 0 and TryCastSpellOnPlayer("Rapture") or nil) or
    (ps <= 0 and health < 0.2 and TryCastSpellOnPlayer("Power Word: Barrier") or nil) or
    (pwb <= 0 and health < 0.2 and TryCastSpellOnPlayer("Pain Suppression") or nil) or
    (cast_remaining > GlobalCD() + 1.5 and TryCastSpellOnUnit("Mind Blast", unit)) or
    (UnitAuraAbsorb("player", "Power Word: Shield") < absorb and TryShield("player") or nil) or
    (TryShield("player") or nil) or
    TryCastSpellOnTarget("Penance") or
    (not IsPlayerMoving() and TryCastSpellOnPlayer("Shadow Mend")) or
    nil
end
function private.dot_mouseover(name)
  return
    (UnitName("mouseover") == name and UnitMyAuraDuration("mouseover", "Shadow Word: Pain") <= 0 and TryCastSpellOnUnit("Shadow Word: Pain", "mouseover") or nil) or
    nil
end
function private.cast_spell_on_mouseover(name, spell)
  return
    (UnitName("mouseover") == name and TryCastSpellOnUnit(spell, "mouseover") or nil) or
    nil
end
function private.handle_fel_surge_totem()
  local name = "Fel Surge Totem"
  local result =
    private.dot_mouseover(name) or
    nil
  if result then
    return result
  end
  return nil
end
function private.handle_bile_spitters_and_eggs()
  return
    private.dot_mouseover("Bile Spitter Egg") or
    private.dot_mouseover("Bile Spitter") or
    private.cast_spell_on_mouseover("Bile Spitter Egg", "Smite") or
    private.cast_spell_on_mouseover("Bile Spitter", "Smite") or
    private.cast_spell_on_mouseover("Bile Spitter Egg", "Power Word: Solace") or
    private.cast_spell_on_mouseover("Bile Spitter", "Power Word: Solace") or
    private.cast_spell_on_mouseover("Bile Spitter Egg", "Shadow Word: Death") or
    private.cast_spell_on_mouseover("Bile Spitter", "Shadow Word: Death") or
    nil
end
function priorities.mage_tower()
  return
    skill.DoNothingIfMounted() or
    skill.KeepRepurposedFelFocuserUp() or
    KeepPowerWordFortitudeUp() or
    private.handle_target_and_focus() or
    skill.DoNothingOutOfCombat() or
    skill.AcquireTarget() or
    (UnitName("target") == "Tugar Bloodtotem" and skill.UnitCast("target") == "Fel Burst" and (skill.UnitCast("player") == "Penance" or skill.UnitCast("player") == "Mind Sear") and TryMacro("/stopcasting") or nil) or
    (PlayerHealthPercent() >= 1 and PlayerMyAuraDuration("Masochism") > 3 and skill.UnitCast("player") == "Shadow Mend" and TryMacro("/stopcasting") or nil) or
    skill.AvoidInterrupt() or
    skill.DoNothingIfChanneling() or
    skill.DoNothingIfCasting() or
    skill.StartAttack() or
    TryCastQueuedSpell() or

    TryDispelAnythingFromUnit("player") or
    private.stay_alive_mage_tower() or


    private.handle_fel_surge_totem() or
    private.handle_fel_burst() or
    private.dot_mouseover("Tugar Bloodtotem") or
    private.dot_mouseover("Jormog the Behemoth") or
    (not IsPlayerMoving() and PlayerMyAuraDuration("Masochism") <= 0 and skill.TryHealUnit("Shadow Mend", "player", 0.8, 0.5)) or
    (not IsPlayerMoving() and skill.TryHealUnit("Shadow Mend", "player", 0.8, 0.4) or nil) or
    skill.TryUseEquippedTrinketOnUnit("Soulletting Ruby", "target") or
    (not IsPlayerMoving() and skill.TryHealUnit("Shadow Mend", "player", 0.6, 0.4) or nil) or
    (not IsPlayerMoving() and skill.NumEnemiesWithin(8) > 2 and TryCastSpellOnTarget("Mind Sear") or nil) or
    private.handle_bile_spitters_and_eggs() or

    (TargetAuraDuration("Shadow Word: Pain") < 3 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or
    TryCastSpellOnTarget("Shadowfiend") or

    (skill.NumEnemiesWithin(10) > 1 and TryCastSpellOnTarget("Divine Star") or nil) or
    TryCastSpellOnPlayer("Power Infusion") or
    (GetCD("Mind Blast") < 9 and (UnitName("target") == "Jormog the Behemoth" or UnitName("target") == "Tugar Bloodtotem") and TryCastSpellOnTarget("Schism") or nil) or
    TryCastSpellOnTarget("Penance") or
    (GetCD("Psychic Scream") <= 0 and TryCastSpellOnTarget("Mind Blast") or nil) or
    (GetCD("Schism") > 9 and TryCastSpellOnTarget("Shadow Word: Death") or nil) or
    TryCastSpellOnTarget("Power Word: Solace") or
    TryCastSpellOnTarget("Smite") or
    nil
end

function private.update_ttl(self)
  self.string:SetText(tostring(UnitTimeToLive("target")))
end
private.ttl = { ["label"] = "ttl:", ["text"] = "", ["update"] = private.update_ttl, ["click"] = nil, ["init"] = nil }

function private.on_event(self, event, ...)
  if event == "GROUP_ROSTER_UPDATE" then
    if GetNumGroupMembers() > 1 then
      private.saved_priority = private.saved_priority or GetCurrentPriority()
      private.angelic_feathers_to_keep = 3
      --SetPriority(priorities.default)
    else
      SetPriority(private.solo)
      --SetPriority(private.saved_priority)
      private.saved_priority = nil
      private.angelic_feathers_to_keep = 1
    end
  end
end
function module.run()
  if GetCurrentPriority() == priorities.solo then
    private.angelic_feathers_to_keep = 1
  elseif GetCurrentPriority() == priorities.default then
    private.angelic_feathers_to_keep = 3
  end
  return private.priority_selection.run()
end
function module.load()
  private.dot = IsTalentSelected("Purge the Wicked") and "Purge the Wicked" or "Shadow Word: Pain"
  private.priority_selection = PrioritySelectionFor(priorities)
  AddGuiElement(private.priority_selection.gui)
  AddGuiElement(private.ttl)
  AddGuiElement(priest.catharstick)
  AddGuiElement(priest.scales_of_trauma)

  private.frame:RegisterEvent("GROUP_ROSTER_UPDATE")
--  private.frame:RegisterEvent("PLAYER_TALENT_UPDATE")
  private.frame:SetScript("OnEvent", private.on_event)
end
function module.unload()
  private.frame:UnregisterEvent("PLAYER_TALENT_UPDATE")
--  private.frame:UnregisterEvent("GROUP_ROSTER_UPDATE")
  private.frame:SetScript("OnEvent", nil)
end
skill.modules.priest.discipline = module


function priest.DamageOfShadowWordDeath()
  if not UnitExists("target") or not skill.IsUnitAlive("target") or skill.IsUnitPlayerFriend("target") then
    return 0
  end
  local schism = TargetMyAuraDuration("Schism") > GetCD("Shadow Word: Death") and 0.4 or 0
  local below_20 = TargetHealthPercent() < 0.2 and 1.5 or 0
  local multiplier = 1 + (schism + below_20)
  return DamageOfSpell("Shadow Word: Death") * multiplier * skill.UnitDamageTakenMultiplier("target")
end
function priest.TryLethalShadowWordDeath()
  return
  priest.DamageOfShadowWordDeath() > TargetHealth() * 1.1 and
  TryCastSpellOnTarget("Shadow Word: Death") or
  nil
end
function priest.TrySadisticShadowWordDeath(min_remaining_player_health_percent)
  min_remaining_player_health_percent = min_remaining_player_health_percent or 0.6
  local remaining_player_health_percent = (PlayerHealth() - priest.DamageOfShadowWordDeath()) / PlayerHealthMax()
  return
  remaining_player_health_percent > min_remaining_player_health_percent and
  TryCastSpellOnTarget("Shadow Word: Death") or
  nil
end
function priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell(spell)
  return
  priest.DamageOfShadowWordDeath() > DamageOfSpell(spell) and
  priest.TrySadisticShadowWordDeath() or
  nil
end
function priest.update_catharstick(self)
  local amount = UnitAuraMatch("player", "Catharstick", "(%d+) damage will be added.") or 0
  self.string:SetText(string.format("%3.f%% %d", amount / PlayerHealthMax() * 100, amount))
end
priest.catharstick = { ["label"] = "catharstick:", ["text"] = "", ["update"] = priest.update_catharstick, ["click"] = nil, ["init"] = nil }
function priest.update_scales_of_trauma(self)
  local amount = UnitAuraMatch("player", "Scales of Trauma", "Your next Mind Blast deals an additional (%d+) damage.") or UnitAuraMatch("player", "Scales of Trauma", "Your next Holy Fire deals an additional (%d+) damage.") or 0
  self.string:SetText(string.format("%3.f%% %d", amount / 2 / PlayerHealthMax() * 100, amount))
end
priest.scales_of_trauma = { ["label"] = "scales:", ["text"] = "", ["update"] = priest.update_scales_of_trauma, ["click"] = nil, ["init"] = nil }
