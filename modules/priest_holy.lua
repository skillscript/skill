_, skill = ...
priest = priest or {}
skill_char_settings = skill_char_settings or {}
local private = {}

private.frame = CreateFrame("Frame", nil, UIParent)






local flashConcentration = {
  frame = nil,
  trailTarget1 = nil,
  trailTarget2 = nil,
  conservativeRefreshThreshold=4,
  instantCastThreshold=3,
  -- burst heal
  minEmergencyDeficit = 0.70,
  -- simple triage
  maxSafeDeficit=0.50,
  -- long term throughput
  maxLowPriorityDeficit=0.15,
  -- mana efficiency and preparation
}

--function private.init()
--  private.load()
--end
--private.frame = CreateFrame("Frame", nil, UIParent)
--private.frame:SetScript("OnEvent", private.init)
--private.frame:RegisterEvent("ADDON_LOADED")





function private.range(spell)
  if spell == "Holy Nova" then
    return 8
  elseif spell == "Holy Word: Sanctify" then
    return 10
  elseif spell == "Circle of Healing" then
    return 30
  elseif spell == "Prayer of Mending" then
    return 40
  elseif spell == "Holy Fire" then
    return 40
  elseif spell == "Ascended Blast" then
    return 40
  end
end
function private.is_off_global(name)
  return
    name == "Fade" or
    name == "Desperate Prayer" or
    name == "Leap of Faith" or
    name == "Pain Suppression"
end
function private.is_stun(name)
  return false
end
function private.is_disorient(name)
  -- TODO: Add IsFear()
  return name == "Psychic Scream"
end
function private.is_incapacitate(name)
  return false
end
function private.is_silence(name)
  return false
end
function private.is_interrupt(name)
  return false
end

local function TryShield(unit)
  return
    (UnitAuraDuration(unit, "Weakened Soul") <= GlobalCD() or PlayerAuraDuration("Rapture") > GlobalCD()) and
    TryCastSpellOnUnit("Power Word: Shield", unit) or
    nil
end

local function TryDotIfWorth(guid)
  local unit = GuidToUnit(guid)
  if not unit then
    return --TryScanForGuid(guid) or nil
  end
  local duration = DotDurationOfSpell(private.dot)
  if UnitMyAuraDuration(unit, private.dot) > duration * 0.3 then
    return
  end
  local instant_damage = DamageOfSpell(private.dot)
  local duration_damage = DotDamageOfSpell(private.dot)
  local smite_damage = DamageOfSpell("Smite")
  local min_extra_dmg = math.floor(duration / HastedSeconds(1.5)) * smite_damage

  local projected_damage = (instant_damage + duration_damage + min_extra_dmg) * 0.8
  local maximum_damage = math.min(projected_damage, UnitHealth(unit))

  --ALERT("unit: "..tostring(unit))
  --ALERT("instant_damage: "..tostring(instant_damage))
  --ALERT("duration_damage: "..tostring(duration_damage))
  --ALERT("smite_damage: "..tostring(smite_damage))
  --ALERT("min_extra_dmg: "..tostring(min_extra_dmg))
  --ALERT("projected_damage: "..tostring(projected_damage))
  --ALERT("maximum_damage: "..tostring(maximum_damage))
  --ALERT("TargetHealth(): "..tostring(TargetHealth()))
  if maximum_damage > UnitHealth(unit) then
    return
  end
  return TryCastSpellOnUnit(private.dot, unit)
end
local function in_dot_range_and_in_combat(unit)
  return IsUnitInRange(private.dot, unit) and (UnitAffectingCombat(unit) or skill.ShouldAlwaysAttackUnit(unit))
end
local function MaybeDotAllEnemies()
  local enemies = skill.GetEnemies(in_dot_range_and_in_combat)
  --ALERT("#enemies: "..tostring(#enemies))
  for guid, _ in pairs(enemies) do
    --ALERT("guid: "..tostring(guid))
    --ALERT("GuidToUnit(guid): "..tostring(GuidToUnit(guid)))
    local result = TryDotIfWorth(guid) or nil
    if result then
      return result
    end
  end
end

local function TrySchism()
  return
    not IsPlayerMoving() and
    IsTalentSelected("Schism") and
    TryCastSpellOnTarget("Schism") or
    nil
end

local function TrySmite()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Smite") or
    nil
end
local function TryHolyFire()
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget("Holy Fire") or
    nil
end



function private.KeepPowerWordFortitudeUp()
  if UnitLevel("player") < 22 then
    return nil
  end
  for _, unit in ipairs(SKILL_PARTY) do
    if UnitExists(unit) and UnitIsFriend("player", unit) and UnitAuraDuration(unit, "Power Word: Fortitude") < 20 * 60 and IsUnitInRange("Power Word: Fortitude", unit) and not UnitIsDeadOrGhost(unit) then
      return TryCastSpellOnUnit("Power Word: Fortitude", unit)
    end
  end
end

function private.stay_alive()
  local player_health_percent = PlayerHealthPercent()
  return
    (player_health_percent < 0.50 and TryShield("player") or nil) or
    (player_health_percent < 0.45 and PlayerMyAuraDuration("Renew") <= 0 and TryCastSpellOnPlayer("Renew") or nil) or
    (player_health_percent < 0.40 and PlayerAuraDuration("Surge of Light") > GlobalCD() and private.TryFlashHeal("player") or nil) or
    (player_health_percent < 0.35 and TryCastSpellOnPlayer("Prayer of Mending") or nil) or
    (player_health_percent < 0.25 and TryCastSpellOnPlayer("Desperate Prayer") or nil) or
    (player_health_percent < 0.25 and GetCD("Desperate Prayer") > GlobalCD() and TryCastSpellOnPlayer("Holy Word: Serenity") or nil) or
    (player_health_percent < 0.20 and TryCastSpellOnPlayer("Circle of Healing") or nil) or
    (player_health_percent < 0.15 and TryCastSpellOnPlayer("Guardian Spirit") or nil) or
    (player_health_percent < 0.15 and TryCastSpellOnPlayer("Holy Word: Sanctify") or nil) or
    nil
end
function private.save_group()
  return
    nil
end

function private.TryFlashConcentratedHeal(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    PlayerAuraStacks("Flash Concentration") >= 5 or
    (not IsPlayerMoving() or PlayerAuraDuration("Surge of Light") >= GlobalCD()) and
    skill.TryHealUnit("Flash Heal", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function private.TryFlashHeal(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Surge of Light") >= GlobalCD()) and
    skill.TryHealUnit("Flash Heal", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function private.TryHeal(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    not IsPlayerMoving() and
    skill.TryHealUnit("Heal", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function private.TryRenew(unit, ifBelowHealthPercent, maxOverhealPercent)
  return
    skill.TryHealUnit("Renew", unit, ifBelowHealthPercent, maxOverhealPercent) or
    nil
end
function private.highest_deficit_comparator(a, b)
  return UnitHealthAndHealAbsorbDeficit(a) > UnitHealthAndHealAbsorbDeficit(b)
end

function private.KeepPrayerOfMendingOnCd()
  local tank = skill.TryGetFirstTankUnit()
  if tank and UnitMyAuraDuration(tank, "Prayer of Mending") <= 10 then
    local result =
      TryCastSpellOnUnit("Prayer of Mending", tank) or
      nil
    if result then
      return result
    end
  end
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      unit ~= "target" and
      unit ~= "mouseover" and
      UnitMyAuraDuration(unit, "Prayer of Mending") <= 0 and
      TryCastSpellOnUnit("Prayer of Mending", unit) or
      nil
    if result then
      return result
    end
  end
  return nil
end
function private.TryHealHighestDeficitUnit(spell, ifBelowHealthPercent, maxOverhealPercent)
  if skill.IsUsable(spell) then
    for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
      local result =
      skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent) or
      nil
      if result then
        return result
      end
    end
  end
  return nil
end
function private.TryHealHighestDeficitPercentUnit(func, ifBelowHealthPercent, maxOverhealPercent)
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      func(unit, ifBelowHealthPercent, maxOverhealPercent) or
      nil
    if result then
      return result
    end
  end
  return nil
end
function private.TryHealWithPrayerOfHealing()
  if IsPlayerMoving() then
    return nil
  end
  local heal = HealOfSpell("Prayer of Healing")
  local healed_sum = 0
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    healed_sum = healed_sum + (IsUnitInRange("Prayer of Healing", unit) and min(UnitHealthAndHealAbsorbDeficit(unit), heal) or 0)
  end
  return
    healed_sum > heal * 4 and
    TryCastSpellOnPlayer("Prayer of Healing") or
    nil
end
function private.TryHealWithCircleOfHealing()
  local heal = HealOfSpell("Circle of Healing")
  local healed_sum = 0
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    healed_sum = healed_sum + (IsUnitInRange("Circle of Healing", unit) and min(UnitHealthAndHealAbsorbDeficit(unit), heal) or 0)
  end
  return
    healed_sum > heal * 4 and
    TryCastSpellOnPlayer("Circle of Healing") or
    nil
end
function private.TryHealWithPowerWordSanctify()
  local heal = HealOfSpell("Holy Word: Sanctify")
  local healed_sum = 0
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    healed_sum = healed_sum + (IsUnitInRange("Holy Word: Sanctify", unit) and min(UnitHealthAndHealAbsorbDeficit(unit), heal) or 0)
  end
  return
    healed_sum > heal * 3 and
    TryCastSpellOnPlayer("Holy Word: Sanctify") or
    nil
end
function private.KeepRenewOnDamagedTargets()
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    local result =
      UnitHealthAndHealAbsorbDeficit(unit) > 100 and
      UnitMyAuraDuration(unit, "Renew") < 5 and
      private.TryRenew(unit, 0.9, 1.0) or
      nil
    if result then
      return result
    end
  end
  return nil
end
function private.HandleHeal()
  local units = HighestDeficitPercentGroupUnits()
  local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
  return
    private.TryHealHighestDeficitUnit(private.TryPowerWordSerenity, 0.5, 0.0) or
    private.KeepPrayerOfMendingOnCd() or
    (prioritize_aoe and private.TryHealWithPrayerOfHealing() or nil) or
    (prioritize_aoe and private.TryHealWithCircleOfHealing() or nil) or
    (prioritize_aoe and private.TryHealWithPowerWordSanctify() or nil) or
    (PlayerAuraDuration("Surge of Light") > GlobalCD() and private.TryHealHighestDeficitUnit(private.TryFlashHeal, 0.7, 0.0) or nil) or
    private.TryHealHighestDeficitPercentUnit(private.TryFlashConcentratedHeal, 0.5, 0.0) or
    private.TryHealHighestDeficitPercentUnit(private.TryFlashHeal, 0.5, 0.0) or
    private.KeepRenewOnDamagedTargets() or
    (PlayerManaPercent() > 0.8 and private.TryHealHighestDeficitPercentUnit(private.TryFlashHeal, 0.9, 0.0) or nil) or
    private.TryHealHighestDeficitPercentUnit(private.TryHeal, 0.9, 0.0) or
    private.TryHealHighestDeficitPercentUnit(private.TryHeal, 0.7, 0.0) or
    private.TryHealHighestDeficitUnit(private.TryHeal, 0.7, 0.0) or
    private.TryHealWithPrayerOfHealing() or
    private.TryHealWithCircleOfHealing() or
    --private.TryHealWithPowerWordSanctify() or
    --private.TryHealHighestDeficitPercentUnit(private.TryHeal, 0.7, 0.2) or
    --private.TryHealHighestDeficitUnit(private.TryHeal, 0.7, 0.2) or
    --private.TryHealHighestDeficitUnit(private.TryHeal, 0.7, 1.0) or
    nil
end
function private.HandleRaidHeal()
  local units = HighestDeficitPercentGroupUnits()
  local prioritize_aoe = #units > 1 and abs(UnitHealthAndHealAbsorbDeficitPercent(units[1]) - UnitHealthAndHealAbsorbDeficitPercent(units[2])) < 0.4
  return
    private.TryHealHighestDeficitUnit("Power Word: Serenity", 0.5, 0.0) or
    private.KeepPrayerOfMendingOnCd() or
    private.KeepRenewOnDamagedTargets() or
    private.TryHealHighestDeficitPercentUnit(private.TryHeal, 0.7, 0.0) or
    nil
end

function private.IsFlashConcentrationEquipped()
  return IsEquippedItem()
end

local priorities = {}
function priorities.default()
  return
    skill.DoNothingIfMounted() or
    private.stay_alive() or
    private.save_group() or
    (not UnitAffectingCombat("player") and private.KeepPowerWordFortitudeUp() or nil) or
    (not UnitAffectingCombat("player") and IsInInstance() and private.KeepPrayerOfMendingOnCd() or nil) or
    skill.common_priority_start() or

    kyrian.HandleBoonOfTheAscended() or
    skill.DoNothingIfCasting(0) or

    TryPurgeAnythingFromUnit("target") or
    TryDispelAnythingFromParty() or

    flashConcentration.tryRefresh(0) or
    private.HandleHeal() or


    (PlayerManaPercent() < 0.7 and "nothing" or nil) or
    priorities.solo() or
    --skill.AcquireTarget() or
    --TryHolyFire() or
    ----TryCastSpellOnTarget("Shadow Word: Pain") or
    --TryCastSpellOnTarget("Smite") or

    "nothing"
end
function priorities.raid()
  return
    private.HandleRaidHeal() or
    "nothing"
end
function priorities.new()
  return
    skill.DoNothingIfMounted() or
    private.stay_alive() or
    private.save_group() or
    (not UnitAffectingCombat("player") and private.KeepPowerWordFortitudeUp() or nil) or
    (not UnitAffectingCombat("player") and IsInInstance() and private.KeepPrayerOfMendingOnCd() or nil) or
    skill.common_priority_start() or

    kyrian.HandleBoonOfTheAscended() or
    skill.DoNothingIfCasting(0) or

    TryPurgeAnythingFromUnit("target") or
    TryDispelAnythingFromParty() or

    private.HandleHeal() or


    (PlayerManaPercent() < 0.7 and "nothing" or nil) or
    priorities.solo() or
    --skill.AcquireTarget() or
    --TryHolyFire() or
    ----TryCastSpellOnTarget("Shadow Word: Pain") or
    --TryCastSpellOnTarget("Smite") or

    "nothing"
end

private.use_power_infusion = false
private.use_chastise_for_damage = true
function priorities.solo()
  return
  torghast.torghast_stuff() or
  private.stay_alive() or
  private.save_group() or
  (not skill.IsPlayerInCombat() and PlayerAuraDuration("Renew") < 5 and skill.TryHealUnit("Renew", "player", 0.95, 1.0) or nil) or
  (not skill.IsPlayerInCombat() and PlayerAuraDuration("Surge of Light") > GlobalCD() and skill.TryHealUnit("Flash Heal", "player", 0.9, 1.0) or nil) or
  skill.common_priority_start() or
  --skill.AcquireTarget() or
  skill.DoNothingIfCasting(0) or

  kyrian.HandleBoonOfTheAscended() or

  TryPurgeAnythingFromUnit("target") or
  TryDispelAnythingFromParty() or

  (PlayerMyAuraDuration("Renew") < 5 and skill.TryHealUnit("Renew", "player", 0.6, 1.0) or nil) or
  (not IsPlayerMoving() and IsTalentSelected("Divine Star") and skill.NumEnemiesWithin(10) >= 2 and TryCastSpellOnPlayer("Divine Star") or nil) or
  (skill.NumEnemiesWithin(10) >= 3 and TryCastSpellOnPlayer("Holy Nova") or nil) or
  (private.use_power_infusion and not IsPlayerMoving() and skill.TargetTimeToLive() > 20 and TryCastSpellOnPlayer("Power Infusion") or nil) or
  priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Holy Fire") or
  TryHolyFire() or
  priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Holy Word: Chastise") or
  (private.use_chastise_for_damage and TryCastSpellOnTarget("Holy Word: Chastise") or nil) or
  --(private.use_chastise_for_damage and skill.TargetTimeToLive() > 20 and TryCastSpellOnPlayer("Apotheosis") or nil) or
  (skill.TargetTimeToLive(5) > 16 and TargetMyAuraDuration("Shadow Word: Pain") < 5 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or
  ((skill.IsUnitInCombatWithPlayer("mouseover") or skill.ShouldAlwaysAttackUnit("mouseover")) and skill.UnitTimeToLive("mouseover", 5) > 16 and UnitMyAuraDuration("mouseover", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "mouseover") or nil) or
  priest.TryLethalShadowWordDeath() or
  priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Smite") or
  TrySmite() or
  priest.TrySadisticShadowWordDeath() or
  (PlayerAuraDuration("Surge of Light") > GlobalCD() and skill.TryHealUnit("Flash Heal", "player", 0.9, 0.1) or nil) or
  (PlayerHealthPercent() < 0.9 and PlayerAuraDuration("Weakened Soul") <= 0 and TryCastSpellOnPlayer("Power Word: Shield") or nil) or
  TryCastSpellOnPlayer("Prayer of Mending") or
  (PlayerAuraDuration("Renew") < 5 and skill.TryHealUnit("Renew", "player", 1.0, 1.0) or nil) or
  TryCastSpellOnTarget("Holy Nova") or

  "nothing"
end
function priorities.daily()
  return
  skill.HandleJustWingingIt() or
  skill.Aspirant() or

  "nothing"
end

local range_check = LibStub("LibRangeCheck-2.0")
local unitsWithin = {}
local function UnitsWithin(units, maxDistance)
  wipe(unitsWithin)
  for _, unit in ipairs(units) do
    local _, distance = range_check:GetRange(unit)
    distance = distance or 1337
    table.insert(unitsWithin, unit)
  end
  return unitsWithin
end


local function NumUnitsWithin(units, maxDistance)
  local result = 0
  local localUnitsWithin = UnitsWithin(units, maxDistance)
  for _, units in ipairs(localUnitsWithin) do
    result = result + 1
  end
  return result
end
local function sum(units, func)
  local accumulator = 0
  for _, unit in ipairs(units) do
    accumulator = accumulator + func(unit)
  end
  return accumulator
end

function flashConcentration.hasTrail(unit)
  return UnitGUID(unit) == flashConcentration.trailTarget1 or UnitGUID(unit) == flashConcentration.trailTarget2
end

function flashConcentration.hasTrail1(unit)
  return UnitGUID(unit) == flashConcentration.trailTarget1
end

function flashConcentration.hasTrail2(unit)
  return UnitGUID(unit) == flashConcentration.trailTarget2
end


function flashConcentration.healValue()
  return 5000 * (PlayerMyAuraDuration("Resonant Words") > 0 and 1.5 or 1)
end

function flashConcentration.on_event(self, event, ...)
  if event == "COMBAT_LOG_EVENT_UNFILTERED" then
    local _, subevent, _,  sourceGUID,  _, _, _, destGUID, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()
    --print(CombatLogGetCurrentEventInfo())
    if sourceGUID == UnitGUID("player") and (subevent == "SPELL_CAST_SUCCESS") and (spell == "Heal" or spell == "Flash Heal") then
      if destGUID ~= flashConcentration.trailTarget1 and destGUID ~= flashConcentration.trailTarget2 then
        flashConcentration.trailTarget2 = flashConcentration.trailTarget1
        flashConcentration.trailTarget1 = destGUID
      end

    end
  end
end

function flashConcentration.KeepRenewOnDamagedTargets()
  for _, unit in ipairs(CastPrediction.HighestDeficitPercentGroupUnits()) do
    local result =
    UnitHealthAndHealAbsorbDeficit(unit) > 100 and
        UnitMyAuraDuration(unit, "Renew") < 5 and
        private.TryRenew(unit, 0.95, 1.0) or
        nil
    if result then
      return result
    end
  end
  return nil
end

function flashConcentration.tryRefreshWhileMoving(min_stacks)
  min_stacks = min_stacks or 1
  local flashDuration = PlayerMyAuraDuration("Flash Concentration")
  local flashStacks = PlayerMyAuraStacks("Flash Concentration")
  local surgeDuration = PlayerMyAuraDuration("Surge of Light")
  if IsPlayerMoving() and surgeDuration > 0 and flashDuration >= 0.2 and flashStacks >= min_stacks and flashDuration < flashConcentration.conservativeRefreshThreshold then
    return TryCastSpellOnUnit("Flash Heal", CastPrediction.HighestDeficitPercentGroupUnits()[1])
  end
  return nil
end

function flashConcentration.tryRefresh(min_stacks)
  min_stacks = min_stacks or 1
  local flashDuration = PlayerMyAuraDuration("Flash Concentration")
  local flashStacks = PlayerMyAuraStacks("Flash Concentration")
  local surgeDuration = PlayerMyAuraDuration("Surge of Light")
  if flashDuration >= 0.2 and flashStacks >= min_stacks and (flashDuration < flashConcentration.conservativeRefreshThreshold and surgeDuration <= 0 or flashDuration < flashConcentration.instantCastThreshold) and
      CastPrediction.next_spell() ~= "Flash Heal" then
    return TryCastSpellOnUnit("Flash Heal", CastPrediction.HighestDeficitPercentGroupUnits()[1])
  end
  return nil
end

function flashConcentration.healWhileMoving()
  if IsPlayerMoving() then
    local units = CastPrediction.HighestDeficitPercentGroupUnits()
    if CastPrediction.UnitHealthDeficit(units[1]) < 500 then return end
    local unitsWithin30 = UnitsWithin(units, 27.5)
    local cohSum = sum(
        unitsWithin30,
        function(unit) return CastPrediction.UnitHealthDeficit(unit) > 0.4 * flashConcentration.maxLowPriorityDeficit and 1 or 0 end)
    return
      (cohSum >= 3 and TryCastSpellOnPlayer("Circle of Healing") or nil) or
      private.KeepPrayerOfMendingOnCd() or
      private.KeepRenewOnDamagedTargets()
  end
  return nil
end
function flashConcentration.bufferHeal()
  local units = CastPrediction.HighestDeficitPercentGroupUnits()
  if PlayerAuraStacks("Flash Concentration") <= 0 or CastPrediction.UnitHealthDeficitPercent(units[1]) < flashConcentration.maxLowPriorityDeficit then
    return private.KeepPrayerOfMendingOnCd()
  end
  return nil
end

function flashConcentration.getMinimumStacks()
  if not IsPlayerMoving() and PlayerAuraStacks("Flash Concentration") <= 2 then
    return TryCastSpellOnUnit("Flash Heal", CastPrediction.HighestDeficitPercentGroupUnits()[1])
  end
end

function flashConcentration.throughput()
  local units = CastPrediction.HighestDeficitPercentGroupUnits()
  if CastPrediction.UnitHealthDeficitPercent(units[1]) < flashConcentration.maxLowPriorityDeficit then
    return nil
  end
  local target = units[1]
  local spell = "Heal"
  if
    CastPrediction.UnitHealthDeficitPercent(units[1]) < flashConcentration.maxSafeDeficit and
    flashConcentration.hasTrail1(units[1]) and
    units[2] and not flashConcentration.hasTrail(units[2])
    and CastPrediction.UnitHealthDeficitPercent(units[2]) > flashConcentration.maxLowPriorityDeficit
  then
    target = units[2]
  end
  if not IsPlayerMoving() then
    return TryCastSpellOnUnit(spell, target)
  end
  return nil
end

function flashConcentration.efficientRegen()
  local units = CastPrediction.HighestDeficitPercentGroupUnits()
  if CastPrediction.UnitHealthDeficit(units[1]) < 500 then return end
  local unitsWithin30 = UnitsWithin(units, 27.5)
  local cohSum = sum(
  unitsWithin30,
  function(unit) return CastPrediction.UnitHealthDeficit(unit) > 0.4 * flashConcentration.maxLowPriorityDeficit and 1 or 0 end)
  --print("regen")

  return
    (cohSum >= 3 and TryCastSpellOnPlayer("Circle of Healing") or nil) or
    ( not IsPlayerMoving()
      and CastPrediction.next_spell() ~= "Flash Heal"
      and (PlayerMyAuraDuration("Flash Concentration") < 10 and PlayerMyAuraStacks("Surge of Light") <= 0 or PlayerMyAuraStacks("Surge of Light") >= 2)
      and (not IsPlayerMoving() or PlayerMyAuraDuration("Surge of Light") > 0) and TryCastSpellOnUnit("Flash Heal", units[1]) or nil) or
   nil
end

function flashConcentration.serenity()
  local units = CastPrediction.HighestDeficitPercentGroupUnits()
  if CastPrediction.UnitHealthDeficitPercent(units[1]) > flashConcentration.maxSafeDeficit or
    sum(units, CastPrediction.UnitHealthDeficitPercent) > 2 * flashConcentration.maxSafeDeficit and CastPrediction.UnitHealthDeficit(units[1]) > 14000
  then
    return TryCastSpellOnUnit("Holy Word: Serenity", units[1])
  end
end

local debug_last = 0
local function debug()
  if not UnitGUID("target") then return end
  local total = UnitHealth("target") + UnitGetIncomingHeals("target", "player");
  if total ~= debug_last then
    print(""..total - debug_last.." ("..total..")")
    debug_last = total
  end
end

function priorities.FlashAssist()
  return
    skill.DoNothingIfMounted() or
    skill.DoNothingIfDrinking() or
    flashConcentration.tryRefresh() or
    --TryDispelAnythingFromParty() or
    flashConcentration.healWhileMoving() or
    flashConcentration.bufferHeal() or
    private.KeepPowerWordFortitudeUp() or
    "nothing"
end
function flashConcentration.beneficialDamage()
  return skill.IsPlayerInCombat() and skill.IsUnitInCombatWithPlayer("target") and (
    UnitHealth("target") / UnitHealthMax("target") > 0.4 and TryCastSpellOnTarget("Mindgames", "target") or
    TrySmite() or
    nil
  )
end

function flashConcentration.damage()
  return skill.IsPlayerInCombat() and skill.IsUnitInCombatWithPlayer("target") and (

      (skill.NumEnemiesWithin(10) >= 3 and TryCastSpellOnPlayer("Holy Nova") or nil) or
      TryHolyFire() or
      (skill.TargetTimeToLive(5) > 12 and TargetMyAuraDuration("Shadow Word: Pain") < 4 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or

      nil
  )
end

function priorities.FlashAuto()
  --print(CastPrediction.UnitHealth("player"))
  --ALERT("spell: "..(CastPrediction.next_spell() or ""))
  --ALERT("effect: "..(CastPrediction.pending_effect() or ""))
  return
    skill.DoNothingIfMounted() or
    skill.DoNothingIfDrinking() or
    skill.DoNothingIfCasting(0.1) or
    TryCastQueuedSpell() or
    flashConcentration.tryRefresh(0) or
    TryDispelAnythingFromParty() or
    flashConcentration.serenity() or
    flashConcentration.getMinimumStacks() or
    flashConcentration.throughput() or
    private.HandleBoonOfTheAscended() or
    flashConcentration.healWhileMoving() or
    flashConcentration.bufferHeal() or
    flashConcentration.efficientRegen() or
    flashConcentration.damage() or
    flashConcentration.beneficialDamage() or
    private.KeepPowerWordFortitudeUp() or
    "nothing"
end

function priorities.debug()
  return
    "nothing"
end

function private.HandleAngelicFeather()
  return
    not UnitAffectingCombat("player") and
    IsTalentSelected("Angelic Feather") and
    IsPlayerMoving() and
    PlayerAuraDuration("Lifted Spirit") <= 0 and
    PlayerAuraDuration("Unburden") <= 0 and
    PlayerAuraDuration("Angelic Feather") <= 0.2 and
    GetCharges("Angelic Feather") > (private.angelic_feathers_to_keep or 3) and
    TryCastSpellOnPlayer("Angelic Feather") or
    nil
end
local function HandleMawrats()
  if not torghast.IsTargetMawrat() then
    return nil
  end
  return
  (PlayerAuraDuration("Torment: Fracturing Forces") > 0 and "nothing" or nil) or
  TrySmite() or
  (UnitAuraDuration("player", "Catharstick") <= 0 and TryCastSpellOnTarget(private.dot) or nil) or
  nil
end
local function HandleVolatilePhantasm()
  if UnitAuraDuration("player", "Volatile Phantasm", "MAW") <= 0 then
    return nil
  end
  return
  not IsPlayerMoving() and
  skill.NumEnemiesWithin(8) > 0 and
  TryCastSpellOnPlayer("Fade") or
  nil
end
function private.TryQuickHeal(unit, ifBelowHealthPercent, maxOverhealPercent)
  local spell = PlayerAuraStacks("Flash Concentration") >= 5 and "Heal" or "Flash Heal"
  return skill.TryHealUnit(spell, unit, ifBelowHealthPercent, maxOverhealPercent)
end
local function TryInterrupt()
  local spell = TargetCast()
  local should_interrupt =
    spell == "Eternal Torment" or
    spell == "Inner Flames"
  if not should_interrupt then
    return nil
  end
  return
    (IsTalentSelected("Shining Force") and IsUnitInRange("Attack", "tafget") and TryCastSpellOnPlayer("Holy Word: Chastise") or nil) or
    TryCastSpellOnTarget("Holy Word: Chastise") or
    TryCastSpellOnTarget("Psychic Scream") or
    nil
end

function private.HandleBoonOfTheAscended()
  if PlayerAuraDuration("Boon of the Ascended") <= GlobalCD() then
    return nil
  end
  return
  (skill.NumEnemiesWithin(8) >= 5 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
  TryCastSpellOnTarget("Ascended Blast") or
  (skill.NumEnemiesWithin(8) > 0 and TryCastSpellOnPlayer("Ascended Nova") or nil) or
  nil
end
local function UnitTimeToPercent(unit, percent, window)
  if not UnitExists(unit) then
    return 60 * 60
  end
  local real_dps = GetTime() - UnitOldestHealthChangedTime(unit, window) > 5 and -UnitHealthChangedPerSecond(unit, window) or 0
  --local dps = max(real_dps, single_target_dps())
  local dps = real_dps
  local health_remaining = max(0, UnitHealth(unit) - (UnitHealthMax(unit) * percent))
  return (dps > 0) and (health_remaining / dps) or (60 * 60)
end
local function UnitTimeToLive(unit)
  return UnitTimeToPercent(unit, 0, 10)
end
local mage_tower = {}
function mage_tower.on_event(self, event, arg)
  --ALERT("arg: "..tostring(arg))
end
function mage_tower.heal_damaged_npcs()
  local target_name = UnitName("target")
  if target_name ~= "Damaged Soul" and target_name ~= "Damaged Mage" and target_name ~= "Damaged Soldier" and target_name ~= "Damaged Arbalest"then
    return nil
  end
  return
    TryCastSpellOnUnit("Shadow Word: Serenity", "mouseover") or
    nil
end
function mage_tower.handle_flickering_eyes()
  return
    skill.TryHealUnit("Flash Heal", "player", 0.9, 1.0) or
    TryHolyFire() or
    TryCastSpellOnTarget("Holy Word: Chastise") or
    ((skill.IsUnitInCombatWithPlayer("target") or skill.ShouldAlwaysAttackUnit("target")) and skill.UnitTimeToLive("target", 5) > 16 and UnitMyAuraDuration("target", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "target") or nil) or
    ((skill.IsUnitInCombatWithPlayer("mouseover") or skill.ShouldAlwaysAttackUnit("mouseover")) and skill.UnitTimeToLive("mouseover", 5) > 16 and UnitMyAuraDuration("mouseover", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "mouseover") or nil) or
    PlayerMyAuraDuration("Renew") <= 0 and TryCastSpellOnPlayer("Renew") or
    TrySmite() or
    "nothing"
end
function priorities.mage_tower()
  local target_name = UnitName("target")
  if (target_name == "Yve" or target_name == "Flickering Eye" or target_name == "Lord Erdris Thorn") and not skill_char_settings["use_chastise_for_damage"] then
    private.toggle_use_chastise_for_damage()
  end
  return
    skill.DoNothingIfMounted() or
    skill.DoNothingIfDrinking() or
    (not UnitAffectingCombat("player") and private.KeepPowerWordFortitudeUp() or nil) or
    (not UnitAffectingCombat("player") and IsInInstance() and private.KeepPrayerOfMendingOnCd() or nil) or
    skill.common_priority_start() or

    skill.DoNothingIfChanneling() or
    TryPurgeAnythingFromUnit("target") or
    TryPurgeAnythingFromUnit("focus") or
    TryPurgeAnythingFromUnit("mouseover") or
    TryDispelAnythingFromUnit("mouseover") or
    TryDispelAnythingFromParty() or
    skill.DoNothingIfCasting() or

    (target_name == "Lord Erdris Thorn" and UnitHealthPercent() < 1 and mage_tower.heal_lord_erdris_thorn() or nil) or
    mage_tower.heal_damaged_npcs() or
    mage_tower.handle_flickering_eyes() or

    priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Holy Fire") or
    TryHolyFire() or
    priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Holy Word: Chastise") or
    (target_name == "Lord Erdris Thorn" and UnitHealthPercent() < 1 and TryCastSpellOnTarget("Holy Word: Chastise") or nil) or
    (target_name == "Lord Erdris Thorn" and UnitHealthPercent() < 1 and PlayerManaPercent() < 0.8 and TryCastSpellOnPlayer("Apotheosis") or nil) or
    ((skill.IsUnitInCombatWithPlayer("target") or skill.ShouldAlwaysAttackUnit("target")) and skill.UnitTimeToLive("target", 5) > 16 and UnitMyAuraDuration("target", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "target") or nil) or
    ((skill.IsUnitInCombatWithPlayer("mouseover") or skill.ShouldAlwaysAttackUnit("mouseover")) and skill.UnitTimeToLive("mouseover", 5) > 16 and UnitMyAuraDuration("mouseover", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "mouseover") or nil) or
    ((skill.IsUnitInCombatWithPlayer("focus") or skill.ShouldAlwaysAttackUnit("focus")) and skill.UnitTimeToLive("focus", 5) > 16 and UnitMyAuraDuration("focus", "Shadow Word: Pain") < 5 and TryCastSpellOnUnit("Shadow Word: Pain", "focus") or nil) or
    priest.TryLethalShadowWordDeath() or
    priest.TrySadisticShadowWordDeathIfStrongerThanDamageOfSpell("Smite") or
    TrySmite() or
    priest.TrySadisticShadowWordDeath() or
    TryCastSpellOnPlayer("Prayer of Mending") or

    "nothing"
  --local units = HighestDeficitPercentGroupUnits()
  ---- Generating Holy Power, no waste, no overheal
  --for _, unit in ipairs(units) do
  --  local result =
  --    skill.TryHealUnit("Holy Shock", unit, 0.9, 0.0) or
  --    nil
  --  if result then
  --    return result
  --  end
  --end
  --
  --return
  --
  --  (player_health_percent < 0.40 and PlayerAuraDuration("Surge of Light") > GlobalCD() and private.TryFlashHeal("player") or nil) or
  --  (player_health_percent < 0.35 and TryCastSpellOnPlayer("Prayer of Mending") or nil) or
  --  (player_health_percent < 0.25 and TryCastSpellOnPlayer("Desperate Prayer") or nil) or
  --  (player_health_percent < 0.25 and GetCD("Desperate Prayer") > GlobalCD() and TryCastSpellOnPlayer("Holy Word: Serenity") or nil) or
  --  (player_health_percent < 0.20 and TryCastSpellOnPlayer("Circle of Healing") or nil) or
  --  (player_health_percent < 0.15 and TryCastSpellOnPlayer("Guardian Spirit") or nil) or
  --  (player_health_percent < 0.15 and TryCastSpellOn("Holy Word: Sanctify") or nil) or
  --  "nothing"
end
function priorities.torghast()
  local target_name = UnitName("target")
  local catharstick_amount = tonumber(UnitAuraMatch("player", "Catharstick", "(%d+) damage will be added.") or 0)
  local catharstick_percent = catharstick_amount / PlayerHealthMax()
  local scales_of_trauma_amount = tonumber(UnitAuraMatch("player", "Scales of Trauma", "Your next Holy Fire deals an additional (%d+) damage.") or 0)
  local scales_of_trauma_percent = scales_of_trauma_amount / 2 / PlayerHealthMax()
  return
    flashConcentration.tryRefresh(0) or
    flashConcentration.tryRefreshWhileMoving(0) or
    torghast.torghast_stuff() or
    (not UnitAffectingCombat("player") and not IsMounted() and kyrian.TryRefreshPhials() or nil) or
    (not UnitAffectingCombat("player") and not (UnitAuraDuration("player", "Parliament Stone", "MAW") > 0 or UnitAuraDuration("player", "Strigidium", "MAW") > 0) and kyrian.TryDismissSteward() or nil) or
    (not UnitAffectingCombat("player") and not IsPlayerMoving() and skill.TryLoot() or nil) or
    TryCastQueuedSpell() or
    skill.DoNothingIfMounted() or
    --skill.TryPreventFallDamage() or
    skill.KeepRepurposedFelFocuserUp() or
    private.stay_alive() or
    (not UnitAffectingCombat("player") and private.TryQuickHeal("player", 0.7, 0)) or

    private.HandleAngelicFeather() or

    (not UnitAffectingCombat("player") and KeepPowerWordFortitudeUp() or nil) or

    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfDrinking() or
    skill.DoNothingIfTargetOutOfCombat() or
    skill.TryInterruptTarget() or
    skill.TryUseHealthstone(0.33) or
    skill.DoNothingIfCasting() or

    TryDispelAnythingFromUnit("player") or

    skill.AcquireTarget() or
    TryInterrupt() or

    (UnitName("target") ~= "Watchers of Death" and PlayerHealthPercent() < 0.6 and TryCastSpellOnPlayer("Prayer of Mending") or nil) or
    skill.TryPurgeAnythingFromTarget() or
    skill.DoNothingIfChanneling() or
    (UnitAuraDuration("player", "Death Harvester", "MAW") > 0 and not (UnitAuraDuration("player", "Rat-Corpse Bag", "MAW") > 0 and torghast.IsTargetMawrat()) and priest.TryLethalShadowWordDeath() or nil) or
    private.HandleBoonOfTheAscended() or
    HandleMawrats() or
    HandleVolatilePhantasm() or
    (UnitName("target") ~= "Watchers of Death" and UnitAuraDuration("player", "Scales of Trauma", "MAW") > 0 and scales_of_trauma_percent < 1 and private.TryQuickHeal("player", 1, 0) or nil) or
    (UnitName("target") ~= "Watchers of Death" and UnitAuraDuration("player", "Scales of Trauma", "MAW") > 0 and scales_of_trauma_percent < 1 and UnitName("targettarget") == UnitName("player") and private.TryQuickHeal("targettarget", 1, 0) or nil) or
    private.TryQuickHeal("player", 0.5, 0.2) or
    (not IsPlayerMoving() and IsTalentSelected("Divine Star") and skill.NumEnemiesWithin(10) >= 2 and TryCastSpellOnPlayer("Divine Star") or nil) or
    (not IsPlayerMoving() and skill.NumEnemiesWithin(8) > 4 and TryCastSpellOnTarget("Holy Nova") or nil) or
    --(not UnitExists("party1") and UnitTimeToLive("target", 15) > 30 and TryCastSpellOnPlayer("Power Infusion") or nil) or
    (scales_wof_trauma_amount > 0 and scales_of_trauma_percent >= 1 and scales_of_trauma_amount < TargetHealth() and TryHolyFire() or nil) or
    (scales_of_trauma_percent <= 0 and TryHolyFire() or nil) or
    (target_name ~= "Lord of Torment" and target_name ~= "Mawsworn Flametender" and target_name ~= "Empowered Mawsworn Flametender" and TryCastSpellOnTarget("Holy Word: Chastise")) or
    (UnitTimeToLive("target", 15) > 5 and TargetMyAuraDuration("Shadow Word: Pain") <= 0 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or
    (PlayerAuraDuration("Power Overwhelming") > GlobalCD() and PlayerAuraDuration("Power Infusion") > GlobalCD() and TryHolyFire() or nil) or
    private.TryQuickHeal("player", 0.7, 0) or
    TrySmite() or
    (catharstick_amount <= 0 and TargetMyAuraDuration("Shadow Word: Pain") <= 0 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or
    TryCastSpellOnTarget("Holy Nova") or
    (catharstick_amount <= 0 and TryCastSpellOnTarget("Shadow Word: Pain") or nil) or

  "nothing"

end

function private.on_event(self, event, ...)
  --if event == "GROUP_ROSTER_UPDATE" then
  --  if GetNumGroupMembers() > 1 then
  --    private.saved_priority = private.saved_priority or GetCurrentPriority()
  --    SetPriority(priorities.default)
  --    MAX_USED_ANGELIC_FEATHER_CHARGES = 0
  --  else
  --    SetPriority(private.solo)
  --    private.saved_priority = nil
  --    MAX_USED_ANGELIC_FEATHER_CHARGES = 3
  --  end
  --end
end

local module = {
  ["spell"] = {
    ["range"] = private.range,
    ["dispel"] = {
      ["Purify"] = {
        "Magic",
        "Disease",
      },
    },
    ["purge"] = {
      ["Dispel Magic"] = {
        "Magic",
      },
    },
  },
  ["interrupts"] = {},
  ["extra_spells"] = {
    "Ascended Nova",
    "Ascended Blast",
  },
  ["override_range"] = private.range,
  ["is_off_global"] = private.is_off_global,
  ["is_stun"] = private.is_stun,
  ["is_disorient"] = private.is_disorient,
  ["is_incapacitate"] = private.is_incapacitate,
}

function private.init_use_chastise_for_damage(gui)
  skill_char_settings["use_chastise_for_damage"] = skill_char_settings["use_chastise_for_damage"] or true
  gui.string:SetText(tostring(skill_char_settings["use_chastise_for_damage"]))
end
function private.toggle_use_chastise_for_damage(gui, button)
  skill_char_settings["use_chastise_for_damage"] = not skill_char_settings["use_chastise_for_damage"]
  gui.string:SetText(tostring(skill_char_settings["use_chastise_for_damage"]))
end
private.toggle_use_chastise_for_damage_gui = { ["label"] = "Use Chastise for damage:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_use_chastise_for_damage, ["init"] = private.init_use_chastise_for_damage }

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(priorities)
  AddGuiElement(private.priority_selection.gui)
  AddGuiElement(private.toggle_use_chastise_for_damage_gui)
  CastPrediction.load()
  flashConcentration.frame = CreateFrame("Frame", nil, UIParent)
  flashConcentration.frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  flashConcentration.frame:SetScript("OnEvent", flashConcentration.on_event)
  mage_tower.frame = CreateFrame("Frame", nil, UIParent)
  mage_tower.frame:RegisterEvent("PLAYER_TARGET_CHANGED")
  mage_tower.frame:SetScript("OnEvent", mage_tower.on_event)
end
function module.unload()
  flashConcentration.frame:SetScript("OnEvent", nil)
  flashConcentration.frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  flashConcentration.frame = nil
  CastPrediction.unload()

end
skill.modules.priest.holy = module
