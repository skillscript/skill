_, skill = ...
priest = priest or {}
local private = {}

private.frame = CreateFrame("Frame", nil, UIParent)
local module = {}
module.spell = {}
function module.spell.range(spell)
    if spell == "Holy Nova" then
        return 8
    elseif spell == "Ascended Nova" then
        return 8
    elseif spell == "Ascended Blast" then
        return 40
    end
end
module.spell.purge = {
    ["Dispel Magic"] = {
        "Magic",
    },
}
module.spell.dispel = {
    ["Purify"] = {
        "Magic",
        "Disease",
    },
}
module["extra_spells"] = {
    "Ascended Nova",
    "Ascended Blast",
}

function module.is_off_global(name)
    return
    name == "Fade" or
    name == "Desperate Prayer" or
    name == "Leap of Faith" or
    name == "Pain Suppression"
end
function module.is_stun(name)
    return false
end
function module.is_disorient(name)
    -- TODO: Add IsFear()
    return name == "Psychic Scream"
end
function module.is_incapacitate(name)
    return false
end
function module.is_silence(name)
    return false
end
function module.is_interrupt(name)
    return false
end
function GetInsanity()
    return UnitPower("player", Enum.PowerType.Insanity)
end
local function HandleMindflay()
    if not skill.IsCasting() then
        return (TryCastSpellOnTarget("Mind Flay"))
    end
end
local function ApplyDots()
    if IsTalentSelected("Misery") then
        return
        (TargetMyAuraDuration("Vampiric Touch") <= 6 or TargetMyAuraDuration("Shadow Word: Pain") <= 0) and
        TryCastSpellOnTarget("Vampiric Touch") or
        nil
    else
        return
        -- moving and (TargetMyAuraDuration("Shadow Word: Pain") <= 4 and TryCastSpellOnTarget("Shadow Word: Pain")) or
        (TargetMyAuraDuration("Vampiric Touch") <= 6 and TryCastSpellOnTarget("Vampiric Touch")) or
        (TargetMyAuraDuration("Shadow Word: Pain") <= 4 and TryCastSpellOnTarget("Shadow Word: Pain")) or
        nil
    end
end
local function VoidformRotation()
    local boltCD = GetCD("Void Eruption")
    if boltCD < GlobalCD() + 0.4 then
        return TryCastSpellOnTarget("Void Eruption")
    else
        return
        TryCastSpellOnTarget("Devouring Plague") or
        TryCastSpellOnTarget("Mind Blast") or
        TryCastSpellOnTarget("Mindgames") or
        "nothing"
    end
end


local priorities = {}
function priorities.long_single_target()
    return
    skill.DoNothingOutOfCombat() or
    --skill.DoNothingIfTargetOutOfCombat() or
    -- skill.DoNothingIfCasting() or
    --skill.TryInterruptTarget() or

    TryCastSpellOnUnit(GetQueuedSpell(), GetQueuedUnit()) or

    (PlayerMyAuraDuration("Voidform") > 0 and VoidformRotation() or nil) or
    (GetInsanity() >= 90 and TryCastSpellOnTarget("Devouring Plague")) or
    (TryCastSpellOnTarget("Mind Blast")) or
    ApplyDots() or
    (TryCastSpellOnTarget("Mindbender")) or --UnitIsBoss("target") and
        HandleMindflay() or

    "nothing"
end

function module.run()
    return private.priority_selection.run()
end
function module.load()
    private.priority_selection = PrioritySelectionFor(priorities)
    private.frame:RegisterEvent("GROUP_ROSTER_UPDATE")
    --  private.frame:RegisterEvent("PLAYER_TALENT_UPDATE")
    private.frame:SetScript("OnEvent", private.on_event)
    AddGuiElement(private.priority_selection.gui)
end
function module.unload()
    private.frame:UnregisterEvent("PLAYER_TALENT_UPDATE")
    --  private.frame:UnregisterEvent("GROUP_ROSTER_UPDATE")
    private.frame:SetScript("OnEvent", nil)
end
skill.modules.priest.shadow = module