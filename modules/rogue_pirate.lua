_, skill = ...
local private = {}


local function IsOffGlobal(name)
  return
    name == "Riposte" or
    name == "Grappling Hook" or
    name == "Cloak of Shadows" or
    name == "Tricks of the Trade" or
    name == "Vanish" or
    name == "Sprint" or
    name == "Kick" or
    name == "Stealth" or
    name == "Thistle Tea" or
    name == "Will to Survive" or
    name == "Healthstone"
end

local function IsStun(name)
  return
    name == "Cheap Shot" or
    name == "Kidney Shot"
end

local function IsDisorient(name)
  return name == "Blind"
end

local function IsIncapacitate(name)
  return name == "Sap" or name == "Gouge"
end

local function IsSilence(name)
  return 0 == 1
end

local function IsInterrupt(name)
  return name == "Kick"
end

local function GetCP()
  return UnitPower("player", Enum.PowerType.ComboPoints)
end

local function GetCPMax()
  return UnitPowerMax("player", Enum.PowerType.ComboPoints)
end

local function GetEnergy()
  return UnitPower("player", Enum.PowerType.Energy)
end
local function GetEnergyRegen()
  return 10 *  (1 + GetHaste() / 100)
end

local function GetEnergyMax()
  return UnitPowerMax("player", Enum.PowerType.Energy)
end

local function InMeleeRange()
  return IsUnitInRange("Sinister Strike", "target")
end

local function DoNothingIfMounted()
  return IsMounted() and "nothing" or nil
end

local function CPGenerated(spell)
  local missing_cp = GetCPMax() - GetCP()
  return min(missing_cp, PlayerAuraDuration("Broadside") and 2 or 1)
end

local function CPWasted(spell)
  local missing_cp = GetCPMax() - GetCP()
  if PlayerAuraDuration("Curse of the Dreadblades") > 0 then
    if spell == "Sinister Strike" or spell == "Pistol Shot" then
      return GetCPMax() - missing_cp
    end
  end
  return max(0, GetCP() - GetCPMax() + PlayerAuraDuration("Broadside") and 2 or 1)
end


local function can_use_echoing_reprimand()
  if not skill.IsKyrian() then
    return false
  end
  local er = PlayerAuraStacks("Echoing Reprimand")
  return er > 0 and GetCP() == er
end
-- New Function Opportunity // Broadside
local function FinisherCP()
    return PlayerAuraDuration("Opportunity") > 0  and GetCP() >= 4
    or PlayerAuraDuration("Broadside")  > 0 and GetCP() >= 4
    or GetCP() >= GetCPMax()
    or can_use_echoing_reprimand()
    or nil
 end


local function HandleGhostlyStrike()
  local spell = "Ghostly Strike"
  return
    IsTalentSelected(spell) and
    TargetAuraDuration(spell) < 15 * 0.3 and
    GetEnergy() >= 30 and
    GetCP() + CPGenerated(spell) <= GetCPMax() and
    TryCastSpellOnTarget(spell) or nil
end

local function HandleOpportunity()
  return
    PlayerAuraDuration("Opportunity") > 0 and
    GetCP() + CPGenerated("Pistol Shot") <= GetCPMax() and
    TryCastSpellOnTarget("Pistol Shot") or nil
end

local function FinisherCostReduction()
  return 0--4  -- TODO: Handle Artifact weapon Fatebringer trait
end


local function HandleDispatch()
  return
    -- GetCP() >= GetCPMax()
    FinisherCP() and
    GetEnergy() >= 35  and
    TryCastSpellOnTarget("Dispatch") or nil
end

local function HandleBetweenTheEyes()
  return
   --  GetCP() >= GetCPMax()
    FinisherCP()  and
    skill.IsUsable("Between the Eyes") and
   -- PlayerAuraDuration("Master Assassin's Initiative") > 0 and
    TryCastSpellOnTarget("Between the Eyes") or nil
end

local function TryMarkedForDeath()
  return
    not IsStealthed() and
    InMeleeRange() and
    GetCP() <= 1 and
    GlobalCD() < 0.3 and
    IsTalentSelected("Marked for Death") and
    TryCastSpellOnTarget("Marked for Death") or
    nil
end

local function TargetLivesAtLeast(duration)
  local projected_dmg = (200000 + GetNumGroupMembers() * 300000) * duration
  return UnitHealth("target") > projected_dmg
end

local ROLL_THE_BONES_BUFFS = {
  "Broadside",             -- Generators generate an extra CP
  "Buried Treasure",        -- 25% Energy regeneration
  "Grand Melee",            -- 50% Attack Speed and 25% Leech
  "Skull and Crossbones",            -- 25% extra Sinister Strike proc chance
  "Ruthless Precison",  -- 25% crit
  "True Bearing",           -- Finishers reduce CD of many spells
}
local function NumRollTheBonesBuffs()
  local result = 0
  for i, k in ipairs(ROLL_THE_BONES_BUFFS) do
    if PlayerAuraDuration(k) > 0 then
      result = result + 1
    end
  end
  return result
end

local function RollTheBonesDuration()
  for i, k in ipairs(ROLL_THE_BONES_BUFFS) do
    local duration = PlayerAuraDuration(k)
    if duration > 0 then
      return duration
    end
  end
  return 0
end

local function NumValuableRollTheBonesBuffs()
  for i, k in ipairs(VALUABLE_ROLL_THE_BONES_BUFFS) do
    local duration = PlayerAuraDuration(k)
    if duration > 0 then
      return duration
    end
  end
  return 0
end

local function NumVeryValuableRollTheBonesBuffs()
  return 0
end

local DREADBLADES_ROLL_THE_BONES_BUFFS = {
  "Grand Melee",
  "Jolly Roger",
  "Shark Infested Waters",
  "True Bearing",
}
local function ShouldPrepareForCotD()
  return
    false and
    TargetLivesAtLeast(finisher)

end
function private.KeepUpInstantPoison()
  local _, expiration = GetWeaponEnchantInfo()
  local duration = expiration and (GetTime() - expiration / 1000) or 0
  return
  duration < 20 * 60 and
  TryCastSpellOnPlayer("Windfury Weapon") or
  nil
end

local function TryRollTheBones()
  return
    skill.IsUsable("Roll the Bones") and
    IsUnitInRange("Sinister Strike", "target") and
    TryCastSpellOnPlayer("Roll the Bones") or nil
end

local function NumMinorRollTheBonesBuffs()
  local rtb = PlayerAuraDuration("True Bearing") > 0 and 1 or 0
  return NumRollTheBonesBuffs() - rtb
end

local function HandleSliceAndDice()

  local finisherDuraiton = 6 + 6 * GetCP()
  local pandemicDuration = finisherDuraiton * 0.3
  local defaultDps = PlayerHealthMax() * 0.05
  local estimatedDps = math.max(1, GetNumGroupMembers() - 2) * defaultDps
  local worth = TargetHealth() > estimatedDps * finisherDuraiton * 0.8 or skill.ShouldAlwaysAttackTarget()

  return
    skill.IsUsable("Slice and Dice") and
    PlayerAuraDuration("Slice and Dice") < 6 and
   -- worth and
    -- GetCP() >= GetCPMax()
    FinisherCP() and
    TryCastSpellOnPlayer("Slice and Dice") or
    nil
end

local function HandleRollTheBones()

  return


      NumRollTheBonesBuffs() >= 2 and PlayerAuraDuration("Grand Melee") > 0 and PlayerAuraDuration("Buried") > 0
         and TryRollTheBones()
      or

      NumRollTheBonesBuffs() < 2 and PlayerAuraDuration("Broadside") <= 0 and  PlayerAuraDuration("True Bearing") <= 0 and TryRollTheBones()
        or
        nil
end

local function DontCap()
  return
    GetEnergy() > GetEnergyMax() - 10 and
    (TryCastSpellOnTarget("Sinister Strike") or TryCastSpellOnTarget("Pistol Shot")) or
    nil
end

local function KeepRollTheBonesUp()
  local rtb = RollTheBonesDuration()
  local finisher = 6 + 6 * GetCP()
  local pandemic = finisher * 0.3
  finisher = finisher + min(pandemic, rtb)
  return
    TargetLivesAtLeast(finisher) and
    TryRollTheBones() or nil
end

local function DoNothingInStealth()
  return IsStealthed() and "nothing" or nil
end

local function HandleSinisterStrike()
  return
    GetEnergy() >= 35 + 35 - GetEnergyRegen() and


    --CPWasted("Sinister Strike") <= 0 and
    TryCastSpellOnTarget("Sinister Strike") or
    nil
end
local function TryCastEchoingReprimand()
  if not skill.IsKyrian() then
    return nil
  end
  return
    GetEnergy() < GetEnergyMax() - 10 - GetPowerRegen() and
    TryCastSpellOnTarget("Echoing Reprimand") or
    nil
end
local function Heal()
  return
    PlayerHealthPercent() < 0.5 and
    GetEnergy() >= 30 and
    TryCastSpellOnPlayer("Crimson Vial") or
    nil
end
local function HandleBladeFlurry()
  return
    skill.NumEnemiesWithin(5) > 1 and
    GetEnergy() >= 15 and
    TryCastSpellOnPlayer("Blade Flurry") or
    nil
end

local function HandleSerratedBoneSpike()
    return
        (GetCP() < GetCPMax()) and
        TargetMyAuraDuration("Serrated Bone Spike") < 0 and
        TryCastSpellOnTarget("Serrated Bone Spike") or
        nil
    end

local function HandleAmbush()
  return

    PlayerAuraDuration("Sepsis") >= 1
    and
    TryCastSpellOnTarget("Ambush")
   --print("ambush")
    or
    nil
end
local function BladeRush()
  return
    GetEnergy() < GetEnergyMax()  and
   -- PlayerAuraDuration("Master Assassin's Initiative") > 0 and
    TryCastSpellOnTarget("Blade Rush") or
    nil
end
local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
function private.KeepUpPoisons()
  return
    not UnitAffectingCombat("player") and
    not IsMounted() and PlayerAuraDuration("Stealth") <= 0 and
    TryCastSpellOnPlayer("Stealth") or
    nil
end
private.priorities = {}
function private.priorities.low_level_farm()
  return
  initiateLowLevelCombatWithSpell("Pistol Shot") or
  (not UnitAffectingCombat("player") and skill.UnitIsBoss("target") and TryCastSpellOnTarget("Ambush") or nil) or
  (not UnitAffectingCombat("player") and skill.UnitIsBoss("target") and TryCastSpellOnTarget("Sinister Strike") or nil) or
  DoNothingIfMounted() or
  skill.DoNothingIfCasting() or
  (not UnitAffectingCombat("player") and PlayerAuraDuration("Stealth") <= 0 and TryCastSpellOnPlayer("Stealth")) or
  (not UnitExists("target") and skill.AcquireTarget()) or
  skill.KeepRepurposedFelFocuserUp() or
  skill.DoNothingOutOfCombat() or
  skill.DoNothingIfTargetOutOfCombat() or
  DoNothingInStealth() or

  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  skill.StartAttack() or

  TryMarkedForDeath() or
  (UnitAffectingCombat("target") and skill.UnitIsBoss("target") and GetEnergy() < 50 and TryCastSpellOnPlayer("Adrenaline Rush")) or
  Heal() or
  HandleBladeFlurry() or
  -- Finisher
  HandleRollTheBones() or
  HandleSliceAndDice() or
  HandleBetweenTheEyes() or
  HandleDispatch() or
  -- other
  BladeRush() or
  -- Generators without wasting CP
  --HandleSerratedBoneSpike() or
  HandleGhostlyStrike() or
  HandleOpportunity() or
  HandleSinisterStrike() or
  skill.TryUseConcentratedFlame() or
  -- Generators that might waste CP
  DontCap() or
  "nothing"

end

local function TryCastSerratedBoneSpikeOnTarget()
  if not skill.IsNecrolord() then
    return nil
  end
  local cp = (TargetMyAuraDuration("Serrated Bone Spike") > 0) and 2 or 1
  return
    GetCP() + cp <= GetCPMax() and
    TryCastSpellOnTarget("Serrated Bone Spike") or
    nil
end

function private.priorities.default()
  local result =
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.AcquireTarget()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and skill.StartAttack()) or
  --(C_Map.GetBestMapForUnit("player") == 582 and TryCastSpellOnTarget("Pistol Shot")) or
    (not UnitAffectingCombat("player") and not IsMounted() and PlayerAuraDuration("Stealth") <= 0 and TryCastSpellOnPlayer("Stealth") or nil) or
    (not UnitAffectingCombat("player") and not IsMounted() and PlayerAuraDuration("Stealth") <= 0 and TryCastSpellOnPlayer("Stealth") or nil) or
    torghast.torghast_stuff() or
    private.KeepUpPoisons() or
    skill.common_priority_start() or
    DoNothingInStealth() or
    skill.AcquireTarget() or
    skill.StartAttack() or
    --skill.StartAttack() or
    HandleAmbush() or
    TryMarkedForDeath() or
    Heal() or
    HandleBladeFlurry() or
    -- Finisher
    HandleRollTheBones() or
    HandleSliceAndDice() or
    HandleBetweenTheEyes() or
    HandleDispatch() or
    -- other
    BladeRush() or
    -- Generators without wasting CP
    HandleGhostlyStrike() or
    HandleOpportunity() or
    TryCastSerratedBoneSpikeOnTarget() or
    TryCastEchoingReprimand() or
    HandleSinisterStrike() or
    --skill.TryUseConcentratedFlame() or
    -- Generators that might waste CP
    "nothing"
  return result
end
local function range(name)
  if name == "Roll the Bones" then
    return 5
  end
  return 0
end
local module = {
  ["run"] = run,
  ["interrupts"] = {
    "Kick",
    "Kidney Shot",
  },
  --["spell"] = {
  --  ["range"] = range,
  --},
  ["save_interrupt_for"]= {},
  ["range_override"] = {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}




function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.rogue.outlaw = module
