_, skill = ...
local private = {}


local function IsOffGlobal(name)
  return
    name == "Riposte" or
    name == "Grappling Hook" or
    name == "Cloak of Shadows" or
    name == "Tricks of the Trade" or
    name == "Vanish" or
    name == "Sprint" or
    name == "Kick" or
    name == "Stealth" or
    name == "Thistle Tea" or
    name == "Will to Survive" or
    name == "Shadowstep" or
    name == "Symbols of Death" or
    name == "Shadow Dance" or
    name == "Healthstone"
end

local function IsStun(name)
  return
    name == "Cheap Shot" or
    name == "Kidney Shot"
end

local function IsDisorient(name)
  return name == "Blind"
end

local function IsIncapacitate(name)
  return name == "Sap" or name == "Gouge"
end

local function IsSilence(name)
  return 0 == 1
end

local function IsInterrupt(name)
  return name == "Kick"
end

local function GetCP()
  return UnitPower("player", Enum.PowerType.ComboPoints)
end

local function GetCPMax()
  return UnitPowerMax("player", Enum.PowerType.ComboPoints)
end

local function GetEnergy()
  return UnitPower("player", Enum.PowerType.Energy)
end
local function GetEnergyRegen()
  return 10 *  (1 + GetHaste() / 100)
end

local function GetEnergyMax()
  return UnitPowerMax("player", Enum.PowerType.Energy)
end

local function InMeleeRange()
  return IsUnitInRange("Backstab", "target")
end

local function DoNothingIfMounted()
  return IsMounted() and "nothing" or nil
end

local function CPGenerated(spell)
  local missing_cp = GetCPMax() - GetCP()
  return min(missing_cp,  1)
end

local function HandleSymbolOfDeath()
   return
        -- 1 Case Flagellation kein CD
        GetCD("Flagellation") <= GlobalCD() and
        PlayerAuraDuration("Shadow Dance") >= 1 and
        GetCP() >= GetCPMax() - 1 and
        TryCastSpellOnPlayer("Symbols of Death") or


        -- 2 Case Flagellation cd
         PlayerAuraDuration("Slice and Dice") >= (9+ GlobalCD()) and --new 9+ gcd
       -- TargetMyAuraDuration("Rupture") > 8 and
        GetCD("Flagellation") > 3 and
        GetCharges("Shadow Dance") >= 1 and
        GetCP() <= 2 and
        GetEnergy() >= 15 and  --45
        TryCastSpellOnPlayer("Symbols of Death") or

        nil
   end


local function HandleFlagellation()
    return
        PlayerAuraDuration("Symbols of Death") > GlobalCD() and
        GetCP() >= GetCPMax() - 1 and
        TryCastSpellOnTarget("Flagellation") or
        nil
    end

local function DontCap()
  return
    GetEnergy() > GetEnergyMax() - 10 and
    GetCP() <= GetCPMax() - 1 and
    TryCastSpellOnTarget("Backstab") or
    nil
end

local function HandleShadowBlades()
    return
        GetCD("Symbols of Death") <= GlobalCD() and
        GetCD("Shadow Dance") <= GlobalCD() and
        TryCastSpellOnPlayer("Shadow Blades") or
        nil
    end

local function HandleBackstab()
  return

    GetEnergy() >= (35 + 30 - GetEnergyRegen()) and
    skill.NumEnemiesWithin(10) < 3 and
    GetCP() <= GetCPMax() -1  and -- -1 weg
    TryCastSpellOnTarget("Backstab") or
    nil

end

function pool(energy)
  return
    (GetEnergy() < energy or GetEnergy() < GetEnergyMax() - 0.01) and "nothing"
    or nil
end

local function HandleShadowDance()
    return

        -- 1 Case Symbol auf CD

        PlayerAuraDuration("Slice and Dice") > 8 and
      --  TargetMyAuraDuration("Rupture") > 8 and -- new
        ((GetCD("Symbols of Death")+22) > GetChargeCD("Shadow Dance")) and
        GetCP() <= 2 and
        GetCharges("Shadow Dance") >= 1 and
        ((IsTalentSelected("Master of Shadows") and GetEnergy() > 40) or (IsTalentSelected("Shuriken Tornado") and GetEnergy() > 50))and --65
        TryCastSpellOnPlayer("Shadow Dance") or

        -- 2 Case Symbol kein CD + Flagellation auf CD
          PlayerAuraDuration("Slice and Dice") >= 8 and
         -- TargetMyAuraDuration("Rupture") > 8 and -- new
          PlayerAuraDuration("Symbols of Death") >= 1 and
          GetCD("Flagellation") >= 3 and
          GetCP() <= 2 and
          GetCharges("Shadow Dance") >= 1 and
          TryCastSpellOnPlayer("Shadow Dance") or

        -- 3 Case Symbol + Flagellation kein CD
          PlayerAuraDuration("Slice and Dice") >= 8 and
        --  TargetMyAuraDuration("Rupture") > 8 and -- new
          GetCD("Symbols of Death") <= GlobalCD() and
          GetCD("Flagellation") <= GlobalCD() and
          GetCP() <= 2 and
          GetCharges("Shadow Dance") >= 1 and
          ((IsTalentSelected("Master of Shadows") and GetEnergy() > 40) or (IsTalentSelected("Shuriken Tornado") and GetEnergy() > 50)) and  --65
          TryCastSpellOnPlayer("Shadow Dance") or

         -- neue case zum testen
         PlayerAuraDuration("Symbols of Death") > GlobalCD() and
         PlayerAuraDuration("Shadow Blades") > GlobalCD() and
         ((IsTalentSelected("Master of Shadows") and GetEnergy() > 40) or (IsTalentSelected("Shuriken Tornado") and GetEnergy() > 50))and  --new  65
         GetCP() <= 2 and
         TryCastSpellOnPlayer("Shadow Dance") or
          nil
    end

local function HandleShurikenTornado()
    return
        GetCD("Symbols of Death") <= GlobalCD() and
        IsTalentSelected("Shuriken Tornado") and
        skill.NumEnemiesWithin(10) >= 3 and
        TryCastSpellOnPlayer("Shuriken Tornado") or
        nil
    end

local function HandleShurikenStorm()
    return
        skill.NumEnemiesWithin(10) >= 3 and
        TryCastSpellOnPlayer("Shuriken Storm") or
        nil
    end

local function HandleBlackPowder()
    return

            GetCP() >= 4 and
           (skill.NumEnemiesWithin(10) >= 3)  and
                     PlayerMyAuraDuration("Shadow Dance") > GlobalCD()and
            TryCastSpellOnTarget("Black Powder") or
        skill.NumEnemiesWithin(10) >= 3 and        GetCP() >= GetCPMax() -1  and
        TryCastSpellOnPlayer("Black Powder") or
        nil
    end

local function HandleShadowstrike()
    return
        (PlayerMyAuraDuration("Shadow Dance") >= GlobalCD())  and
         GetCP() <= 4 and
         skill.NumEnemiesWithin(5) < 3 and
        TryCastSpellOnTarget("Shadowstrike") or
        nil
    end


local function TryMarkedForDeath()
  return
    not IsStealthed() and
    InMeleeRange() and
    GetCP() <= 1 and
    GlobalCD() < 0.3 and
    IsTalentSelected("Marked for Death") and
    TryCastSpellOnTarget("Marked for Death") or
    nil
end



local function TryCastSerratedBoneSpikeOnTarget()
  if not skill.IsNecrolord() then
    return nil
  end
  local cp = (TargetMyAuraDuration("Serrated Bone Spike") > 0) and 2 or 1
  return
    GetCP() + cp <= GetCPMax() and
    TryCastSpellOnTarget("Serrated Bone Spike") or
    nil
end

local function HandleSliceAndDice()
    return
        PlayerAuraDuration("Slice and Dice") < 4 and
        GetCP() >= (GetCPMax()-1) and
        TryCastSpellOnPlayer("Slice and Dice") or
        (skill.NumEnemiesWithin(10) <= 6) and
        PlayerAuraDuration("Slice and Dice") <= GlobalCD() and
        TryCastSpellOnPlayer("Slice and Dice") or
        nil
    end

local function HandleRupture()

  return
    skill.IsUsable("Rupture") and
    (TargetMyAuraDuration("Rupture") <= 6) and
    PlayerAuraDuration("Shadow Dance") <= GlobalCD() and --new
    (GetCP() >= GetCPMax() -1 ) and
    (skill.NumEnemiesWithin(10) <= 5) and
    TryCastSpellOnTarget("Rupture") or

    --new
        skill.IsUsable("Rupture") and
        (TargetMyAuraDuration("Rupture") <= 0.5) and
        (GetCP() >= 4 ) and
        PlayerAuraDuration("Shadow Dance") >= GlobalCD() and
        (skill.NumEnemiesWithin(10) <= 5) and
        TryCastSpellOnTarget("Rupture") or
    nil
  end

local function HandleEviscerate()
    return

        GetCP() >= 4 and
       (skill.NumEnemiesWithin(10) < 3)  and


        PlayerMyAuraDuration("Shadow Dance") > GlobalCD()and --GlobalCD() oder 1sek
        TryCastSpellOnTarget("Eviscerate") or
       (skill.NumEnemiesWithin(10) < 3)  and
        GetCP() >= GetCPMax()-1 and
        TryCastSpellOnTarget("Eviscerate") or

        nil

    end



local function DoNothingInStealth()
  return IsStealthed() and "nothing" or nil
end



local function Heal()
  return
    PlayerHealthPercent() < 0.5 and
    GetEnergy() >= 30 and
    TryCastSpellOnPlayer("Crimson Vial") or
    nil
end





local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end



private.priorities = {}


function private.priorities.default()
  local result =

    (not UnitAffectingCombat("player") and not IsMounted() and PlayerAuraDuration("Stealth") <= 0 and TryCastSpellOnPlayer("Stealth") or nil) or
    (not UnitAffectingCombat("player") and not IsMounted() and PlayerAuraDuration("Stealth") <= 0 and TryCastSpellOnPlayer("Stealth") or nil) or
    skill.common_priority_start() or
    DoNothingInStealth() or
    skill.AcquireTarget() or
    skill.StartAttack() or
    Heal() or


    HandleShadowBlades()or
    HandleShurikenTornado() or
    HandleSymbolOfDeath() or
    HandleShadowDance() or
    HandleFlagellation() or
HandleShadowstrike() or


    HandleSliceAndDice() or


    HandleRupture() or
HandleEviscerate() or

    HandleBlackPowder() or


    HandleBackstab() or
    HandleShurikenStorm() or

    DontCap() or
    "nothing"

  return result
end


local module = {
  ["run"] = run,
  ["interrupts"] = {
    "Kick",
    "Kidney Shot",
  },
  --["spell"] = {
  --  ["range"] = range,
  --},
  ["save_interrupt_for"]= {},
  ["range_override"] = {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}




function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.rogue.subtlety = module