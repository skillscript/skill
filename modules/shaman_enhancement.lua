_, skill = ...

local private = {}

local function IsOffGlobal(name)
  return
    name == "Wind Shear" or
    name == "Astral Shift"
end

local function IsStun(name)
  return false
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsInterrupt(name)
  return name == "Wind Shear"
end

local function GetMaelstrom()
  return PlayerAuraStacks("Maelstrom Weapon")
end

function private.get_earth_shield_target()
  if UnitExists("pet") and UnitName("pet") == "Greater Earth Elemental" then
    return "pet"
  end
  local tank = skill.TryGetFirstTankUnit()
  if tank then
    return tank
  end
  for _, unit in ipairs(GroupCombatCastUnits()) do
    if unit ~= "mouseover" and unit ~= "targettarget" and unit ~= "target" and UnitMyAuraDuration(unit, "Earth Shield") > 0 then
      return unit
    end
  end
  return "player"
end
function private.HandleEarthShield()
  if not IsTalentSelected("Earth Shield") then
    return nil
  end
  private.earth_shield_target = private.earth_shield_target or private.get_earth_shield_target()
  if private.earth_shield_target == "player" and PlayerMyAuraDuration("Lightning Shield") > 0 then
    private.earth_shield_target = nil
  end
  return
    private.earth_shield_target and
    --(UnitMyAuraDuration(private.earth_shield_target, "Earth Shield") < 2 * 60 or UnitMyAuraStacks(private.earth_shield_target, "Earth Shield") <= 1) and
    UnitMyAuraDuration(private.earth_shield_target, "Earth Shield") < 2 * 60 and
    TryCastSpellOnUnit("Earth Shield", private.earth_shield_target) or
    nil
end
local function HandleBeaconOf(kind, role)
  if private.earth_shield_target then
    local result = KeepUpBeacon("Beacon of "..kind, BEACON[kind])
    if result then
      return result
    end
  else
    BEACON[kind] = skill.TryGetFirstRoleUnit(role)
  end
end
function private.HandleBeacons()
  return
  HandleBeaconOf("Light", "TANK") or
  (not BEACON["Light"] and KeepUpBeacon("Beacon of Light", "player")) or
  (IsTalentSelected("Beacon of Faith") and HandleBeaconOf("Faith", "HEALER") or nil) or
  nil
end

-- TODO: Move to shaman.lua.
local shaman = {}
function shaman.DoNothingGhostWolf()
  return PlayerAuraDuration("Ghost Wolf") > 0 and "nothing" or
  nil
end
private.should_cancel_ghost_wolf_in_melee = true
function shaman.HandleGhostWolf()
  local duration = PlayerAuraDuration("Ghost Wolf")
  if false and UnitAffectingCombat("player") then
    return
      private.should_cancel_ghost_wolf_in_melee and
      duration > 0 and
      IsUnitInRange("Stormstrike", "target") and
      TryCastSpellOnPlayer("Ghost Wolf") or
      nil
  else
    return
      skill.ShouldUseRunSpeed() and
      duration <= 0 and
      TryCastSpellOnPlayer("Ghost Wolf") or
      nil
  end
end

local function DropFuryOfAirOutOfCombat()
  if not IsTalentSelected("Fury of Air") or UnitAffectingCombat("player") then
    return
  end
  return PlayerAuraDuration("Fury of Air") > 0 and TryCastSpellOnPlayer("Fury of Air")
end

local function KeepUpFlametongue()
  return
    PlayerAuraDuration("Flametongue") < 16 * 0.3 and
    TryCastSpellOnTarget("Flametongue") or
    nil
end

local function TryStormbringerStormstrike()
  return
    PlayerAuraDuration("Stormbringer") > GlobalCD() and
    TryCastSpellOnTarget("Stormstrike") or
    nil
end

function private.TrySundering()
  return
    IsTalentSelected("Sundering") and
    TryCastSpellOnTarget("Sundering") or
    nil
end

local function TryCrashLightning()
  return
    TryCastSpellOnTarget("Crash Lightning") or
    nil
end

function private.TryStormstrike()
  return
    (PlayerAuraDuration("Ascendance") > GlobalCD() and TryCastSpellOnTarget("Windstrike") or nil) or
    TryCastSpellOnTarget("Stormstrike") or
    nil
end

function private.StayAlive()
  return
    (PlayerHealthPercent() < 0.1 and TryCastSpellOnPlayer("Earth Elemental") or nil) or
    (PlayerHealthPercent() < 0.2 and skill.TrySelfHealWithConcentratedFlame() or nil) or
    (PlayerHealthPercent() < 0.5 and GetMaelstrom() >= 5 and TryCastSpellOnPlayer("Healing Surge") or nil) or
    (PlayerHealthPercent() < 0.4 and TryCastSpellOnPlayer("Astral Shift") or nil) or
    (PlayerHealthPercent() < 0.5 and TryCastSpellOnPlayer("Healing Stream Totem") or nil) or
    nil
end

function private.HandleAoe()
  local num_enemies = skill.NumEnemiesWithin(8)
  if num_enemies < 2 then
    return nil
  end
  return
    private.TryHailstorm(num_enemies) or
    private.TrySundering() or
    private.TryMaelstromedLightning(5) or
    private.TryCrashLightning() or
    private.TryMaelstromedElementalBlast(5) or
    private.TryEarthenSpike() or
    private.TryStormstrike() or
    nil
end
function private.HandleHealingStreamTotem()
  return
    PlayerHealthPercent() < 0.8 and
    TryCastSpellOnPlayer("Healing Stream Totem") or
    nil
end

function private.KeepWindfuryTotemActive()
  return
    PlayerAuraDuration("Windfury Totem") <= 0 and
    private.windfury_totem_timeout < GetTime() and
    TryCastSpellOnPlayer("Windfury Totem") or
    nil
end

function private.KeepFlameShockOnSingleTarget()
  return
    skill.NumEnemiesWithin(8) == 1 and
    TargetMyAuraDuration("Flame Shock") <= 0 and
    TryCastSpellOnTarget("Flame Shock") or
    nil
end
function private.TryPrimordialWave()
  return
    skill.NumEnemiesWithin(8) >= 1 and
    TargetMyAuraDuration("Flame Shock") <= 0 and
    TryCastSpellOnTarget("Primordial Wave") or
    nil
end
function private.RefreshFlameShockOnTarget()
  return
    TargetMyAuraDuration("Flame Shock") <= 18 * 0.3 and
    TryCastSpellOnTarget("Flame Shock") or
    nil
end
function private.TryMaelstromedElementalBlast(min_stacks)
  min_stacks = min_stacks or 5
  return
    IsTalentSelected("Elemental Blast") and
    GetMaelstrom() >= min_stacks and
    TryCastSpellOnTarget("Elemental Blast") or
    nil
end
function private.TryMaelstromedLightning(min_stacks)
  min_stacks = min_stacks or 5
  return
    GetMaelstrom() >= min_stacks and
    (skill.NumEnemiesWithin(8) >= 2 and TryCastSpellOnTarget("Chain Lightning") or TryCastSpellOnTarget("Lightning Bolt") or nil) or
    nil
end
function private.TryEarthenSpike()
  return
    IsTalentSelected("Earthen Spike") and
    TryCastSpellOnTarget("Earthen Spike") or
    nil
end
function private.TryHailstorm(min_stacks)
  min_stacks = min(5, max(1, min_stacks))
  return
    PlayerAuraDuration("Hailstorm") >= min_stacks and
    private.TryFrostShock() or
    nil
end
function private.TryCrashingStorm()
  return
    IsTalentSelected("Crashing Storm") and
    private.TryCrashLightning() or
    nil
end
function private.TryLavaLash()
  return
    TryCastSpellOnTarget("Lava Lash") or
    nil
end
function private.TryCrashLightning()
  return
    TryCastSpellOnTarget("Crash Lightning") or
    nil
end
function private.TryFrostShock()
  return
    TryCastSpellOnTarget("Frost Shock") or
    nil
end
function private.KeepUpWindfuryWeapon()
  local _, duration = GetWeaponEnchantInfo()
  duration = duration and duration / 1000 or 0
  return
    duration < 20 * 60 and
    TryCastSpellOnPlayer("Windfury Weapon") or
    nil
end
function private.KeepUpFlametongueWeapon()
  local _, _, _, _, _, duration = GetWeaponEnchantInfo()
  duration = duration and duration / 1000 or 0
  return
    duration < 20 * 60 and
    TryCastSpellOnPlayer("Flametongue Weapon") or
    nil
end
function private.KeepUpLightningShield()
  return
    private.earth_shield_target ~= "player" and
    PlayerMyAuraDuration("Earth Shield") <= 0 and
    PlayerAuraDuration("Lightning Shield") <= 10 * 60 and
    TryCastSpellOnPlayer("Lightning Shield") or
    nil
end
function private.KeepUpSelfBuffs()
  return
    private.KeepUpWindfuryWeapon() or
    private.KeepUpFlametongueWeapon() or
    private.KeepUpLightningShield() or
    private.HandleEarthShield() or
    nil
end

function private.HandleDeathseersSatchel()
  return
      UnitAffectingCombat("player") and
      not IsPlayerMoving() and
      --TryCastSpellOnPlayer("Capacitor Totem") and
      --TryCastSpellOnPlayer("Tremor Totem") and
      --TryCastSpellOnPlayer("Earthbind Totem") and
      --TryCastSpellOnPlayer("Healing Stream Totem") or
      nil
end
function private.SpikeOfTheIceGuardian()
  return
    TargetHealthPercent() < 0.1 and
    TryCastSpellOnTarget("Frost Shock") or
    nil
end

private.priorities = {}
function private.ShouldUseRunSpeed()
  return not UnitAffectingCombat("player") and IsPlayerMoving()
end
function private.EarthElementalDuration()
  local i = 1
  while true do
    local exists, name, start, duration = GetTotemInfo(i)
    if not exists then
      return 0
    end
    if name == "Greater Earth Elemental" then
      return max(0, start + duration - GetTime())
    end
    i = i + 1
  end
  return 0
end
function private.EarthElementalDurationMax()
  local i = 1
  while true do
    local exists, name, _, duration = GetTotemInfo(i)
    if not exists then
      return 0
    end
    if name == "Greater Earth Elemental" then
      return duration
    end
    i = i + 1
  end
  return 0
end
function private.HealEarthElemental()
  local duration = private.EarthElementalDuration()
  local duration_max = private.EarthElementalDurationMax()
  if duration_max <= 0 or GetMaelstrom() < 5 then
    return nil
  end
  local threshold = duration / duration_max
  return
    (IsTalentSelected("Earth Shield") and UnitName("targettarget") == "Greater Earth Elemental" and (UnitMyAuraDuration("targettarget", "Earth Shield") <= 0 or UnitAuraStacks("targettarget", "Earth Shield") <= 1) and TryCastSpellOnUnit("Earth Shield", "targettarget") or nil) or
    (IsTalentSelected("Earth Shield") and UnitName("mouseover") == "Greater Earth Elemental" and (UnitMyAuraDuration("mouseover", "Earth Shield") <= 0 or UnitAuraStacks("mouseover", "Earth Shield") <= 1) and TryCastSpellOnUnit("Earth Shield", "mouseover") or nil) or
    (UnitName("targettarget") == "Greater Earth Elemental" and skill.TryHealUnit("Healing Surge", "targettarget", threshold, 0.1) or nil) or
    (UnitName("mouseover") == "Greater Earth Elemental" and skill.TryHealUnit("Healing Surge", "mouseover", threshold, 0.1) or nil) or
    nil
end
function private.priorities.default()
  return
    torghast.torghast_stuff() or
    skill.DoNothingIfMounted() or
    (not UnitAffectingCombat("player") and GetMaelstrom() >= 5 and skill.TryHealUnit("Healing Surge", "player", 0.9, 0.1) or nil) or
    (not UnitAffectingCombat("player") and private.KeepUpSelfBuffs() or nil) or
    --(private.ShouldUseRunSpeed() and PlayerAuraDuration("Ghost Wolf") <= 0 and TryCastSpellOnPlayer("Ghost Wolf") or nil) or
    shaman.HandleGhostWolf() or
    shaman.DoNothingGhostWolf() or
    skill.common_priority_start() or
    private.StayAlive() or
    private.HealEarthElemental() or
    (UnitAuraStacks("player", "Deadened Earth", "MAW") >= 2 and TryCastSpellOnPlayer("Earth Elemental") or nil) or
    skill.AcquireTarget() or
    skill.StartAttack() or

    private.HandleEarthShield() or
    skill.TryPurgeAnythingFromTarget() or
    private.KeepWindfuryTotemActive() or
    private.HandleAoe() or
    private.TryPrimordialWave() or
    private.KeepFlameShockOnSingleTarget() or
    private.TryHailstorm(1) or
    private.TryEarthenSpike() or
    private.TryMaelstromedElementalBlast(5) or
    private.TryMaelstromedLightning(8) or
    private.TryStormstrike() or
    private.TryCrashingStorm() or
    private.TryLavaLash() or
    private.TryCrashLightning() or
    skill.TryUseConcentratedFlame() or
    private.RefreshFlameShockOnTarget() or
    private.TryFrostShock() or
    private.TrySundering() or
    private.TryMaelstromedLightning(5) or

    private.KeepUpSelfBuffs() or
    private.HandleHealingStreamTotem() or
    "nothing"
end
function private.priorities.daily()
  return skill.dailies()
end
local function range(spell)
  if spell == "Sundering" then
    return 8
  end
  return nil
end
local module = {
  ["interrupts"] = {
    "Wind Shear",
  },
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Purge"] = {
        "Magic",
      },
    },
    ["dispel"] = {
      ["Cleanse Spirit"] = {
        "Curse",
      },
    },
  },
  ["extra_spells"] = {
    "Windstrike",
  },
  ["save_interrupt_for"] = {
    ["Coldheart Agent"] = {
      ["Terror"] = "Wind Shear",
    },
    ["Empowered Coldheart Agent"] = {
      ["Terror"] = "Wind Shear",
    },
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
  private.frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  private.frame:SetScript("OnEvent", private.on_event)
end
function module.unload()
  private.frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  private.frame:SetScript("OnEvent", nil)
end

skill.modules.shaman.enhancement = module



private.frame = CreateFrame("Frame", nil, UIParent)
private.windfury_totem_timeout = 0
function private.on_event(self, event, ...)
  local _, sub_event, _, source_guid, _, _, _, _, _, _, _, _,	spell, _, failed_type = CombatLogGetCurrentEventInfo()
  if sub_event == "SPELL_CAST_SUCCESS" then
    if source_guid == UnitGUID("player") and spell == "Windfury Totem" then
      private.windfury_totem_timeout = GetTime() + 2
    end
  end
end
