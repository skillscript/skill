_, skill = ...

local private = {}

local function IsOffGlobal(name)
  return
    name == "Wind Shear" or
    name == "Spiritwalker's Grace" or
    name == "Gift of the Naaru" or
    name == "Heroism" or
    name == "Healthstone" or
    name == "Astral Shift"
end

local function IsStun(name)
  return false
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return name == "Wind Shear"
end

local function DefaultOffGlobal()
  return
  "nothing"
end

local function DoNothingInGhostWolf()
  return PlayerAuraDuration("Ghost Wolf") > 0 and "nothing" or nil
end

local function TargetAffectingCombat(unit)
  return UnitAffectingCombat(unit)
end
local function HandleTargetLavaSurge()
  return
    TargetMyAuraDuration("Flame Shock") > GlobalCD() and
    PlayerAuraDuration("Lava Surge") > 0 and
    PlayerAuraDuration("Lava Surge") >= GlobalCD() and
    TryCastSpellOnTarget("Lava Burst") or
    nil
end
local function HandleUnitFlameShock(unit)
  local duration = 21
  local pandemicThreshold = duration * 0.3
  if UnitAuraDuration(unit, "Flame Shock") < pandemicThreshold + GetCD("Flame Shock") then
    return TryCastSpellOnUnit("Flame Shock", unit)
  end
end
local function HandleTargetFlameShock()
  return HandleUnitFlameShock("target")
end
local function ToCombatCastUnit(unit)
  for k, combatCastUnit in pairs(UniqueExistingCombatCastUnits()) do
    if UnitGUID(unit) == UnitGUID(combatCastUnit) then
      return combatCastUnit
    end
  end
end
local function ChooseFlameShockUnit()
  local enemies = skill.GetEnemies(TargetAffectingCombat)
  for guid, unit in pairs(enemies) do
    enemies[guid] = ToCombatCastUnit(unit)
  end
  local minDuration = 1000
  local result
  for k, unit in pairs(enemies) do
    --ALERT("unit: "..tostring(unit))
    local newDuration = UnitAuraDuration(unit, "Flame Shock")
    result = newDuration < minDuration and unit or result
    minDuration = newDuration
    if minDuration == 0 then
      break
    end
  end
  return result
end
local function TryFocusUnit(unit)
  if UnitExists("focus") and UnitGUID(unit) == UnitGUID("focus") then
    return
  end
  if UnitGUID(unit) == UnitGUID("focus") then
    return
  end
  return
    UnitGUID(unit) == UnitGUID("focus")
end
local function HandleOtherFlameShock()
  local unit = ChooseFlameShockUnit()
  --ALERT("ChooseFlameShockUnit(): "..tostring(ChooseFlameShockUnit()))
  return unit and TryCastSpellOnUnit("Flame Shock", unit) or nil
end
local function HardCastLavaBurst()
  local remainingCd = GetChargeCDMax("Lava Burst") - GetChargeCD("Lava Burst")
  if GetCharges("Lava Burst") == 1 and (remainingCd > GlobalCD() + HastedSeconds(2.0)) then
    return
  end
  local cast_time = GetCastTime("Lava Burst")
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Spiritwalker's Grace") > GlobalCD() + cast_time) and
    TargetMyAuraDuration("Flame Shock") > GlobalCD() + cast_time and
    TryCastSpellOnTarget("Lava Burst") or
    nil
end
local function TryLightning()
  local enemies = skill.GetEnemies(UnitAffectingCombat)
  local numEnemies = skill.CountEnemies(enemies)
  local spell = numEnemies >= 3 and cleave_mode ~= "no_aoe" and "Chain Lightning" or "Lightning Bolt"
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Spiritwalker's Grace") > GlobalCD() + GetCastTime(spell)) and
    TryCastSpellOnTarget(spell) or nil
end

local function GetHealAmount(spell, unit)
  spell = ValidName(spell)
  if spell == "Riptide" then
    return 3500 -- * GetSpellBonusHealing() * VersatilityMultiplier() * DeepHealingMultiplier(unit)
  end
  return 0
end

local function GetHighestDeficitTarget()
  local highestDeficit = 0
  local target
  for i, unit in ipairs(UniqueExistingCombatCastUnits()) do
    local deficit = UnitExists(unit) and UnitIsFriend("player", unit) and UnitHealthAndHealAbsorbDeficit(unit) or 0
    if deficit > highestDeficit then
      highestDeficit = deficit
      target = unit
    end
  end
  return target or "player"
end
local function KeepRiptideOnCd()
  local remainingCd = GetChargeCDMax("Riptide") - GetChargeCD("Riptide")
  --for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
  --  local result =
  --end
  local unit = GetHighestDeficitTarget()
  return
    skill.IsUnitAlive(unit) and
    UnitHealthAndHealAbsorbDeficit(unit) > GetHealAmount("Riptide", unit) and
    (GetCharges("Riptide") >= 2 or remainingCd < GlobalCD()) and
    TryCastSpellOnUnit("Riptide", unit) or
    nil
end
local function KeepHealingStreamOnCd()
  local remainingCd = GetChargeCDMax("Healing Stream Totem") - GetChargeCD("Healing Stream Totem")
  return
    (GetCharges("Healing Stream Totem") >= 2 or remainingCd < GlobalCD()) and
    TryCastSpellOnPlayer("Healing Stream Totem") or nil
end

local function IsInGroup()
  return UnitExists("party1")
end
local function PredictedCastDuration(spell)
  return select(4, GetSpellInfo(spell)) / 1000
end
local function PredictedHealing(spell, target)
  if not target then
    return 0
  end
  --local cast_duration = select(4, GetSpellInfo(spell)) / 1000
  local CR_VERSATILITY = 29
  local amount = 0
  local spellpower = GetSpellBonusHealing()
  local deep_healing_multiplier = 1 + (1 - UnitHealthPercent(target)) / 35 * GetMasteryEffect() / 100
  local multiplier = (1 + GetCombatRating(CR_VERSATILITY) / 8500) * deep_healing_multiplier

  if spell == "Riptide" then
    multiplier = multiplier * (IsTalentSelected("Torrent") and 1.3 or 1)
    amount = 0.96725 * spellpower
  end
  if spell == "Healing Wave" then
    amount = 2.482 * spellpower
  end
  if spell == "Healing Surge" then
    amount = 2.292 * spellpower
  end
  return amount * multiplier
end

local function KeepTidalWavesUp()
  if IsInGroup() or PlayerHealth() < PlayerHealthMax() - PredictedHealing("Riptide", "player") then
    local spell, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
    local castRemaining = spell and endTimeMS / 1000 - GetTime() or 0
    if PlayerAuraDuration("Tidal Waves") < GlobalCD() + GlobalCDMax() + castRemaining then
      --ALERT("GetHighestDeficitTarget(): "..tostring(GetHighestDeficitTarget()))
      local unit = GetHighestDeficitTarget()
      return skill.IsUnitAlive(unit) and TryCastSpellOnUnit("Riptide", unit) or nil
    end
  end
  return nil
end
-- TODO: Move to shaman.lua.
local shaman = {}
function shaman.DoNothingGhostWolf()
  return
    PlayerAuraDuration("Ghost Wolf") > 0 and
    "nothing" or
    nil
end

local function DoDmg()
  if not UnitExists("target") then
    return nil
  end
  return
    HandleTargetLavaSurge() or
    HandleTargetFlameShock() or
    skill.TryUseConcentratedFlame() or
    HandleOtherFlameShock() or
    HardCastLavaBurst() or
    nil
end
function private.KeepUpWaterShield()
  return
    (PlayerAuraDuration("Water Shield") < 20 * 60 or PlayerAuraStacks("Water Shield") <= 1) and
    TryCastSpellOnPlayer("Water Shield") or
    nil
end
function private.HandleEarthShield()
  private.earth_shield_target = private.earth_shield_target or private.get_earth_shield_target()
  if private.earth_shield_target == "player" and PlayerMyAuraDuration("Lightning Shield") > 0 then
    private.earth_shield_target = nil
  end
  return
  private.earth_shield_target and
  UnitMyAuraDuration(private.earth_shield_target, "Earth Shield") < 2 * 60 and
  TryCastSpellOnUnit("Earth Shield", private.earth_shield_target) or
  nil
end

function private.KeepUpEarthShieldOnTank()
  local tank = skill.TryGetFirstTankUnit() or "party1"
  return
    tank and
    (UnitAuraDuration(tank, "Earth Shield") < 2 * 60 or UnitAuraDuration(tank, "Earth Shield") <= 1) and
    TryCastSpellOnUnit("Earth Shield", tank) or
    nil
end

function private.TryChainHealIfWorth()
  local max_heal = HealOfSpell("Chain Heal")
  local sum_deficit = 0
  local max_deficit = 0
  local max_deficit_unit
  for _, unit in ipairs(GroupCombatCastUnits()) do
    local deficit = min(max_heal, UnitHealthDeficit(unit))
    sum_deficit = sum_deficit + deficit
    if deficit > max_deficit then
      max_deficit = deficit
      max_deficit_unit = unit
    end
  end
  return
    max_deficit_unit and
    sum_deficit > max_heal * 2 and
    (not IsPlayerMoving() or PlayerAuraDuration("Spiritwalker's Grace") > GlobalCD() + GetCastTime("Chain Heal")) and
    PlayerAuraDuration("Flash Flood") > GlobalCD() and
    skill.TryHealUnit("Chain Heal", max_deficit_unit, 0.8, 0.5) or
    nil
end
function private.NoOverhealHealingWave(unit)
  return
    (not IsPlayerMoving() or PlayerAuraDuration("Spiritwalker's Grace") > GlobalCD() + GetCastTime("Healing Wave")) and
    skill.TryHealUnit("Healing Wave", unit, 0.95, 0.0) or
    nil
end
function private.HandleMinimalHealing()
  local tank = skill.TryGetFirstTankUnit()
  return
    private.TryChainHealIfWorth() or
    (tank and skill.TryHealUnit("Healing Surge", tank, 0.7, 0.0) or nil) or
    (tank and skill.TryHealUnit("Healing Wave", tank, 0.9, 0.0) or nil) or
    ForUniqueFilteredCombatCastUnits(private.NoOverhealHealingWave, UnitExists, skill.IsUnitAlive, skill.IsUnitPlayerFriend) or
    nil
end
function private.OutOfCombatUpkeep()
  if UnitAffectingCombat("player") then
    return
  end
  return
    (IsPlayerMoving() and PlayerAuraDuration("Ghost Wolf") <= 0 and TryCastSpellOnPlayer("Ghost Wolf") or nil) or
    (IsInInstance() and KeepTidalWavesUp() or nil) or
    private.KeepUpWaterShield() or
    private.KeepUpEarthShieldOnTank() or
    private.KeepUpFlametongueWeapon() or
    nil
end
function private.KeepUpFlametongueWeapon()
  local _, duration = GetWeaponEnchantInfo()
  duration = duration and duration / 1000 or 0
  return
  duration < 20 * 60 and
  TryCastSpellOnPlayer("Flametongue Weapon") or
  nil
end

private.priorities = {}
function private.priorities.default()
  return
    skill.DoNothingIfMounted() or
    skill.DoNothingIfCasting() or
    private.OutOfCombatUpkeep() or
    skill.common_priority_start() or
    shaman.DoNothingGhostWolf() or

    (PlayerManaPercent() < 0.3 and private.KeepUpWaterShield()) or
    KeepTidalWavesUp() or
    TryDispelAnythingFromParty() or
    skill.TryPurgeAnythingFromTarget() or
    private.KeepUpEarthShieldOnTank() or
    private.HandleMinimalHealing() or

    skill.AcquireTarget() or
    skill.StartAttack() or
    DoDmg() or

    KeepRiptideOnCd() or
    KeepHealingStreamOnCd() or

    private.KeepUpWaterShield() or
    TryLightning() or

    "nothing"
end
function private.priorities.test()
  return
  TryCastQueuedSpell() or
  TryCastSpellOnPlayer("Healing Wave") or
  "nothing"
end

local function range(spell)
end
local module = {
  ["spell"] = {
    ["range"] = range,
    ["purge"] = {
      ["Purge"] = {
        "Magic",
      },
    },
    ["dispel"] = {
      ["Purify Spirit"] = {
        "Magic",
        "Curse",
      },
    },
  },
  ["interrupts"] = {
    "Wind Shear",
  },
  ["save_interrupt_for"]= {
  },
  ["range_override"] = {
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["range_override"] = {
    ["Power Siphon"] = 100,
    ["Felstorm"] = 100,
  },
}
function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.shaman.restoration = module
