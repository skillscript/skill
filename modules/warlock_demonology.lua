_, skill = ...

local function IsOffGlobal(name)
  return false
end

local function IsStun(name)
  return name == "Axe Toss"
end

local function IsDisorient(name)
  return false
end

local function IsIncapacitate(name)
  return false
end

local function IsSilence(name)
  return false
end

local function IsInterrupt(name)
  return false
end

local function DefaultOffGlobal()
  return
    "nothing"
end

local function IsFelGuardSummoned()
  return UnitExists("pet") and UnitName("pet") == "Shaashak"
end
function GetWildImps()
  return UnitPower("player", Enum.PowerType.WildImps)
end
local function GetSoulShards()
  local spell, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
  local currentSoulShards = UnitPower("player", Enum.PowerType.SoulShards)
  local adjustment = 0
  if spell == "Hand of Gul'dan" then
    adjustment = -min(currentSoulShards, 3)
  elseif spell == "Demonbolt" then
    adjustment = 2
  elseif spell == "Shadow Bolt" then
    adjustment = 1
  end
  return currentSoulShards + adjustment
end
local function TryCallDreadstalkers()
  local spell = "Call Dreadstalkers"
  return
    GetSoulShards() >= 2 and
    GetCD(spell) <= GlobalCD() and
    TryCastSpellOnTarget(spell) or nil
end
local function TryGrimoireFelguard()
  local spell = "Grimoire: Felguard"
  return
    GetSoulShards() >= 1 and
    IsTalentSelected(spell) and
    GetCD(spell) <= GlobalCD() and
    TryCastSpellOnTarget(spell) or nil
end
local function TryDemonicStrength()
  local spell = "Demonic Strength"
  return
    IsFelGuardSummoned() and
    GetCD(spell) <= GlobalCD() and
    UnitAuraDuration("pet", "Felstorm") <= 0 and
    TryCastSpellOnTarget(spell) or nil
end
local function TryFelstorm()
  local spell = "Felstorm"
  return
    IsFelGuardSummoned() and
    GetCD(spell) <= GlobalCD() and
    UnitAuraDuration("pet", spell) <= 0 and
    GetCD("Demonic Strength") > 7 and
    TryCastSpellOnPlayer(spell) or nil
end
local function TrySoulStrike()
  local spell = "Soul Strike"
  return
    IsFelGuardSummoned() and
    GetSoulShards() < 5 and
    IsTalentSelected(spell) and
    GetCD(spell) <= GlobalCD() and
    TryCastSpellOnTarget(spell) or
    TryCastSpellOnTarget("Command Demon") or
    nil
end
local function TrySummonVilefiend()
  local spell = "Summon Vilefiend"
  return
    GetSoulShards() >= 1 and
    IsTalentSelected(spell) and
    GetCD(spell) <= GlobalCD() and
    TryCastSpellOnTarget(spell) or nil
end
local function TryHandOfGulDan(soulShards)
  local spell = "Hand of Gul'dan"
  return
    GetSoulShards() == soulShards and
    TryCastSpellOnTarget(spell) or nil
end
local function TryDemonbolt()
  local spell = "Demonbolt"
  return
    GetSoulShards() <= 3 and
    PlayerAuraDuration("Demonic Core") > GlobalCD() and
    PlayerAuraStacks("Demonic Core") >= 2 and
    TryCastSpellOnTarget(spell) or nil
end
local function TryPowerSiphon()
  local spell = "Power Siphon"
  return
    IsTalentSelected(spell) and
    PlayerAuraStacks("Demonic Core") <= 2 and
    zPets.NumPetTypeActive("Wild Imp") >= 2 and
    GetCD(spell) <= GlobalCD() and
    TryCastSpellOnTarget(spell) or nil
end
local function TryShadowBolt()
  local spell = "Shadow Bolt"
  return
    not IsPlayerMoving() and
    TryCastSpellOnTarget(spell) or nil
end
local function UseUpDemonicCore()
  return
    PlayerAuraDuration("Demonic Core") > 0 and
    PlayerAuraDuration("Demonic Core") < GlobalCD() + GlobalCDMax() and
    TryCastSpellOnTarget("Demonbolt") or nil
end
local function TryMovingDemonbolt()
  local spell = "Demonbolt"
  return
    GetSoulShards() <= 3 and
    PlayerAuraDuration("Demonic Core") > GlobalCD() and
    IsPlayerMoving() and
    TryCastSpellOnTarget(spell) or nil
end
local function TryGetSoulShards(minNumSoulShards)
  if GetSoulShards() >= minNumSoulShards then
    return
  end
  return
    PlayerAuraDuration("Demonic Core") > GlobalCD() and
    TryCastSpellOnTarget("Demonbolt") or
    TryCastSpellOnTarget("Shadow Bolt") or
    nil
end
local function TrySummonFelguard()
  local spell, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
  if (UnitExists("pet") and UnitHealthPercent("pet") > 0) or IsMounted() or spell == "Summon Felguard" then
    return nil
  end
  return
    TryGetSoulShards(1) or
    TryCastSpellOnPlayer("Summon Felguard") or nil
end

local function TryResummonLowHpFelguard()
  return
    UnitHealthPercent("pet") < 0.05 and
    GetSoulShards() >= 1 and
    TrySummonFelguard() or nil
end

local function HandleHealing()
  return
    not IsPlayerMoving() and
    PlayerHealthPercent() < 0.5 and
    TryCastSpellOnTarget("Drain Life") or
    nil
end
local function HandleHealthFunnel()
  return
    not IsPlayerMoving() and
    UnitHealthPercent("pet") < 0.6 and
    TryCastSpellOnUnit("Health Funnel", "pet") or
    nil
end

local function HandleLowLevelHealing()
  return
  not IsPlayerMoving() and
  PlayerHealthPercent() < 0.5 and
  TryCastSpellOnTarget("Drain Life") or
  nil
end
local function initiateLowLevelCombatWithSpell(spell)
  local target_name = UnitName("target")
  if not UnitAffectingCombat("target") and (target_name == "Blackrock Enforcer" or target_name == "Iron Dockworker" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") then
    return TryCastSpellOnTarget(spell)
  end
  return nil
end
local private = {}
private.priorities = {}
function private.priorities.low_level_farm()
  return
  HandleLowLevelHealing() or
  (not UnitAffectingCombat("player") and skill.AcquireTarget()) or
  (skill.UnitIsBoss("target") and skill.IsUnitAlive("target") and not skill.IsUnitPlayerFriend("target") and (TryCastSpellOnTarget("Summon Demonic Tyrant") or skill.TryUseConcentratedFlame())) or
  initiateLowLevelCombatWithSpell("Soul Strike") or
  skill.DoNothingOutOfCombat() or
  skill.StartAttack() or
  skill.TryInterruptTarget() or
  TryCastQueuedSpell() or
  (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or
  --(skill.IsUnitBoss("target") and (TryCastSpellOnPlayer("Time Warp") or TryCastSpellOnPlayer("Combustion"))) or

  private.priorities.default() or
  "nothing"
end
function private.priorities.default()
  return
    torghast.torghast_stuff() or
    skill.common_priority_start() or
    TrySummonFelguard() or
    skill.DoNothingIfCasting() or

    HandleHealing() or
    HandleHealthFunnel() or
    (not IsPlayerMoving() and TryCastSpellOnTarget("Scouring Tithe") or nil) or
    TryCallDreadstalkers() or
    TryGrimoireFelguard() or
    TryDemonicStrength() or
    TryFelstorm() or
    TrySoulStrike() or
    TrySummonVilefiend() or
    TryHandOfGulDan(5) or
    TryHandOfGulDan(4) or
    TryDemonbolt() or
    TryPowerSiphon() or
    TryHandOfGulDan(3) or

    UseUpDemonicCore() or

    TryResummonLowHpFelguard() or
    TryShadowBolt() or
    TryMovingDemonbolt() or
    "nothing"
end

local function uhgps(gui)
  gui.string:SetText(string.format("%.0f", UnitHealthGainedPerSecond("player", 6)))
end
local function uhlps(gui)
  gui.string:SetText(string.format("%.0f", UnitHealthLostPerSecond("player", 6)))
end
local GUI = {
  { ["label"] = "UnitHealthGainedPerSecond:", ["text"] = "0", ["update"] = uhgps, ["click"] = nil, ["init"] = nil },
  { ["label"] = "UnitHealthLostPerSecond:", ["text"] = "0", ["update"] = uhlps, ["click"] = nil, ["init"] = nil },
}
local module = {
  ["run"] = run,
  ["gui"] = GUI,
  ["interrupts"] = {},
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["extra_spells"] = {
    "Scouring Tithe",
  },
}

function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end

skill.modules.warlock.demonology = module