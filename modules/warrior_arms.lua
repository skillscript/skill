_, skill = ...

local function IsOffGlobal(name)
  return
    name == "Will to Survive" or
    name == "Healthstone" or
    name == "Pummel"
end
local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function KeepBattleShoutUpOnParty()
  if IsMounted() then
    return nil
  end
  for _, unit in ipairs(PARTY) do
    if string.find(unit, "pet") == nil and UnitAuraDuration(unit, "Battle Shout") <= 10 * 60 and IsUnitInRange("Battle Shout", unit) then
      return TryCastSpellOnPlayer("Battle Shout") or nil
    end
  end
  return nil
end

local function IsStun(name)
end

local function IsDisorient(name)
end

local function IsIncapacitate(name)
end

local function IsSilence(name)
end

local function IsInterrupt(name)
  return name == "Pummel"
end

local function DoNothingIfMounted()
  return
    IsMounted() and
    "nothing" or nil
end

local function DoNothingIfCasting()
  local spell = UnitCastingInfo("player")
  return
    spell and
    "nothing" or nil
end


local function DefaultOffGlobal()
  local health_percent = PlayerHealthPercent()
  return
--    --OffGlobalCD() > 0 and "nothing" or
--    DoNothingIfMounted() or
--    skill.DoNothingOutOfCombat() or
--    TryBattleCry() or
--    health_percent < 0.4 and TryOnSelf("Enraged Regeneration") or
    "nothing"
end

local function RageCost(name)
  return
    name == "Mortal Strike" and 30 or
    0
end
local function GetRage()
  return UnitPower("player", Enum.PowerType.Rage)
end

local function DontClipMs()
  return
    GetCD("Mortal Strike") > GlobalCD() and
    GetCD("Mortal Strike") < GlobalCDMax() * 0.75
    and "nothing"
end
local function TargetTimeToLive()
  local oldest_changed_time = UnitOldestHealthChangedTime("target", 15)
  local incoming_dps = UnitTimeToLiveByHealthChanged("target", 5)
  local is_worth = oldest_changed_time > 5 and incoming_dps > 20
  return is_worth and (UnitHealth("target") / incoming_dps) or 0
end
local function HandleSweepingStrikes()
  return
    skill.NumEnemiesWithin(8) > 1 and
    TryCastSpellOnTarget("Sweeping Strikes") or
    nil
end
local function HandleColossusSmash()
  return
    TryCastSpellOnTarget(IsTalentSelected("Warbreaker") and "Warbreaker" or "Colossus Smash") or
    nil
end
local function HandleAvatar()
  return
    TargetMyAuraDuration("Colossus Smash") > 6 and
    IsUnitInRange("Mortal Strike", "target") and
    TryCastSpellOnPlayer("Avatar") or
    nil
end
local function TryCondemn()
  return
    skill.IsVenthyr() and
    TryCastSpellOnTarget("Execute") or
    nil
end
local function HandleBladestorm()
  return
    (TargetMyAuraDuration("Colossus Smash") > 6 and PlayerAuraDuration("Bladestorm") <= 0) and
    IsUnitInRange("Mortal Strike", "target") and
    TryCastSpellOnPlayer("Bladestorm") or
    nil
end
local function HandleRavager()
  return
    (TargetMyAuraDuration("Colossus Smash") > 6 and PlayerAuraDuration("Bladestorm") <= 0) and
    IsUnitInRange("Mortal Strike", "target") and
    TryCastSpellOnPlayer("Ravager") or
    nil
end
local function TrySuddenDeathExecute()
  return
    PlayerAuraDuration("Sudden Death") > 0 and
    TryCastSpellOnTarget("Execute") or
    nil
end
local function TryMortalStrike()
  return
    GetRage() >= 30 and
    TryCastSpellOnTarget("Mortal Strike") or
    nil
end
local function TrySlam()
  return
    TryCastSpellOnTarget("Slam") or
    nil
end
local function TryOverpower()
  return
    PlayerAuraDuration("Overpower!") <= 0 and
    TryCastSpellOnTarget("Overpower") or
    nil
end
local function TrySlamOrWhirlwind()
  if cleave_mode == "no_aoe" then
    return "Slam"
  end
  if cleave_mode == "aoe" then
    return "Whirlwind"
  end
  local fob = IsTalentSelected("Fervor of Battle")
  return
    GetRage() >= (fob and 30 or 20) + RageCost("Mortal Strike") and
    TryCastSpellOnTarget(fob and "Whirlwind" or "Slam") or
    nil
end

local function ExecuteRotation()

end

local function TryImpendingVictory()
  return
    PlayerHealthPercent() < 0.25 and
    TryCastSpellOnTarget("Impending Victory") or
    nil
end

local private = {}
private.priorities = {}
function private.priorities.default()
  return
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty()) or
    skill.common_priority_start() or
    skill.StartAttack() or
    skill.AcquireTarget() or

    HandleColossusSmash() or
    TrySuddenDeathExecute() or
    TryMortalStrike() or
    DontClipMs() or
    --TryBladestorm() or
    TryOverpower() or
    TryImpendingVictory() or
    KeepBattleShoutUp() or
    TrySlamOrWhirlwind() or
    KeepBattleShoutUpOnParty() or
    "nothing"
end

local function TryAvatar()
  if not skill_char_settings["use_avatar"] then
    return nil
  end
  if not IsUnitInRange("Mortal Strike", "target") then
    return nil
  end
  return
    skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    TryCastSpellOnPlayer("Avatar") or
    nil
end
local function TryRavager()
  return
    IsTalentSelected("Ravager") and
    IsUnitInRange("Mortal Strike", "target") and
    TryCastSpellOnPlayer("Ravager") or
    nil
end
local function TryDeadlyCalm()
  return
    IsTalentSelected("Deadly Calm") and
    IsUnitInRange("Mortal Strike", "target") and
    TryCastSpellOnPlayer("Deadly Calm") or
    nil
end
local function TrySkullsplitter()
  return
    IsTalentSelected("Skullsplitter") and
    TryCastSpellOnTarget("Skullsplitter") or
    nil
end
local function TryRend()
  return
    IsTalentSelected("Rend") and
    TryCastSpellOnTarget("Rend") or
    nil
end
local function TryCleave()
  return
    IsTalentSelected("Cleave") and
    TryCastSpellOnTarget("Cleave") or
    nil
end
local function TryWarbreaker()
  return
    IsTalentSelected("Warbreaker") and
    TryCastSpellOnTarget("Warbreaker") or
    nil
end
local function TryColossusSmash()
  return
    not IsTalentSelected("Warbreaker") and
    TryCastSpellOnTarget("Colossus Smash") or
    nil
end
local function TryBladestorm()
  if not skill_char_settings["use_bladestorm"] then
    return nil
  end
  if IsTalentSelected("Ravager") or not IsUnitInRange("Mortal Strike", "target") then
    return nil
  end
  return
    skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    TryCastSpellOnPlayer("Bladestorm") or
    nil
end
local function simcraft_execute()
  --deadly_calm
  --rend,if=remains<=duration*0.3
  --skullsplitter,if=rage<60&(!talent.deadly_calm.enabled|buff.deadly_calm.down)
  --avatar,if=cooldown.colossus_smash.remains<8&gcd.remains=0
  --ravager,if=buff.avatar.remains<18&!dot.ravager.remains
  --cleave,if=spell_targets.whirlwind>1&dot.deep_wounds.remains<gcd
  --warbreaker
  --colossus_smash
  --ondemn,if=debuff.colossus_smash.up|buff.sudden_death.react|rage>65
  --overpower,if=charges=2
  --bladestorm,if=buff.deadly_calm.down&rage<50
  --mortal_strike,if=dot.deep_wounds.remains<=gcd
  --skullsplitter,if=rage<40
  --overpower
  --condemn
  --execute
  return
    TryDeadlyCalm() or
    (TargetMyAuraDuration("Rend") < 5 and TryRend() or nil) or
    (GetRage() < 60 and (PlayerAuraDuration("Deadly Calm") <= 0 or not IsTalentSelected("Deadly Calm")) and TrySkullsplitter() or nil) or
    (GetCD("Colossus Smash") < 8 and GlobalCD() <= 0 and TryAvatar() or nil) or
    (PlayerAuraDuration("Avatar") > 0 and PlayerAuraDuration("Avatar") < 18 and TryRavager() or nil) or
    (TargetMyAuraDuration("Deep Wounds") < GlobalCDMax() and skill.NumEnemiesWithin(8) > 1 and TryCleave() or nil) or
    TryWarbreaker() or
    TryColossusSmash() or
    ((GetRage() > 65 or TargetMyAuraDuration("Colossus Smash") > GlobalCD()) and TryCondemn() or nil) or
    (PlayerAuraDuration("Overpower") >= 2 and TryCastSpellOnTarget("Overpower") or nil) or
    (PlayerAuraDuration("Deadly Calm") <= 0 and GetRage() < 50 and TryBladestorm() or nil) or
    (GetRage() < 40 and TrySkullsplitter() or nil) or
    TryCastSpellOnTarget("Overpower") or
    TryCondemn() or
    TryCastSpellOnTarget("Execute") or
    nil
end
local function simcraft_hac()
  --skullsplitter,if=rage<60&buff.deadly_calm.down
  --avatar,if=cooldown.colossus_smash.remains<1
  --cleave,if=dot.deep_wounds.remains<=gcd
  --warbreaker
  --bladestorm
  --ravager
  --colossus_smash
  --rend,if=remains<=duration*0.3&buff.sweeping_strikes.up
  --cleave
  --mortal_strike,if=buff.sweeping_strikes.up|dot.deep_wounds.remains<gcd&!talent.cleave.enabled
  --overpower,if=talent.dreadnaught.enabled
  --condemn
  --execute,if=buff.sweeping_strikes.up
  --overpower
  --whirlwind
  return
    (GetRage() < 60 and PlayerAuraDuration("Deadly Calm") <= 0 and TrySkullsplitter() or nil) or
    (GetCD("Colossus Smash") < 1 and TryAvatar() or nil) or
    (TargetMyAuraDuration("Deep Wounds") < GlobalCDMax() and TryCleave() or nil) or
    TryWarbreaker() or
    TryBladestorm() or
    TryRavager() or
    TryColossusSmash() or
    (TargetMyAuraDuration("Rend") < 5 and PlayerAuraDuration("Sweeping Strikes") > 0 and TryRend() or nil) or
    TryCleave() or
    ((PlayerAuraDuration("Sweeping Strikes") > 0 or (TargetMyAuraDuration("Deep Wounds") < GlobalCDMax() and not IsTalentSelected("Cleave"))) and TryCastSpellOnTarget("Mortal Strike") or nil) or
    (IsTalentSelected("Dreadnaught") and TryCastSpellOnTarget("Overpower") or nil) or
    TryCondemn() or
    (PlayerAuraDuration("Sweeping Strikes") > 0 and simcraft_execute() or nil) or
    TryCastSpellOnTarget("Overpower") or
    TryCastSpellOnTarget("Whirlwind") or
    nil
end
local function simcraft_single_target()
  --avatar,if=cooldown.colossus_smash.remains<8&gcd.remains=0
  --rend,if=remains<=duration*0.3
  --cleave,if=spell_targets.whirlwind>1&dot.deep_wounds.remains<gcd
  --warbreaker
  --colossus_smash
  --ravager,if=buff.avatar.remains<18&!dot.ravager.remains
  --overpower,if=charges=2
  --bladestorm,if=buff.deadly_calm.down&(debuff.colossus_smash.up&rage<30|rage<70)
  --mortal_strike,if=buff.overpower.stack>=2&buff.deadly_calm.down|(dot.deep_wounds.remains<=gcd&cooldown.colossus_smash.remains>gcd)
  --deadly_calm
  --skullsplitter,if=rage<60&buff.deadly_calm.down
  --overpower
  --condemn,if=buff.sudden_death.react
  --execute,if=buff.sudden_death.react
  --mortal_strike
  --whirlwind,if=talent.fervor_of_battle.enabled&rage>60
  --slam
  return
    (GetCD("Colossus Smash") < 8 and GlobalCD() <= 0 and TryAvatar() or nil) or
    (TargetMyAuraDuration("Rend") < 5 and TryRend() or nil) or
    (skill.NumEnemiesWithin(8) > 1 and TargetMyAuraDuration("Deep Wounds") < GlobalCDMax() and TryCleave() or nil) or
    TryWarbreaker() or
    TryColossusSmash() or
    (PlayerAuraDuration("Avatar") > 0 and PlayerAuraDuration("Avatar") < 18 and TryRavager() or nil) or
    (GetCharges("Overpower") == 2 and TryCastSpellOnTarget("Overpower") or nil) or
    (PlayerAuraDuration("Deadly Calm") <= 0 and (TargetMyAuraDuration("Colossus Smash") > 0 and GetRage() < 30 or GetRage() < 70) and TryBladestorm() or nil) or
    (((PlayerAuraDuration("Overpower") >= 2 and PlayerAuraDuration("Deadly Calm") <= 0) or (TargetMyAuraDuration("Deep Wounds") < GlobalCDMax() and GetCD("Colossus Smash") > GlobalCD())) and TryCastSpellOnTarget("Mortal Strike") or nil) or
    TryDeadlyCalm() or
    (GetRage() < 60 and PlayerAuraDuration("Deadly Calm") <= 0 and TrySkullsplitter() or nil) or
    TryCastSpellOnTarget("Overpower") or
    TryCondemn() or
    TryCastSpellOnTarget("Execute") or
    TryCastSpellOnTarget("Mortal Strike") or
    (IsTalentSelected("Fervor of Battle") and GetRage() > 60 and TryCastSpellOnTarget("Whirlwind")) or
    TryCastSpellOnTarget("Slam") or
    nil
end
local function HandleDroppingVictoryRush()
  return
  PlayerAuraDuration("Victory Rush") > GlobalCD() and
  PlayerAuraDuration("Victory Rush") < GlobalCD() + GlobalCDMax() and
  PlayerHealthPercent() < 0.8 and
  TryCastSpellOnTarget("Victory Rush") or nil
end
local function HandleReadyImpendingVictory()
  return
  IsTalentSelected("Impending Victory") and
  PlayerHealthPercent() < 0.7 and
  TryCastSpellOnTarget("Impending Victory") or nil
end
local function HandleHealing()
  return
    HandleDroppingVictoryRush() or
    HandleReadyImpendingVictory() or
    (UnitAuraStacks("player", "Tarnished Medallion", "MAW") > 0 and PlayerHealthPercent() < 0.8 and TryCastSpellOnPlayer("Berserker Rage") or nil) or
    nil
end
function IgnorePainAbsorb()
  return UnitAuraAbsorb("player", "Ignore Pain")
end
local function HandleIgnorePain()
  return
  PlayerHealthPercent() < 0.7 and
  (skill.NumEnemiesWithin(8) >= 5 or UnitClassification("target") == "elite") and
  TryCastSpellOnPlayer("Ignore Pain") or
  nil
end
function private.priorities.simcraft()
  --sweeping_strikes,if=spell_targets.whirlwind>1&(cooldown.bladestorm.remains>15|talent.ravager.enabled)
  --run_action_list,name=hac,if=raid_event.adds.exists
  --run_action_list,name=execute,if=(talent.massacre.enabled&target.health.pct<35)|target.health.pct<20|(target.health.pct>80&covenant.venthyr)
  --run_action_list,name=single_target
  return
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or

    skill.common_priority_start() or
    (PlayerAuraDuration("Spell Reflection") <= (select(2, TargetCast()) or 0) and skill.TryInterruptTarget() or nil) or
    skill.TryUseHealthstone(0.33) or
    HandleHealing() or
    HandleIgnorePain() or
    (PlayerAuraDuration("Battle Shout") <= 0 and TryCastSpellOnPlayer("Battle Shout") or nil) or
    skill.StartAttack() or
    skill.AcquireTarget() or

    --skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    ((IsTalentSelected("Ravager") or GetCD("Bladestorm") > 15) and skill.NumEnemiesWithin(8) > 1 and TryCastSpellOnPlayer("Sweeping Strikes") or nil) or

    (UnitAuraStacks("player", "Resonant Throatbands", "MAW") > 0 and not torghast.IsTargetMawrat() and skill.NumEnemiesWithin(5) > 0 and TryCastSpellOnPlayer("Piercing Howl") or nil) or
    (UnitAuraStacks("player", "Resonant Throatbands", "MAW") > 0 and not torghast.IsTargetMawrat() and UnitAuraStacks("player", "Umbral Ear Trumpet", "MAW") > 0 and TargetMyAuraDuration("Resonant Throatbands") <= 0 and skill.NumEnemiesWithin(10) > 0 and TryCastSpellOnPlayer("Battle Shout") or nil) or

    (skill.NumEnemiesWithin(8) >= 5 and simcraft_hac() or nil) or
    ((skill.IsVenthyr() and TargetHealthPercent() > 0.8 or TargetHealthPercent() < 0.2 or IsTalentSelected("Massacre") and TargetHealthPercent() < 0.35) and simcraft_execute() or nil) or
    simcraft_single_target() or
    KeepBattleShoutUpOnParty() or
    "nothing"
end

local function range(spell)
  if spell == "Recklessness" then
    return 2
  elseif spell == "Whirlwind" then
    return 2
  elseif spell == "Warbreaker" then
    return 2
  elseif spell == "Battle Shout" then
    return 50
  elseif spell == "Execute" then
    return 2
  end
  return nil
end
local module = {
  ["spell"] = {
    ["range"] = range,
  },
  ["interrupts"] = {
    "Pummel",
  },
  ["extra_spells"] = {
    "Execute",
  },
  ["save_interrupt_for"]= {},
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}

skill_char_settings = skill_char_settings or {}

function private.init_use_avatar(gui)
  skill_char_settings["use_avatar"] = skill_char_settings["use_avatar"] or true
  gui.string:SetText(tostring(skill_char_settings["use_avatar"]))
end
function private.toggle_use_avatar(gui)
  skill_char_settings["use_avatar"] = not skill_char_settings["use_avatar"]
  gui.string:SetText(tostring(skill_char_settings["use_avatar"]))
end
private.toggle_use_avatar_gui = { ["label"] = "Use Avatar:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_use_avatar, ["init"] = private.init_use_avatar }

function private.init_use_bladestorm(gui)
  skill_char_settings["use_bladestorm"] = skill_char_settings["use_bladestorm"] or true
  gui.string:SetText(tostring(skill_char_settings["use_bladestorm"]))
end
function private.toggle_use_bladestorm(gui, button)
  skill_char_settings["use_bladestorm"] = not skill_char_settings["use_bladestorm"]
  gui.string:SetText(tostring(skill_char_settings["use_bladestorm"]))
end
private.toggle_use_bladestorm_gui = { ["label"] = "Use Bladestorm:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_use_bladestorm, ["init"] = private.init_use_bladestorm }


function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
  AddGuiElement(private.toggle_use_avatar_gui)
  AddGuiElement(private.toggle_use_bladestorm_gui)
end
function module.unload()
end

skill.modules.warrior.arms = module
