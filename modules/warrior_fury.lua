_, skill = ...
local private = {}

local HEALTH_HISTOGRAM_STREAMS = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local damage_events = {}

local function IsOffGlobal(name)
  return
    name == "Enraged Regeneration" or
    name == "Will to Survive" or
    name == "Healthstone" or
    name == "Berserker Rage" or
    name == "Charge" or
    name == "Heroic Leap" or
    name == "Spell Reflection" or
    name == "Pummel"
end

local function IsStun(name)
end

local function IsDisorient(name)
end

local function IsIncapacitate(name)
end

local function IsSilence(name)
end

local function IsInterrupt(name)
  return name == "Pummel"
end
local function IsReflect(name)
  return name == "Spell Reflection"
end


local function GetRage()
  return UnitPower("player", SPELL_POWER_RAGE)
end


local function UseTrinkets()
  return
    nil
end
local function HandleRampage()
  return
    (GetRage() > 90 or PlayerAuraDuration("Enrage") <= 0) and
    TryCastSpellOnTarget("Rampage") or nil
end
local function HandleSiegebreaker()
  return
    IsTalentSelected("Siegebreaker") and
    (PlayerAuraDuration("Recklessness") > GlobalCD() + 10 or GetCD("Recklessness") > GlobalCD() + GetCDMax("Siegebreaker")) and
    TryCastSpellOnTarget("Siegebreaker") or nil
end
local function TryDragonRoar()
  return
    IsTalentSelected("Dragon Roar") and
    TryCastSpellOnTarget("Dragon Roar") or nil
end
local function TryOnslaught()
  return
    IsTalentSelected("Onslaught") and
    TryCastSpellOnTarget("Onslaught") or nil
end
local function TrySiegebreaker()
  return
    IsTalentSelected("Siegebreaker") and
    TryCastSpellOnTarget("Siegebreaker") or nil
end
local function TryBladestorm()
  if not skill_char_settings["use_bladestorm"] then
    return nil
  end
  return
    skill.TryUseAnyEquippedTrinket() or
    TryCastSpellOnTarget("Bladestorm") or
    nil
end
local function TryWhirlwind()
  return
    TryCastSpellOnTarget("Whirlwind") or nil
end
local function TryRagingBlow()
  return
    TryCastSpellOnTarget("Raging Blow") or nil
end
local function TryBloodthirst()
  return
    TryCastSpellOnTarget("Bloodthirst") or nil
end
local function TryRampage()
  return
    TryCastSpellOnTarget("Rampage") or nil
end
local function TryCondemn()
  return
    skill.IsVenthyr() and
    TryCastSpellOnTarget("Execute") or nil
end
local function TryExecute()
  return
    not skill.IsVenthyr() and
    TryCastSpellOnTarget("Execute") or nil
end
local function HandleRecklessness()
  --local ttl = (skill.UnitTimeToLive("target", 2) + skill.UnitTimeToLive("target", 4) + skill.UnitTimeToLive("target", 8) + skill.UnitTimeToLive("target", 16)) / 4
  return
    (skill.NumEnemiesWithin(8) >= 5 or (UnitClassification("target") == "elite" and skill.NumEnemiesWithin(8) > 0)) and
    --ttl > 14 and
    TryCastSpellOnPlayer("Recklessness") or nil
end
function private.TryCondemn(unit)
  unit = unit or "target"
  return
    skill.IsVenthyr() and
    TryCastSpellOnUnit("Execute", unit) or
    nil
end
local function HandleDragonRoar()
  return
    IsTalentSelected("Dragon Roar") and
    PlayerAuraDuration("Enrage") > GlobalCD() and
    TryCastSpellOnPlayer("Dragon Roar") or
    nil
end
local function HandleDroppingVictoryRush()
  return
    PlayerAuraDuration("Victory Rush") > GlobalCD() and
    PlayerAuraDuration("Victory Rush") < GlobalCD() + GlobalCDMax() and
    PlayerHealthPercent() < 0.8 and
    TryCastSpellOnTarget("Victory Rush") or nil
end
local function HandleReadyImpendingVictory()
  return
    IsTalentSelected("Impending Victory") and
    PlayerHealthPercent() < 0.7 and
    TryCastSpellOnTarget("Impending Victory") or nil
end
local function HandleEnragedRegeneration()
  return
    PlayerHealthPercent() < 0.5 and
    TryCastSpellOnPlayer("Enraged Regeneration") or nil
end
function private.HandleCommandingShout()
  for _, unit in ipairs(HighestDeficitPercentGroupUnits()) do
    if UnitHealthPercent(unit) > 0.2 then
      return nil
    end
    local result =
      IsUnitInRange("Rallying Cry", unit) and
      TryCastSpellOnPlayer("Rallying Cry") or
      nil
    if nil then
      return result
    end
  end
  return nil
end
local function HandleHealing()
  local unit = HighestDeficitPercentGroupUnits()[1]
  return
    --private.HandleCommandingShout() or
    HandleDroppingVictoryRush() or
    (PlayerAuraDuration("Enraged Regeneration") > GlobalCD() and PlayerHealthPercent() < 0.8 and TryCastSpellOnTarget("Bloodthirst") or nil) or
    HandleEnragedRegeneration() or
    HandleReadyImpendingVictory() or
    (UnitAuraStacks("player", "Tarnished Medallion", "MAW") > 0 and PlayerHealthPercent() < 0.8 and TryCastSpellOnPlayer("Berserker Rage") or nil) or
    (PlayerAuraDuration("Enraged Regeneration") <= 0 and UnitAuraStacks("player", "Unbound Fortitude", "MAW") > 0 and (8  * (1 + PlayerAuraStacks("Soul Remnant's Blessing") * 0.2)) > (3 * 60 * (UnitAuraStacks("player", "Warlord's Resolve", "MAW") > 0 and 1/7.5 or 1)) and PlayerHealthPercent() < 0.8 and TryCastSpellOnPlayer("Enraged Regeneration") or nil) or
    nil
end

function IgnorePainAbsorb()
  return UnitAuraAbsorb("player", "Ignore Pain")
end
local function HandleIgnorePain()
  return
    PlayerHealthPercent() < 0.7 and
    (skill.NumEnemiesWithin(8) >= 5 or UnitClassification("target") == "elite") and
    TryCastSpellOnPlayer("Ignore Pain") or
    nil
end

local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function KeepBattleShoutUpOnParty()
  if IsMounted() then
    return nil
  end
  for _, unit in ipairs(PARTY) do
    if string.find(unit, "pet") == nil and UnitAuraDuration(unit, "Battle Shout") <= 10 * 60 and IsUnitInRange("Battle Shout", unit) then
      return TryCastSpellOnPlayer("Battle Shout") or nil
    end
  end
  return nil
end

function private.common()
    return
    (not UnitAffectingCombat("player") and not IsPlayerMoving() and skill.TryLoot() or nil) or
    TryCastQueuedSpell() or
    skill.DoNothingIfMounted() or
    --skill.TryPreventFallDamage() or
    skill.KeepRepurposedFelFocuserUp() or
    skill.DoNothingOutOfCombat() or
    skill.DoNothingIfDrinking() or
    skill.DoNothingIfTargetOutOfCombat() or
    nil
end


private.def_stance = false
function ToggleDefStance(enabled)
  enabled = enabled or not private.def_stance
  if private.def_stance ~= enabled then
    private.toggle_def_stance()
  end
end
function debug()
  ALERT(C_EquipmentSet.GetEquipmentSetID("Shield"))
  ALERT(C_EquipmentSet.GetEquipmentSetID("2H"))
end
function private.handle_def_stance()
  local name = private.def_stance and "Shield" or "2H"
  local id = C_EquipmentSet.GetEquipmentSetID(name)
  if not id then
    ALERT_ONCE("No equipment set with name '"..name.."' found. This is required for Def Stance.")
    return nil
  end
  local _, _, _, is_equipped = C_EquipmentSet.GetEquipmentSetInfo(id)
  if not is_equipped then
    return
      TryMacro("/equipset "..name) or
      "nothing"
  end
  if not private.def_stance then
    return nil
  end
  local in_range = IsUnitInRange("Bloodthirst", "target")
  return
    (PlayerHealthPercent() < 0.33 and TryCastSpellOnPlayer("Rallying Cry") or nil) or
    HandleHealing() or
    skill.StartAttack() or
    skill.AcquireTarget() or

    TryCondemn() or
    (in_range and GetCharges("Shield Block") >= 2 and IgnorePainAbsorb() <= (PlayerHealthMax() * 0.2 * (1 - PlayerHealthPercent())) and PlayerAuraDuration("Shield Block") < 0.1 and TryCastSpellOnPlayer("Shield Block") or nil) or
    (in_range and GetCharges("Shield Block") < GetChargesMax("Shield Block") and PlayerAuraDuration("Shield Block") < 0.1 and PlayerAuraDuration("Ignore Pain") <= 0 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (in_range and PlayerAuraDuration("Ignore Pain") < GetCD("Ignore Pain") and PlayerAuraDuration("Shield Block") <= 0 and TryCastSpellOnPlayer("Shield Block") or nil) or
    (in_range and GetRage() > 80 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (in_range and GetRage() > 80 and GetCharges("Shield Block") >= 2 and TryCastSpellOnPlayer("Shield Block") or nil) or
    TryCastSpellOnTarget("Shield Slam") or
    (GetRage() > 95 and TryCastSpellOnPlayer("Slam") or nil) or
    nil
end

private.priorities = {}
function private.priorities.def_stance()
  return private.def_stance()
end
function private.priorities.default()
  return
    torghast.torghast_stuff() or
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or
    skill.common_priority_start() or
    (PlayerAuraDuration("Spell Reflection") <= (select(2, TargetCast()) or 0) and skill.TryInterruptTarget() or nil) or
    --skill.TryUseHealthstone(0.33) or
    HandleHealing() or
    HandleIgnorePain() or
    (PlayerAuraDuration("Battle Shout") <= 0 and TryCastSpellOnPlayer("Battle Shout") or nil) or
    skill.StartAttack() or
    skill.AcquireTarget() or

    (skill.NumEnemiesWithin(6) > 1 and PlayerAuraDuration("Whirlwind") <= GlobalCD() and TryCastSpellOnTarget("Whirlwind") or nil) or
    (UnitAuraStacks("player", "Resonant Throatbands", "MAW") > 0 and not torghast.IsTargetMawrat() and skill.NumEnemiesWithin(5) > 0 and TryCastSpellOnPlayer("Piercing Howl") or nil) or
    (UnitAuraStacks("player", "Resonant Throatbands", "MAW") > 0 and not torghast.IsTargetMawrat() and UnitAuraStacks("player", "Umbral Ear Trumpet", "MAW") > 0 and TargetMyAuraDuration("Resonant Throatbands") <= 0 and skill.NumEnemiesWithin(10) > 0 and TryCastSpellOnPlayer("Battle Shout") or nil) or
    (PlayerAuraDuration("Enrage") > GlobalCD() and UnitAuraStacks("player", "Voracious Culling Blade", "MAW") > 0 and private.TryCondemn() or nil) or
    HandleRampage() or
    --HandleRecklessness() or
    HandleSiegebreaker() or
    private.TryCondemn() or
    (UnitAuraStacks("player", "Hurricane Heart", "MAW") > 1 and PlayerHealthPercent() < 0.8 and skill.NumEnemiesWithin(6) > 2 and GetRage() < 80 and TryCastSpellOnTarget("Whirlwind") or nil) or
    (UnitAuraStacks("player", "Hurricane Heart", "MAW") > 1 and PlayerHealthPercent() < 0.5 and GetRage() < 80 and TryCastSpellOnTarget("Whirlwind") or nil) or
    --private.TryCondemn("mouseover") or
    TryOnslaught() or
    (PlayerAuraDuration("Enrage") > GlobalCD() and GetCharges("Raging Blow") >= 2 and TryCastSpellOnTarget("Raging Blow") or nil) or
    TryCastSpellOnTarget("Bloodthirst") or
    HandleDragonRoar() or
    TryCastSpellOnTarget("Raging Blow") or
    TryCastSpellOnTarget("Whirlwind") or

    "nothing"
end
function private.target_ttl()
  local dps = 5000
  local estimated_dps = math.max(1, GetNumGroupMembers() - 1.5) * dps
  return TargetHealth() / estimated_dps
end
local function TryRecklessness()
  if not skill_char_settings["use_recklessness"] then
    return nil
  end
  return
    GlobalCD() <= 0 and
    (IsTalentSelected("Anger Management") or
     TargetHealthPercent() < 0.2 or
     (IsTalentSelected("Massacre") and TargetHealthPercent() < 0.35) or
     skill.HeroismDuration() > 0 or
     private.target_ttl() > 100-- or
     --(private.target_ttl() < 15 and skill.NumEnemiesWithin(16) <= 1)
    ) and
    (skill.NumEnemiesWithin(8) <= 1 or PlayerAuraDuration("Whirlwind") > 0) and
    (skill.TryUseAnyEquippedTrinket() or TryCastSpellOnTarget("Recklessness") or nil) or
    nil
end
function private.is_will_of_the_berserker_equipped()

end
function private.simcraft_single()
  --raging_blow,if=runeforge.will_of_the_berserker.equipped&buff.will_of_the_berserker.remains<gcd
  --crushing_blow,if=runeforge.will_of_the_berserker.equipped&buff.will_of_the_berserker.remains<gcd
  --siegebreaker,if=spell_targets.whirlwind>1|raid_event.adds.in>15
  --rampage,if=buff.recklessness.up|(buff.enrage.remains<gcd|rage>90)|buff.frenzy.remains<1.5
  --condemn
  --execute
  --bladestorm,if=buff.enrage.up&(spell_targets.whirlwind>1|raid_event.adds.in>45)
  --bloodthirst,if=buff.enrage.down|conduit.vicious_contempt.rank>5&target.health.pct<35&!talent.cruelty.enabled
  --bloodbath,if=buff.enrage.down|conduit.vicious_contempt.rank>5&target.health.pct<35&!talent.cruelty.enabled
  --dragon_roar,if=buff.enrage.up&(spell_targets.whirlwind>1|raid_event.adds.in>15)
  --onslaught
  --raging_blow,if=charges=2
  --crushing_blow,if=charges=2
  --bloodthirst
  --bloodbath
  --raging_blow
  --crushing_blow
  --whirlwind
  local enrage_duration = PlayerAuraDuration("Enrage")
  local num_enemies_8 = skill.NumEnemiesWithin(8)
  local adds_not_within_15 = skill.NumEnemiesWithin(16) <= num_enemies_8
  local adds_not_within_45 = adds_not_within_15
  return
    ((private.is_will_of_the_berserker_equipped() and PlayerAuraDuration("Will of the Berserker") < GlobalCD()) and TryRagingBlow() or nil) or
    --(... TryCrushingBlow()) or
    ((num_enemies_8 > 1 or adds_not_within_15) and TrySiegebreaker() or nil) or
    ((PlayerAuraDuration("Recklessness") > 0 or (enrage_duration < GlobalCD() or GetRage() > 90) or (PlayerAuraDuration("Frenzy") > GlobalCD(0) and PlayerAuraDuration("Frenzy") < GlobalCDMax())) and TryRampage() or nil) or
    TryCondemn() or
    TryExecute() or
    ((enrage_duration > 0 and (num_enemies_8 > 1 or adds_not_within_45)) and TryBladestorm() or nil) or
    --((((enrage_duration <= 0) or (IsTalentSelected("Cruelty") and (TargetHealthPercent() < 0.35)) and (soulbinds.is_active("Vicious Contempt") > 5))) and TryBloodthirst() or nil) or
    (enrage_duration <= 0 and TryBloodthirst() or nil) or
    ((enrage_duration > 0 and (num_enemies_8 > 1 or adds_not_within_15)) and TryDragonRoar() or nil) or
    TryOnslaught() or
    (GetCharges("Raging Blow") >= 2 and TryRagingBlow() or nil) or
    --(GetCharges("Crushing Blow") >= 2 and TryCrushingBlow() or nil)or
    TryBloodthirst() or
    TryRagingBlow() or
    --TryCrushingBlow() or
    --KeepBattleShoutUpOnParty() or
    TryWhirlwind() or
    "nothing"
end
function private.priorities.simcraft()
  --rampage,if=cooldown.recklessness.remains<3&talent.reckless_abandon.enabled
  --recklessness,if=gcd.remains=0&((buff.bloodlust.up|talent.anger_management.enabled|raid_event.adds.in>10)|target.time_to_die>100|(talent.massacre.enabled&target.health.pct<35)|target.health.pct<20|target.time_to_die<15&raid_event.adds.in>10)&(spell_targets.whirlwind=1|buff.meat_cleaver.up)
  --whirlwind,if=spell_targets.whirlwind>1&!buff.meat_cleaver.up|raid_event.adds.in<gcd&!buff.meat_cleaver.up
  --use_item,name=wakeners_frond
  return
    private.handle_def_stance() or
    torghast.torghast_stuff() or
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or
    private.common() or

    skill.common_priority_start() or
    (PlayerAuraDuration("Spell Reflection") <= (select(2, TargetCast()) or 0) and skill.TryInterruptTarget() or nil) or
    --skill.TryUseHealthstone(0.33) or
    HandleHealing() or
    HandleIgnorePain() or
    (PlayerAuraDuration("Battle Shout") <= 0 and TryCastSpellOnPlayer("Battle Shout") or nil) or
    skill.StartAttack() or
    skill.AcquireTarget() or

    (IsTalentSelected("Reckless Abandon") and GetCD("Recklessness") < 3 and TryCastSpellOnTarget("Rampage")) or
    skill.TryUseEquippedTrinketOnUnit("Inscrutable Quantum Device", "target") or
    TryRecklessness() or
    ((skill.NumEnemiesWithin(8) > 1 and PlayerAuraDuration("Whirlwind") <= 0 or skill.NumEnemiesWithin(16) > 1 and PlayerAuraDuration("Whirlwind") <= 0) and TryWhirlwind()) or
    --UseTrinkets() or

    private.simcraft_single() or

    "nothing"
end
function private.priorities.low_level_farm()
  return
    private.priorities.default() or
    "nothing"
end
function private.priorities.daily()
  return
    skill.dailies() or
    "nothing"
end

local function range(spell)
  if spell == "Recklessness" then
    return 2
  elseif spell == "Bladestorm" then
    return 2
  elseif spell == "Whirlwind" then
    return 2
  elseif spell == "Battle Shout" then
    return 50
  elseif spell == "Rallying Cry" then
    return 40
  elseif spell == "Execute" then
    return 2
  end
  return nil
end
local module = {
  ["interrupts"] = {
    "Pummel",
  },
  ["spell"] = {
    ["range"] = range,
  },
  ["extra_spells"] = {
    "Execute",
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
  ["is_reflect"] = IsReflect,
}


skill_char_settings = skill_char_settings or {}

function private.init_use_recklessness(gui)
  skill_char_settings["use_recklessness"] = skill_char_settings["use_recklessness"] or true
  gui.string:SetText(tostring(skill_char_settings["use_recklessness"]))
end
function private.toggle_use_recklessness(gui)
  skill_char_settings["use_recklessness"] = not skill_char_settings["use_recklessness"]
  gui.string:SetText(tostring(skill_char_settings["use_recklessness"]))
end
private.toggle_use_recklessness_gui = { ["label"] = "Use Recklessness:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_use_recklessness, ["init"] = private.init_use_recklessness }

function private.init_use_bladestorm(gui)
  skill_char_settings["use_bladestorm"] = skill_char_settings["use_bladestorm"] or true
  gui.string:SetText(tostring(skill_char_settings["use_bladestorm"]))
end
function private.toggle_use_bladestorm(gui, button)
  skill_char_settings["use_bladestorm"] = not skill_char_settings["use_bladestorm"]
  gui.string:SetText(tostring(skill_char_settings["use_bladestorm"]))
end
private.toggle_use_bladestorm_gui = { ["label"] = "Use Bladestorm:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_use_bladestorm, ["init"] = private.init_use_bladestorm }

function private.init_def_stance(gui)
  gui.string:SetText(tostring(private.def_stance))
  private.def_stance_gui = gui
end
function private.toggle_def_stance(gui, button)
  private.def_stance = not private.def_stance
  private.def_stance_gui.string:SetText(tostring(private.def_stance))
  ALERT("def_stance = "..tostring(private.def_stance))
end
private.toggle_def_stance_gui = { ["label"] = "Def Stance:", ["text"] = "", ["update"] = nil, ["click"] = private.toggle_def_stance, ["init"] = private.init_def_stance }



function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
  AddGuiElement(private.toggle_use_recklessness_gui)
  AddGuiElement(private.toggle_use_bladestorm_gui)
  AddGuiElement(private.toggle_def_stance_gui)
end
function module.unload()
end

skill.modules.warrior.fury = module