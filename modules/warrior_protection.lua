_, skill = ...
local private = {}
private.frame = CreateFrame("Frame", nil, UIParent)
private.frame:RegisterEvent("ADDON_LOADED")
private.gui = {}
private.priorities = {}

local function IsOffGlobal(name)
  return
  --name == "Shield Block" or
  --name == "Ignore Pain" or
  --name == "Last Stand" or
  --name == "Spell Reflection" or
  --name == "Shield Wall" or
  --name == "Taunt" or
  --name == "Berserker Rage" or
  --name == "Will to Survive" or
  --name == "Healthstone" or
  name == "Pummel"
end

local function IsStun(spell)
  return spell == "Shockwave"
end

local function IsDisorient(spell)
  return false
end

local function IsIncapacitate(spell)
  return false
end

local function IsSilence(spell)
  return false
end

local function IsInterrupt(spell)
  return spell == "Pummel"
end

local function GetRangeOverride(spell)
  if spell == "Thunder Clap" then
    return 8 + (IsTalentSelected("Crackling Thunder") and 4 or 0)
  elseif spell == "Revenge" then
    return 8
  elseif spell == "Battle Shout" then
    return 40
  elseif spell == "Shockwave" then
    return 10
  end
end

local function AcquireTarget()
  return
  UnitIsDead("target") or
  UnitIsFriend("player", "target") or
  not UnitGUID("target") and
  "Shield of the Righteous"
end

local function DefaultOffGlobal()
  return "nothing"
end

local function GetRage()
  return UnitPower("player", SPELL_POWER_RAGE)
end
local function freeRevenge()
  if IsSpellOverlayed(6572) then
    return TryCastSpellOnTarget("Revenge")
  end
end
local function IgnorePainAbsorb()
  for i = 1, 40 do
    local name, _, _, _, _, _, _, _, _, _, _, _, _, _, _, absorb = UnitAura("player", i)
    if name == "Ignore Pain" and absorb then
      return absorb
    end
  end
  return 0
end

local function KeepShieldBlockOnCD()
  return
    GetRage() >= 30 and
    IsUnitInRange("Shield Slam", "target") and
    GetCharges("Shield Block") >= GetChargesMax("Shield Block") - 1 and
    GetChargeCD("Shield Block") < 0.5 and
    TryCastSpellOnPlayer("Shield Block") or
    nil
end
local function HandleShockwave()
  return nil --sreturn skill.NumEnemiesWithin(8) >= 3 and TryCastSpellOnTarget("Shockwave") or nil
end
local function HandleAlmostExpiredIgnorePain()
  if ignore_pain_absorb < PlayerHealthMax() * 0.01 or ignore_pain_duration < GlobalCD() then
    return
    (GetRage() >= 70 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (GetRage() >= 55 and GetCD("Shield Slam") < GlobalCD()) or
    nil
  end
end
local function HandleIgnorePain()
  local ignore_pain_absorb = IgnorePainAbsorb()
  local ignore_pain_duration = PlayerAuraDuration("Ignore Pain")
  local block_duration = max(PlayerAuraDuration("Shield Block"), IsTalentSelected("Bolster") and PlayerAuraDuration("Last Stand") or 0)
  local ignoring_pain = ignore_pain_absorb > PlayerHealthMax() * 0.01 and ignore_pain_duration > GlobalCD()
  return
    (not ignoring_pain and GetRage() >= 70 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (not ignoring_pain and block_duration <= 0 and GetChargeCD("Shield Block") > GlobalCDMax() and GetCD("Shield Slam") <= GlobalCD() and GetRage() >= 55 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (not ignoring_pain and block_duration <= 0 and GetChargeCD("Shield Block") > GlobalCDMax() and GetCD("Thunder Clap") <= GlobalCD() and GetRage() >= 65 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    (GetRage() >= 90 and TryCastSpellOnPlayer("Ignore Pain") or nil) or
    nil
end
local function HandleDemoralizingShout()
  return
  PlayerAuraDuration("Shield Block") <= GlobalCD() and
  GetRage() <= 60 and
  skill.NumEnemiesWithin(8) > 0 and
  TryCastSpellOnPlayer("Demoralizing Shout") or
  nil
end
local function HandleLowHp()
  if PlayerHealthPercent() < 0.6 and GetCD("Last Stand") <= 0 then
    return TryCastSpellOnPlayer("Last Stand") or nil
  end
  if PlayerHealthPercent() < 0.4 and PlayerAuraDuration("Last Stand") < GlobalCD() and GetCD("Rallying Cry") <= GlobalCD() then
    return TryCastSpellOnPlayer("Rallying Cry") or nil
  end
  if PlayerHealthPercent() < 0.2 and PlayerAuraDuration("Last Stand") < GlobalCD() and PlayerAuraDuration("Rallying Cry") < GlobalCD() then
    return
      skill.TrySelfHealWithConcentratedFlame() or
      (PlayerAuraDuration("Shield Block") < 0.5 and TryCastSpellOnPlayer("Shield Block") or nil) or
      (PlayerAuraDuration("Shield Block") < 0.5 and TryCastSpellOnPlayer("Shield Wall") or nil) or
      (PlayerAuraDuration("Shield Wall") < 0.5 and TryCastSpellOnPlayer("Spell Reflection") or nil) or
      nil
  end
end
local function HandleAvatar()
  if skill.NumEnemiesWithin(8) >= 3 or skill.UnitIsBoss("target") then
    return IsUnitInRange("Shield Slam", "target") and TryCastSpellOnPlayer("Avatar") or nil
  end
end
local PARTY = {
  "player",
  "party1",
  "party2",
  "party3",
  "party4",
}
local function DumpRage()
  if GetRage() < 80 then
    return nil
  end
  return
    (skill.NumEnemiesWithin(8) <= 1 and private.TryCondemn() or nil) or
    TryCastSpellOnTarget("Execute") or
    TryCastSpellOnTarget("Revenge") or
    nil
end
local function KeepBattleShoutUpOnParty()
  for i, unit in ipairs(PARTY) do
    if UnitExists(unit) and UnitAuraDuration(unit, "Battle Shout") <= 10 * 60 and IsUnitInRange("Battle Shout", unit) then
      return TryCastSpellOnPlayer("Battle Shout") or nil
    end
  end
end
function private.priorities.low_level_farm()
  local target_name = UnitName("target")
  return
    ((target_name == "Blackrock Enforcer" or target_name == "Blackrock Forge Specialist" or target_name == "Aquatic Technician") and (TryCastSpellOnTarget("Heroic Throw") or skill.StartAttack())) or
    skill.DoNothingIfTargetOutOfCombat() or
    TryCastQueuedSpell() or
    (not UnitExists("target") and not UnitAffectingCombat("player") and "nothing") or
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or
    skill.TryInterruptTarget() or
    (skill.NumEnemiesWithin(5) >= 2 and TryCastSpellOnTarget("Thunder Clap")) or
    (GetRage() >= 30 and TryCastSpellOnTarget("Revenge")) or
    (skill.UnitIsBoss("target") and skill.TryUseConcentratedFlame() or nil) or
    TryCastSpellOnTarget("Shield Slam") or
    TryCastSpellOnTarget("Heroic Throw") or
    --KeepBattleShoutUpOnParty() or
    TryCastSpellOnTarget("Devastate") or
    skill.AcquireTarget() or
    "nothing"
end
function private.TryCondemn()
  return
  skill.IsVenthyr() and
  TryCastSpellOnTarget("Execute") or
  nil
end

function private.priorities.low_level()
  return
    skill.common_priority_start() or
    skill.StartAttack() or
    skill.AcquireTarget() or
    TryCastSpellOnTarget("Victory Rush") or
    (skill.NumEnemiesWithin(8) > 1 and TryCastSpellOnTarget("Whirlwind") or nil) or
    TryCastSpellOnTarget("Shield Slam") or
    TryCastSpellOnTarget("Execute") or
    (GetRage() >= 40 and TryCastSpellOnTarget("Slam") or nil) or
    "nothing"
end
function private.priorities.wowhead()
  local heal_spell = IsTalentSelected("Impending Victory") and "Impending Victory" or "Victory Rush"
  local heal_spell_threshold = IsTalentSelected("Impending Victory") and 0.7 or 0.8
  return
    (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or
    skill.common_priority_start() or
    skill.StartAttack() or
    skill.AcquireTarget() or

    HandleAvatar() or
    HandleShockwave() or
    KeepShieldBlockOnCD() or
    HandleIgnorePain() or
    HandleLowHp() or

    (IsTalentSelected("Impending Victory") and PlayerHealthPercent() < heal_spell_threshold and TryCastSpellOnTarget(heal_spell) or nil) or
    (PlayerHealthPercent() < 0.8 and PlayerAuraDuration("Victorious") > 0 and TryCastSpellOnTarget(heal_spell) or nil) or
    (PlayerHealthPercent() < 1 and PlayerAuraDuration("Victorious") > 0 and PlayerAuraDuration("Victorious") < 5 and TryCastSpellOnTarget(heal_spell) or nil) or
    (PlayerHealthPercent() >= 0.95 and skill.NumEnemiesWithin(10) >= 3 and TryCastSpellOnTarget("Revenge") or nil) or
    (PlayerHealthPercent() >= 0.95 and skill.NumEnemiesWithin(10) >= 4 and TryCastSpellOnTarget("Thunder Clap") or nil) or
    TryCastSpellOnTarget("Shield Slam") or
    TryCastSpellOnTarget("Thunder Clap") or
    (skill.NumEnemiesWithin(10) > 1 and GetCD("Thunder Clap") < 0.75 and "nothing" or nil) or
    (PlayerAuraDuration("Revenge!") > GlobalCD() and TryCastSpellOnTarget("Revenge") or nil) or
    (skill.NumEnemiesWithin(10) <= 1 and GetRage() >= 60 and private.TryCondemn() or nil) or
    DumpRage() or
    (not IsTalentSelected("Devastator") and TryCastSpellOnTarget("Devastate") or nil) or

    "nothing"
end

function private.priorities.easy_dungeon()
  local heal_spell = IsTalentSelected("Impending Victory") and "Impending Victory" or "Victory Rush"
  local heal_spell_threshold = IsTalentSelected("Impending Victory") and 0.7 or 0.8
  return
  (not UnitAffectingCombat("player") and KeepBattleShoutUpOnParty() or nil) or
      skill.common_priority_start() or
      skill.StartAttack() or
      skill.AcquireTarget() or

      HandleAvatar() or
      (skill.NumEnemiesWithin(8) >= 3 and PlayerMyAuraDuration("Shield Block") <= 0.5 and PlayerHealthPercent() < 0.65 and TryCastSpellOnTarget("Shockwave") or nil) or
      KeepShieldBlockOnCD() or
      HandleIgnorePain() or
      HandleLowHp() or

      (skill.NumEnemiesWithin(10) >= 3 and TryCastSpellOnTarget("Revenge") or nil) or
      (skill.NumEnemiesWithin(10) >= 4 and TryCastSpellOnTarget("Thunder Clap") or nil) or
      TryCastSpellOnTarget("Shield Slam") or
      (IsTalentSelected("Impending Victory") and PlayerHealthPercent() < heal_spell_threshold and TryCastSpellOnTarget(heal_spell) or nil) or
      (PlayerHealthPercent() < 0.8 and PlayerAuraDuration("Victorious") > 0 and TryCastSpellOnTarget(heal_spell) or nil) or
      (PlayerHealthPercent() < 1 and PlayerAuraDuration("Victorious") > 0 and PlayerAuraDuration("Victorious") < 5 and TryCastSpellOnTarget(heal_spell) or nil) or
      TryCastSpellOnTarget("Thunder Clap") or
      (skill.NumEnemiesWithin(10) > 1 and GetCD("Thunder Clap") < 0.75 and "nothing" or nil) or
      freeRevenge() or
      DumpRage() or
      TryCastSpellOnTarget("Devastate") or

      "nothing"
end



local function range(spell)
  if spell == "Rallying Cry" then
    return 40
  elseif spell == "Thunder Clap" then
    return IsTalentSelected("Crackling Thunder") and 10 or 6
  elseif spell == "Battle Shout" then
    return 100
  elseif spell == "Demoralizing Shout" then
    return 40
  elseif spell == "Heroic Throw" then
    return 30
  elseif spell == "Heroic Leap" then
    return 40
  elseif spell == "Shockwave" then
    return 8
  elseif spell == "Revenge" then
    return 8
  end
end


local module = {
  ["spell"] = {
    ["range"] = range,
  },
  ["interrupts"] = {
    "Pummel",
    "Shockwave",
  },
  ["save_interrupt_for"] = {
    ["Coldheart Agent"] = {
      ["Terror"] = "Pummel",
    },
  },
  ["extra_spells"] = {
    "Execute",
  },
  ["is_off_global"] = IsOffGlobal,
  ["is_stun"] = IsStun,
  ["is_disorient"] = IsDisorient,
  ["is_silence"] = IsSilence,
  ["is_incapacitate"] = IsIncapacitate,
  ["is_interrupt"] = IsInterrupt,
}
function module.run()
  return private.priority_selection.run()
end
function module.load()
  private.priority_selection = PrioritySelectionFor(private.priorities)
  AddGuiElement(private.priority_selection.gui)
end
function module.unload()
end
skill.modules.warrior.protection = module

local low_level = {
  ["interrupts"] = {
    "Pummel",
  },
}
low_level.run = module.run
low_level.load = module.load
skill.modules.warrior.low_level = low_level