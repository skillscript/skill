_, skill = ...

local RUST_NAVIGATION = false

private = {}
private.default_settings = {}
private.default_settings.alert = true
private.pixels = {}
private.num_pixels = 0
private.anchor = -0
private.frame = CreateFrame("Frame", "skill", UIParent)
private.SKILL_ON = 42
private.SKILL_OFF = 23
--private.R = (1 + 3 + #COMBAT_CAST_KEYS + 4 + (RUST_NAVIGATION and 2 or 0)) * 65536
--private.R = (1 + 3 + #COMBAT_CAST_KEYS + (RUST_NAVIGATION and (4 + 2) or 0)) * 65536
private.G = 2 * (2^8)

function private.val2col(value)
  value = mod(value, 2 ^ 24)
  local r = value / 2 ^ 16
  value = mod(value, 2 ^ 16)
  local g = value / 2 ^ 8
  local b = mod(value, 2 ^ 8)
  return floor(r), floor(g), floor(b)
end

function private.create_pixel(name)
  if private.pixels[name] then
    ALERT("Not creating pixel with the name '" .. tostring(name) .. "' for the second time.")
    return
  end

  local f = CreateFrame("Frame", name, private.frame)
  f:SetPoint("TOPLEFT", private.frame, "TOPLEFT", pixelScale(private.anchor), 0)
  f:SetPoint("BOTTOMRIGHT", private.frame, "TOPLEFT", pixelScale(private.anchor + 1), -1)
  f:SetPoint("RIGHT", private.frame, "LEFT", pixelScale(private.anchor + 1), 0)
  local t = f:CreateTexture(nil, "OVERLAY")
  t:SetColorTexture(0, 0, 0, 1)
  t:SetAllPoints(f)
  f.texture = t

  private.anchor = private.anchor + 1;
  private.pixels[name] = f
  private.num_pixels = private.num_pixels + 1
  return f
end

function private.print_skill_state()
  if (skill_settings.state == private.SKILL_ON) then
    ALERT("skill: on")
  else
    ALERT("skill: off")
  end
end

function skill.is_skill_enabled()
  return skill_settings.state == private.SKILL_ON
end

--local frame_time_debug = false
--local frame_time_min = 1000 * 1000
--local frame_time_max = 0
--local frame_time_cur = 0
--local update_frame = 0

local last_ready_to_execute = 0
local last_ready_to_select = 0
local elapsed_0 = 0
local elapsed_1 = 0
local elapsed_2 = 0
local elapsed_3 = 0
local elapsed_4 = 0
local elapsed_5 = 0
local elapsed_6 = 0
function private.run(self, elapsed)
  skill.combat_cast.on_update()
  --if frame_time_debug then
  --  debugprofilestart()
  --end
  --local res = select(max(1, GetCurrentResolution()), GetScreenResolutions())
  --local height = string.sub(res, string.find(res, "x") + 1)
  --ALERT("res: "..tostring(res))
  --ALERT("height: "..tostring(height))
  --ALERT("GetScreenHeight(): "..tostring(GetScreenHeight()))
  --ALERT("GetScreenHeight()/height: "..tostring(GetScreenHeight()/height))
  --ALERT("UIParent:GetEffectiveScale(): "..tostring(UIParent:GetEffectiveScale()))
  local result = skill.is_skill_enabled() and private.module.run() or "nothing"
  local action = skill.combat_cast.actions[result]
  if action then
    action = action > 0 and action + (255 - action) * 2^8 or 0
    skill.set_pixel_value("action", action)
    local paused = IsModifierKeyDown() or ChatFrame1EditBox:HasFocus() or ChatFrame1EditBox:HasFocus()
    local control = (paused and 0 or skill_settings.state) + private.G + private.num_pixels * (2^16)
    skill.set_pixel_value("control", control)

    elapsed_6 = elapsed_5
    elapsed_5 = elapsed_4
    elapsed_4 = elapsed_3
    elapsed_3 = elapsed_2
    elapsed_2 = elapsed_1
    elapsed_1 = elapsed_0
    elapsed_0 = elapsed
    local sum_3 = elapsed_3 + elapsed_4 + elapsed_5 + elapsed_6
    local sum_2 = elapsed_2 + elapsed_3 + elapsed_4 + elapsed_5
    local sum_1 = elapsed_1 + elapsed_2 + elapsed_3 + elapsed_4
    local sum_0 = elapsed_0 + elapsed_1 + elapsed_2 + elapsed_3
    local avg_three_frame_time_sum = math.max(sum_0, sum_1, sum_2, sum_3) / 4 * 3
    local spell_queue_window = C_CVar.GetCVar("SpellQueueWindow") / 1000
    local cd = math.max(avg_three_frame_time_sum, spell_queue_window)
    local ready_to_execute = skill.combat_cast.is_ready() and (GetTime() - last_ready_to_execute) > cd
    last_ready_to_execute = ready_to_execute and GetTime() or last_ready_to_execute
    local ready_to_select = not skill.combat_cast.is_ready() and (GetTime() - last_ready_to_select) > cd
    last_ready_to_select = ready_to_select and GetTime() or last_ready_to_select
    if skill_settings.state == private.SKILL_ON and (ready_to_execute or ready_to_select) then
      skill.set_pixel_value("frame", galois.increment())
    end
    --update_frame = update_frame + 1
  else
    ALERT_ONCE(string.format("Not a CombatCast value: %s", result))
  end
  --if frame_time_debug then
  --  local time_elapsed = debugprofilestop() * 1000
  --  frame_time_cur = time_elapsed
  --  frame_time_min = min(frame_time_min, time_elapsed)
  --  frame_time_max = max(frame_time_max, time_elapsed)
  --  ALERT("============")
  --  ALERT("frame_time_min: "..tostring(frame_time_min))
  --  ALERT("frame_time_cur: "..tostring(frame_time_cur))
  --  ALERT("frame_time_max: "..tostring(frame_time_max))
  --end
end

function private.create_pixels()
  private.create_pixel("control")
  private.create_pixel("frame")
  skill.set_pixel_value("frame", 1)
  private.create_pixel("action")
  for i = 0, #COMBAT_CAST_KEYS do
    private.create_pixel("CombatCastPixelButton" .. i)
  end
  if RUST_NAVIGATION then
    private.create_pixel("turnleft")
    private.create_pixel("turnright")
    private.create_pixel("moveforward")
    private.create_pixel("movebackward")
    C_CVar.SetCVar("mapFade", 0)
    private.create_pixel("task_move_x")
    private.create_pixel("task_move_y")
  end
end

function private.check_bindings()
  for i = 1, GetNumBindings() do
    local commandName, category, binding1, binding2 = GetBinding(i)
    if commandName == "Toggle Skill" and category == "Skill" then
      if binding1 == nil and binding2 == nil then
        ALERT("'Toggle Skill' not bound. Auto-assigning to F6.")
        for j = 1, GetNumBindings() do
          local unboundCommandName, _, unboundBinding1, unboundBinding2 = GetBinding(j)
          if unboundBinding1 == "F6" or unboundBinding2 == "F6" then
            ALERT("Unbinding " .. unboundCommandName)
            break
          end
        end
        SetBinding("F6", "Toggle Skill")
        SaveBindings(GetCurrentBindingSet() or 1)
      end
    end
  end
end

local function unload_current_module()
  if private.module then
    if private.module.unload and type(private.module.unload) == "function" then
      private.module.unload()
    else
      ALERT_ONCE(string.format("Module for '%s' has no function type field called 'unload'.", private.module_name))
    end
  end
end

local function unset_module()
  unload_current_module()
  private.module = nil
  private.frame:SetScript("OnUpdate", nil)
end
local function set_module()
  unload_current_module()
  local class = PlayerClass()
  local spec = PlayerSpec()
  if not class or not spec then
    return
  end
  skill.modules[class] = skill.modules[class] or {}
  private.module = skill.modules[class][spec] or nil
  private.module_name = class .. "." .. spec
  if private.module then
    if private.module.load and type(private.module.load) == "function" then
      private.module.load()
    else
      private.frame:SetScript("OnUpdate", nil)
      ALERT(string.format("Module for '%s' has no function type field called 'load'. This will be an error in the future.", private.module_name))
    end
    if private.module.run and type(private.module.run) == "function" then
      private.frame:SetScript("OnUpdate", private.run)
      ALERT_ONCE(string.format("module: %s", private.module_name))
    else
      private.frame:SetScript("OnUpdate", nil)
      ALERT(string.format("Module for '%s' has no function type field called 'run'.", private.module_name))
    end
  else
    ALERT(string.format("No skill module for '%s'.", private.module_name))
    private.module = nil
    private.frame:SetScript("OnUpdate", nil)
  end
end
function private.on_event(_, event, ...)
  if event == "ADDON_LOADED" then
    local name = ...
    if name == "skill" then
      private.frame:UnregisterEvent("ADDON_LOADED")
      skill_settings = skill_settings or private.default_settings
      skill_settings.state = skill_settings.state or private.SKILL_OFF
      private.print_skill_state()

      private.frame:SetFrameStrata("TOOLTIP")
      private.frame:SetPoint("TOPLEFT", 0, 0)
      private.frame:SetWidth(100)
      private.frame:SetHeight(pixelScale(1))
      private.frame:Show()
      private.create_pixels()
      if private.pixels["control"] then
        private.frame:RegisterEvent("UPDATE_BINDINGS")
        private.frame:RegisterEvent("PLAYER_LEAVING_WORLD")
        private.frame:RegisterEvent("PLAYER_TALENT_UPDATE")
      else
        ALERT_ONCE("No control pixel.")
        return
      end
    end
  elseif event == "PLAYER_LEAVING_WORLD" then
    --ALERT("PLAYER_LEAVING_WORLD")
    unset_module()
  elseif event == "PLAYER_TALENT_UPDATE" then
    unset_module()
    set_module()
    reload_gui()
  elseif event == "UPDATE_BINDINGS" then
    private.check_bindings()
  end
end

function skill.set_pixel_value(pixel, value)
  if not private.pixels[pixel] then
    ALERT(string.format("Unknown pixel: %s", pixel))
    return
  end
  value = tonumber(value)
  local r, g, b = private.val2col(floor(value))
  private.pixels[pixel].texture:SetColorTexture(r / 255, g / 255, b / 255, 1)
end

function ToggleSkill()
  if (skill_settings.state == private.SKILL_ON) then
    skill_settings.state = private.SKILL_OFF
  else
    skill_settings.state = private.SKILL_ON
  end
  private.print_skill_state()
end

private.frame:SetScript("OnEvent", private.on_event)
private.frame:RegisterEvent("ADDON_LOADED")
--private.frame:RegisterAllEvents()
