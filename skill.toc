## Interface: 90000
## Version: v9.0.0
## Title: skill

## SavedVariables: skill_settings
## SavedVariablesPerCharacter: skill_char_settings
## Dependencies:
## RequiredDeps:

libs/LibRangeCheck-2.0/LibStub-1.0/LibStub.lua
libs/LibRangeCheck-2.0/CallbackHandler-1.0/CallbackHandler-1.0.lua
libs/LibRangeCheck-2.0/LibRangeCheck-2.0/LibRangeCheck-2.0.lua

init.lua

libs/keycodes.lua
libs/cast_prediction.lua
libs/combat_cast.lua
libs/combat_log.lua
libs/common.lua
libs/enemy_tracker.lua
libs/galois_field.lua
libs/health.lua
libs/health_history.lua
libs/low_level.lua
libs/movement.lua
libs/priority_selection.lua
libs/queue.lua
libs/rate_limiter.lua
libs/spell_stats.lua
libs/task.lua
libs/tillers.lua
libs/zPets.lua

modules/deathknight_blood.lua
modules/deathknight_frost.lua
modules/deathknight_unholy.lua
modules/demonhunter_havoc.lua
modules/demonhunter_vengeance.lua
modules/druid_feral.lua
modules/druid_guardian.lua
modules/druid_restoration.lua
modules/hunter_beastmaster.lua
modules/hunter_marksmanship.lua
modules/mage_fire.lua
modules/mage_frost.lua
modules/monk_brewmaster.lua
modules/monk_mistweaver.lua
modules/monk_windwalker.lua
modules/paladin.lua
modules/paladin_holy.lua
modules/paladin_protection.lua
modules/paladin_retribution.lua
modules/priest_discipline.lua
modules/priest_holy.lua
modules/priest_shadow.lua
modules/rogue_pirate.lua
modules/rogue_subtlety.lua
modules/shaman_enhancement.lua
modules/shaman_restoration.lua
modules/warlock_affliction.lua
modules/warlock_demonology.lua
modules/warlock_destruction.lua
modules/warrior_arms.lua
modules/warrior_fury.lua
modules/warrior_protection.lua

bindings.xml

skill.lua
gui/gui.lua
