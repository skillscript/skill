local TALENTS = {
  [1] = 2,
  [2] = 1,
  [3] = 3,
  [4] = 3,
  [5] = 2,
  [6] = 1,
  [7] = 3,
}
local deeper_stratagem = TALENTS[3] == 1
local vigor = TALENTS[3] == 3

local function min(a, b)
  return a < b and a or b
end

local function max(a, b)
  return a > b and a or b
end

local function GetPowerRegen()
  return 11 + (vigor and 1 or 0)
end

local function GetEnergyMax()
  return 100 + (vigor and 50 or 0)
end

local function GetCPMax()
  return 5 + (deeper_stratagem and 1 or 0)
end

local function IsTalentSelected(talent)
  if talent == "Deeper Stratagem" then
    return deeper_stratagem
  end
  return false
end

local function IsOffGlobal(name)
  return
  name == "Evasion" or
  name == "Cloak of Shadows" or
  name == "Vanish" or
  name == "Sprint" or
  name == "Shadow Blades" or
  name == "Shadowstep" or
  name == "Symbols of Death" or
  name == "Shadow Dance" or
  name == "Crimson Vial" or
  name == "Kick" or
  name == "Stealth" or
  name == "Thistle Tea" or
  name == "Will to Survive" or
  name == "Healthstone"
end

local function ALERT(var)
  print(var)
end

local function GetCP()
  return 3
end

local function GetEnergy()
  return 125
end

local function TimeToEarliestShadowTechniques()
  return 1.1
end

local function TimeToLatestShadowTechniques()
  return 3.9
end







local function EnergyGenerated(spell)
  if spell == "Shadow Techniques" then
    return 8
  elseif spell == "Symbols of Death" then
    return 40
  end
  return 0
end

local function EnergyConsumed(spell)
  if spell == "Shadowstrike" then
    return 40
  elseif spell == "Eviscerate" or spell == "Backstab" then
    return 35
  elseif spell == "Nightblade" or spell == "Death from Above" then
    return 25
  end
  return 0
end

local function CPGenerated(spell)
  if spell == "Shadowstrike" then
    return 2
  elseif spell == "Backstab" then
    return 1
  elseif spell == "Shadow Techniques" then
    return 1
  end
  return 0
end

local function CPConsumed(spell, cp)
  if spell == "Eviscerate" or spell == "Nightblade" or spell == "Death from Above" then
    return 5 + (IsTalentSelected("Deeper Stratagem") and 1 or 0)
  end
  return 0
end

local function EnergyFromRelentlessStrikes(spell, cp)
  if (spell == "Eviscerate" or spell == "Death from Above" or spell == "Nightblade") then
    return cp * 6
  end
  return 0
end



local SEQUENCES = {
  -- GB + SoD + DfA
  [0] = {},
  -- SoD + DfA
  [1] = {
    {
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    }, {
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
    },
  },
  -- DfA
  [2] = {
    {
      {"Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    }, {
      {"Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
    },
  },
  ---- SoD + BS + DfA
  --[2] = {
  --  {
  --    {"Symbols of Death", "Backstab"},
  --    {"Death from Above"},
  --    {"Shadow Dance", "Shadowstrike"},
  --    {"Shadowstrike"},
  --    {"Shadowstrike"},
  --    {"Eviscerate"},
  --  }, {
  --    {"Symbols of Death", "Backstab"},
  --    {"Death from Above"},
  --    {"Shadow Dance", "Shadowstrike"},
  --    {"Shadowstrike"},
  --    {"Eviscerate"},
  --    {"Shadowstrike"},
  --  },
  --},
  -- BS + SoD + DfA
  [3] = {
    {
      {"Backstab"},
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    }, {
      {"Backstab"},
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
    },
  },
  -- BS + BS + SoD + DfA
  [4] = {
    {
      {"Backstab"},
      {"Backstab"},
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    }, {
      {"Backstab"},
      {"Backstab"},
      {"Symbols of Death", "Death from Above"},
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
    },
  },
  -- default
  [5] = {
    {
      {"Shadow Dance", "Eviscerate"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
    }, {
      {"Shadow Dance", "Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
      {"Shadowstrike"},
    }, {
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
      {"Shadowstrike"},
    }, {
      {"Shadow Dance", "Shadowstrike"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    }, {
      {"Shadow Dance", "Eviscerate"},
      {"Shadowstrike"},
      {"Shadowstrike"},
      {"Eviscerate"},
    },
  },
}
local OPTIMAL = "OPTIMAL"
local INVALID = "INVALID"
local WASTEFUL_CAPPING_CP = "WASTEFUL_CAPPING_CP"
local WASTEFUL_FINISHING_SUBOPTIMALLY = "WASTEFUL_FINISHING_SUBOPTIMALLY"
local function testSequence(sequence, initial_cp, st)
  local energy_reg = GetPowerRegen()
  local e = 0
  local e_min = 0
  local e_max = GetEnergyMax()
  local result = OPTIMAL
  local cp = initial_cp
  for global, bucket in ipairs(sequence) do
    local global_triggered = false
    for _, spell in ipairs(bucket) do
      if global_triggered then
        local content = ""
        for _, debug_spell in ipairs(bucket) do
          content = content.."\n".. debug_spell
        end
        ALERT("Tried to execute a second on-global spell ("..spell..") in one bucket: "..content)
        return INVALID
      end
      if spell == "Symbols of Death" and PlayerAuraDuration(spell) > 0 then
      end
      global_triggered = not IsOffGlobal(spell)
      local cost = EnergyConsumed(spell)
      e_min = min(e_min, e - cost)
      local reg = global_triggered and energy_reg or 0
      reg = reg + EnergyFromRelentlessStrikes(spell, cp) + EnergyGenerated(spell)
      reg = reg + (global_triggered and global == st and 8 or 0)
      e = e - cost + reg
      cp = cp + CPGenerated(spell) - CPConsumed(spell, cp)

      -- post global started
      if cp < 0 then
        result = WASTEFUL_FINISHING_SUBOPTIMALLY
      elseif cp > GetCPMax() then
        result = WASTEFUL_CAPPING_CP
      end
      cp = max(0, min(GetCPMax(), cp))
      cp = cp + (global_triggered and global == st and 1 or 0)
      if cp < 0 then
        result = WASTEFUL_FINISHING_SUBOPTIMALLY
      elseif cp > GetCPMax() then
        result = WASTEFUL_CAPPING_CP
      end
      cp = max(0, min(GetCPMax(), cp))
      e_max = min(e_max, GetEnergyMax() - max(0, e))
    end
  end
  if result == OPTIMAL and -e_min < e_max then
    return result, -e_min, e_max
  end
end

local function printSequence(sequence)
  local result
  for global, bucket in ipairs(sequence) do
    for _, spell in ipairs(bucket) do
      if not IsOffGlobal(spell) then
        result = result and result .. ", " .. spell or spell
      end
    end
  end
  print(result)
end

local OPTIMAL_SHADOW_DANCES = {}
function calcOptimalShadowDances(sequences)
  OPTIMAL_SHADOW_DANCES = {
    [0] = {  -- cp
      --    {  -- e_min
      --      {  -- e_max
      --        ["early"] = 0,
      --        ["late"] = 0,
      --      }
      --    }
    },
    [1] = {},
    [2] = {},
    [3] = {},
    [4] = {},
    [5] = {},
    [6] = {},
  }
  for _, sequence in ipairs(sequences) do
    for cp = 0, GetCPMax() do
      for st, _ in ipairs(sequence) do
        local result, e_min, e_max = testSequence(sequence, cp, st)
        if result == OPTIMAL then
          OPTIMAL_SHADOW_DANCES[cp][e_min] = OPTIMAL_SHADOW_DANCES[cp][e_min] or {}
          OPTIMAL_SHADOW_DANCES[cp][e_min][e_max] = OPTIMAL_SHADOW_DANCES[cp][e_min][e_max] or {}
          local st_early = OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["early"] or 10
          local st_late = (st == #sequence and 10 or OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["late"]) or 0
          OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["early"] = min(st, st_early)
          OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["late"] = max(st, st_late)
          print("==============")
          printSequence(sequence)
          print("cp: "..cp)
          print("e_min: "..e_min)
          print("e_max: "..e_max)
          print("st_early: "..OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["early"])
          print("st_late: "..OPTIMAL_SHADOW_DANCES[cp][e_min][e_max]["late"])
        elseif result == WASTEFUL then
        end
      end
    end
  end
end

local function IsInShadowDanceWindow(cp, e_min, e_max, st_early, st_late)
  return
    GetCP() == cp and
    GetEnergy() >= e_min and
    GetEnergy() <= e_max and
    TimeToEarliestShadowTechniques() >= st_early - 0.95 and
    TimeToLatestShadowTechniques() <= st_late - 0.05
end

local function TryEnterOptimalShadowDance()
  for priority, sequences in pairs(SEQUENCES) do
    calcOptimalShadowDances(sequences)
    for cp, e_min_table in pairs(OPTIMAL_SHADOW_DANCES) do
      local max_e_min = 0
      local min_e_max = GetEnergyMax()
      local min_st_early = 10
      local max_st_late = 0
      for e_min, e_max_table in pairs(e_min_table) do
        max_e_min = max(max_e_min, e_min)
        for e_max, st in pairs(e_max_table) do
          min_e_max = min(min_e_max, e_max)
          min_st_early = min(min_st_early, st["early"])
          max_st_late = max(max_st_late, st["late"])
          if IsInShadowDanceWindow(cp, e_min, e_max, st["early"], st["late"]) then
            return sequences[1][1][1]
          end
        end
      end
      if IsInShadowDanceWindow(cp, max_e_min, min_e_max, min_st_early, max_st_late) then
        return sequences[1][1][1]
      end
    end
  end
end

print(TryEnterOptimalShadowDance())
